package com.ys.zhslsgcgl.constant;

/**
 * @Description:评分加分字典类型
 * @Author: dzg
 * @CreateDate: 2022/11/11 10:29
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
public class PfjfType {
    //评分加分_流程类型_县级初审
    public static final String PFJF_LCLX_CS = "A";
    //评分加分_流程类型_市级复审
    public static final String PFJF_LCLX_FS = "B";

    //评分加分_审核状态_待审核
    public static final String PFJF_SHZT_DSH = "0";
    //评分加分_审核状态_审核中(县级审批完等待市级审批)
    public static final String PFJF_SHZT_SZH = "1";
    //评分加分_审核状态_已审核
    public static final String PFJF_SHZT_YSH = "2";

    //评分加分_审核意见_初审同意
    public static final String PFJF_SHYJ_CSTY = "A";
    //评分加分_审核意见_初审不同意
    public static final String PFJF_SHYJ_CSBTY = "B";
    //评分加分_审核意见_复审同意
    public static final String PFJF_SHYJ_FSTY = "C";
    //评分加分_审核意见_复审不同意
    public static final String PFJF_SHYJ_FSBTY = "D";
}
