package com.ys.zhslsgcgl.bo;

import com.ys.zhslsgcgl.bo.PageQueryBo;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

/**
 * @Description: 人员管理分页查询Bo
 * @Author fsx
 * @CreateDate 2022/11/5 20:35
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PageRylbBo extends PageQueryBo {

	@ApiModelProperty(value = "关键字")
	private String keyword;

	@ApiModelProperty(value = "人员类型 (A-关键岗位人员 B-特殊工种人员 C-危大作业人员 D-劳务人员)")
	private String rylx;

	@ApiModelProperty(value = "单位类别 (1-业主单位 2-设计单位 3-监理单位 4-施工单位 5-质检单位)")
	private String dwlx;

	@ApiModelProperty(value = "项目类型 (0-三峡后续 1-移民后扶 2-河道堤防 3-湖库工程 4-农村水利 5-泵站工程 6-饮水安全 7-连通工程 8-水美乡村 9-水土保持 10-山洪灾害 11-节水项目 12-维修养护 13-其他)")
	private String xmlx;

	@ApiModelProperty(value = "日期：年-月",required = true)
	@NotEmpty(message = "日期必传，格式：年-月")
	private String rq;

	@ApiModelProperty(value = "单位ID")
	private String dwid;

	@ApiModelProperty(value = "标段ID")
	private String bdid;
}
