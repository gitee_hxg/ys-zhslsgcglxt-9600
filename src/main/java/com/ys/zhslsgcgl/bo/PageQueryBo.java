package com.ys.zhslsgcgl.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:分页查询公用参数
 * @Author: wugangzhi
 * @CreateData: 2022/11/5 21:45
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class PageQueryBo {
    @ApiModelProperty("页码")
    private int pageNum;

    @ApiModelProperty("每页大小")
    private int pageSize;

}
