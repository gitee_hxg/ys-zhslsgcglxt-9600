package com.ys.zhslsgcgl.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.time.LocalDateTime;

/**
 * @Description: 项目信息Bo
 * @Author fsx
 * @CreateDate 2022/11/5 20:35
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class XmxxBo extends PageQueryBo {

	@ApiModelProperty(value = "关键字")
	private String keyword;

	@ApiModelProperty(value = "项目类型(0-三峡后续 1-移民后扶 2-河道堤防 3-湖库工程 4-农村水利 5-泵站工程 6-饮水安全 7-连通工程 8-水美乡村 9-水土保持 10-山洪灾害 11-节水项目 12-维修养护 13-其他)")
	private String xmlx;

	@ApiModelProperty(value = "日期")
	private String rq;

}
