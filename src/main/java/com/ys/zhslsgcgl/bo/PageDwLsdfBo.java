package com.ys.zhslsgcgl.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:单位历史得分
 * @Author: fsx
 * @CreateData: 2022/11/17 11:50
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class PageDwLsdfBo extends PageQueryBo{

    @ApiModelProperty("单位id")
    private String dwid;

    @ApiModelProperty("日期：年-月")
    private String rq;
}
