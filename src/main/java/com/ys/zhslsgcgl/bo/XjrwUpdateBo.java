package com.ys.zhslsgcgl.bo;

import cn.hutool.json.JSONObject;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import springfox.documentation.spring.web.json.Json;

import java.util.Date;
import java.util.List;

/**
 * @Description:修改巡检情况
 * @Author: wugangzhi
 * @CreateData: 2022/11/17 2:43
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class XjrwUpdateBo {
    @ApiModelProperty(value = "巡检任务ID")
    private String xjrwid;

    @ApiModelProperty(value = "巡检天气")
    private String xjtq;

    @ApiModelProperty(value = "履职情况")
    private String lzqk;

    @ApiModelProperty(value = "任务实际开始时间")
    private Date sjkssj;

    @ApiModelProperty(value = "任务实际完成时间")
    private Date sjwcsj;

    @ApiModelProperty(value = "巡检轨迹坐标点(多组用分号分隔)")
    private String points;

    @ApiModelProperty(value = "巡检措施内容List")
    List<XjrwCsnrListBo> xjcsnrList;

}
