package com.ys.zhslsgcgl.bo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Description: 设备管理查询参数
 * @Author: wugangzhi
 * @CreateDate: 2022/11/5 20:33
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class SbxxBo extends PageQueryBo {

    @ApiModelProperty(value="项目ID")
    private String xmid;

    @ApiModelProperty(value="标段ID")
    private String bdid;

    @ApiModelProperty(value="设备ID")
    private String sbid;

    @ApiModelProperty(value = "设备状态(0-离线 1-在线)")
    private String sbzt;
}
