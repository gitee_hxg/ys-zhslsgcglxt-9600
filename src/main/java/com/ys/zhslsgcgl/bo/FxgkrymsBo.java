package com.ys.zhslsgcgl.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 新增风险管控人员明细传入参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/17 14:45
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class FxgkrymsBo {
    @ApiModelProperty(value="管控ID")
    private String gkid;

    @ApiModelProperty(value="管控组织ID")
    private String gkzzid;

    @ApiModelProperty(value="责任人ID (从系统管理用中选择)")
    private String zrrid;
}
