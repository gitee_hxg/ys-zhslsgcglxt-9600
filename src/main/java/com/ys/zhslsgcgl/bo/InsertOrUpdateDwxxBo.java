package com.ys.zhslsgcgl.bo;

import com.ys.zhslsgcgl.entity.SgcXjrwEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;

/**
 * @Description: APP模块-新增或者编辑巡检任务
 * @Author: tjy
 * @CreateDate: 2022/11/15 20:33
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class InsertOrUpdateDwxxBo {
    @ApiModelProperty(value = "选中管控措施id")
    ArrayList<String> xzgckss;

    @ApiModelProperty(value="巡检任务,巡检任务ID要传")
    private SgcXjrwEntity sgcXjrwEntity;



}
