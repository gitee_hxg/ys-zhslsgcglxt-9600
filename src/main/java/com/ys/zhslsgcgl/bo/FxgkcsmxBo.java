package com.ys.zhslsgcgl.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * @Description: 管控措施明细
 * @Author: chenxingjian
 * CreateDate: 2022/11/15 20:44
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class FxgkcsmxBo {
    @ApiModelProperty(value = "管控组织ID(岗位)")
    private String gkgwid;

    @ApiModelProperty(value = "任务截止时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date rwjzsj;

    @ApiModelProperty(value = "管控措施信息")
    private List<FxgkcsxjBo> cslist;
}
