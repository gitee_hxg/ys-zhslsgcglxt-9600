package com.ys.zhslsgcgl.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 分页查询扣分管理传入参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/9 11:31
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class PagekfglBo extends PageQueryBo{
    @ApiModelProperty(value = "标段ID")
    private String bdid;

    @ApiModelProperty(value = "单位ID")
    private String dwid;

    @ApiModelProperty(value = "项目ID")
    private String xmid;

    @ApiModelProperty(value = "开始时间")
    private String kssj;

    @ApiModelProperty(value = "结束时间")
    private String jssj;
}
