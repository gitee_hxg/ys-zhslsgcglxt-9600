package com.ys.zhslsgcgl.bo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 评分扣分明细
 * @Author: wugangzhi
 * @CreateData: 2022/11/8 17:15
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class KfglmxBo {
    /**
     * 评分标准ID
     */
    @TableField(value = "PFBZID")
    @ApiModelProperty(value="评分标准ID")
    private String pfbzid;

    /**
     * 次数
     */
    @TableField(value = "CS")
    @ApiModelProperty(value="次数")
    private Integer cs;
}
