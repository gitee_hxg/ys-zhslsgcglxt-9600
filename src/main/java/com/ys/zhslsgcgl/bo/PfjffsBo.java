package com.ys.zhslsgcgl.bo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:评分加分_复核bo
 * @Author: dzg
 * @CreateDate: 2022/11/12 10:51
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class PfjffsBo {

    /**
     * 评分加分ID
     */
    @ApiModelProperty(value = "评分加分ID",required = true)
    private String pfjfid;

    /**
     * 复核意见 (C-复审同意 D-复审不同意)
     */
    @ApiModelProperty(value="复核意见(C-同意 D-不同意)",required = true)
    private String fhyj;

    /**
     * 复核备注
     */
    @ApiModelProperty(value="复核备注")
    private String fhbz;

}
