package com.ys.zhslsgcgl.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:巡检措施内容Bo
 * @Author: wugangzhi
 * @CreateData: 2022/11/17 2:47
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class XjrwCsnrListBo {
    @ApiModelProperty(value="管控措施ID")
    private String gkcsid;

    @ApiModelProperty(value="是否巡检(0-否 1-是)")
    private String sfxj;
}
