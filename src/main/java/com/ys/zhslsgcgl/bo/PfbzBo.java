package com.ys.zhslsgcgl.bo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 检查计分-评分标准管理查询参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/8 10:47
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class PfbzBo extends PageQueryBo{

    @ApiModelProperty(value = "标题")
    private String bt;

    @ApiModelProperty(value = "类型")
    private String lx;

    @ApiModelProperty(value = "大分类")
    private String dfl;

    @ApiModelProperty(value = "详细分类")
    private String xxfl;

    @ApiModelProperty(value = "性质分类")
    private String xzfl;
}
