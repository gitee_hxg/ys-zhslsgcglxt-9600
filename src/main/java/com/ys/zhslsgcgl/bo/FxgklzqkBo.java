package com.ys.zhslsgcgl.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 查询履职情况传入参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/17 19:40
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class FxgklzqkBo {
    @ApiModelProperty(value = "管控id")
    private String gkid;

    @ApiModelProperty(value = "组织类型ID(单位或岗位)")
    private String zzlxid;

    @ApiModelProperty(value = "任务状态 (0-未开始 1-巡检中 2-已完成)")
    private String rwzt;
}
