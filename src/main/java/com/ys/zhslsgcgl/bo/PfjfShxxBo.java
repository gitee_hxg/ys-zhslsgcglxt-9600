package com.ys.zhslsgcgl.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:评分加分审核查询参数
 * @Author: dzg
 * @CreateDate: 2022/11/11 16:13
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class PfjfShxxBo {

    @ApiModelProperty(value = "评分加分id")
    private String pfjfid;

    @ApiModelProperty(value = "流程类型")
    private String lclx;

}
