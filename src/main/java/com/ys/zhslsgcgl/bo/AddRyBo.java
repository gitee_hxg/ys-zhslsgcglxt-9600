package com.ys.zhslsgcgl.bo;

import com.ys.zhslsgcgl.entity.SgcRyglEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 人员管理-新增人员传参
 * @Author: fsx
 * CreateDate: 2022/11/11 10:51
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class AddRyBo extends SgcRyglEntity {

    @ApiModelProperty("Base64编码人脸图片")
    private String rltpBase64Url;
}
