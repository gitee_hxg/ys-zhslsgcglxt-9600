package com.ys.zhslsgcgl.bo;


import com.ys.common.core.bo.CommonPageBo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class QueryPagexjrwBo extends CommonPageBo {
    @ApiModelProperty(value = "责任人ID")
    private String zrrid;
}
