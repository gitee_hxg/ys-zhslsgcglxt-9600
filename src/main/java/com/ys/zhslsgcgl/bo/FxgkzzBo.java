package com.ys.zhslsgcgl.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Description: 风险管控组织传入参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/14 15:54
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class FxgkzzBo {
    @ApiModelProperty(value = "管控ID")
    private String gkid;

    @ApiModelProperty(value = "风险管控组织信息")
    private List<String> zzxxlist;
}
