package com.ys.zhslsgcgl.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Description: 风险管控措施巡检关联传参
 * @Author: chenxingjian
 * CreateDate: 2022/11/17 17:16
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class FxgkcsxjBo {
    @ApiModelProperty(value = "管控措施名称")
    private String gkcsmc;

    @ApiModelProperty(value = "管控措施id")
    private String gkcsid;
}
