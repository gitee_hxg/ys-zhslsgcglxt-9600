package com.ys.zhslsgcgl.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Value;

import javax.validation.constraints.NotBlank;

/**
 * @Description: 项目管理-根据名称和状态模糊查询项目信息
 * @Author: tjy
 * @CreateDate: 2022/1111 9:33
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class ListXmxxByXmmcZtBo {
    @ApiModelProperty(value = "项目名称")
    private String xmmc;

    @ApiModelProperty(value = "项目状态")
    @NotBlank(message = "项目状态不能为空")
    private String xmzt;
}
