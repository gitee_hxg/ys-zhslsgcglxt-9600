package com.ys.zhslsgcgl.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:分页查询设备绑定的人员列表
 * @Author: wugangzhi
 * @CreateData: 2022/11/22 11:21
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class SbryxxBo extends PageQueryBo{
    @ApiModelProperty(value="设备ID")
    private String sbid;

}
