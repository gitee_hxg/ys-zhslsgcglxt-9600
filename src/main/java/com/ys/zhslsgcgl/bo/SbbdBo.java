package com.ys.zhslsgcgl.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Description:设备标段绑定BO
 * @Author: wugangzhi
 * @CreateData: 2022/11/22 9:23
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class SbbdBo {

    @ApiModelProperty(value="设备ID")
    private String sbid;

    @ApiModelProperty(value="标段ID列表")
    private List<String> bdidList;

}
