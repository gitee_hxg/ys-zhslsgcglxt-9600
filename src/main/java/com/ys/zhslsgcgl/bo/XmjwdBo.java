package com.ys.zhslsgcgl.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class XmjwdBo {
    @ApiModelProperty(value = "项目id")
    private String xmid;

    @ApiModelProperty(value = "经度")
    private BigDecimal lon;

    @ApiModelProperty(value = "纬度")
    private BigDecimal lat;
}
