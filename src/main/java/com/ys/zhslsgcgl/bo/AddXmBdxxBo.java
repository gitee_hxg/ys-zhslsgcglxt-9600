package com.ys.zhslsgcgl.bo;

import com.ys.zhslsgcgl.entity.SgcBdzbdwGlbEntity;
import com.ys.zhslsgcgl.entity.SgcXmbdEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Description: 项目管理-根据名称和状态模糊查询项目信息
 * @Author: tjy
 * @CreateDate: 2022/1111 9:33
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class AddXmBdxxBo extends SgcXmbdEntity {
    @ApiModelProperty(value="只设置其中的中标单位ID,法人代表,联系电话")
    List<SgcBdzbdwGlbEntity> bdzddwref;


}
