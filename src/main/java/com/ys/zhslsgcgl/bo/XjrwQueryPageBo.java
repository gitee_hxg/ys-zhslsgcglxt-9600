package com.ys.zhslsgcgl.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:分页查询巡检任务查询条件
 * @Author: wugangzhi
 * @CreateData: 2022/11/17 0:32
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class XjrwQueryPageBo extends PageQueryBo{

    @ApiModelProperty(value = "任务状态(0-未开始 1-巡检中 2-已完成)")
    private String rwzt;

    @ApiModelProperty(value = "巡检人ID")
    private String xjr;

    @ApiModelProperty(value = "开始时间(yyyy-MM-dd)")
    private String startTime;

    @ApiModelProperty(value = "结束时间(yyyy-MM-dd)")
    private String endTime;
}
