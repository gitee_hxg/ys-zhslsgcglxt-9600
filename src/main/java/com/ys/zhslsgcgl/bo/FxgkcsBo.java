package com.ys.zhslsgcgl.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;


import java.util.Date;
import java.util.List;

/**
 * @Description: 风险管控传入参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/14 11:16
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class FxgkcsBo{
    @ApiModelProperty(value = "管控id")
    private String gkid;
    @ApiModelProperty(value = "管控措施明细")
    private List<FxgkcsmxBo> mxlist;
}
