package com.ys.zhslsgcgl.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class QueryPageBdryByXmidBo extends PageQueryBo{
    @ApiModelProperty(value = "项目id")
    private String xmid;
}
