package com.ys.zhslsgcgl.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 市场监督-单位管理查询参数
 * @Author: wugangzhi
 * @CreateDate: 2022/11/5 20:33
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class DwxxBo extends PageQueryBo {

    @ApiModelProperty(value="所属行政区划编码")
    private String xzqhdm;

    @ApiModelProperty(value="单位名称")
    private String dwmc;

    @ApiModelProperty(value="单位类别 (对应字典值:1-业主单位 2-设计单位 3-监理单位 4-施工单位 5-质检单位)")
    private String dwlb;

    @ApiModelProperty(value="关键字")
    private String keyword;

    @ApiModelProperty(value="日期，格式:年-月")
    private String rq;
}
