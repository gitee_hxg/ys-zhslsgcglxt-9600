package com.ys.zhslsgcgl.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:评分加分查询参数
 * @Author: dzg
 * @CreateDate: 2022/11/11 13:07
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class PfjfBo extends PageQueryBo{

    @ApiModelProperty(value="项目名称")
    private String xmmc;

    @ApiModelProperty(value="公司名称")
    private String gsmc;

    @ApiModelProperty(value="审核状态")
    private String shzt;

    @ApiModelProperty(value="审核意见")
    private String shyj;

}
