package com.ys.zhslsgcgl.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 风险管控分页查询传入参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/12 14:14
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class PageFxgkBo extends PageQueryBo{
    @ApiModelProperty(value = "项目ID")
    private String xmid;

    @ApiModelProperty(value = "标段ID")
    private String bdid;

    @ApiModelProperty(value = "关键字")
    private String gjz;

    @ApiModelProperty(value = "危险源类别")
    private String wxylb;

    @ApiModelProperty(value = "危险源级别")
    private String wxyjb;

    @ApiModelProperty(value = "风险等级")
    private String fxdj;
}
