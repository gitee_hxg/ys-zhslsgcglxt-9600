package com.ys.zhslsgcgl.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:
 * @Author: wugangzhi
 * @CreateData: 2022/11/25 13:12
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class RykqayhzBo extends PageQueryBo{
    @ApiModelProperty(value = "项目ID")
    private String xmid;

    @ApiModelProperty(value = "员工ID")
    private String ygid;

    @ApiModelProperty(value = "年月(yyyy-MM)")
    private String ny;
}
