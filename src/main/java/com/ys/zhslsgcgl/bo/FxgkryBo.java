package com.ys.zhslsgcgl.bo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Description: 新增风险管控人员传入参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/17 14:42
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class FxgkryBo {
    @ApiModelProperty(value = "风险管控人员明细")
    private List<FxgkrymsBo> rylist;
}
