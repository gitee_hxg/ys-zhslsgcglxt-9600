package com.ys.zhslsgcgl.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class XmxxglBo extends PageQueryBo {
    @ApiModelProperty(value = "行政区划")
    private String xzqh;

    @ApiModelProperty(value = "项目名称")
    private String xmmc;

    @ApiModelProperty(value = "项目状态")
    private String xmzt;
}
