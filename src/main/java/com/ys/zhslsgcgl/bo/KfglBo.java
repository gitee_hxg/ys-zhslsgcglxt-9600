package com.ys.zhslsgcgl.bo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Description: 市场监督-扣分管理传入参数
 * @Author: wugangzhi
 * @CreateDate: 2022/11/5 20:33
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class KfglBo {

    @ApiModelProperty(value="评分扣分ID")
    private String pfkfid;

    @ApiModelProperty(value="项目ID")
    private String xmid;


    @ApiModelProperty(value="标段ID")
    private String bdid;


    @ApiModelProperty(value="单位ID")
    private String dwid;

    @ApiModelProperty(value="总扣分值")
    private BigDecimal zkfz;


    @ApiModelProperty(value="一般问题次数")
    private Integer ybwtcs;


    @ApiModelProperty(value="较重问题次数")
    private Integer jzwtcs;


    @ApiModelProperty(value="严重问题次数")
    private Integer yzwtcs;


    @ApiModelProperty(value="评分时间")
    private Date pfsj;


    @ApiModelProperty(value="检查人")
    private String jrx;


    @ApiModelProperty(value="备注")
    private String bz;

    @ApiModelProperty(value="扣分标准明细")
    private List<KfglmxBo> mxList;

}
