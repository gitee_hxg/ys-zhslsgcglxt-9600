package com.ys.zhslsgcgl.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Description:履职情况督查填写
 * @Author: wugangzhi
 * @CreateData: 2022/11/24 17:56
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
@Validated
public class XjlzqkUpdateBo {

    @NotBlank(message = "巡检任务ID不能为空")
    @ApiModelProperty(value = "巡检任务ID",required = true)
    private String xjrwid;

    @NotNull(message = "安全分扣分不能为空")
    @ApiModelProperty(value = "安全分扣分",required = true)
    private double aqfkf;

    @NotBlank(message = "督查人员ID不能为空")
    @ApiModelProperty(value = "督查人员ID",required = true)
    private String dcry;

    @ApiModelProperty(value = "督查备注")
    private String bz;
}
