package com.ys.zhslsgcgl.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 人员考勤信息Dto
 * @Author fsx
 * @CreateDate 2022/11/10 16:53
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class RyKqxxDto {

    @ApiModelProperty("员工id")
    private String ygid;

    @ApiModelProperty("到岗天数")
    private Integer dgts;

}
