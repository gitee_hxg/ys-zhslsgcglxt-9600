package com.ys.zhslsgcgl.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 人员今日打卡记录Dto
 * @Author fsx
 * @CreateDate 2022/11/10 16:53
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class RyJrdkjlDto {

    @ApiModelProperty("打卡记录id")
    private String jlid;

    @ApiModelProperty("员工id")
    private String ygid;

    @ApiModelProperty("打卡记录图片路径")
    private String url;
}
