package com.ys.zhslsgcgl.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:巡检任务管控措施内容
 * @Author: wugangzhi
 * @CreateData: 2022/11/17 2:30
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class XjrwgcnrDto {

    @ApiModelProperty(value="管控措施ID")
    private String gkcsid;

    @ApiModelProperty(value="管控措施名称")
    private String gkcsmc;

    @ApiModelProperty(value="是否巡检(0-否 1-是)")
    private String sfxj;
}
