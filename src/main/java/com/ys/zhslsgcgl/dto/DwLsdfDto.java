package com.ys.zhslsgcgl.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DwLsdfDto {

    @ApiModelProperty("日期")
    private String rq;

    @ApiModelProperty("考勤扣分")
    private Double kqkf;

    @ApiModelProperty("检查扣分")
    private Double jckf = 0.0;

    @ApiModelProperty("加分")
    private Double jf = 0.0;

    @ApiModelProperty("最终得分")
    private Double zzdf;


}
