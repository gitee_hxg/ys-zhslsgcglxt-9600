package com.ys.zhslsgcgl.dto;

import com.ys.zhslsgcgl.entity.SgcAqfxgkEntity;
import com.ys.zhslsgcgl.entity.SgcAqfxgkGkcsEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Description: APP模块-根据管控id和责任人id查询管控和管控信息
 * @Author: tjy
 * @CreateDate: 2022/11/15 14:33
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class ListGkAndGkxxByIdDto extends SgcAqfxgkEntity {
    @ApiModelProperty(value = "管控措施")
    List<SgcAqfxgkGkcsEntity> sgcAqfxgkGkcsEntitys;

}
