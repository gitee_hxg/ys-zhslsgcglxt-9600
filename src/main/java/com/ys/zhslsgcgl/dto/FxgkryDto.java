package com.ys.zhslsgcgl.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 风险管控人员措施返回参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/12 15:12
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class FxgkryDto {
    @ApiModelProperty(value = "管控ID")
    private  String gkid;

    @ApiModelProperty(value = "责任人ID")
    private String zrrid;

    @ApiModelProperty(value = "责任人姓名")
    private String zrrxm;

    @ApiModelProperty(value = "责任人手机号")
    private String zrrsjh;
}
