package com.ys.zhslsgcgl.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;
import java.util.Date;

/**
 * @Description: 计算单位历史得分记录
 * @Author: wugangzhi
 * @CreateData: 2022/11/25 17:02
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class DwlsdfjlDto {
    @ApiModelProperty("单位ID")
    private String dwid;

    @ApiModelProperty(value = "日期(yyyy-MM-dd)")
    private LocalDate rq;

    @ApiModelProperty("评分加分")
    private double jf;

    @ApiModelProperty("检查记分")
    private double jcjf;

    @ApiModelProperty("考勤记分")
    private double kqjf;

    @ApiModelProperty("评分加分")
    private double scjf;

    @ApiModelProperty("检查记分")
    private double scjcjf;

    @ApiModelProperty("考勤记分")
    private double sckqjf;

    @ApiModelProperty("上次最终得分")
    private double sczzdf;
}
