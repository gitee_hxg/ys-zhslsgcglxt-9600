package com.ys.zhslsgcgl.dto;

import com.ys.zhslsgcgl.entity.SgcXmxxEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description: 项目管理-根据项目ID查询项目信息和投资信息
 * @Author: tjy
 * @CreateDate: 2022/1111 9:33
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class ListXmxxAndTzByXmidDto extends SgcXmxxEntity {
    @ApiModelProperty(value = "实际投资金额")
    private BigDecimal sjtzje;

}
