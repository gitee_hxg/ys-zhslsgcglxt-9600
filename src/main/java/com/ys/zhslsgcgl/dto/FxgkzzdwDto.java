package com.ys.zhslsgcgl.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Description: 查询风险管控措施返回参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/15 10:39
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class FxgkzzdwDto {

    @ApiModelProperty(value = "管控单位名称")
    private String dwmc;

    @ApiModelProperty(value = "组织类型ID(单位)")
    private String zzdwid;

    @ApiModelProperty(value = "管控组织ID(单位)")
    private String gkdwid;

    @ApiModelProperty(value = "管控岗位")
    private List<FxgkgwDto> gkgwlist;

}
