package com.ys.zhslsgcgl.dto;

import com.ys.zhslsgcgl.entity.SgcXmxxEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 项目管理-分页查询项目信息
 * @Author: tjy
 * @CreateDate: 2022/11/9 14:33
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class QueryPageXmxxDto extends SgcXmxxEntity {
    @ApiModelProperty(value = "项目类型字典值")
    private String xmlxzdz;

    @ApiModelProperty(value = "项目状态字典值")
    private String xmztzdz;

    @ApiModelProperty(value = "行政区划字典值")
    private String xzqhdmzdz;
}
