package com.ys.zhslsgcgl.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 项目分页查询列表返回Dto
 * @Author fsx
 * @CreateDate 2022/11/5 21:00
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class PageXmxxDto{

    @ApiModelProperty(value = "项目id")
    private String xmid;

    @ApiModelProperty(value = "项目名称")
    private String xmmc;

}
