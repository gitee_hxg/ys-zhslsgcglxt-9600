package com.ys.zhslsgcgl.dto;

import com.ys.zhslsgcgl.entity.SgcAqfxgkGkcsEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Description:巡检任务详情Dto
 * @Author: wugangzhi
 * @CreateData: 2022/11/17 2:05
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class XjrwxqDto {
    @ApiModelProperty(value = "巡检任务ID")
    private String xjrwid;

    @ApiModelProperty(value = "任务状态(0-未开始 1-巡检中 2-已完成)")
    private String rwzt;

    @ApiModelProperty(value = "管控ID")
    private String gkid;


    @ApiModelProperty(value = "巡检人ID")
    private String xjr;

    @ApiModelProperty(value = "巡检人姓名")
    private String xjrxm;

    @ApiModelProperty(value = "巡检天气")
    private String xjtq;

    @ApiModelProperty(value = "履职情况")
    private String lzqk;

    @ApiModelProperty(value = "任务计划开始时间")
    private Date rwkssj;

    @ApiModelProperty(value = "任务截止时间")
    private Date rwjzsj;

    @ApiModelProperty(value = "任务实际开始时间")
    private Date sjkssj;

    @ApiModelProperty(value = "任务实际完成时间")
    private Date sjwcsj;

    @ApiModelProperty(value = "任务创建时间")
    private Date createdTime;

    @ApiModelProperty(value = "项目名称")
    private String xmmc;

    @ApiModelProperty(value = "标段名称")
    private String bdmc;

    @ApiModelProperty(value = "危险源类别")
    private String wxylb;

    @ApiModelProperty(value = "危险项目")
    private String wxxm;

    @ApiModelProperty(value = "危险源级别")
    private String wxyjb;

    @ApiModelProperty(value = "部位")
    private String bw;

    @ApiModelProperty(value = "可能导致的事故类型")
    private String kndzsglx;

    @ApiModelProperty(value = "风险等级（范围）")
    private String fxdj;

    @ApiModelProperty(value = "巡检内容List")
    List<XjrwgcnrDto> gkcsList=new ArrayList<XjrwgcnrDto>();

    @ApiModelProperty(value = "安全分扣分")
    private double aqfkf;

    @ApiModelProperty(value = "督查人员ID")
    private String dcry;

    @ApiModelProperty(value = "督查人员姓名")
    private String dcryxm;

    @ApiModelProperty(value = "督查人员电话")
    private String dcrydh;

    @ApiModelProperty(value = "督查备注")
    private String bz;

    @ApiModelProperty(value = "巡检轨迹坐标点(多组用分号分隔)")
    private String points;
}
