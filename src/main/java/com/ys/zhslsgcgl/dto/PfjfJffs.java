package com.ys.zhslsgcgl.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:评分加分加分分数
 * @Author: dzg
 * @CreateDate: 2022/11/11 17:34
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class PfjfJffs {
    /**
     * 加分分数
     *
     * 施工单位承接的项目未发生质量与安全责任事故
     * 合同金额小于3000万加7分
     * 合同金额大于3000万小于5000万加8分
     * 合同金额大于等于5000万加9分
     */
    @ApiModelProperty(value = "加分分数")
    private double jffs;

}
