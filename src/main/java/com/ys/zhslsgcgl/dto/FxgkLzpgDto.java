package com.ys.zhslsgcgl.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 履职批改返回参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/17 23:23
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class FxgkLzpgDto {
    @ApiModelProperty(value = "管控措施名称")
    private String gkcsmc;

    @ApiModelProperty(value = "管控措施id")
    private String gkcsid;

    @ApiModelProperty(value = "管控组织id")
    private String gkzzid;

    @ApiModelProperty(value = "是否巡检")
    private String sfxj;
}
