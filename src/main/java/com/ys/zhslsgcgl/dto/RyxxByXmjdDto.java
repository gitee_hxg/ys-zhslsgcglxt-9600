package com.ys.zhslsgcgl.dto;

import com.ys.common.core.annotation.dic.DicField;
import com.ys.zhslsgcgl.entity.SgcRyglEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 人员分页查询列表返回Dto
 * @Author fsx
 * @CreateDate 2022/11/5 21:00
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class RyxxByXmjdDto extends SgcRyglEntity {

    @ApiModelProperty(value = "单位id")
    private String dwid;

    @ApiModelProperty(value = "单位类别")
    @DicField(tableName = "SGC_DWGL",fieldName="DWLB")
    private String dwlb;

    @ApiModelProperty(value = "单位名称")
    private String dwmc;

    @ApiModelProperty(value = "文件组合路径")
    private String filepath;

    @ApiModelProperty(value = "头像路径")
    private String rlxxurl;

    @ApiModelProperty(value = "身份证正面路径")
    private String sfzzmurl;

    @ApiModelProperty(value = "身份证反面路径")
    private String sfzfmurl;

}
