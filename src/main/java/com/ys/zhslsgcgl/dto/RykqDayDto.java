package com.ys.zhslsgcgl.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.ys.zhslsgcgl.entity.SgcRykqDayEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class RykqDayDto extends SgcRykqDayEntity {

    @ApiModelProperty(value = "每日打卡记录图片地址")
    private String url;
}
