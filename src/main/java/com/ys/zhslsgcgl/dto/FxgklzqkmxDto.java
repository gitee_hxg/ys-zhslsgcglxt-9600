package com.ys.zhslsgcgl.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Description: 查询履职情况明细返回参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/17 21:25
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class FxgklzqkmxDto {
    @ApiModelProperty(value = "责任单位")
    private String zrdw;

    @ApiModelProperty(value = "组织类型id(岗位)")
    private String gkgwid;

    @ApiModelProperty(value = "监督措施")
    private String jdcs;

    @ApiModelProperty(value = "姓名")
    private String xm;
}
