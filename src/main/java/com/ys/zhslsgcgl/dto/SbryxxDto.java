package com.ys.zhslsgcgl.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:设备绑定用户信息
 * @Author: wugangzhi
 * @CreateData: 2022/11/17 0:20
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class SbryxxDto {
    @ApiModelProperty(value = "员工ID")
    private String ygid;

    @ApiModelProperty(value = "员工姓名")
    private String ygxm;

    @ApiModelProperty(value = "身份证号码")
    private String sfzhm;

    @ApiModelProperty(value = "手机号")
    private String sjh;

    @ApiModelProperty(value = "职位名称")
    private String zwDic;

    @ApiModelProperty(value = "员工类型名称")
    private String yglxDic;

    @ApiModelProperty(value = "单位名称")
    private String dwmc;

    @ApiModelProperty(value = "单位类别名称")
    private String dwlbDic;

    @ApiModelProperty(value = "项目名称")
    private String xmmc;

    @ApiModelProperty(value = "标段名称")
    private String bdmc;

    @ApiModelProperty(value = "是否绑定(0-未绑定 1-绑定)")
    private String sfbd;


}
