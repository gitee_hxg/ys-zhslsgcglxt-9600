package com.ys.zhslsgcgl.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.ys.zhslsgcgl.entity.SgcAqfxgkZzxxEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @Description: 查询风险管控单位返回参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/15 18:06
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class FxgkdwDto {

    @ApiModelProperty(value="管控组织ID(岗位)")
    private String gkdwid;

    @ApiModelProperty(value="组织类型ID")
    private String zzlxid;

    @ApiModelProperty(value="创建人")
    private String createdBy;

    @ApiModelProperty(value="创建时间")
    private Date createdTime;

    @ApiModelProperty(value="更新人")
    private String updatedBy;

    @ApiModelProperty(value="更新时间")
    private Date updatedTime;

    @ApiModelProperty(value = "所属单位名称")
    private String dwmc;
}
