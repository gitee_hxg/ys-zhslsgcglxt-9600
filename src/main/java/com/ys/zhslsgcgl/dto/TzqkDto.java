package com.ys.zhslsgcgl.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description: 投资情况返回参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/11 13:40
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class TzqkDto {
    @ApiModelProperty(value = "完成投资")
    private BigDecimal wctz;

    @ApiModelProperty(value = "计划投资")
    private BigDecimal jhtz;
}
