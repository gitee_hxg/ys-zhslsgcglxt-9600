package com.ys.zhslsgcgl.dto;

import com.ys.zhslsgcgl.entity.SgcXmxxEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Description: 项目管理-根据名称和状态模糊查询项目信息
 * @Author: tjy
 * @CreateDate: 2022/1111 9:33
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class ListXmxxByXmmcZtDto {
    @ApiModelProperty(value = "项目信息集合")
    List<SgcXmxxEntity> sgcXmxxEntitys;

    @ApiModelProperty(value = "项目总数")
    private String zs;

    @ApiModelProperty(value = "当前状态的项目数")
    private String dqztxms;

}
