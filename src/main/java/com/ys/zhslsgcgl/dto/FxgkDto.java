package com.ys.zhslsgcgl.dto;

import com.ys.zhslsgcgl.entity.SgcAqfxgkEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 查询风险管控主表返回参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/15 15:40
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class FxgkDto extends SgcAqfxgkEntity {
    @ApiModelProperty(value = "工程项目名称")
    private String xmmc;

    @ApiModelProperty(value = "标段名称")
    private String bdmc;
}
