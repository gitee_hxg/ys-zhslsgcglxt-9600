package com.ys.zhslsgcgl.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 项目总览返回参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/11 12:48
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class XmzlDto {
    @ApiModelProperty(value = "项目总数")
    private int xmzs;

    @ApiModelProperty(value = "小型工程")
    private int xxgc;

    @ApiModelProperty(value = "中型工程")
    private int zxgc;

    @ApiModelProperty(value = "重大型工程")
    private int zdgc;
}
