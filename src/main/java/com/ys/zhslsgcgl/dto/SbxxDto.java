package com.ys.zhslsgcgl.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ys.zhslsgcgl.entity.SgcDwglEntity;
import com.ys.zhslsgcgl.entity.SgcSbxxEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * @Description:设备管理
 * @Author: wugangzhi
 * @CreateData: 2022/11/17 0:20
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class SbxxDto{

    @ApiModelProperty(value = "设备ID")
    private String sbid;

    @ApiModelProperty(value = "注册设备的IP")
    private String ipaddress;

    @ApiModelProperty(value = "注册设备的端口")
    private Integer port;

    @ApiModelProperty(value = "注册设备的用户名")
    private String username;

    @ApiModelProperty(value = "注册设备的密码")
    private String password;

    @ApiModelProperty(value = "设备状态(0-离线 1-在线)")
    private String sbzt;

    @ApiModelProperty(value = "创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    @ApiModelProperty(value = "项目ID")
    private String xmid;

    @ApiModelProperty(value = "项目名称")
    private String xmmc;

    @ApiModelProperty(value = "设备绑定的标段列表")
    List<BdsbglDto> bdList;

}
