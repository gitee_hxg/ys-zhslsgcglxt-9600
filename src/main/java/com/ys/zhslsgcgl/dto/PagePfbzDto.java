package com.ys.zhslsgcgl.dto;

import com.ys.zhslsgcgl.entity.SgcPfbzEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 检查计分-评分标准管理查询返回参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/8 10:53
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class PagePfbzDto extends SgcPfbzEntity {
    @ApiModelProperty(value = "分类ID")
    private String flid;

    @ApiModelProperty(value = "性质分类字典值")
    private String xzflDic;

    @ApiModelProperty(value = "类型字典值")
    private String lxDic;

    @ApiModelProperty(value = "大分类字典值")
    private String dflDic;

    @ApiModelProperty(value = "详细分类字典值")
    private String xxflDic;

    @ApiModelProperty(value = "记分单位字典值")
    private String jfdwDic;
}
