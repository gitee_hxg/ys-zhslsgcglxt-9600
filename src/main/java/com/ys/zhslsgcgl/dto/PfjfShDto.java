package com.ys.zhslsgcgl.dto;

import com.ys.zhslsgcgl.entity.SgcPfjfEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:评分加分审核信息
 * @Author: dzg
 * @CreateDate: 2022/11/11 14:50
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class PfjfShDto extends SgcPfjfEntity {
    /**
     * 项目名称
     */
    @ApiModelProperty(value = "项目名称")
    private String xmmc;

    /**
     * 质量飞检情况 (0-飞检不合格 1-飞检合格)
     */
    @ApiModelProperty(value = "质量飞检情况")
    private String zlfjqk;

    /**
     * 事故管理
     */
    @ApiModelProperty(value = "事故管理")
    private String sggl;

    /**
     * 流程类型
     */
    @ApiModelProperty(value = "流程类型(A-县级初审 B-市级复审)")
    private String lclx;

    /**
     * 审核状态
     */
    @ApiModelProperty(value = "审核状态 (0-待审核  1-审核中(县级审批等待市级审批)  2-已审核 )")
    private String shzt;

    /**
     * 审核意见
     */
    @ApiModelProperty(value = "审核意见 (A-初审同意 B初审不同意 C-复审同意 D-复审不同意)")
    private String shyj;

    /**
     * 初审信息
     */
    @ApiModelProperty(value = "初审信息")
    private PfjfCsxxDto pfjfCsDto;

    /**
     * 复审信息
     */
    @ApiModelProperty(value = "复审信息")
    private PfjfFsxxDto pfjfFsDto;


    /**
     * 流程状态
     */
    @ApiModelProperty(value = "流程状态(0-待审核  1-初审不通过 2-初审通过  3-复审不通过 4-复审通过)")
    private String lczt;


}
