package com.ys.zhslsgcgl.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

/**
 * @Description: 在建项目情况返回参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/11 15:35
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class ZjxmDto {
    @ApiModelProperty(value = "在建项目数")
    private int zjxms;

    @ApiModelProperty(value = "在建项目总投资")
    private BigDecimal zjxmztz;

    @ApiModelProperty(value = "在建重大项目数")
    private int zjzdxms;

    @ApiModelProperty(value = "在建重大项目总投资")
    private BigDecimal zjzdxmztz;
}
