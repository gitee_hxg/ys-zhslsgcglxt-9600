package com.ys.zhslsgcgl.dto;

import com.ys.zhslsgcgl.entity.SgcDwglEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Description: 项目管理-根据项目ID查询标段和单位信息
 * @Author: tjy
 * @CreateDate: 2022/11/15 14:33
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class ListBdxxByXmidmcyhDto {
    @ApiModelProperty(value = "标段对应单位集合")
    List<SgcDwglEntity> sgcDwglEntitys;

    @ApiModelProperty(value = "标段对应单位的所有名称,逗号分隔")
    private String dwmcs;

    @ApiModelProperty(value = "标段id")
    private String bdid;

    @ApiModelProperty(value = "标段名称")
    private String bdmc;

    @ApiModelProperty(value = "项目状态")
    private String xmzt;

    @ApiModelProperty(value = "标段金额(万元)")
    private BigDecimal bdje;

    @ApiModelProperty(value = "建设状态:(0-在建工程 1-完工待验收 2-完工已验收)")
    private String jszt;

}
