package com.ys.zhslsgcgl.dto;

import com.ys.zhslsgcgl.entity.SgcDwglEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @Description:失信曝光返回结果
 * @Author: dzg
 * @CreateDate: 2022/11/11 12:43
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class SxbgDto extends SgcDwglEntity {

    @ApiModelProperty(value="曝光ID")
    private String pbid;

    @ApiModelProperty(value = "单位类别字典值")
    private String dwlbDic;

    @ApiModelProperty(value="公司状态字典值")
    private String gsztDic;

    @ApiModelProperty(value="失信曝光时间")
    private Date sxbgTime;

    @ApiModelProperty(value="失信曝光标题")
    private String bt;

    @ApiModelProperty(value="失信曝光简介")
    private String jj;

    @ApiModelProperty(value="失信曝光点击量")
    private int djl;

    @ApiModelProperty(value="外部连接")
    private String wbll;

    @ApiModelProperty(value="曝光内容")
    private String nr;


}
