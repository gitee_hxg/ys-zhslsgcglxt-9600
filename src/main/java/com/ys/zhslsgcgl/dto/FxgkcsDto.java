package com.ys.zhslsgcgl.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 查询风险管控措施返回参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/15 20:44
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class FxgkcsDto {

    @ApiModelProperty(value = "管控措施ID")
    private String gkcsid;

    @ApiModelProperty(value = "管控措施名称")
    private String gkcsmc;
}
