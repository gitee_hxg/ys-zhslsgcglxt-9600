package com.ys.zhslsgcgl.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description:巡检任务返回列表
 * @Author: wugangzhi
 * @CreateData: 2022/11/17 0:20
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class XjrwDto {

    @ApiModelProperty(value = "巡检任务ID")
    private String xjrwid;

    @ApiModelProperty(value = "任务状态(0-未开始 1-巡检中 2-已完成)")
    private String rwzt;

    @ApiModelProperty(value = "管控ID")
    private String gkid;


    @ApiModelProperty(value = "巡检人ID")
    private String xjr;

    @ApiModelProperty(value = "巡检人姓名")
    private String xjrxm;

    @ApiModelProperty(value = "巡检天气")
    private String xjtq;

    @ApiModelProperty(value = "履职情况")
    private String lzqk;

    @ApiModelProperty(value = "任务计划开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date rwkssj;

    @ApiModelProperty(value = "任务截止时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date rwjzsj;

    @ApiModelProperty(value = "任务实际开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date sjkssj;

    @ApiModelProperty(value = "任务实际完成时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date sjwcsj;

    @ApiModelProperty(value = "任务创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    @ApiModelProperty(value = "项目名称")
    private String xmmc;

    @ApiModelProperty(value = "标段名称")
    private String bdmc;


    @ApiModelProperty(value="危险源类别")
    private String wxylb;

    @ApiModelProperty(value="危险项目")
    private String wxxm;


    @ApiModelProperty(value="危险源级别 (一般，重大)")
    private String wxyjb;


    @ApiModelProperty(value="部位")
    private String bw;


    @ApiModelProperty(value="可能导致的事故类型")
    private String kndzsglx;


    @ApiModelProperty(value="风险等级（范围） (低风险，一般风险，重大风险)")
    private String fxdj;




}
