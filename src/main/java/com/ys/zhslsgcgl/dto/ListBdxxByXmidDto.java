package com.ys.zhslsgcgl.dto;


import com.ys.zhslsgcgl.entity.SgcDwglEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description: 项目管理-根据项目ID查询标段信息
 * @Author: tjy
 * @CreateDate: 2022/11/9 14:33
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class ListBdxxByXmidDto extends SgcDwglEntity {
    @ApiModelProperty(value = "标段id")
    private String bdid;

    @ApiModelProperty(value = "标段名称")
    private String bdmc;

    @ApiModelProperty(value = "项目状态")
    private String xmzt;

    @ApiModelProperty(value = "标段金额(万元)")
    private BigDecimal bdje;

    @ApiModelProperty(value = "建设状态:(0-在建工程 1-完工待验收 2-完工已验收)")
    private String jszt;


}
