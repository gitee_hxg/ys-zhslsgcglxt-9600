package com.ys.zhslsgcgl.dto;

import com.ys.zhslsgcgl.entity.SgcDwglEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 市场监督-单位管理分页查询返回参数
 * @Author: wugangzhi
 * @CreateDate: 2022/11/5 20:33
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class DwxxDto extends SgcDwglEntity{

    @ApiModelProperty(value = "地区")
    private String dq;

    @ApiModelProperty(value = "单位类别字典值")
    private String dwlbDic;

    @ApiModelProperty(value="公司状态字典值")
    private String gsztDic;


}
