package com.ys.zhslsgcgl.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @Description:
 * @Author: dzg
 * @CreateDate: 2022/11/11 16:11
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class PfjfFsxxDto {
    /**
     * 审核记录ID
     */
//    @ApiModelProperty(value="审核记录ID")
//    private String shjlid;

    /**
     * 评分加分ID
     */
//    @ApiModelProperty(value="评分加分ID")
//    private String pfjfid;

    /**
     * 流程类型 (A-县级初审 B-市级复审)
     */
//    @ApiModelProperty(value="流程类型 (A-县级初审 B-市级复审)")
//    private String lclx;

    /**
     * 审核状态 (0-待审核  1-审核中(县级审批等待市级审批)  2-已审核 )
     */
//    @ApiModelProperty(value="审核状态 (0-待审核  1-审核中(县级审批等待市级审批)  2-已审核 )")
//    private String shzt;

    /**
     * 审核意见 (A-初审同意 B初审不同意 C-复审同意 D-复审不同意)
     */
    @ApiModelProperty(value="审核意见 (A-初审同意 B初审不同意 C-复审同意 D-复审不同意)")
    private String shyj;

    /**
     * 审核备注
     */
    @ApiModelProperty(value="审核备注")
    private String shbz;

    /**
     * 审核单位
     */
//    @ApiModelProperty(value="审核单位")
//    private String shdw;

    /**
     * 审核人员
     */
    @ApiModelProperty(value="审核人员")
    private String shry;

    /**
     * 审核内容
     */
//    @ApiModelProperty(value="审核内容")
//    private String spnr;

    /**
     * 审核时间
     */
    @ApiModelProperty(value="审核时间")
    private Date shsj;

}
