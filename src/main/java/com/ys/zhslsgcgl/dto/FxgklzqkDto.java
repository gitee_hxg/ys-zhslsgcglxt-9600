package com.ys.zhslsgcgl.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * @Description: 查询履职情况返回参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/17 19:48
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class FxgklzqkDto {
    @ApiModelProperty(value = "责任单位")
    private String zrdw;

    @ApiModelProperty(value = "职位")
    private String zw;

    @ApiModelProperty(value = "姓名")
    private String xm;

    @ApiModelProperty(value = "电话")
    private String dh;

    @ApiModelProperty(value = "监督措施list")
    private List<String> jdcslist;

    @ApiModelProperty(value = "任务开始时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date rwkssj;

    @ApiModelProperty(value = "任务结束时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date rwjzsj;

    @ApiModelProperty(value = "任务状态")
    private String rwzt;

    @ApiModelProperty(value = "巡检任务id")
    private String xjrwid;

    @ApiModelProperty(value = "管控id")
    private String gkid;

    @ApiModelProperty(value = "组织类型id")
    private String zzlxid;

    @ApiModelProperty(value = "管控组织id")
    private String gkzzid;
}
