package com.ys.zhslsgcgl.dto;

import com.ys.zhslsgcgl.entity.SgcPfkfEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Description: 分页查询扣分管理返回参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/8 19:09
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class PfkfDto extends SgcPfkfEntity {
    @ApiModelProperty(value = "项目名称")
    private String xmmc;

    @ApiModelProperty(value = "单位名称")
    private String dwmc;

    @ApiModelProperty(value = "标段名称")
    private String bdmc;

    private List<PfkfmxDto> pfkfmxList;
}
