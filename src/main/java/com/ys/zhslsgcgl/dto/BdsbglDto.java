package com.ys.zhslsgcgl.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:标段设备绑定dto
 * @Author: wugangzhi
 * @CreateData: 2022/11/21 18:12
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class BdsbglDto {

    @ApiModelProperty(value = "标段ID")
    private String bdid;

    @ApiModelProperty(value = "标段名称")
    private String bdmc;
}
