package com.ys.zhslsgcgl.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * @Description: 查询风险管控岗位返回参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/15 20:35
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class FxgkgwDto {

    @ApiModelProperty(value = "岗位名称")
    private String gwmc;

    @ApiModelProperty(value = "组织类型ID(岗位)")
    private String zzgwid;

    @ApiModelProperty(value = "管控组织ID")
    private String gkgwid;

    @ApiModelProperty(value = "任务截止时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date rwjzsj;

    @ApiModelProperty(value = "管控措施")
    private List<FxgkcsDto> gkcslist;

    @ApiModelProperty(value = "管控人员")
    private List<FxgkryDto> gkrylist;
}
