package com.ys.zhslsgcgl.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.ys.zhslsgcgl.entity.SgcPfjfEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Description:加分管理_评分加分dto
 * @Author: dzg
 * @CreateDate: 2022/11/11 13:02
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class PfjfDto extends SgcPfjfEntity{

    /**
     * 项目名称
     */
    @ApiModelProperty(value = "项目名称")
    private String xmmc;

    /**
     * 单位名称
     */
    @ApiModelProperty(value="单位名称")
    private String dwmc;

    /**
     * 审核状态
     */
    @ApiModelProperty(value="审核状态")
    private String shzt;

    /**
     * 审核意见
     */
    @ApiModelProperty(value="审核意见")
    private String shyj;

    /**
     * 流程类型名称
     */
    @ApiModelProperty(value="流程类型名称")
    private String lxcxmc;

    /**
     * 审核状态名称
     */
    @ApiModelProperty(value="审核状态名称")
    private String shztmc;

    /**
     * 审核意见名称
     */
    @ApiModelProperty(value="审核意见名称")
    private String shyjmc;




}
