package com.ys.zhslsgcgl.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 查询管控组织返回参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/12 15:09
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class FxgkzzDto {
    @ApiModelProperty(value = "管控组织名称")
    private String pmc;

    @ApiModelProperty(value = "管控组织ID")
    private String gkzzid;

    @ApiModelProperty(value = "组织类型父ID")
    private String zzlxpid;

    @ApiModelProperty(value = "组织类型ID")
    private String zzlxid;

    @ApiModelProperty(value = "项目ID")
    private String xmid;

    @ApiModelProperty(value = "组织类型名称")
    private String zzlxmc;

    @ApiModelProperty(value = "级别")
    private String jb;

}
