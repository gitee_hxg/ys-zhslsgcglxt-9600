package com.ys.zhslsgcgl.dto;

import com.ys.zhslsgcgl.entity.SgcDwglEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @Description:诚信管理-分页查询返回参数
 * @Author: dzg
 * @CreateDate: 2022/11/9 11:20
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class CxglDto extends SgcDwglEntity {
    @ApiModelProperty(value = "地区")
    private String dq;

    @ApiModelProperty(value = "单位类别字典值")
    private String dwlbDic;

    @ApiModelProperty(value="公司状态字典值")
    private String gsztDic;

    @ApiModelProperty(value="黑名单时间")
    private Date hmdTime;

    @ApiModelProperty(value="重点关注时间")
    private Date zdgzTime;

    @ApiModelProperty(value="失信曝光时间")
    private Date sxbgTime;

    @ApiModelProperty(value="黑名单状态")
    private int hmdStatus;

    @ApiModelProperty(value="重点关注状态")
    private int zdgzStatus;

    @ApiModelProperty(value="失信曝光状态")
    private int sxbgStatus;


}
