package com.ys.zhslsgcgl.dto;

import com.ys.zhslsgcgl.entity.SgcRyglEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 项目管理-根据项目ID分页查询查询人员信息
 * @Author: tjy
 * @CreateDate: 2022/11/9 14:33
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class QueryPageBdryByXmidDto extends SgcRyglEntity {
    @ApiModelProperty(value = "标段名称")
    private String bdmc;

    @ApiModelProperty(value = "公司名称")
    private String gsmc;
}
