package com.ys.zhslsgcgl.dto;

import com.ys.zhslsgcgl.entity.SgcPfbzEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description: 评分扣分明细表返回参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/10 16:12
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class PfkfmxDto extends SgcPfbzEntity {
    @ApiModelProperty(value = "次数")
    private int cs;

    @ApiModelProperty(value = "记分单位字典值")
    private String jfdwDic;

    @ApiModelProperty(value = "类型字典值")
    private String lxDic;

    @ApiModelProperty(value = "性质分类字典值")
    private String xzflDic;
}
