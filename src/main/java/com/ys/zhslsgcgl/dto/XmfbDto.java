package com.ys.zhslsgcgl.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 项目分布返回参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/11 13:27
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class XmfbDto {
    @ApiModelProperty(value = "1000万以下项目")
    private int yqwyx;

    @ApiModelProperty(value = "1000万到2000万项目")
    private int yqwdlqw;

    @ApiModelProperty(value = "2000万到3000万项目")
    private int lqwdsqw;

    @ApiModelProperty(value = "3000万到4000万项目")
    private int sqwdsqw;

    @ApiModelProperty(value = "4000万以上项目")
    private int sqwys;
}
