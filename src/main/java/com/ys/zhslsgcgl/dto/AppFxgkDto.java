package com.ys.zhslsgcgl.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @Description: App查询巡检任务返回参数
 * @Author: chenxingjian
 * CreateDate: 2022/11/15 18:58
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class AppFxgkDto {
    @ApiModelProperty(value = "项目名称")
    private String xmmc;

    @ApiModelProperty(value = "标段名称")
    private String bdmc;

    @ApiModelProperty(value = "管控ID")
    private String gkid;

    @ApiModelProperty(value = "巡检任务")
    private String xjrw;

    @ApiModelProperty(value = "风险等级")
    private String fxdj;

    @ApiModelProperty(value = "部位")
    private String bw;

    @ApiModelProperty(value = "发布时间")
    private Date fbsj;

    @ApiModelProperty(value = "管控组织ID")
    private String gkzzid;

    @ApiModelProperty(value = "巡检任务ID")
    private String xjrwid;

    @ApiModelProperty(value = "任务周期")
    private String rwzq;
}
