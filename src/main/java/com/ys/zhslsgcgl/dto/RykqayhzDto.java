package com.ys.zhslsgcgl.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 员工考勤按月汇总记录
 * @Author: wugangzhi
 * @CreateData: 2022/11/25 13:02
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class RykqayhzDto {
    @ApiModelProperty("年月")
    private String ny;

    @ApiModelProperty("员工ID")
    private String ygid;

    @ApiModelProperty("员工姓名")
    private String ygxm;

    @ApiModelProperty("身份证号码")
    private String sfzhm;

    @ApiModelProperty("职位名称")
    private String zwDic;

    @ApiModelProperty("员工类型名称")
    private String yglxDic;

    @ApiModelProperty("考勤天数")
    private int kqts;

    @ApiModelProperty("到岗天数")
    private int dgts;

    @ApiModelProperty("考勤扣分")
    private double kfz;

    @ApiModelProperty("项目ID")
    private String xmid;

    @ApiModelProperty("项目名称")
    private String xmmc;

    @ApiModelProperty("标段名称")
    private String bdmc;
}
