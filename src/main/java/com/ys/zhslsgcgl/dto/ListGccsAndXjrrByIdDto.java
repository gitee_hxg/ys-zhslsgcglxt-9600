package com.ys.zhslsgcgl.dto;

import com.ys.zhslsgcgl.entity.SgcAqfxgkGkcsEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 项目管理-根据责任人id和巡检任务id查询管控措施和巡检任务
 * @Author: tjy
 * @CreateDate: 2022/11/15 14:33
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class ListGccsAndXjrrByIdDto extends SgcAqfxgkGkcsEntity {
    @ApiModelProperty(value = "0-未完成 1-完成")
    private String dwmcs;

}
