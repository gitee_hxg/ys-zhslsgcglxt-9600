package com.ys.zhslsgcgl.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Description: 人员列表返回Dto
 * @Author fsx
 * @CreateDate 2022/11/5 21:00
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Data
public class RyxxByDwfzDto {

    @ApiModelProperty(value = "单位id")
    private String dwid;

    @ApiModelProperty(value = "单位类别")
    private String dwlb;

    @ApiModelProperty(value = "单位名称")
    private String dwmc;

    @ApiModelProperty(value = "人员list")
    private List<PageRylbDto> ryxx;

}
