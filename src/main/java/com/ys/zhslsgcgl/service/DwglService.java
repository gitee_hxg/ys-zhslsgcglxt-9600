package com.ys.zhslsgcgl.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import com.ys.common.core.util.YsAssert;
import com.ys.middleground.start.util.UserUtils;
import com.ys.zhslsgcgl.bo.DwxxBo;
import com.ys.zhslsgcgl.dto.DwxxDto;
import com.ys.zhslsgcgl.entity.SgcDwglEntity;
import com.ys.zhslsgcgl.entity.SgcDwxxLsdfEntity;
import com.ys.zhslsgcgl.entity.SgcRyglEntity;
import com.ys.zhslsgcgl.mapper.SgcDwglMapper;
import com.ys.zhslsgcgl.mapper.SgcDwxxLsdfMapper;
import com.ys.zhslsgcgl.mapper.SgcRyglMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description: 市场监督-单位管理
 * @Author: wugangzhi
 * @CreateData: 2022/11/5 22:44
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Service
public class DwglService {

    /**
     * 单位管理mapper
     */
    @Autowired
    private SgcDwglMapper sgcDwglMapper;

    /**
     * 人员管理mapper
     */
    @Autowired
    private SgcRyglMapper sgcRyglMapper;

    /**
     * 单位最终得分
     */
    @Autowired
    private SgcDwxxLsdfMapper sgcDwxxLsdfMapper;


    /**
     * 单位信息下拉查询
     * @return
     */
    public List<Map<String,String>> listDwxx(){
        return sgcDwglMapper.listDwxx();
    }

    /**
     * 分页查询单位信息
     * @param dwxxBo
     * @return
     */
    public PageInfo queryPageDwxx(DwxxBo dwxxBo) {
        PageHelper.startPage(dwxxBo.getPageNum(),dwxxBo.getPageSize());
        List<DwxxDto> list = sgcDwglMapper.queryPageDwxx(dwxxBo);
        return new PageInfo(list);
    }

    /**
     *添加单位信息
     * @param sgcDwglEntity
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int addDwxx(SgcDwglEntity sgcDwglEntity) {
        //验证单位名称是否重复
        if(StringUtil.isNotEmpty(sgcDwglEntity.getDwmc())) {
            int count1 = sgcDwglMapper.selectCount(new QueryWrapper<SgcDwglEntity>().eq("DWMC", sgcDwglEntity.getDwmc()));
            YsAssert.isTrue(count1 < 1, "新增失败：单位名称已存在");
        }

        //设置操作人,操作时间
        sgcDwglEntity.setCreatedBy(UserUtils.getUserId());
        sgcDwglEntity.setCreatedTime(new Date());
        return sgcDwglMapper.insert(sgcDwglEntity);
    }

    /**
     * 修改单位信息
     * @param sgcDwglEntity
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateDwxx(SgcDwglEntity sgcDwglEntity) {
        //验证单位名称是否重复
        if(StringUtil.isNotEmpty(sgcDwglEntity.getDwmc())) {
            int count1 = sgcDwglMapper.selectCount(new QueryWrapper<SgcDwglEntity>().eq("DWMC", sgcDwglEntity.getDwmc()).ne("DWID", sgcDwglEntity.getDwid()));
            YsAssert.isTrue(count1 < 1, "修改失败：单位名称已存在");
        }

        
        //设置操作人,操作时间
        sgcDwglEntity.setUpdatedBy(UserUtils.getUserId());
        sgcDwglEntity.setUpdatedTime(new Date());
        return sgcDwglMapper.updateById(sgcDwglEntity);
    }

    /**
     * 根据单位ID删除单位信息
     * @param dwid
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int deleteDwxxByDwid(String dwid) {
        //判断单位是否绑定人员
        int count1 =  sgcRyglMapper.selectCount(new QueryWrapper<SgcRyglEntity>().eq("DWID",dwid));
        YsAssert.isTrue(count1<1,"删除失败：该单位已绑定人员信息,不允许删除");
        return sgcDwglMapper.deleteById(dwid);
    }

}
