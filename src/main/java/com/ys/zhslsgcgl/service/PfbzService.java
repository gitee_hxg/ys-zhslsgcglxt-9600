package com.ys.zhslsgcgl.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import com.ys.common.core.util.YsAssert;
import com.ys.middleground.start.util.UserUtils;
import com.ys.zhslsgcgl.bo.PfbzBo;
import com.ys.zhslsgcgl.dto.PagePfbzDto;
import com.ys.zhslsgcgl.entity.SgcPfbzEntity;
import com.ys.zhslsgcgl.entity.SgcPfbzFlbEntity;
import com.ys.zhslsgcgl.mapper.SgcPfbzFlbMapper;
import com.ys.zhslsgcgl.mapper.SgcPfbzMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @Description: 检查计分-评分标准管理
 * @Author: chenxingjian
 * CreateDate: 2022/11/8 10:32
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Service
public class PfbzService {
    /**
     * 评分标准表Mapper
     */
    @Autowired
    private SgcPfbzMapper sgcPfbzMapper;

    /**
     * 评分标准信息表Mapper
     */
    @Autowired
    private SgcPfbzFlbMapper sgcPfbzFlbMapper;


    /**
     * 分页查询评分标准
     * @param pfbzBo
     * @return
     */
    public PageInfo queryPagePfbz(PfbzBo pfbzBo){
        PageHelper.startPage(pfbzBo.getPageNum(), pfbzBo.getPageSize());
        List<PagePfbzDto> list = sgcPfbzMapper.queryPagePfbz(pfbzBo);
        return new PageInfo<>(list);
    }

    /**
     * 新增评分标准
     * @param sgcPfbzEntity
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int addPfbz(SgcPfbzEntity sgcPfbzEntity){
        //验证评分标准名称是否重复
        if(StringUtil.isNotEmpty(sgcPfbzEntity.getBt())){
            int count1 = sgcPfbzMapper.selectCount(new QueryWrapper<SgcPfbzEntity>().eq("BT", sgcPfbzEntity.getBt()));
            YsAssert.isTrue(count1 < 1, "新增失败：评分标准已存在");
        }
        //设置操作人、操作时间
        sgcPfbzEntity.setCreatedBy(UserUtils.getUserId());
        sgcPfbzEntity.setCreatedTime(new Date());
        return sgcPfbzMapper.insert(sgcPfbzEntity);
    }

    /**
     * 根据评分标准id删除评分标准
     * @param pfbzid
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int removePfbz(String pfbzid){
        return sgcPfbzMapper.deleteById(pfbzid);
    }

    /**
     * 修改评分标准
     * @param sgcPfbzEntity
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int updatePfbz(SgcPfbzEntity sgcPfbzEntity){
        //验证评分标准名称是否重复
        if(StringUtil.isNotEmpty(sgcPfbzEntity.getBt())){
            int count1 = sgcPfbzMapper.selectCount(new QueryWrapper<SgcPfbzEntity>().eq("BT", sgcPfbzEntity.getBt()).ne("PFBZID", sgcPfbzEntity.getPfbzid()));
            YsAssert.isTrue(count1 < 1, "新增失败：评分标准已存在");
        }
        //设置操作人、操作时间
        sgcPfbzEntity.setUpdatedBy(UserUtils.getUserId());
        sgcPfbzEntity.setUpdatedTime(new Date());
        return sgcPfbzMapper.updateById(sgcPfbzEntity);
    }

    /**
     * 大分类-详细分类查询
     * @param flid
     * @return
     */
    public List<SgcPfbzFlbEntity> listDflorXfl(String flid){
        List<SgcPfbzFlbEntity> list = new ArrayList<>();

        //当未选择大分类时，只查询大分类
        if(StringUtil.isEmpty(flid)){
            flid = "0";
            list = sgcPfbzFlbMapper.listDflXxfl(flid);
        }
        //选择大分类后查询大分类下的详细分类结果
        else {
            list= sgcPfbzFlbMapper.listDflXxfl(flid);
        }
        return list;
    }
}
