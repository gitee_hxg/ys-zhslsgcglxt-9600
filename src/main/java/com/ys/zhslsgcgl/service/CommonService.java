package com.ys.zhslsgcgl.service;


import com.ys.zhslsgcgl.mapper.CommonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description:公共Service
 * @Author: wugangzhi
 * @CreateDate: 2022/11/6 15:43
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Service
public class CommonService {
    @Autowired
    private CommonMapper commonMapper;


    /**
     * 查询字典
     * @param tableParas
     * @return
     */
    public Map<Object, List<Map<String, Object>>> listByDicCode(String tableParas) {
        Map<Object, List<Map<String, Object>>> result=new HashMap<>();
        List<Map<String, Object>> mapList=commonMapper.listZdByParam(tableParas);
        if(!CollectionUtils.isEmpty(mapList)){
            result= mapList.stream().collect(Collectors.groupingBy(i ->i.get("tablePara")));
        }
        return result;
    }

    /**
     * 根据角色id查询用户
     *
     * @param jsid
     * @return
     */
    public List<Map<String,String>> listYhByJsid(String jsid) {
        List<Map<String,String>> maps = commonMapper.selectYhByJsid(jsid);
        return maps;
    }
}
