package com.ys.zhslsgcgl.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ys.common.core.dto.R;
import com.ys.common.core.util.UUIDUtil;
import com.ys.middleground.start.util.UserUtils;
import com.ys.zhslsgcgl.bo.AddXmBdxxBo;
import com.ys.zhslsgcgl.bo.XmjwdBo;
import com.ys.zhslsgcgl.bo.XmxxglBo;
import com.ys.zhslsgcgl.dto.*;
import com.ys.zhslsgcgl.entity.*;
import com.ys.zhslsgcgl.mapper.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.netsdk.utils.ReadExcel.getCellValue;

@Service
public class XmglService {
    /**
     * 项目信息Mapper
     */
    @Autowired
    SgcXmxxMapper sgcXmxxMapper;

    /**
     * 项目标段Mapper
     */
    @Autowired
    SgcXmbdMapper sgcXmbdMapper;

    /**
     * 项目标段中标单位Mapper
     */
    @Autowired
    SgcBdzbdwGlbMapper sgcBdzbdwGlbMapper;

    /**
     * 单位Mapper
     */
    @Autowired
    SgcDwglMapper sgcDwglMapper;

    /**
     * 标段人员关联Mapper
     */
    //@Autowired
    //SgBdryGlbMapper sgBdryGlbMapper;

    /**
     * 人员Mapper
     */
    @Autowired
    SgcRyglMapper sgcRyglMapper;

    @Autowired
    SgcZdgzMapper sgcZdgzMapper;

    @Autowired
    SgcAqfxgkMapper sgcAqfxgkMapper;

    @Autowired
    SgcAqfxgkGkcsMapper sgcAqfxgkGkcsMapper;

    @Autowired
    SgcAqfxgkZzxxMapper sgcAqfxgkZzxxMapper;

    @Autowired
    SgcAqfxgkZrryMapper sgcAqfxgkZrryMapper;

    /**
     * 分页查询项目信息
     *
     * @param xmxxglBo
     * @return
     */
    public PageInfo<List<QueryPageXmxxDto>> queryPageXmxx(XmxxglBo xmxxglBo) {
        Page<Object> objects = PageHelper.startPage(xmxxglBo.getPageNum(), xmxxglBo.getPageSize());
        List<SgcXmxxEntity> sgcXmxxEntities = sgcXmxxMapper.selectList(new LambdaQueryWrapper<SgcXmxxEntity>().
                likeRight(StringUtils.isNoneBlank(xmxxglBo.getXzqh()), SgcXmxxEntity::getXzqhdm, xmxxglBo.getXzqh())
                .likeRight(StringUtils.isNoneBlank(xmxxglBo.getXmmc()), SgcXmxxEntity::getXmmc, xmxxglBo.getXmmc())
                .eq(StringUtils.isNoneBlank(xmxxglBo.getXmzt()), SgcXmxxEntity::getXmzt, xmxxglBo.getXmzt())
                .orderByDesc(SgcXmxxEntity::getKgrq));
        long total = objects.getTotal();
        //字典值转换
        List<QueryPageXmxxDto> queryPageXmxxDtos = handleSgcXmxxEntity(sgcXmxxEntities);
        PageInfo pageInfo = new PageInfo(queryPageXmxxDtos);
        pageInfo.setTotal(total);
        return pageInfo;
    }

    /**
     * 字典值转换
     *
     * @param sgcXmxxEntities
     * @return
     */
    private List<QueryPageXmxxDto> handleSgcXmxxEntity(List<SgcXmxxEntity> sgcXmxxEntities) {
        ArrayList<QueryPageXmxxDto> queryPageXmxxDtos = new ArrayList<>();
        for (SgcXmxxEntity sgcXmxxEntity : sgcXmxxEntities) {
            QueryPageXmxxDto queryPageXmxxDto = new QueryPageXmxxDto();
            BeanUtils.copyProperties(sgcXmxxEntity, queryPageXmxxDto);
            String xmlx = sgcXmxxEntity.getXmlx();
            //查询项目类型字典值
            queryPageXmxxDto.setXmlxzdz(sgcXmxxMapper.selectXmlxzdz(xmlx));
            //查询项目状态字典值
            //项目状态(0-在建工程 1-完工待验收 2-完工已验收)
            String xmzt = sgcXmxxEntity.getXmzt();
            if ("0".equals(xmzt)) {
                queryPageXmxxDto.setXmztzdz("在建工程");
            }
            if ("1".equals(xmzt)) {
                queryPageXmxxDto.setXmztzdz("完工待验收");
            }
            if ("2".equals(xmzt)) {
                queryPageXmxxDto.setXmztzdz("完工已验收");
            }
            queryPageXmxxDtos.add(queryPageXmxxDto);

            //查询行政区划类型字典值
            String xzqh = sgcXmxxEntity.getXzqhdm();
            queryPageXmxxDto.setXzqhdmzdz(sgcXmxxMapper.selectXzqhxzdz(xzqh));
        }
        return queryPageXmxxDtos;
    }

    /**
     * 编辑项目经纬度
     *
     * @param xmjwdBo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateXmjwd(XmjwdBo xmjwdBo) {
        SgcXmxxEntity sgcXmxxEntity = new SgcXmxxEntity();
        BeanUtils.copyProperties(xmjwdBo, sgcXmxxEntity);
        return sgcXmxxMapper.updateById(sgcXmxxEntity);
    }

    /**
     * 编辑项目信息
     *
     * @param sgcXmxxEntity
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateXmxx(SgcXmxxEntity sgcXmxxEntity) {
        return sgcXmxxMapper.updateById(sgcXmxxEntity);
    }

    /**
     * 新增项目信息
     *
     * @param sgcXmxxEntity
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int addXmxx(SgcXmxxEntity sgcXmxxEntity) {
        //sgcXmxxEntity.setXmid(UUIDUtil.uuid32());
        sgcXmxxEntity.setCreatedBy(UserUtils.getUserName());
        sgcXmxxEntity.setCreatedTime(new Date());
        return sgcXmxxMapper.insert(sgcXmxxEntity);
    }

    /**
     * 根据项目ID删除项目信息
     *
     * @param xmid
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public R deleteXmxxByXmid(String xmid) {
        List<SgcXmbdEntity> sgcXmbdEntities = sgcXmbdMapper.selectList(new LambdaQueryWrapper<SgcXmbdEntity>().eq(SgcXmbdEntity::getXmid, xmid));
        if (CollectionUtils.isNotEmpty(sgcXmbdEntities)) {
            return R.error("该项目存在标段信息,不能删除");
        }
        return R.ok(sgcXmxxMapper.deleteById(xmid));
    }

    /**
     * 根据项目ID查询标段和单位信息
     *
     * @param xmid
     * @return
     */
    public List<ListBdxxByXmidDto> listBdxxByXmid(String xmid) {
        //根据项目id查询标段
        List<SgcXmbdEntity> sgcXmbdEntities = sgcXmbdMapper.selectList(new LambdaQueryWrapper<SgcXmbdEntity>().eq(SgcXmbdEntity::getXmid, xmid));

        //根据项目id查询项目信息
        SgcXmxxEntity sgcXmxxEntity = sgcXmxxMapper.selectById(xmid);

        ArrayList<ListBdxxByXmidDto> objects = new ArrayList<>();
        //根据标段id查询标段对应公司
        for (SgcXmbdEntity sgcXmbdEntity : sgcXmbdEntities) {
            List<SgcBdzbdwGlbEntity> sgcBdzbdwGlbEntities = sgcBdzbdwGlbMapper.selectList(new LambdaQueryWrapper<SgcBdzbdwGlbEntity>().
                    eq(SgcBdzbdwGlbEntity::getBdid, sgcXmbdEntity.getBdid()).orderByDesc(SgcBdzbdwGlbEntity::getCreatedTime));

            if (CollectionUtils.isNotEmpty(sgcBdzbdwGlbEntities)) {
                for (SgcBdzbdwGlbEntity sgcBdzbdwGlbEntity : sgcBdzbdwGlbEntities) {
                    SgcDwglEntity sgcDwglEntity = sgcDwglMapper.selectOne(new LambdaQueryWrapper<SgcDwglEntity>().eq(SgcDwglEntity::getDwid, sgcBdzbdwGlbEntity.getDwid()).orderByDesc(SgcDwglEntity::getCreatedTime));
                    ListBdxxByXmidDto listBdxxByXmidDto = new ListBdxxByXmidDto();
                    BeanUtils.copyProperties(sgcDwglEntity, listBdxxByXmidDto);
                    listBdxxByXmidDto.setFrdb(sgcBdzbdwGlbEntity.getFrdb());
                    listBdxxByXmidDto.setLxdh(sgcBdzbdwGlbEntity.getLxdh());
                    listBdxxByXmidDto.setBdid(sgcXmbdEntity.getBdid());
                    listBdxxByXmidDto.setBdmc(sgcXmbdEntity.getBdmc());
                    listBdxxByXmidDto.setBdje(sgcXmbdEntity.getBdje());
                    listBdxxByXmidDto.setJszt(sgcXmbdEntity.getJszt());
                    listBdxxByXmidDto.setXmzt(sgcXmxxEntity.getXmzt());
                    objects.add(listBdxxByXmidDto);
                }
            }
        }
        return objects;
    }

    /**
     * 根据项目ID删除标段信息
     *
     * @param xmid
     * @return
     */
    public int deleteBdxxByXmid(String xmid) {
        return sgcXmbdMapper.delete(new LambdaQueryWrapper<SgcXmbdEntity>().eq(SgcXmbdEntity::getXmid, xmid));
    }

    /**
     * 新增项目标段信息
     *
     * @param sgcXmbdEntity
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int addXmBdxx(AddXmBdxxBo sgcXmbdEntity) {
        //添加中间表
        String bdid = UUIDUtil.uuid32();
        List<SgcBdzbdwGlbEntity> bdzddwref = sgcXmbdEntity.getBdzddwref();

        for (SgcBdzbdwGlbEntity sgcBdzbdwGlbEntityref : bdzddwref) {
            SgcBdzbdwGlbEntity sgcBdzbdwGlbEntity = new SgcBdzbdwGlbEntity();
            sgcBdzbdwGlbEntity.setId(UUIDUtil.uuid32());
            sgcBdzbdwGlbEntity.setBdid(bdid);
            sgcBdzbdwGlbEntity.setDwid(sgcBdzbdwGlbEntityref.getDwid());
            sgcBdzbdwGlbEntity.setFrdb(sgcBdzbdwGlbEntityref.getFrdb());
            sgcBdzbdwGlbEntity.setLxdh(sgcBdzbdwGlbEntityref.getLxdh());
            sgcBdzbdwGlbEntity.setCreatedBy(UserUtils.getUserName());
            sgcBdzbdwGlbEntity.setCreatedTime(new Date());
            sgcBdzbdwGlbMapper.insert(sgcBdzbdwGlbEntity);
        }

        //添加标段表
        SgcXmbdEntity sgcXmbdEntity1 = new SgcXmbdEntity();
        BeanUtils.copyProperties(sgcXmbdEntity, sgcXmbdEntity1);
        sgcXmbdEntity1.setBdid(bdid);
        sgcXmbdEntity1.setCreatedBy(UserUtils.getUserName());
        sgcXmbdEntity1.setCreatedTime(new Date());
        return sgcXmbdMapper.insert(sgcXmbdEntity1);
    }

    /**
     * 根据项目ID查询项目信息
     *
     * @param xmid
     * @return
     */
    public SgcXmxxEntity listXmxxByXmid(String xmid) {
        return sgcXmxxMapper.selectById(xmid);
    }


    /**
     * 根据标段ID查询单位信息
     *
     * @param bdid
     * @return
     */
    public List<SgcDwglEntity> listDwxxByBdid(String bdid) {
        List<SgcBdzbdwGlbEntity> sgcBdzbdwGlbEntities = sgcBdzbdwGlbMapper.selectList(new LambdaQueryWrapper<SgcBdzbdwGlbEntity>()
                .eq(SgcBdzbdwGlbEntity::getBdid, bdid).orderByDesc(SgcBdzbdwGlbEntity::getCreatedTime));
        ArrayList<SgcDwglEntity> sgcDwglEntities = new ArrayList<>();

        for (SgcBdzbdwGlbEntity sgcBdzbdwGlbEntity : sgcBdzbdwGlbEntities) {
            String dwid = sgcBdzbdwGlbEntity.getDwid();
            SgcDwglEntity sgcDwglEntity = sgcDwglMapper.selectById(dwid);
            sgcDwglEntities.add(sgcDwglEntity);

        }

        return sgcDwglEntities;
    }

    /**
     * 编辑单位信息
     *
     * @param sgcDwglEntity
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateDwxx(SgcDwglEntity sgcDwglEntity) {
        return sgcDwglMapper.updateById(sgcDwglEntity);
    }

    /**
     * 根据标段ID删除标段信息
     *
     * @param bdid
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public R deleteBdxxByBdid(String bdid) {
        //查询标段对应人员
        List<SgcRyglEntity> sgcRyglEntities = sgcRyglMapper.selectList(new LambdaQueryWrapper<SgcRyglEntity>().eq(SgcRyglEntity::getBdid, bdid));
        if (CollectionUtils.isNotEmpty(sgcRyglEntities)) {
            return R.error("该标段已绑定人员,不能删除");
        }
        //查询标段对应单位
        //List<SgcBdzbdwGlbEntity> sgcBdzbdwGlbEntities = sgcBdzbdwGlbMapper.selectList(new LambdaQueryWrapper<SgcBdzbdwGlbEntity>().eq(SgcBdzbdwGlbEntity::getBdid, bdid));
        //if (CollectionUtils.isNotEmpty(sgcBdzbdwGlbEntities)) {
        //    return R.error("该标段已绑定单位,不能删除");
        //}
        sgcBdzbdwGlbMapper.delete(new LambdaQueryWrapper<SgcBdzbdwGlbEntity>().eq(SgcBdzbdwGlbEntity::getBdid, bdid));
        return R.ok(sgcXmbdMapper.deleteById(bdid));

    }


    /**
     * 根据单位ID删除单位信息
     *
     * @param dwid
     * @return
     */
    public R deleteDwxxByBdid(String dwid) {
        //List<SgcBdzbdwGlbEntity> sgcBdzbdwGlbEntities = sgcBdzbdwGlbMapper.selectList(new LambdaQueryWrapper<SgcBdzbdwGlbEntity>().eq(SgcBdzbdwGlbEntity::getDwid, dwid));
        //if (CollectionUtils.isNotEmpty(sgcBdzbdwGlbEntities)) {
        //    R.error("该单位已绑定标段");
        //}
        return R.ok(sgcDwglMapper.deleteById(dwid));
    }

    /**
     * 编辑标段信息
     *
     * @param sgcXmbdEntity
     * @return
     */
    public R updateBdxx(AddXmBdxxBo sgcXmbdEntity) {
        //要修改标段单位中间表,先删除标段单位中间表
        sgcBdzbdwGlbMapper.delete(new LambdaQueryWrapper<SgcBdzbdwGlbEntity>().
                eq(SgcBdzbdwGlbEntity::getBdid, sgcXmbdEntity.getBdid()));

        //再添加标段单位中间表
        List<SgcBdzbdwGlbEntity> bdzddwref = sgcXmbdEntity.getBdzddwref();
        for (SgcBdzbdwGlbEntity sgcBdzbdwGlbEntityref : bdzddwref) {
            SgcBdzbdwGlbEntity sgcBdzbdwGlbEntity = new SgcBdzbdwGlbEntity();
            sgcBdzbdwGlbEntity.setId(UUIDUtil.uuid32());
            sgcBdzbdwGlbEntity.setBdid(sgcXmbdEntity.getBdid());
            sgcBdzbdwGlbEntity.setDwid(sgcBdzbdwGlbEntityref.getDwid());
            sgcBdzbdwGlbEntity.setFrdb(sgcBdzbdwGlbEntityref.getFrdb());
            sgcBdzbdwGlbEntity.setLxdh(sgcBdzbdwGlbEntityref.getLxdh());
            sgcBdzbdwGlbMapper.insert(sgcBdzbdwGlbEntity);
        }

        //修改标段表
        sgcXmbdEntity.setUpdatedBy(UserUtils.getUserName());
        sgcXmbdEntity.setUpdatedTime(new Date());
        return R.ok(sgcXmbdMapper.updateById(sgcXmbdEntity));
    }

    /**
     * 根据名称和状态模糊查询项目信息
     *
     * @param xmmc
     * @param xmzt
     * @return
     */
    public ListXmxxByXmmcZtDto listXmxxByXmmcZt(String xmmc, String xmzt) {
        //查询数据
        List<SgcXmxxEntity> sgcXmxxEntities = sgcXmxxMapper.selectList(new LambdaQueryWrapper<SgcXmxxEntity>().eq(SgcXmxxEntity::getXmzt, xmzt)
                .likeRight(StringUtils.isNotBlank(xmmc), SgcXmxxEntity::getXmmc, xmmc));

        //进行统计
        ListXmxxByXmmcZtDto listXmxxByXmmcZtDto = new ListXmxxByXmmcZtDto();
        Integer zs = sgcXmxxMapper.selectCount(new LambdaQueryWrapper<SgcXmxxEntity>());
        Integer dqzts = 0;
        if (CollectionUtils.isNotEmpty(sgcXmxxEntities)) {
            dqzts = sgcXmxxEntities.size();
        }
        listXmxxByXmmcZtDto.setZs(String.valueOf(zs));
        listXmxxByXmmcZtDto.setSgcXmxxEntitys(sgcXmxxEntities);
        listXmxxByXmmcZtDto.setDqztxms(String.valueOf(dqzts));

        return listXmxxByXmmcZtDto;
    }

    /**
     * 根据项目ID查询项目信息和投资信息
     *
     * @param xmid
     * @return
     */
    public ListXmxxAndTzByXmidDto listXmxxAndTzByXmid(String xmid) {
        //查询项目信息
        SgcXmxxEntity sgcXmxxEntity = sgcXmxxMapper.selectOne(new LambdaQueryWrapper<SgcXmxxEntity>().eq(SgcXmxxEntity::getXmid, xmid));
        //返回结果
        ListXmxxAndTzByXmidDto listXmxxAndTzByXmidDto = new ListXmxxAndTzByXmidDto();
        if (!ObjectUtils.isEmpty(sgcXmxxEntity)) {
            BeanUtils.copyProperties(sgcXmxxEntity, listXmxxAndTzByXmidDto);
            List<SgcXmbdEntity> sgcXmbdEntities = sgcXmbdMapper.selectList(new LambdaQueryWrapper<SgcXmbdEntity>().eq(SgcXmbdEntity::getXmid, sgcXmxxEntity.getXmid()));

            //计算金额
            BigDecimal totalAmount = new BigDecimal(0);
            for (SgcXmbdEntity sgcXmbdEntity : sgcXmbdEntities) {
                totalAmount = totalAmount.add(sgcXmbdEntity.getBdje());
            }
            listXmxxAndTzByXmidDto.setSjtzje(totalAmount);
        }
        return listXmxxAndTzByXmidDto;
    }


    public void downAqb() throws IOException, InvalidFormatException {
        File file2 = new File("H:\\biao\\水工程建设系统-项目标段人员信息收集（宜昌市东风渠灌区续建配套与现代化改造一期项目）1115.xlsx");

        Workbook workbook2 = new XSSFWorkbook(file2);

        //ArrayList<String> strings = new ArrayList<>();

        Sheet sheet = workbook2.getSheetAt(3);
//遍历行
        for (int i = 5; i < sheet.getLastRowNum() + 1; i++) {
//                rowNum = i;
            Row row = sheet.getRow(i);
            if (row == null) {
                continue;
            }
            //主表
            SgcAqfxgkEntity sgcAqfxgkEntity = new SgcAqfxgkEntity();
            String zbid = UUIDUtil.uuid32();
            sgcAqfxgkEntity.setGkid(zbid);
            Cell cell1 = row.getCell(1);
            String cellzb1 = getCellValue(cell1);
            sgcAqfxgkEntity.setWxylb(cellzb1);

            Cell cell2 = row.getCell(2);
            String cellzb2 = getCellValue(cell2);
            sgcAqfxgkEntity.setWxxm(cellzb2);

            //Cell cell3 = row.getCell(3);
            //String cellzb3 = getCellValue(cell3);
            //sgcAqfxgkEntity.setKndzsglx(cellzb3);


            Cell cell4 = row.getCell(4);
            String cellzb4 = getCellValue(cell4);
            if (StringUtils.isNotBlank(cellzb4)) {
                sgcAqfxgkEntity.setWxyjb(cellzb4);
            } else {
                Cell cell5 = row.getCell(5);
                String cellzb5 = getCellValue(cell5);
                sgcAqfxgkEntity.setWxyjb(cellzb5);
            }

            Cell cell6 = row.getCell(6);
            String cellzb6 = getCellValue(cell6);
            sgcAqfxgkEntity.setBw(cellzb6);

            //Cell cell6 = row.getCell(6);
            //String cellzb6 = getCellValue(cell6);
            //sgcAqfxgkEntity.setKndzsglx(cellzb6);

            Cell cell7 = row.getCell(7);
            String cellzb7 = getCellValue(cell7);
            sgcAqfxgkEntity.setKndzsglx(cellzb7);

            Cell cell8 = row.getCell(8);
            String cellzb8 = getCellValue(cell8);
            sgcAqfxgkEntity.setFxdj(cellzb8);

            sgcAqfxgkEntity.setCreatedBy("admin");
            sgcAqfxgkEntity.setCreatedTime(new Date());

            //TODO
            //sgcAqfxgkEntity.setXmid();
            sgcAqfxgkMapper.insert(sgcAqfxgkEntity);

            SgcAqfxgkZzxxEntity sgcAqfxgkZzxxEntityj = new SgcAqfxgkZzxxEntity();
            String xmfr = UUIDUtil.uuid32();
            sgcAqfxgkZzxxEntityj.setGkzzid(xmfr);
            sgcAqfxgkZzxxEntityj.setZzlxid("032e8f2fzv554ab0a6ddd0cfbf83f102");
            //sgcAqfxgkZzxxEntityj.setGkzzpid("0");
            //sgcAqfxgkZzxxEntityj.setXmid("cscscscs");
            //sgcAqfxgkZzxxEntityj.setJb(1);
            //sgcAqfxgkZzxxEntityj.setPxz(1);
            sgcAqfxgkZzxxEntityj.setGkid(zbid);
            sgcAqfxgkZzxxEntityj.setCreatedBy("admin");
            sgcAqfxgkZzxxEntityj.setCreatedTime(new Date());
            sgcAqfxgkZzxxMapper.insert(sgcAqfxgkZzxxEntityj);
            //sgcAqfxgkZzxxEntityj.setUpdatedBy();
            //sgcAqfxgkZzxxEntityj.setUpdatedTime();

            SgcAqfxgkZzxxEntity sgcAqfxgkZzxxEntityc = new SgcAqfxgkZzxxEntity();
            String jldw = UUIDUtil.uuid32();
            sgcAqfxgkZzxxEntityc.setGkzzid(jldw);
            sgcAqfxgkZzxxEntityc.setZzlxid("032e8f2fzv554ab0a6ddd0cfbf83f103");
            //sgcAqfxgkZzxxEntityc.setGkzzpid("0");
            //sgcAqfxgkZzxxEntityc.setXmid("cscscscs");
            //sgcAqfxgkZzxxEntityc.setJb(1);
            //sgcAqfxgkZzxxEntityc.setPxz(2);
            sgcAqfxgkZzxxEntityc.setGkid(zbid);
            sgcAqfxgkZzxxEntityc.setCreatedBy("admin");
            sgcAqfxgkZzxxEntityc.setCreatedTime(new Date());
            sgcAqfxgkZzxxMapper.insert(sgcAqfxgkZzxxEntityc);

            SgcAqfxgkZzxxEntity sgcAqfxgkZzxxEntityzyfzr = new SgcAqfxgkZzxxEntity();
            String sgdw = UUIDUtil.uuid32();
            sgcAqfxgkZzxxEntityzyfzr.setGkzzid(sgdw);
            sgcAqfxgkZzxxEntityzyfzr.setZzlxid("032e8f2fzv554ab0a6ddd0cfbf83f104");
            //sgcAqfxgkZzxxEntityzyfzr.setGkzzpid(gljid);
            //sgcAqfxgkZzxxEntityzyfzr.setXmid("cscscscs");
            //sgcAqfxgkZzxxEntityzyfzr.setJb(2);
            //sgcAqfxgkZzxxEntityzyfzr.setPxz(1);
            sgcAqfxgkZzxxEntityzyfzr.setGkid(zbid);
            sgcAqfxgkZzxxEntityzyfzr.setCreatedBy("admin");
            sgcAqfxgkZzxxEntityzyfzr.setCreatedTime(new Date());
            sgcAqfxgkZzxxMapper.insert(sgcAqfxgkZzxxEntityzyfzr);

            SgcAqfxgkZzxxEntity sgcAqfxgkZzxxEntityzyfzr2 = new SgcAqfxgkZzxxEntity();
            String fddbr = UUIDUtil.uuid32();
            sgcAqfxgkZzxxEntityzyfzr2.setGkzzid(fddbr);
            sgcAqfxgkZzxxEntityzyfzr2.setZzlxid("032e8f2fzv554ab0a6ddd0cfbf83f113");
            //sgcAqfxgkZzxxEntityzyfzr2.setGkzzpid(gljid);
            //sgcAqfxgkZzxxEntityzyfzr2.setXmid("cscscscs");
            //sgcAqfxgkZzxxEntityzyfzr2.setJb(2);
            //sgcAqfxgkZzxxEntityzyfzr2.setPxz(2);
            sgcAqfxgkZzxxEntityzyfzr2.setGkid(zbid);
            sgcAqfxgkZzxxEntityzyfzr2.setCreatedBy("admin");
            sgcAqfxgkZzxxEntityzyfzr2.setCreatedTime(new Date());
            sgcAqfxgkZzxxMapper.insert(sgcAqfxgkZzxxEntityzyfzr2);

            SgcAqfxgkZzxxEntity sgcAqfxgkZzxxEntityzyfzr3 = new SgcAqfxgkZzxxEntity();
            String xcfzr = UUIDUtil.uuid32();
            sgcAqfxgkZzxxEntityzyfzr3.setGkzzid(xcfzr);
            sgcAqfxgkZzxxEntityzyfzr3.setZzlxid("032e8f2fzv554ab0a6ddd0cfbf83f114");
            //sgcAqfxgkZzxxEntityzyfzr3.setGkzzpid(gljid);
            //sgcAqfxgkZzxxEntityzyfzr3.setXmid("cscscscs");
            //sgcAqfxgkZzxxEntityzyfzr3.setJb(2);
            //sgcAqfxgkZzxxEntityzyfzr3.setPxz(3);
            sgcAqfxgkZzxxEntityzyfzr3.setGkid(zbid);
            sgcAqfxgkZzxxEntityzyfzr3.setCreatedBy("admin");
            sgcAqfxgkZzxxEntityzyfzr3.setCreatedTime(new Date());
            sgcAqfxgkZzxxMapper.insert(sgcAqfxgkZzxxEntityzyfzr3);

            SgcAqfxgkZzxxEntity sgcAqfxgkZzxxEntityzyfzr4 = new SgcAqfxgkZzxxEntity();
            String aqglry = UUIDUtil.uuid32();
            sgcAqfxgkZzxxEntityzyfzr4.setGkzzid(aqglry);
            sgcAqfxgkZzxxEntityzyfzr4.setZzlxid("032e8f2fzv554ab0a6ddd0cfbf83f115");
            //sgcAqfxgkZzxxEntityzyfzr4.setGkzzpid(gljid);
            //sgcAqfxgkZzxxEntityzyfzr4.setXmid("cscscscs");
            //sgcAqfxgkZzxxEntityzyfzr4.setJb(2);
            //sgcAqfxgkZzxxEntityzyfzr4.setPxz(4);
            sgcAqfxgkZzxxEntityzyfzr4.setGkid(zbid);
            sgcAqfxgkZzxxEntityzyfzr4.setCreatedBy("admin");
            sgcAqfxgkZzxxEntityzyfzr4.setCreatedTime(new Date());
            sgcAqfxgkZzxxMapper.insert(sgcAqfxgkZzxxEntityzyfzr4);

            SgcAqfxgkZzxxEntity sgcAqfxgkZzxxEntityzyfzr5 = new SgcAqfxgkZzxxEntity();
            String zjlgcs = UUIDUtil.uuid32();
            sgcAqfxgkZzxxEntityzyfzr5.setGkzzid(zjlgcs);
            sgcAqfxgkZzxxEntityzyfzr5.setZzlxid("032e8f2fzv554ab0a6ddd0cfbf83f117");
            //sgcAqfxgkZzxxEntityzyfzr5.setGkzzpid(gljid);
            //sgcAqfxgkZzxxEntityzyfzr5.setXmid("cscscscs");
            //sgcAqfxgkZzxxEntityzyfzr5.setJb(2);
            //sgcAqfxgkZzxxEntityzyfzr5.setPxz(5);
            sgcAqfxgkZzxxEntityzyfzr5.setGkid(zbid);
            sgcAqfxgkZzxxEntityzyfzr5.setCreatedBy("admin");
            sgcAqfxgkZzxxEntityzyfzr5.setCreatedTime(new Date());
            sgcAqfxgkZzxxMapper.insert(sgcAqfxgkZzxxEntityzyfzr5);

            SgcAqfxgkZzxxEntity sgcAqfxgkZzxxEntityzyfzr6 = new SgcAqfxgkZzxxEntity();
            String jlgcs = UUIDUtil.uuid32();
            sgcAqfxgkZzxxEntityzyfzr6.setGkzzid(jlgcs);
            sgcAqfxgkZzxxEntityzyfzr6.setZzlxid("032e8f2fzv554ab0a6ddd0cfbf83f118");
            //sgcAqfxgkZzxxEntityzyfzr6.setGkzzpid(glcid);
            //sgcAqfxgkZzxxEntityzyfzr6.setXmid("cscscscs");
            //sgcAqfxgkZzxxEntityzyfzr6.setJb(2);
            //sgcAqfxgkZzxxEntityzyfzr6.setPxz(1);
            sgcAqfxgkZzxxEntityzyfzr6.setGkid(zbid);
            sgcAqfxgkZzxxEntityzyfzr6.setCreatedBy("admin");
            sgcAqfxgkZzxxEntityzyfzr6.setCreatedTime(new Date());
            sgcAqfxgkZzxxMapper.insert(sgcAqfxgkZzxxEntityzyfzr6);

            SgcAqfxgkZzxxEntity sgcAqfxgkZzxxEntityzyfzr7 = new SgcAqfxgkZzxxEntity();
            String xmjl = UUIDUtil.uuid32();
            sgcAqfxgkZzxxEntityzyfzr7.setGkzzid(xmjl);
            sgcAqfxgkZzxxEntityzyfzr7.setZzlxid("032e8f2fzv554ab0a6ddd0cfbf83f120");
            //sgcAqfxgkZzxxEntityzyfzr7.setGkzzpid(glcid);
            //sgcAqfxgkZzxxEntityzyfzr7.setXmid("cscscscs");
            //sgcAqfxgkZzxxEntityzyfzr7.setJb(2);
            //sgcAqfxgkZzxxEntityzyfzr7.setPxz(2);
            sgcAqfxgkZzxxEntityzyfzr7.setGkid(zbid);
            sgcAqfxgkZzxxEntityzyfzr7.setCreatedBy("admin");
            sgcAqfxgkZzxxEntityzyfzr7.setCreatedTime(new Date());
            sgcAqfxgkZzxxMapper.insert(sgcAqfxgkZzxxEntityzyfzr7);

            SgcAqfxgkZzxxEntity sgcAqfxgkZzxxEntityzyfzr8 = new SgcAqfxgkZzxxEntity();
            String aqy = UUIDUtil.uuid32();
            sgcAqfxgkZzxxEntityzyfzr8.setGkzzid(aqy);
            sgcAqfxgkZzxxEntityzyfzr8.setZzlxid("032e8f2fzv554ab0a6ddd0cfbf83f121");
            //sgcAqfxgkZzxxEntityzyfzr8.setGkzzpid(glcid);
            //sgcAqfxgkZzxxEntityzyfzr8.setXmid("cscscscs");
            //sgcAqfxgkZzxxEntityzyfzr8.setJb(2);
            //sgcAqfxgkZzxxEntityzyfzr8.setPxz(3);
            sgcAqfxgkZzxxEntityzyfzr8.setGkid(zbid);
            sgcAqfxgkZzxxEntityzyfzr8.setCreatedBy("admin");
            sgcAqfxgkZzxxEntityzyfzr8.setCreatedTime(new Date());
            sgcAqfxgkZzxxMapper.insert(sgcAqfxgkZzxxEntityzyfzr8);

            SgcAqfxgkZzxxEntity sgcAqfxgkZzxxEntityzyfzr9 = new SgcAqfxgkZzxxEntity();
            String zyry = UUIDUtil.uuid32();
            sgcAqfxgkZzxxEntityzyfzr9.setGkzzid(zyry);
            sgcAqfxgkZzxxEntityzyfzr9.setZzlxid("032e8f2fzv554ab0a6ddd0cfbf83f123");
            //sgcAqfxgkZzxxEntityzyfzr8.setGkzzpid(glcid);
            //sgcAqfxgkZzxxEntityzyfzr9.setXmid("cscscscs");
            //sgcAqfxgkZzxxEntityzyfzr8.setJb(2);
            //sgcAqfxgkZzxxEntityzyfzr8.setPxz(3);
            sgcAqfxgkZzxxEntityzyfzr9.setGkid(zbid);
            sgcAqfxgkZzxxEntityzyfzr9.setCreatedBy("admin");
            sgcAqfxgkZzxxEntityzyfzr9.setCreatedTime(new Date());
            sgcAqfxgkZzxxMapper.insert(sgcAqfxgkZzxxEntityzyfzr9);


            //措施
            //Integer ts = 2;
            //if (StringUtils.isNotBlank(cellzb9)) {
            //    while (true) {
            //        if (!cellzb9.contains(String.valueOf(ts))) {
            //            String substring1 = cellzb9.substring(cellzb9.indexOf(String.valueOf(ts - 1)) + 2, cellzb9.toCharArray().length - 1);
            //
            //            SgcAqfxgkGkcsEntity sgcAqfxgkGkcsEntity = new SgcAqfxgkGkcsEntity();
            //            sgcAqfxgkGkcsEntity.setGkcsid(UUIDUtil.uuid32());
            //            sgcAqfxgkGkcsEntity.setGkcsmc(substring1);
            //            //TODO
            //            sgcAqfxgkGkcsEntity.setGkzzid(zyfzr);
            //            sgcAqfxgkGkcsEntity.setPxz(ts - 1);
            //            sgcAqfxgkGkcsEntity.setCreatedBy("admin");
            //            sgcAqfxgkGkcsEntity.setCreatedTime(LocalDateTime.now());
            //            sgcAqfxgkGkcsMapper.insert(sgcAqfxgkGkcsEntity);
            //
            //            break;
            //        } else {
            //            String substring1 = cellzb9.substring(cellzb9.indexOf(String.valueOf(ts - 1)) + 2, cellzb9.indexOf(String.valueOf(ts)) - 1);
            //
            //            SgcAqfxgkGkcsEntity sgcAqfxgkGkcsEntity = new SgcAqfxgkGkcsEntity();
            //            sgcAqfxgkGkcsEntity.setGkcsid(UUIDUtil.uuid32());
            //            sgcAqfxgkGkcsEntity.setGkcsmc(substring1);
            //            //TODO
            //            sgcAqfxgkGkcsEntity.setGkzzid(zyfzr);
            //            sgcAqfxgkGkcsEntity.setPxz(ts - 1);
            //            sgcAqfxgkGkcsEntity.setCreatedBy("admin");
            //            sgcAqfxgkGkcsEntity.setCreatedTime(LocalDateTime.now());
            //            sgcAqfxgkGkcsMapper.insert(sgcAqfxgkGkcsEntity);
            //
            //            ts++;
            //        }
            //
            //    }
            //}

            Cell cell9 = row.getCell(10);
            String cellzb9 = getCellValue(cell9);
            cellzb9 = cellzb9.replaceAll(" ", "");
            cellzb9 = cellzb9.replaceAll("\n", "");
            String[] split = cellzb9.split("；");
            for (int j = 0; j < split.length; j++) {
                String s = split[j];
                String substring = s.substring(2);
                //int i1 = substring.lastIndexOf("。");
                //int length = substring.length();
                if ((substring.lastIndexOf("。") + 1) == substring.length()) {
                    substring = substring.substring(0, substring.lastIndexOf("。"));
                }
                //trimFirstAndLastChar(substring,'。');
                char c = substring.toCharArray()[0];
                if ('.' == c) {
                    substring = substring.substring(1);
                }

                SgcAqfxgkGkcsEntity sgcAqfxgkGkcsEntity = new SgcAqfxgkGkcsEntity();
                sgcAqfxgkGkcsEntity.setGkcsid(UUIDUtil.uuid32());
                sgcAqfxgkGkcsEntity.setGkcsmc(substring);
                //TODO
                sgcAqfxgkGkcsEntity.setGkzzid(fddbr);
                sgcAqfxgkGkcsEntity.setPxz(j + 1);
                sgcAqfxgkGkcsEntity.setCreatedBy("admin");
                sgcAqfxgkGkcsEntity.setCreatedTime(new Date());
                sgcAqfxgkGkcsMapper.insert(sgcAqfxgkGkcsEntity);
            }

            //getzzxx(row,11,aqzrr);
            //getzzxx(row,13,aqjgzrr);
            //getzzxx(row,15,aqjgbmfzr);
            //getzzxx(row,17,aqjgglry);
            //getzzxx(row,19,zyzrrsk);
            //getzzxx(row,21,aqglrygc);
            //getzzxx(row,23,gwrygc);

            zzxx(row, 12, xcfzr);
            zzxx(row, 14, aqglry);
            zzxx(row, 16, zjlgcs);
            zzxx(row, 18, jlgcs);
            zzxx(row, 20, xmjl);
            zzxx(row, 22, aqy);
            zzxx(row, 24, zyry);


            //人员
            Cell cell90 = row.getCell(9);
            String cellzb90 = getCellValue(cell90);
            cellzb90 = cellzb90.trim();
            SgcAqfxgkZrryEntity sgcAqfxgkZrryEntity = new SgcAqfxgkZrryEntity();
            sgcAqfxgkZrryEntity.setId(UUIDUtil.uuid32());
            sgcAqfxgkZrryEntity.setGkzzid(fddbr);
            sgcAqfxgkZrryEntity.setCreatedBy("admin");
            sgcAqfxgkZrryEntity.setCreatedTime(new Date());
            Map<String, String> map = sgcAqfxgkGkcsMapper.selectYhidByName(cellzb90);
            sgcAqfxgkZrryEntity.setZrrid(map.get("id"));
            sgcAqfxgkZrryEntity.setGkid(zbid);
            sgcAqfxgkZrryMapper.insert(sgcAqfxgkZrryEntity);

            zrrInsert(row, 11, xcfzr,zbid);
            zrrInsert(row, 13, aqglry,zbid);
            zrrInsert(row, 15, zjlgcs,zbid);
            zrrInsert(row, 17, jlgcs,zbid);
            zrrInsert(row, 19, xmjl,zbid);
            zrrInsert(row, 21, aqy,zbid);
            zrrInsert(row, 23, zyry,zbid);

            //if (StringUtils.isNotBlank(cellzb9)) {
            //    int i2 = cellzb9.indexOf('2');
            //    if (i2 != -1) {
            //        cellzb9.substring(2, i2 - 1);
            //    } else {
            //        String substring1 = cellzb9.substring(2, cellzb9.toCharArray().length - 1);
            //        SgcAqfxgkGkcsEntity sgcAqfxgkGkcsEntity = new SgcAqfxgkGkcsEntity();
            //        sgcAqfxgkGkcsEntity.setGkcsid(UUIDUtil.uuid32());
            //        sgcAqfxgkGkcsEntity.setGkcsmc(substring1);
            //        //TODO
            //        sgcAqfxgkGkcsEntity.setxmid
            //        sgcAqfxgkGkcsEntity.setPxz();
            //        sgcAqfxgkGkcsEntity.setCreatedBy("admin");
            //        sgcAqfxgkGkcsEntity.setCreatedTime(LocalDateTime.now());
            //
            //    }


            //    int i3 = cellzb9.indexOf('3');
            //    if (i3 != -1) {
            //        cellzb9.substring(i2 + 2, i3 - 1);
            //    }
            //
            //    int i4 = cellzb9.indexOf('4');
            //    if (i4 != -1) {
            //        cellzb9.substring(i3 + 2, i4 - 1);
            //    }
            //
            //    int i5 = cellzb9.indexOf('5');
            //    if (i5 != -1) {
            //        cellzb9.substring(i4 + 2, i5 - 1);
            //    }
            //
            //    int i6 = cellzb9.indexOf('6');
            //    if (i6 != -1) {
            //        cellzb9.substring(i5 + 2, i6 - 1);
            //    }
            //
            //
            //}


            //if (!strings.contains(xiang))
            //    strings.add(xiang);
//id
//                Cell cell11 = row.getCell(24);
//                String id = getCellValue(cell11);

//                stringStringHashMap.put(code, id);


        }

    }

    private void zrrInsert(Row row, Integer i, String fddbr,String zbid) {
        Cell cell91 = row.getCell(i);
        String cellzb90 = getCellValue(cell91);
        cellzb90 = cellzb90.trim();
        SgcAqfxgkZrryEntity sgcAqfxgkZrryEntity = new SgcAqfxgkZrryEntity();
        sgcAqfxgkZrryEntity.setId(UUIDUtil.uuid32());
        sgcAqfxgkZrryEntity.setGkzzid(fddbr);
        Map<String, String> map = sgcAqfxgkGkcsMapper.selectYhidByName(cellzb90);
        sgcAqfxgkZrryEntity.setZrrid(map.get("id"));
        sgcAqfxgkZrryEntity.setCreatedBy("admin");
        sgcAqfxgkZrryEntity.setCreatedTime(new Date());
        sgcAqfxgkZrryEntity.setGkid(zbid);
        sgcAqfxgkZrryMapper.insert(sgcAqfxgkZrryEntity);
    }

    private void zzxx(Row row, Integer i, String zw) {
        Cell cell9 = row.getCell(i);
        String cellzb9 = getCellValue(cell9);
        cellzb9 = cellzb9.replaceAll(" ", "");
        cellzb9 = cellzb9.replaceAll("\n", "");
        String[] split = cellzb9.split("；");
        for (int j = 0; j < split.length; j++) {
            String s = split[j];
            String substring = s.substring(2);
            if ((substring.lastIndexOf("。") + 1) == substring.length()) {
                substring = substring.substring(0, substring.lastIndexOf("。"));
            }

            char c = substring.toCharArray()[0];
            if ('.' == c) {
                substring = substring.substring(1);
            }

            SgcAqfxgkGkcsEntity sgcAqfxgkGkcsEntity = new SgcAqfxgkGkcsEntity();
            sgcAqfxgkGkcsEntity.setGkcsid(UUIDUtil.uuid32());
            sgcAqfxgkGkcsEntity.setGkcsmc(substring);
            //TODO
            sgcAqfxgkGkcsEntity.setGkzzid(zw);
            sgcAqfxgkGkcsEntity.setPxz(j + 1);
            sgcAqfxgkGkcsEntity.setCreatedBy("admin");
            sgcAqfxgkGkcsEntity.setCreatedTime(new Date());
            sgcAqfxgkGkcsMapper.insert(sgcAqfxgkGkcsEntity);
        }
    }

    /**
     * 去除字符串首尾出现的某个字符.
     *
     * @param source  源字符串.
     * @param element 需要去除的字符.
     * @return String.
     */
    public static String trimFirstAndLastChar(String source, char element) {
        boolean beginIndexFlag = true;
        boolean endIndexFlag = true;
        do {
            int beginIndex = source.indexOf(element) == 0 ? 1 : 0;
            int endIndex = source.lastIndexOf(element) + 1 == source.length() ? source.lastIndexOf(element) : source.length();
            source = source.substring(beginIndex, endIndex);
            beginIndexFlag = (source.indexOf(element) == 0);
            endIndexFlag = (source.lastIndexOf(element) + 1 == source.length());
        } while (beginIndexFlag || endIndexFlag);
        return source;
    }


    private void getzzxx(Row row, Integer i, String zw) {
        Cell cell9 = row.getCell(i);
        String cellzb9 = getCellValue(cell9);
        Integer ts = 2;
        if (StringUtils.isNotBlank(cellzb9)) {
            while (true) {
                if (!cellzb9.contains(String.valueOf(ts))) {
                    String substring1 = cellzb9.substring(cellzb9.indexOf(String.valueOf(ts - 1)) + 2, cellzb9.toCharArray().length - 1);

                    SgcAqfxgkGkcsEntity sgcAqfxgkGkcsEntity = new SgcAqfxgkGkcsEntity();
                    sgcAqfxgkGkcsEntity.setGkcsid(UUIDUtil.uuid32());
                    sgcAqfxgkGkcsEntity.setGkcsmc(substring1);
                    //TODO
                    sgcAqfxgkGkcsEntity.setGkzzid(zw);
                    sgcAqfxgkGkcsEntity.setPxz(ts - 1);
                    sgcAqfxgkGkcsEntity.setCreatedBy("admin");
                    sgcAqfxgkGkcsEntity.setCreatedTime(new Date());
                    sgcAqfxgkGkcsMapper.insert(sgcAqfxgkGkcsEntity);

                    break;
                } else {
                    String substring1 = cellzb9.substring(cellzb9.indexOf(String.valueOf(ts - 1)) + 2, cellzb9.indexOf(String.valueOf(ts)) - 1);

                    SgcAqfxgkGkcsEntity sgcAqfxgkGkcsEntity = new SgcAqfxgkGkcsEntity();
                    sgcAqfxgkGkcsEntity.setGkcsid(UUIDUtil.uuid32());
                    sgcAqfxgkGkcsEntity.setGkcsmc(substring1);
                    //TODO
                    sgcAqfxgkGkcsEntity.setGkzzid(zw);
                    sgcAqfxgkGkcsEntity.setPxz(ts - 1);
                    sgcAqfxgkGkcsEntity.setCreatedBy("admin");
                    sgcAqfxgkGkcsEntity.setCreatedTime(new Date());
                    sgcAqfxgkGkcsMapper.insert(sgcAqfxgkGkcsEntity);

                    ts++;
                }

            }
        }
    }

    /**
     * 根据项目ID查询标段和单位信息
     *
     * @param xmid
     * @return
     */
    public List<ListBdxxByXmidmcyhDto> listBdxxByXmidmcyh(String xmid) {
        //根据项目id查询标段
        List<SgcXmbdEntity> sgcXmbdEntities = sgcXmbdMapper.selectList(new LambdaQueryWrapper<SgcXmbdEntity>().eq(SgcXmbdEntity::getXmid, xmid));

        //根据项目id查询项目信息
        SgcXmxxEntity sgcXmxxEntity = sgcXmxxMapper.selectById(xmid);

        ArrayList<ListBdxxByXmidmcyhDto> objects = new ArrayList<>();
        //根据标段id查询标段对应公司
        for (SgcXmbdEntity sgcXmbdEntity : sgcXmbdEntities) {
            List<SgcBdzbdwGlbEntity> sgcBdzbdwGlbEntities = sgcBdzbdwGlbMapper.selectList(new LambdaQueryWrapper<SgcBdzbdwGlbEntity>().
                    eq(SgcBdzbdwGlbEntity::getBdid, sgcXmbdEntity.getBdid()).orderByDesc(SgcBdzbdwGlbEntity::getCreatedTime));

            ListBdxxByXmidmcyhDto listBdxxByXmidmcyhDto = new ListBdxxByXmidmcyhDto();
            objects.add(listBdxxByXmidmcyhDto);
            ArrayList<SgcDwglEntity> sgcDwglEntities = new ArrayList<>();

            listBdxxByXmidmcyhDto.setSgcDwglEntitys(sgcDwglEntities);
            String dwmcs = "";
            listBdxxByXmidmcyhDto.setBdid(sgcXmbdEntity.getBdid());
            listBdxxByXmidmcyhDto.setBdmc(sgcXmbdEntity.getBdmc());
            listBdxxByXmidmcyhDto.setBdje(sgcXmbdEntity.getBdje());
            listBdxxByXmidmcyhDto.setJszt(sgcXmbdEntity.getJszt());
            listBdxxByXmidmcyhDto.setXmzt(sgcXmxxEntity.getXmzt());


            if (CollectionUtils.isNotEmpty(sgcBdzbdwGlbEntities)) {
                for (SgcBdzbdwGlbEntity sgcBdzbdwGlbEntity : sgcBdzbdwGlbEntities) {
                    SgcDwglEntity sgcDwglEntity = sgcDwglMapper.selectOne(new LambdaQueryWrapper<SgcDwglEntity>().eq(SgcDwglEntity::getDwid, sgcBdzbdwGlbEntity.getDwid()).orderByDesc(SgcDwglEntity::getCreatedTime));
                    sgcDwglEntities.add(sgcDwglEntity);
                    dwmcs = dwmcs + sgcDwglEntity.getDwmc() + ",";

                    ListBdxxByXmidDto listBdxxByXmidDto = new ListBdxxByXmidDto();
                    BeanUtils.copyProperties(sgcDwglEntity, listBdxxByXmidDto);
                    listBdxxByXmidDto.setFrdb(sgcBdzbdwGlbEntity.getFrdb());
                    listBdxxByXmidDto.setLxdh(sgcBdzbdwGlbEntity.getLxdh());
                    listBdxxByXmidDto.setBdid(sgcXmbdEntity.getBdid());
                    listBdxxByXmidDto.setBdmc(sgcXmbdEntity.getBdmc());
                    listBdxxByXmidDto.setBdje(sgcXmbdEntity.getBdje());
                    listBdxxByXmidDto.setJszt(sgcXmbdEntity.getJszt());
                    listBdxxByXmidDto.setXmzt(sgcXmxxEntity.getXmzt());
                }

            }
            if ((dwmcs.lastIndexOf(",") + 1) == dwmcs.toCharArray().length) {
                dwmcs = dwmcs.substring(0, dwmcs.length() - 1);
            }

            listBdxxByXmidmcyhDto.setDwmcs(dwmcs);


        }


        return objects;
    }
}
