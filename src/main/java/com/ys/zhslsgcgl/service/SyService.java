package com.ys.zhslsgcgl.service;

import com.ys.common.core.dto.R;
import com.ys.zhslsgcgl.mapper.SgcXmxxMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description: 首页
 * @Author: chenxingjian
 * CreateDate: 2022/11/11 10:38
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Service
public class SyService {

    @Autowired
    private SgcXmxxMapper sgcXmxxMapper;

    /**
     * 查询项目总览
     * @return
     */
    public R getXmzl(){
        return R.ok(sgcXmxxMapper.getXmzl());
    }

    /**
     * 查询项目分布
     * @return
     */
    public R getXmfb(){
        return R.ok(sgcXmxxMapper.getXmfb());
    }

    /**
     * 查询投资情况
     * @return
     */
    public R getTzqk(){
        return R.ok(sgcXmxxMapper.getTzqk());
    }

    /**
     * 查询在建项目情况
     * @return
     */
    public R getZjxm(){
        return R.ok(sgcXmxxMapper.getZjxm());
    }
}
