package com.ys.zhslsgcgl.service;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.netsdk.bean.DeviceInfo;
import com.netsdk.demo.module.DahuaApp;
import com.netsdk.demo.module.DeviceCache;
import com.netsdk.utils.FilePathUtils;
import com.ys.common.web.util.DicCatchUtil;
import com.ys.middleground.start.util.UserUtils;
import com.ys.zhslsgcgl.bo.AddRyBo;
import com.ys.zhslsgcgl.bo.PageRylbBo;
import com.ys.zhslsgcgl.bo.RykqayhzBo;
import com.ys.zhslsgcgl.bo.XmxxBo;
import com.ys.zhslsgcgl.dto.*;
import com.ys.zhslsgcgl.entity.SgcRyglEntity;
import com.ys.zhslsgcgl.entity.SgcRysbGlbEntity;
import com.ys.zhslsgcgl.entity.SgcSbbdGlbEntity;
import com.ys.zhslsgcgl.mapper.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description: 市场监督-人员管理
 * @Author: fsx 2585449236@qq.com
 * @CreateDate: 2022/10/31 11:29
 * @Version: 1.0
 * Copyright (c) 2020,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Service
public class RyglService{

    /**
     * 字典处理
     */
    @Resource
    DicCatchUtil dicCatchUtil;

    /**
     * 人员管理接口
     */
    @Resource
    SgcRyglMapper sgcRyglMapper;

    /**
     * 人员每天考勤管理接口
     */
    @Resource
    SgcRykqDayMapper rykqDayMapper;
    /**
     * 人员设备关联
     */
    @Resource
    SgcRysbGlbMapper sgcRysbGlbMapper;

    /**
     * 设备管理接口
     */
    @Resource
    SgcSbxxMapper sgcSbxxMapper;
    @Resource
    SgcSbbdGlbMapper sgcSbbdGlbMapper;

    @Resource
    DahuaApp dahuaApp;

    @Value("${custom.hostIp}")
    private String hostIp;
    @Value("${custom.hostPort}")
    private int hostPort;
    @Value("${custom.midPath}")
    private String midPath;
    @Value("${custom.filePrefix}")
    private String filePrefix;

    /**
     * 分页查询人员信息
     * @param pageRylbBo
     * @return
     */
    public PageInfo<PageRylbDto> queryPageRygl(PageRylbBo pageRylbBo) {
        PageHelper.startPage(pageRylbBo.getPageNum(), pageRylbBo.getPageSize());
        List<PageRylbDto> dataList = sgcRyglMapper.queryPageRygl(pageRylbBo);
        dicCatchUtil.handleDicValueForDto(dataList);
        return new PageInfo<>(dataList);
    }

    /**
     * 根据单位分页查询人员信息
     * @param pageRylbBo
     * @return
     */
    public PageInfo<PageRylbDto> queryPageRyByDwid(PageRylbBo pageRylbBo){
        PageHelper.startPage(pageRylbBo.getPageNum(),pageRylbBo.getPageSize());
        List<PageRylbDto> list = sgcRyglMapper.queryPageRyByDwid(pageRylbBo);
        dicCatchUtil.handleDicValueForDto(list);
        //考勤天数
        Integer kqts = sgcRyglMapper.getKqtsByRq(pageRylbBo.getRq());
        //按月查询人员实际到岗天数
        Map<String, Integer> rykqxx = sgcRyglMapper.getRyKqtsByNy(pageRylbBo.getRq()).stream().collect(Collectors.toMap(RyKqxxDto::getYgid, RyKqxxDto::getDgts));
        //查询当天打卡人员记录图片
        Map<String, String> rxdkjlxx = sgcRyglMapper.getRyJrdkjl().stream().collect(Collectors.toMap(RyJrdkjlDto::getYgid, RyJrdkjlDto::getUrl));

        list.forEach(ryxx->{
            String ygid = ryxx.getYgid();
            ryxx.setKqts(kqts);
            ryxx.setDgts(rykqxx.get(ygid));
            ryxx.setJrdkurl(rxdkjlxx.get(ygid));
            String filepath = ryxx.getFilepath();
            if(StringUtils.isNotBlank(filepath)){
                //SFZZM-C:\home\file\sgc\ryxx\各种组合砂_64.jpg,SFZFM-C:\home\file\sgc\ryxx\花岗岩_246（1）.jpg,RLXX-C:\home\file\sgc\ryxx\粉砂_66（3）.jpg
                String[] rxtp = filepath.split(",");
                for (String s : rxtp) {
                    String[] split = s.split("-");
                    if("SFZZM".equals(split[0])){
                        ryxx.setSfzzmurl(split[1]);
                    }else if("SFZFM".equals(split[0])){
                        ryxx.setSfzfmurl(split[1]);
                    }else if("RLXX".equals(split[0])){
                        ryxx.setRlxxurl(split[1]);
                    }
                }
            }
        });
        return new PageInfo<>(list);
    }

    /**
     * 根据项目id和标段id查询人员信息
     * @param xmid
     * @param bdid
     * @return
     */
    public List<RyxxByDwfzDto> listRyxxByXmidOrBdid(String xmid, String bdid, String rq,String ygxm){
        List<PageRylbDto> list = sgcRyglMapper.listRyxxByXmidOrBdid(xmid,bdid,ygxm);
        dicCatchUtil.handleDicValueForDto(list);
        //1-业主单位 2-设计单位 3-监理单位 4-施工单位 5-质检单位
        Map<String, List<PageRylbDto>> map = list.stream().collect(Collectors.groupingBy(PageRylbDto::getDwlb));
        List<RyxxByDwfzDto> result = new ArrayList<>();
        //考勤天数
        Integer kqts = sgcRyglMapper.getKqtsByRq(rq);
        //按月查询人员实际到岗天数
        Map<String, Integer> rykqxx = sgcRyglMapper.getRyKqtsByNy(rq).stream().collect(Collectors.toMap(RyKqxxDto::getYgid, RyKqxxDto::getDgts));
        //查询当天打卡人员记录图片
        Map<String, String> rxdkjlxx = sgcRyglMapper.getRyJrdkjl().stream().collect(Collectors.toMap(RyJrdkjlDto::getYgid, RyJrdkjlDto::getUrl));

        map.forEach((k, v)->{
            RyxxByDwfzDto ryxxByDwfzDto = new RyxxByDwfzDto();
            ryxxByDwfzDto.setDwlb(k);
            ryxxByDwfzDto.setDwid(v.get(0).getDwid());
            ryxxByDwfzDto.setDwmc(v.get(0).getDwmc());
            for (PageRylbDto ryxx : v) {
                String ygid = ryxx.getYgid();
                ryxx.setKqts(kqts);
                ryxx.setDgts(rykqxx.get(ygid));
                ryxx.setJrdkurl(rxdkjlxx.get(ygid));
                String filepath = ryxx.getFilepath();
                if(StringUtils.isNotBlank(filepath)){
                    //SFZZM-C:\home\file\sgc\ryxx\各种组合砂_64.jpg,SFZFM-C:\home\file\sgc\ryxx\花岗岩_246（1）.jpg,RLXX-C:\home\file\sgc\ryxx\粉砂_66（3）.jpg
                    String[] rxtp = filepath.split(",");
                    for (String s : rxtp) {
                        String[] split = s.split("-");
                        if("SFZZM".equals(split[0])){
                            ryxx.setSfzzmurl(split[1]);
                        }else if("SFZFM".equals(split[0])){
                            ryxx.setSfzfmurl(split[1]);
                        }else if("RLXX".equals(split[0])){
                            ryxx.setRlxxurl(split[1]);
                        }
                    }
                }
            }
            ryxxByDwfzDto.setRyxx(v);
            result.add(ryxxByDwfzDto);
        });
        return result;
    }

    /**
     * 根据人员id和月份查询考勤记录
     * @param ygid
     * @param rq
     * @param sfdk 是否打卡  0：否，1：是
     * @return
     */
    public List<RykqDayDto> listRyKqjlByMonth(String ygid, String rq , Integer sfdk){
        return sgcRyglMapper.listRyKqjlByMonth(ygid,rq,sfdk);
    }

    /**
     * 搜索面板查询项目列表
     * @param xmxxBo
     * @return
     */
    public PageInfo<PageXmxxDto> pageXmlb(XmxxBo xmxxBo) {
        PageHelper.startPage(xmxxBo.getPageNum(), xmxxBo.getPageSize());
        List<PageXmxxDto> dataList = sgcRyglMapper.pageXmlb(xmxxBo);
        return new PageInfo<>(dataList);
    }

    /**
     * 查询项目信息下拉数据
     * @return
     */
    public List<Map<String,String>> listXmxx() {
        return sgcRyglMapper.listXmxx();
    }

    /**
     * 查询标段信息下拉数据
     * @return
     */
    public List<Map<String,String>> listBdxx(String xmid) {
        return sgcRyglMapper.listBdxx(xmid);
    }

    /**
     * 查询单位信息下拉数据
     * @return
     */
    public List<Map<String,String>> listDwxx(String bdid) {
        return sgcRyglMapper.listDwxx(bdid);
    }

    /**
     * 增加人员
     * @param ryBo
     * @return
     */
    @Transactional
    public int insertRygl(AddRyBo ryBo) {
        SgcRyglEntity sgcRygl = ryBo;
      /*  //验证工号是否重复
        if(StringUtil.isNotEmpty(sgcRygl.getGh())) {
            int count = sgcRyglMapper.selectCount(new QueryWrapper<SgcRyglEntity>().eq("GH", sgcRygl.getGh()));
            YsAssert.isTrue(count < 1, "新增失败：工号已存在");
        }*/
        
        sgcRygl.setCreatedBy(UserUtils.getUserName());
        sgcRygl.setCreatedTime(new Date());
//        addSbUserAndFace(sgcRygl);
        new Thread(()->addSbUserAndFace(sgcRygl)).start();
        return sgcRyglMapper.insert(sgcRygl);
    }


    /**
     * 删除人员
     * @param id
     * @return
     */
    @Transactional
    public int deleteRygl(String id) {
        /*SgcRyglEntity sgcRyglEntity = sgcRyglMapper.selectById(id);
        String sbid = sgcRyglEntity.getSbid();*/
        QueryWrapper<SgcRysbGlbEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("YGID",id);
        List<SgcRysbGlbEntity> sbList = sgcRysbGlbMapper.selectList(queryWrapper);
        if(CollectionUtil.isNotEmpty(sbList)){
            new Thread(() -> deleteUser(id, sbList)).start();
        }
        return sgcRyglMapper.deleteById(id);
    }


    /**
     * 修改人员
     * @param ryBo
     * @return
     */
    @Transactional
    public int updateRygl(AddRyBo ryBo) {
        SgcRyglEntity sgcRygl = ryBo;
        sgcRygl.setUpdatedBy(UserUtils.getUserName());
        sgcRygl.setUpdatedTime(new Date());
        new Thread(()->addSbUserAndFace(sgcRygl)).start();
        return sgcRyglMapper.updateById(sgcRygl);
    }

    /**
     * 大华sdk新增/修改用户、人脸信息
     **/
    @Transactional
    public void addSbUserAndFace(SgcRyglEntity sgcRygl) {
        String ygid = sgcRygl.getYgid();
        String url = sgcRyglMapper.getUrl(ygid);
        url = url.replace(filePrefix, "");
        url = url.replaceAll("\\\\", "/");
        String result = "http://" + hostIp + ":" + hostPort + midPath + url;
        String urlPath = FilePathUtils.encodeUrlPath(result);
        /*List<SgcSbxxEntity> sbxxEntityList = sgcSbxxMapper
            .selectList(Wrappers.emptyWrapper());*/
        String bdid = sgcRygl.getBdid();
        /*SgcSbxxEntity sgcSbxxEntity = sgcSbxxMapper
            .selectOne(Wrappers.<SgcSbxxEntity>lambdaQuery().eq(SgcSbxxEntity::getBdid, bdid));*/
        List<SgcSbbdGlbEntity> sbList = sgcSbbdGlbMapper.selectList(new QueryWrapper<SgcSbbdGlbEntity>()
                .eq( "BDID",bdid ));
        //String sbid = sgcSbbdGlbEntity.getSbid();
        //更新设备信息
        if(CollectionUtil.isNotEmpty(sbList)){
            sbList.forEach(sb->{
                DeviceInfo deviceInfo = DeviceCache.deviceInfoMap.get(sb.getSbid());
                if (ObjectUtil.isNotNull(deviceInfo)) {
                    dahuaApp.addUser(sgcRygl, deviceInfo.getM_hLoginHandle(), sb.getSbid());
                    dahuaApp.deleteFace(sgcRygl.getYgid(), deviceInfo.getM_hLoginHandle());
                    dahuaApp.addFace(sgcRygl.getYgid(), urlPath, deviceInfo.getM_hLoginHandle());
                }
            });

        }
     /*   //  有设备id，根据设备id操作设备人员、人脸数据，无则遍历操作
        sbxxEntityList.stream().filter(at-> {
            if (ObjectUtil.isNotEmpty(sbid)) {
                return sbid.equals(at.getId());
            }
            return true;
        }).forEach(it->{
            DeviceInfo deviceInfo = DeviceCache.deviceInfoMap.get(it.getId());
            if (ObjectUtil.isNotNull(deviceInfo)) {
                dahuaApp.addUser(sgcRygl, deviceInfo.getM_hLoginHandle(), it.getId());
                dahuaApp.deleteFace(sgcRygl.getYgid(), deviceInfo.getM_hLoginHandle());
                dahuaApp.addFace(sgcRygl.getYgid(), urlPath, deviceInfo.getM_hLoginHandle());
            }
        });*/
    }

    /**
     * 大华sdk删除用户
     **/
    /*private void deleteUser(String id, String sbid) {
        List<SgcSbxxEntity> sbxxEntityList = sgcSbxxMapper
            .selectList(Wrappers.emptyWrapper());
        //  有设备id，根据设备id操作设备人员、人脸数据，无则遍历操作
        sbxxEntityList.stream().filter(at->{
            if (ObjectUtil.isNotEmpty(sbid)) {
                return sbid.equals(at.getId());
            }
            return true;
        }).forEach(it->{
            DeviceInfo deviceInfo = DeviceCache.deviceInfoMap.get(it.getId());
            if (ObjectUtil.isNotNull(deviceInfo)) {
                dahuaApp.deleteUser(id, deviceInfo.getM_hLoginHandle());
            }
        });
    }*/
    private void deleteUser(String id, List<SgcRysbGlbEntity> sbList) {
        /*List<SgcSbxxEntity> sbxxEntityList = sgcSbxxMapper
                .selectList(Wrappers.emptyWrapper());*/
        //  遍历删除设备人员信息
        sbList.forEach( sb->{
            DeviceInfo deviceInfo = DeviceCache.deviceInfoMap.get(sb.getId());
            if (ObjectUtil.isNotNull(deviceInfo)) {
                dahuaApp.deleteUser(id, deviceInfo.getM_hLoginHandle());
                sgcRysbGlbMapper.deleteById(sb);
            }
        } );
        /*sbxxEntityList.stream().filter(at->{
            if (ObjectUtil.isNotEmpty(sbid)) {
                return sbid.equals(at.getId());
            }
            return true;
        }).forEach(it->{
            DeviceInfo deviceInfo = DeviceCache.deviceInfoMap.get(it.getId());
            if (ObjectUtil.isNotNull(deviceInfo)) {
                dahuaApp.deleteUser(id, deviceInfo.getM_hLoginHandle());
            }
        });*/
    }

    /**
     * 计算月份内单位下的所有人员考勤扣分
     * @param dwid
     * @param rq
     * @return
     */
    public List<DwLsdfDto> listDwKqkfByMonth(String dwid, String rq){
        List<DwLsdfDto> dwLsdfDtos = sgcRyglMapper.listDwKqkfByMonth(dwid, rq);
        dwLsdfDtos.forEach(i ->{
            i.setZzdf(i.getKqkf()==null? 60.0 : (60.0 - i.getKqkf()));
        });
        return dwLsdfDtos;
    }


    /**
     * 分页查询人员考勤按月汇总
     * @param bo
     * @return
     */
    public PageInfo queryPageRykqayhz(RykqayhzBo bo) {
        PageHelper.startPage(bo.getPageNum(), bo.getPageSize());
        List<RykqayhzDto> dataList = sgcRyglMapper.queryPageRykqayhz(bo);
        return new PageInfo<>(dataList);
    }

}
