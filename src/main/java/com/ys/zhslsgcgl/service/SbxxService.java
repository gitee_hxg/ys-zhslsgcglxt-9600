package com.ys.zhslsgcgl.service;

import cn.hutool.core.collection.CollectionUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ys.common.core.util.UUIDUtil;
import com.ys.middleground.start.util.UserUtils;
import com.ys.zhslsgcgl.bo.SbbdBo;
import com.ys.zhslsgcgl.bo.SbryxxBo;
import com.ys.zhslsgcgl.bo.SbxxBo;
import com.ys.zhslsgcgl.dto.BdsbglDto;
import com.ys.zhslsgcgl.dto.SbryxxDto;
import com.ys.zhslsgcgl.dto.SbxxDto;
import com.ys.zhslsgcgl.entity.SgcSbbdGlbEntity;
import com.ys.zhslsgcgl.mapper.SgcSbbdGlbMapper;
import com.ys.zhslsgcgl.mapper.SgcSbxxMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Description:设备管理Service
 * @Author: wugangzhi
 * @CreateData: 2022/11/17 1:09
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Service
public class SbxxService {
    //设备信息
    @Autowired
    private SgcSbxxMapper sgcSbxxMapper;

    //设备标段关联Mapper
    @Autowired
    private SgcSbbdGlbMapper sgcSbbdGlbMapper;

    /**
     * 分页查询设备信息
     * @param sbxxBo
     * @return
     */
    public PageInfo queryPageSbxx(SbxxBo sbxxBo) {
        PageHelper.startPage(sbxxBo.getPageNum(),sbxxBo.getPageSize());
        List<SbxxDto> list = sgcSbxxMapper.queryPageSbxx(sbxxBo);
        List<BdsbglDto> bdList=null;
        for(SbxxDto dto:list) {
            //根据设备ID查询绑定的标段列表
            bdList = bdList = new ArrayList<>();
            bdList = sgcSbxxMapper.listBdxxBySbid(dto.getSbid());
            dto.setBdList(bdList);
        }

        return new PageInfo(list);
    }


    /**
     * 修改设备绑定的标段
     * @param sbbdBo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateSbdbgl(SbbdBo sbbdBo){
        List<SgcSbbdGlbEntity> sbbdList=new ArrayList<>();
        SgcSbbdGlbEntity sgcSbbdGlbEntity=null;
        //先删除绑定的设备标段
        int row=sgcSbbdGlbMapper.deleteBySbid(sbbdBo.getSbid());
        if(row>0){
            for(String bdid:sbbdBo.getBdidList()){
                sgcSbbdGlbEntity=new SgcSbbdGlbEntity();
                sgcSbbdGlbEntity.setId(UUIDUtil.uuid32());
                sgcSbbdGlbEntity.setSbid(sbbdBo.getSbid());
                sgcSbbdGlbEntity.setBdid(bdid);
                sgcSbbdGlbEntity.setCreatedBy(UserUtils.getUserId());
                sgcSbbdGlbEntity.setCreatedTime(new Date());
                sbbdList.add(sgcSbbdGlbEntity);
            }
            //批量添加设备绑定的标段
            if(CollectionUtil.isNotEmpty(sbbdList)) {
                sgcSbbdGlbMapper.batchInsert(sbbdList);
            }
        }
        return  row;
    }


    /**
     * 分页查询设备人员
     * @param sbryxxBo
     * @return
     */
    public PageInfo queryPageSbry(SbryxxBo sbryxxBo) {
        PageHelper.startPage(sbryxxBo.getPageNum(),sbryxxBo.getPageSize());
        List<SbryxxDto> list = sgcSbxxMapper.queryPageSbry(sbryxxBo);
        return new PageInfo(list);
    }


}
