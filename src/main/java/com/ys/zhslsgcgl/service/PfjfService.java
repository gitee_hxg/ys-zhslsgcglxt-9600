package com.ys.zhslsgcgl.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ys.common.core.util.UUIDUtil;
import com.ys.middleground.start.util.UserUtils;
import com.ys.zhslsgcgl.bo.PfjfBo;
import com.ys.zhslsgcgl.bo.PfjfShxxBo;
import com.ys.zhslsgcgl.bo.PfjffsBo;
import com.ys.zhslsgcgl.constant.PfjfType;
import com.ys.zhslsgcgl.dto.*;
import com.ys.zhslsgcgl.entity.SgcPfjfEntity;
import com.ys.zhslsgcgl.entity.SgcPfjfShjlEntity;
import com.ys.zhslsgcgl.entity.SgcPfjfShztEntity;
import com.ys.zhslsgcgl.entity.SgcXmbdEntity;
import com.ys.zhslsgcgl.mapper.SgcPfjfMapper;
import com.ys.zhslsgcgl.mapper.SgcPfjfShjlMapper;
import com.ys.zhslsgcgl.mapper.SgcPfjfShztMapper;
import com.ys.zhslsgcgl.mapper.SgcXmbdMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Description:检查计分-加分管理
 * @Author: dzg
 * @CreateDate: 2022/11/10 14:40
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Service
public class PfjfService {

    /**
     * 评分加分mapper
     */
    @Autowired
    private SgcPfjfMapper sgcPfjfMapper;

    /**
     * 评分加分_审核状态mapper
     */
    @Autowired
    private SgcPfjfShztMapper sgcPfjfShztMapper;

    /**
     * 评分加分_审核记录mapper
     */
    @Autowired
    private SgcPfjfShjlMapper sgcPfjfShjlMapper;

    /**
     * 项目标段mapper
     */
    @Autowired
    private SgcXmbdMapper sgcXmbdMapper;


    /**
     * 加分申请列表查询
     *
     * @param
     * @return
     */
    public PageInfo queryPagePfjf(PfjfBo pfjfBo) {
        PageHelper.startPage(pfjfBo.getPageNum(), pfjfBo.getPageSize());
        List<PfjfDto> list = sgcPfjfMapper.queryPagePfjf(pfjfBo);
        return new PageInfo(list);
    }

    /**
     * 根据pfjfid查询申请信息和审核信息
     *
     * @param pfjfid
     * @return
     */
    public PfjfShDto queryPfjfById(String pfjfid) {
        //查询申请加分信息
        PfjfShDto pfjfShDto = sgcPfjfMapper.queryPfjfById(pfjfid);
        //查询初审信息
        PfjfShxxBo pfjfShxxBo = new PfjfShxxBo();
        pfjfShxxBo.setPfjfid(pfjfid);
        pfjfShxxBo.setLclx(PfjfType.PFJF_LCLX_CS);
        PfjfCsxxDto csxxDto = sgcPfjfMapper.queryPfjfCsxx(pfjfShxxBo);
        if (csxxDto != null) {
            pfjfShDto.setPfjfCsDto(csxxDto);
        }
        //查询复审信息
        pfjfShxxBo.setLclx(PfjfType.PFJF_LCLX_FS);
        PfjfFsxxDto fsxxDto = sgcPfjfMapper.queryPfjfFsxx(pfjfShxxBo);
        if (fsxxDto != null) {
            pfjfShDto.setPfjfFsDto(fsxxDto);
        }

        //判断流程状态(0-待审核 1-初审不通过 2-初审通过  3-复审不通过 4-复审通过)
        if(pfjfShDto.getShzt().equals(PfjfType.PFJF_SHZT_DSH)){
            //待审核
            pfjfShDto.setLczt("0");
        }else if(pfjfShDto.getLclx().equals(PfjfType.PFJF_LCLX_CS) && pfjfShDto.getShyj().equals(PfjfType.PFJF_SHYJ_CSBTY)){
            //初审不通过
            pfjfShDto.setLczt("1");
        }else if(pfjfShDto.getLclx().equals(PfjfType.PFJF_LCLX_CS) && pfjfShDto.getShyj().equals(PfjfType.PFJF_SHYJ_CSTY)){
            //初审通过
            pfjfShDto.setLczt("2");
        }else if(pfjfShDto.getLclx().equals(PfjfType.PFJF_LCLX_FS) && pfjfShDto.getShyj().equals(PfjfType.PFJF_SHYJ_FSBTY)){
            //复审不通过
            pfjfShDto.setLczt("3");
        }else if(pfjfShDto.getLclx().equals(PfjfType.PFJF_LCLX_FS) && pfjfShDto.getShyj().equals(PfjfType.PFJF_SHYJ_FSTY)){
            //复审通过
            pfjfShDto.setLczt("4");
        }

        return pfjfShDto;
    }

    /**
     * 根据标段id得到加分分数
     * <p>
     * * 施工单位承接的项目未发生质量与安全责任事故
     * 合同金额小于3000万加7分
     * 合同金额大于等于3000万小于5000万加8分
     * 合同金额大于等于5000万加9分
     *
     * @param bdid
     * @return
     */
    public PfjfJffs queryJffs(String bdid) {
        double jffs = 0;
        PfjfJffs jffsObj = new PfjfJffs();
        SgcXmbdEntity xmbdEntity = sgcXmbdMapper.selectById(bdid);
        BigDecimal bdje=xmbdEntity.getBdje();
        if (bdje!=null && bdje.doubleValue()>0){
            double je = xmbdEntity.getBdje().doubleValue();
            if (je < 3000) {
                jffs = 7;
            } else if (je >= 3000 && je < 5000) {
                jffs = 8;
            } else if (je >= 5000) {
                jffs = 9;
            }
        }
        jffsObj.setJffs(jffs);
        return jffsObj;
    }


    /**
     * 添加加分申请
     *
     * @param sgcPfjfEntity
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int addPfjf(SgcPfjfEntity sgcPfjfEntity) {
        //id赋值
//        sgcPfjfEntity.setPfjfid(UUIDUtil.uuid32());
        //设置操作人,操作时间
        sgcPfjfEntity.setCreatedBy(UserUtils.getUserName());
        sgcPfjfEntity.setCreatedTime(new Date());
        //新增加分申请
        sgcPfjfMapper.insert(sgcPfjfEntity);

        //新增审核状态
        SgcPfjfShztEntity shztEntity = new SgcPfjfShztEntity();
        shztEntity.setShztid(UUIDUtil.uuid32());
        shztEntity.setPfjfid(sgcPfjfEntity.getPfjfid());
        //县级初审
        shztEntity.setLclx(PfjfType.PFJF_LCLX_CS);
        //待审核
        shztEntity.setShzt(PfjfType.PFJF_SHZT_DSH);
        //数据时间
        shztEntity.setShsj(new Date());
        sgcPfjfShztMapper.insert(shztEntity);

        //新增审核记录
        SgcPfjfShjlEntity shjlEntity = new SgcPfjfShjlEntity();
        shjlEntity.setShjlid(UUIDUtil.uuid32());
        shjlEntity.setPfjfid(sgcPfjfEntity.getPfjfid());
        shjlEntity.setLclx(PfjfType.PFJF_LCLX_CS);
        shjlEntity.setShzt(PfjfType.PFJF_SHZT_DSH);
        shjlEntity.setShsj(new Date());
        int result = sgcPfjfShjlMapper.insert(shjlEntity);

        return result;
    }

    /**
     * 提交初审信息
     *
     * @param sgcPfjfShztEntity
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int submitCsxx(SgcPfjfShztEntity sgcPfjfShztEntity) {
        //查询审核状态
        sgcPfjfShztEntity.setShztid(getPfjfShzt(sgcPfjfShztEntity.getPfjfid()).getShztid());

        //审核意见 (A-初审同意 B初审不同意 C-复审同意 D-复审不同意)
        String shyj = sgcPfjfShztEntity.getShyj();
        if (shyj.equals("A")) {
            /**
             * 初审同意
             * 审核中(县级审批完等待市级审批)
             */
            sgcPfjfShztEntity.setShzt(PfjfType.PFJF_SHZT_SZH);
            //县级初审
            sgcPfjfShztEntity.setLclx(PfjfType.PFJF_LCLX_CS);
        } else if (shyj.equals("B")) {
            /**
             * 初审不同意
             * 结束审核流程，审核状态为已审核，审核意见为初审不同意。
             */
            sgcPfjfShztEntity.setShzt(PfjfType.PFJF_SHZT_YSH);
            //县级初审
            sgcPfjfShztEntity.setLclx(PfjfType.PFJF_LCLX_CS);
        }
        //审核人
        sgcPfjfShztEntity.setShry(UserUtils.getUserName());
        //审核时间
        sgcPfjfShztEntity.setShsj(new Date());
        //更新审核状态信息
        sgcPfjfShztMapper.updateById(sgcPfjfShztEntity);

        //新增审核记录
        int result = addPfjfShjl(sgcPfjfShztEntity);
        return result;
    }

    public SgcPfjfShztEntity getPfjfShzt(String pfjfid) {
        //查询审核状态
        SgcPfjfShztEntity shztEntity = sgcPfjfShztMapper.selectOne(new QueryWrapper<SgcPfjfShztEntity>().eq("PFJFID", pfjfid));
        return shztEntity;
    }

    /**
     * 提交复审信息
     *
     * @param pfjffsBo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int submitFsxx(PfjffsBo pfjffsBo) {
        SgcPfjfShztEntity sgcPfjfShztEntity = getPfjfShzt(pfjffsBo.getPfjfid());
        //复核意见
        sgcPfjfShztEntity.setShyj(pfjffsBo.getFhyj());
        //复核备注
        sgcPfjfShztEntity.setShbz(pfjffsBo.getFhbz());
        //审核意见 (A-初审同意 B初审不同意 C-复审同意 D-复审不同意)
        String shyj = sgcPfjfShztEntity.getShyj();
        if (shyj.equals("C")) {
            /**
             * 复审同意
             * 审核状态变为已审核
             */
            sgcPfjfShztEntity.setShzt(PfjfType.PFJF_SHZT_YSH);
            //流程类型更新为市级复审
            sgcPfjfShztEntity.setLclx(PfjfType.PFJF_LCLX_FS);
        } else if (shyj.equals("D")) {
            /**
             * 复审不同意
             * 审核状态为已审核，审核意见为复审不同意，结束审核流程
             */
            sgcPfjfShztEntity.setShzt(PfjfType.PFJF_SHZT_YSH);
            //流程类型更新为级复核
            sgcPfjfShztEntity.setLclx(PfjfType.PFJF_LCLX_FS);
        }
        //审核人
        sgcPfjfShztEntity.setShry(UserUtils.getUserName());
        //审核时间
        sgcPfjfShztEntity.setShsj(new Date());
        //审核单位
        sgcPfjfShztEntity.setShdw("");
        //审批内容
        sgcPfjfShztEntity.setSpnr(pfjffsBo.getFhbz());

        //更新审核状态信息
        sgcPfjfShztMapper.updateById(sgcPfjfShztEntity);
        //新增审核记录
        int result = addPfjfShjl(sgcPfjfShztEntity);
        return result;
    }

    /**
     * 添加审核记录
     *
     * @param sgcPfjfShztEntity
     * @return
     */
    public int addPfjfShjl(SgcPfjfShztEntity sgcPfjfShztEntity) {
        //新增审核记录
        SgcPfjfShjlEntity shjlEntity = new SgcPfjfShjlEntity();
        shjlEntity.setShjlid(UUIDUtil.uuid32());
        shjlEntity.setPfjfid(sgcPfjfShztEntity.getPfjfid());
        shjlEntity.setLclx(sgcPfjfShztEntity.getLclx());
        shjlEntity.setShzt(sgcPfjfShztEntity.getShzt());
        shjlEntity.setShyj(sgcPfjfShztEntity.getShyj());
        shjlEntity.setShbz(sgcPfjfShztEntity.getShbz());
        shjlEntity.setShdw(sgcPfjfShztEntity.getShdw());
        shjlEntity.setSpnr(sgcPfjfShztEntity.getSpnr());
        shjlEntity.setShsj(sgcPfjfShztEntity.getShsj());
        shjlEntity.setShry(sgcPfjfShztEntity.getShry());
        int result = sgcPfjfShjlMapper.insert(shjlEntity);
        return result;
    }


    /**
     * 修改加分申请
     *
     * @param sgcPfjfEntity
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int updatePfjf(SgcPfjfEntity sgcPfjfEntity) {
        //设置操作人,操作时间
        sgcPfjfEntity.setUpdatedTime(new Date());
        sgcPfjfEntity.setUpdatedBy(UserUtils.getUserName());
        return sgcPfjfMapper.updateById(sgcPfjfEntity);
    }

    /**
     * 根据ID删除评分加分
     *
     * @param pfjfid
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int deletePfjfById(String pfjfid) {
        int result = 0;
        SgcPfjfShztEntity shztEntity=sgcPfjfShztMapper.selectOne(new QueryWrapper<SgcPfjfShztEntity>().eq("PFJFID", pfjfid));
        String shzt = shztEntity.getShzt();
        //待审核的数据才能删除
        if (shzt.equals("0")) {
            sgcPfjfShztMapper.deleteById(pfjfid);
            result = sgcPfjfMapper.deleteById(pfjfid);
        }
        return result;
    }





}
