package com.ys.zhslsgcgl.service;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ys.common.core.dto.R;
import com.ys.common.core.dto.UserInfo;
import com.ys.common.core.util.UUIDUtil;
import com.ys.middleground.start.util.UserUtils;
import com.ys.zhslsgcgl.bo.KfglBo;
import com.ys.zhslsgcgl.bo.KfglmxBo;
import com.ys.zhslsgcgl.bo.PagekfglBo;
import com.ys.zhslsgcgl.dto.PfkfDto;
import com.ys.zhslsgcgl.dto.PfkfmxDto;
import com.ys.zhslsgcgl.entity.SgcPfkfEntity;
import com.ys.zhslsgcgl.entity.SgcPfkfMxbEntity;
import com.ys.zhslsgcgl.mapper.SgcPfkfMapper;
import com.ys.zhslsgcgl.mapper.SgcPfkfMxbMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Description: 检查计分-扣分管理
 * @Author: chenxingjian
 * CreateDate: 2022/11/8 17:47
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Service
public class PfkfglService {
    @Autowired
    private SgcPfkfMapper sgcPfkfMapper;

    @Autowired
    private SgcPfkfMxbMapper sgcPfkfMxbMapper;

    /**
     * 新增项目评分
     * @param kfglBo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public R addPfkf(KfglBo kfglBo){
        int row = 0;
        //评分扣分表实体
        SgcPfkfEntity sgcPfkfEntity = new SgcPfkfEntity();
        //评分扣分明细表实体
        List<SgcPfkfMxbEntity> pfkfmxblist = new ArrayList<>();
        //操作人
        String czr = UserUtils.getUserId();
        //操作时间
        Date now = new Date();
        //设置失效时间
        Date pfsj = kfglBo.getPfsj();
        Calendar cal = Calendar.getInstance();
        cal.setTime(pfsj);
        cal.add(Calendar.YEAR,2);
        Date sxsj = cal.getTime();


        if(CollectionUtil.isNotEmpty(kfglBo.getMxList())){
            String pfkfid = kfglBo.getPfkfid();
            //新增评分扣分明细
            for(KfglmxBo kfglmxBo : kfglBo.getMxList()){
                SgcPfkfMxbEntity sgcPfkfMxbEntity = new SgcPfkfMxbEntity();
                //主键ID
                sgcPfkfMxbEntity.setPfkfmxid(UUIDUtil.uuid32());
                sgcPfkfMxbEntity.setPfkfid(pfkfid);
                sgcPfkfMxbEntity.setPfbzid(kfglmxBo.getPfbzid());
                sgcPfkfMxbEntity.setCs(kfglmxBo.getCs());
                //设置操作人、操作时间
                sgcPfkfMxbEntity.setCreatedBy(czr);
                sgcPfkfMxbEntity.setCreatedTime(now);
                pfkfmxblist.add(sgcPfkfMxbEntity);
            }
        }

        //新增评分扣分
        sgcPfkfEntity.setPfkfid(kfglBo.getPfkfid());
        sgcPfkfEntity.setXmid(kfglBo.getXmid());
        sgcPfkfEntity.setBdid(kfglBo.getBdid());
        sgcPfkfEntity.setDwid(kfglBo.getDwid());
        sgcPfkfEntity.setPfsj(kfglBo.getPfsj());
        sgcPfkfEntity.setPfsxsj(sxsj);
        sgcPfkfEntity.setJrx(kfglBo.getJrx());
        sgcPfkfEntity.setZkfz(kfglBo.getZkfz());
        sgcPfkfEntity.setYbwtcs(kfglBo.getYbwtcs());
        sgcPfkfEntity.setJzwtcs(kfglBo.getJzwtcs());
        sgcPfkfEntity.setYzwtcs(kfglBo.getYzwtcs());
        sgcPfkfEntity.setBz(kfglBo.getBz());
        //设置操作人、操作时间
        sgcPfkfEntity.setCreatedBy(czr);
        sgcPfkfEntity.setCreatedTime(now);

        row = sgcPfkfMapper.insert(sgcPfkfEntity);
        if (row > 0){
            if (CollectionUtil.isNotEmpty(pfkfmxblist)){
                sgcPfkfMxbMapper.batchInsert(pfkfmxblist);
            }
        }

        return R.ok(row);
    }

    /**
     * 验证20天内是否新增
     * @param dwid
     * @param bdid
     * @return
     */
    public R checkTjsj(String dwid,String bdid) throws ParseException {
        //操作时间
        Date now = new Date();
        //验证标段下公司是否20天内提交过项目评分
        List<SgcPfkfEntity> list  = sgcPfkfMapper.selectList(new QueryWrapper<SgcPfkfEntity>().eq("DWID",dwid).eq("BDID",bdid));
        if (CollectionUtil.isNotEmpty(list)) {
            //获取以前评价的最新时间
            Date date = list.stream().max(Comparator.comparing(SgcPfkfEntity::getCreatedTime)).get().getCreatedTime();
            //计算当前时间与最新时间相差天数
            int datediff =getDayDiffer(date,now);
            if (datediff < 20) {
                return R.error("此项目距离上次评分不足20天，请下次再来");
            }
        }
        return R.ok();
    }

    /**
     * 分页查询扣分管理信息
     * @param pagekfglBo
     * @return
     */
    public PageInfo queryPagePfkf(PagekfglBo pagekfglBo){
        List<PfkfDto> result = new ArrayList<>();
        //分页
        PageHelper.startPage(pagekfglBo.getPageNum(),pagekfglBo.getPageSize());
        List<PfkfDto> list = sgcPfkfMapper.queryPagePfkf(pagekfglBo);
        //查询评分扣分明细
        for (PfkfDto pfkfDto : list){
            List<PfkfmxDto> pfkfmxList = sgcPfkfMxbMapper.listPfkfmx(pfkfDto.getPfkfid());
            pfkfDto.setPfkfmxList(pfkfmxList);
            result.add(pfkfDto);
        }
        return new PageInfo(list);
    }

    /**
     * 编辑项目评分
     * @param kfglBo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int updatePfkf(KfglBo kfglBo){
        int row = 0;
        //评分扣分表实体
        SgcPfkfEntity sgcPfkfEntity = new SgcPfkfEntity();
        List<SgcPfkfMxbEntity> pfkfmxblist = new ArrayList<>();
        //操作人
        String czr = UserUtils.getUserId();
        //操作时间
        Date now = new Date();
        //设置失效时间
        Calendar cal = Calendar.getInstance();
        cal.setTime(kfglBo.getPfsj());
        Date sxsj = cal.getTime();


        if(CollectionUtil.isNotEmpty(kfglBo.getMxList())){
            String pfkfid = kfglBo.getPfkfid();
            //编辑评分扣分明细
            for(KfglmxBo kfglmxBo : kfglBo.getMxList()){
                //评分扣分明细表实体
                SgcPfkfMxbEntity sgcPfkfMxbEntity = new SgcPfkfMxbEntity();
                //主键ID
                sgcPfkfMxbEntity.setPfkfmxid(UUIDUtil.uuid32());
                sgcPfkfMxbEntity.setPfkfid(pfkfid);
                sgcPfkfMxbEntity.setPfbzid(kfglmxBo.getPfbzid());
                sgcPfkfMxbEntity.setCs(kfglmxBo.getCs());
                //设置操作人、操作时间
                sgcPfkfMxbEntity.setCreatedBy(czr);
                sgcPfkfMxbEntity.setCreatedTime(now);
                pfkfmxblist.add(sgcPfkfMxbEntity);
            }
        }
        //编辑评分扣分
        sgcPfkfEntity.setPfkfid(kfglBo.getPfkfid());
        sgcPfkfEntity.setXmid(kfglBo.getXmid());
        sgcPfkfEntity.setBdid(kfglBo.getBdid());
        sgcPfkfEntity.setDwid(kfglBo.getDwid());
        sgcPfkfEntity.setPfsj(kfglBo.getPfsj());
        sgcPfkfEntity.setPfsxsj(sxsj);
        sgcPfkfEntity.setJrx(kfglBo.getJrx());
        sgcPfkfEntity.setZkfz(kfglBo.getZkfz());
        sgcPfkfEntity.setYbwtcs(kfglBo.getYbwtcs());
        sgcPfkfEntity.setJzwtcs(kfglBo.getJzwtcs());
        sgcPfkfEntity.setYzwtcs(kfglBo.getYzwtcs());
        sgcPfkfEntity.setBz(kfglBo.getBz());
        //设置操作人、操作时间
        sgcPfkfEntity.setCreatedBy(czr);
        sgcPfkfEntity.setCreatedTime(now);

        row = sgcPfkfMapper.updateById(sgcPfkfEntity);
        if (row > 0){
            if (CollectionUtil.isNotEmpty(pfkfmxblist)){
                //先删除表中的原有数据再插入
                List<String> ids = new ArrayList<>();
                List<SgcPfkfMxbEntity> list = sgcPfkfMxbMapper.selectList(new QueryWrapper<SgcPfkfMxbEntity>().eq("PFKFID",kfglBo.getPfkfid()));
                for (SgcPfkfMxbEntity sgcPfkfMxbEntity : list){
                    ids.add(sgcPfkfMxbEntity.getPfkfmxid());
                }
                sgcPfkfMxbMapper.deleteBatchIds(ids);
                //批量插入
                sgcPfkfMxbMapper.batchInsert(pfkfmxblist);
            }
        }
        return row;
    }

    /**
     * 删除项目评分
     * @param pfkfid
     * @return
     */
    public R deletePfkfByid(String pfkfid){
        int row = 0;
        List<String> ids = new ArrayList<>();

        //查询同pfkfid下扣分明细表的mxid集合
        List<SgcPfkfMxbEntity> list = sgcPfkfMxbMapper.selectList(new QueryWrapper<SgcPfkfMxbEntity>().eq("PFKFID",pfkfid));
        for (SgcPfkfMxbEntity sgcPfkfMxbEntity : list){
            ids.add(sgcPfkfMxbEntity.getPfkfmxid());
        }

        row = sgcPfkfMapper.deleteById(pfkfid);
        if (row > 0){
            //删除主表的同时通过idlist批量删除明细表数据
            sgcPfkfMxbMapper.deleteBatchIds(ids);
        }
        return R.ok(row);
    }

    /**
     * 计算两个日期之间的天数
     * @param startDate
     * @param endDate
     * @return
     * @throws ParseException
     */
    public static int getDayDiffer(Date startDate, Date endDate) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        long startDateTime = dateFormat.parse(dateFormat.format(startDate)).getTime();
        long endDateTime = dateFormat.parse(dateFormat.format(endDate)).getTime();
        return (int) ((endDateTime - startDateTime) / (1000 * 3600 * 24));
    }

}
