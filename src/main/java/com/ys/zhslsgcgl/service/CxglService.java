package com.ys.zhslsgcgl.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import com.ys.common.core.util.YsAssert;
import com.ys.middleground.start.util.UserUtils;
import com.ys.zhslsgcgl.bo.DwxxBo;
import com.ys.zhslsgcgl.bo.PageDwLsdfBo;
import com.ys.zhslsgcgl.dto.CxglDto;
import com.ys.zhslsgcgl.dto.SxbgDto;
import com.ys.zhslsgcgl.entity.*;
import com.ys.zhslsgcgl.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @Description:市场监督-诚信管理service
 * @Author: dzg
 * @CreateDate: 2022/11/8 11:32
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Service
public class CxglService {

    /**
     * 诚信管理mapper
     */
    @Autowired
    private SgcCxglMapper sgcCxglMapper;

    /**
     * 黑名单mapper
     */
    @Autowired
    private SgcHmdMapper sgcHmdMapper;

    /**
     * 重点关注mapper
     */
    @Autowired
    private SgcZdgzMapper sgcZdgzMapper;

    /**
     * 失信曝光mapper
     */
    @Autowired
    private SgcSxbgMapper sgcSxbgMapper;

    /**
     * 评分标准mapper
     */
    @Autowired
    private SgcPfbzMapper sgcPfbzMapper;

    /**
     * 单位管理mapper
     */
    @Autowired
    private SgcDwglMapper sgcDwglMapper;

    /**
     * 单位历史得分mapper
     */
    @Autowired
    private SgcDwxxLsdfMapper sgcDwxxLsdfMapper;

    /**
     * 分页查询单位信息历史得分
     * @param dwLsdfBo
     * @return
     */
    public PageInfo queryPageLsdf(PageDwLsdfBo dwLsdfBo) {
        PageHelper.startPage(dwLsdfBo.getPageNum(),dwLsdfBo.getPageSize());
        List<SgcDwxxLsdfEntity> list = sgcDwxxLsdfMapper.queryPageLsdf(dwLsdfBo);
        return new PageInfo(list);
    }


    /**
     * 分页查询诚信管理单位信息
     * @param dwxxBo
     * @return
     */
    public PageInfo queryPageCxgl(DwxxBo dwxxBo) {
        PageHelper.startPage(dwxxBo.getPageNum(),dwxxBo.getPageSize());
        List<CxglDto> list = sgcCxglMapper.queryPageCxgl(dwxxBo);
        return new PageInfo(list);
    }

    /**
     * 分页查询诚信管理单位信息黑名单
     * @param dwxxBo
     * @return
     */
    public PageInfo queryPageCxglHmd(DwxxBo dwxxBo) {
        PageHelper.startPage(dwxxBo.getPageNum(),dwxxBo.getPageSize());
        List<CxglDto> list = sgcCxglMapper.queryPageCxglHmd(dwxxBo);
        return new PageInfo(list);
    }

    /**
     * 分页查询诚信管理单位信息重点关注
     * @param dwxxBo
     * @return
     */
    public PageInfo queryPageCxglZdgz(DwxxBo dwxxBo) {
        PageHelper.startPage(dwxxBo.getPageNum(),dwxxBo.getPageSize());
        List<CxglDto> list = sgcCxglMapper.queryPageCxglZdgz(dwxxBo);
        return new PageInfo(list);
    }

    /**
     * 分页查询诚信管理单位信息失信曝光
     * @param dwxxBo
     * @return
     */
    public PageInfo queryPageCxglSxbg(DwxxBo dwxxBo) {
        PageHelper.startPage(dwxxBo.getPageNum(),dwxxBo.getPageSize());
        List<SxbgDto> list = sgcCxglMapper.queryPageCxglSxbg(dwxxBo);
        return new PageInfo(list);
    }


    /**
     * 添加黑名单
     *
     * @param sgcHmdEntity
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int addHmd(SgcHmdEntity sgcHmdEntity) {
        //验证是否已加入黑名单
        if (StringUtil.isNotEmpty(sgcHmdEntity.getDwid())) {
            int count1 = sgcHmdMapper.selectCount(new QueryWrapper<SgcHmdEntity>().eq("DWID", sgcHmdEntity.getDwid()));
            YsAssert.isTrue(count1 < 1, "新增失败：该单位已在黑名单中");
        }
        //设置操作人,操作时间
        sgcHmdEntity.setCreatedBy(UserUtils.getUserId());
        sgcHmdEntity.setCreatedTime(new Date());

        //根据单位id删除重点关注(黑名单和重点关注二选一)
        sgcZdgzMapper.deleteById(sgcHmdEntity.getDwid());
        //更新公司状态为黑名单
        SgcDwglEntity dwglEntity=sgcDwglMapper.selectById(sgcHmdEntity.getDwid());
        dwglEntity.setGszt("0");
        sgcDwglMapper.updateById(dwglEntity);
        return sgcHmdMapper.insert(sgcHmdEntity);
    }

    /**
     * 添加失信曝光
     *
     * @param sgcSxbgEntity
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int addSxbg(SgcSxbgEntity sgcSxbgEntity) {
        //验证单位是否已曝光
        if (StringUtil.isNotEmpty(sgcSxbgEntity.getDwid())) {
            int count1 = sgcSxbgMapper.selectCount(new QueryWrapper<SgcSxbgEntity>().eq("DWID", sgcSxbgEntity.getDwid()));
            YsAssert.isTrue(count1 < 1, "新增失败：该单位已曝光");
        }
        //设置操作人,操作时间
        sgcSxbgEntity.setCreatedBy(UserUtils.getUserId());
        sgcSxbgEntity.setCreatedTime(new Date());
        return sgcSxbgMapper.insert(sgcSxbgEntity);
    }

    /**
     * 添加重点关注
     *
     * @param sgcZdgzEntity
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int addZdgz(SgcZdgzEntity sgcZdgzEntity) {
        //验证单位是否已曝光
        if (StringUtil.isNotEmpty(sgcZdgzEntity.getDwid())) {
            int count1 = sgcZdgzMapper.selectCount(new QueryWrapper<SgcZdgzEntity>().eq("DWID", sgcZdgzEntity.getDwid()));
            YsAssert.isTrue(count1 < 1, "新增失败：该单位已被重点关注");
        }
        //设置操作人,操作时间
        sgcZdgzEntity.setCreatedBy(UserUtils.getUserId());
        sgcZdgzEntity.setCreatedTime(new Date());
        //根据单位id删除黑名单(黑名单和重点关注二选一)
        sgcHmdMapper.deleteById(sgcZdgzEntity.getDwid());
        //更新公司状态为重点关注
        SgcDwglEntity dwglEntity=sgcDwglMapper.selectById(sgcZdgzEntity.getDwid());
        dwglEntity.setGszt("2");
        sgcDwglMapper.updateById(dwglEntity);
        return sgcZdgzMapper.insert(sgcZdgzEntity);
    }

    /**
     * 根据单位ID解除黑名单
     * @param dwid
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int deleteHmdByDwid(String dwid) {
        //验证单位是否黑名单
        if (StringUtil.isNotEmpty(dwid)) {
            int count1 = sgcHmdMapper.selectCount(new QueryWrapper<SgcHmdEntity>().eq("DWID", dwid));
            YsAssert.isTrue(count1 == 1, "解除失败：该单位不在黑名单");
        }
        return sgcHmdMapper.deleteById(dwid);
    }

    /**
     * 根据单位ID解除关注
     * @param dwid
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int deleteZdgzByDwid(String dwid) {
        //验证单位是否被关注
        if (StringUtil.isNotEmpty(dwid)) {
            int count1 = sgcZdgzMapper.selectCount(new QueryWrapper<SgcZdgzEntity>().eq("DWID", dwid));
            YsAssert.isTrue(count1 == 1, "解除失败：该单位没被关注");
        }
        return sgcZdgzMapper.deleteById(dwid);
    }

    /**
     * 根据曝光id删除失信曝光
     * @param pbid
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int deleteSxbgByPbid(String pbid) {
        return sgcSxbgMapper.deleteById(pbid);
    }

    /**
     * 修改失信曝光信息
     * @param sgcSxbgEntity
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateSxbg(SgcSxbgEntity sgcSxbgEntity) {
        //更新人
        sgcSxbgEntity.setUpdatedBy(UserUtils.getUserId());
        //更新时间
        sgcSxbgEntity.setUpdatedTime(new Date());
        return sgcSxbgMapper.updateById(sgcSxbgEntity);
    }




}
