package com.ys.zhslsgcgl.service;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ys.common.core.dto.R;
import com.ys.common.core.util.UUIDUtil;
import com.ys.middleground.start.util.UserUtils;
import com.ys.zhslsgcgl.bo.XjlzqkUpdateBo;
import com.ys.zhslsgcgl.bo.XjrwCsnrListBo;
import com.ys.zhslsgcgl.bo.XjrwQueryPageBo;
import com.ys.zhslsgcgl.bo.XjrwUpdateBo;
import com.ys.zhslsgcgl.dto.XjrwDto;
import com.ys.zhslsgcgl.dto.XjrwgcnrDto;
import com.ys.zhslsgcgl.dto.XjrwxqDto;
import com.ys.zhslsgcgl.entity.SgcXjrwCsnrEntity;
import com.ys.zhslsgcgl.entity.SgcXjrwEntity;
import com.ys.zhslsgcgl.mapper.SgcXjrwCsnrMapper;
import com.ys.zhslsgcgl.mapper.SgcXjrwMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Description:巡检任务Service
 * @Author: wugangzhi
 * @CreateData: 2022/11/17 1:09
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Service
public class XjrwService {

    @Autowired
    private SgcXjrwMapper sgcXjrwMapper;

    @Autowired
    private SgcXjrwCsnrMapper xjrwCsnrMapper;


    /**
     * 分页查询巡检任务列表
     * @param bo
     * @return
     */
    public PageInfo queryPageXjrw(XjrwQueryPageBo bo) {
        PageHelper.startPage(bo.getPageNum(), bo.getPageSize());
        List<XjrwDto> list = sgcXjrwMapper.queryPageXjrw(bo);
        return new PageInfo(list);
    }


    /**
     * 修改巡检任务状态
     *
     * @param xjrwid
     * @param rwzt
     * @return
     */
    @Transactional
    public int updateXjRwzt(String xjrwid,String rwzt){
        Date now=new Date();
        SgcXjrwEntity xjrwEntity=sgcXjrwMapper.selectById(xjrwid);
        if("1".equals(rwzt)){
            xjrwEntity.setRwzt(rwzt);
            xjrwEntity.setSjkssj(now);
        }
        xjrwEntity.setUpdatedBy(UserUtils.getUserId());
        xjrwEntity.setUpdatedTime(now);
        return sgcXjrwMapper.updateById(xjrwEntity);
    }

    /**
     * 根据巡检任务ID查询巡检详情
     *
     * @param xjrwid
     * @return
     */
    public XjrwxqDto getXjrwXq(String xjrwid){
        XjrwxqDto xjrwxqDto=sgcXjrwMapper.getXjrwXq(xjrwid);
        List<XjrwgcnrDto> gkcsList=sgcXjrwMapper.listAqfxgkGkcsByXjrwid(xjrwid);
        xjrwxqDto.setGkcsList(gkcsList);
        return xjrwxqDto;

    }

    /**
     * 提交巡检情况
     * @param xjrwUpdateBo
     * @return
     */
    @Transactional
    public int updateXjqk(XjrwUpdateBo xjrwUpdateBo) {
        int row=0;
        Date now = new Date();
        List<SgcXjrwCsnrEntity> list = new ArrayList<>();
        SgcXjrwCsnrEntity sgcXjrwCsnrEntity = null;
        //先删除巡检内容措施
        xjrwCsnrMapper.deleteByXjrwid(xjrwUpdateBo.getXjrwid());

        //标志位：判断所有巡检任务是否都完成(0-否 1-是)
        int flag = 1;
        if (CollectionUtil.isNotEmpty(xjrwUpdateBo.getXjcsnrList())) {
            for (XjrwCsnrListBo dto : xjrwUpdateBo.getXjcsnrList()) {
                sgcXjrwCsnrEntity = new SgcXjrwCsnrEntity();
                sgcXjrwCsnrEntity.setId(UUIDUtil.uuid32());
                sgcXjrwCsnrEntity.setXjrwid(xjrwUpdateBo.getXjrwid());
                sgcXjrwCsnrEntity.setGkcsid(dto.getGkcsid());
                sgcXjrwCsnrEntity.setSfxj(dto.getSfxj());
                sgcXjrwCsnrEntity.setCreatedBy(UserUtils.getUserId());
                sgcXjrwCsnrEntity.setCreatedTime(now);
                list.add(sgcXjrwCsnrEntity);
                if ("0".equals(dto.getSfxj())) {
                    flag = 0;
                }
            }


            //根据巡检任务ID查询巡检详情
            SgcXjrwEntity sgcXjrwEntity = sgcXjrwMapper.selectById(xjrwUpdateBo.getXjrwid());
            sgcXjrwEntity.setLzqk(xjrwUpdateBo.getLzqk());
            sgcXjrwEntity.setXjtq(xjrwUpdateBo.getXjtq());
            sgcXjrwEntity.setSjkssj(xjrwUpdateBo.getSjkssj());
            sgcXjrwEntity.setSjwcsj(xjrwUpdateBo.getSjwcsj());
            sgcXjrwEntity.setPoints(xjrwUpdateBo.getPoints());
            sgcXjrwEntity.setSjwcsj(now);
            sgcXjrwEntity.setUpdatedBy(UserUtils.getUserId());
            sgcXjrwEntity.setUpdatedTime(now);
            //判断巡检措施内容是否都完成，全完成则表示任务已巡检完成，否则还是巡检中
           /* if (flag == 1) {
                sgcXjrwEntity.setRwzt("2");
                sgcXjrwEntity.setSjwcsj(now);
            } else {
                sgcXjrwEntity.setRwzt("1");
            }*/
            sgcXjrwEntity.setRwzt("2");
            sgcXjrwEntity.setSjwcsj(now);
            row=sgcXjrwMapper.updateById(sgcXjrwEntity);

            //批量添加巡检措施内容List
            if (CollectionUtil.isNotEmpty(list)) {
                xjrwCsnrMapper.batchInsert(list);
            }

        }
        return row;
    }

    /**
     * 履职情况填写
     * @param xjlzqkUpdateBo
     * @return
     */
    @Transactional
    public R updateXjlzqk(XjlzqkUpdateBo xjlzqkUpdateBo) {
        int row=0;
        //根据巡检任务ID查询详情
        SgcXjrwEntity sgcXjrwEntity=sgcXjrwMapper.selectById(xjlzqkUpdateBo.getXjrwid());
        if(ObjectUtil.isNotEmpty(sgcXjrwEntity)){
            if(!"2".equals(sgcXjrwEntity.getRwzt())){
                return  R.error("该任务还没开始巡检，不允许填写履职情况");
            }
        }
        else {
            sgcXjrwEntity.setXjrwid(xjlzqkUpdateBo.getXjrwid());
            sgcXjrwEntity.setAqfkf(xjlzqkUpdateBo.getAqfkf());
            sgcXjrwEntity.setDcry(xjlzqkUpdateBo.getDcry());
            sgcXjrwEntity.setBz(xjlzqkUpdateBo.getBz());
            row=sgcXjrwMapper.updateById(sgcXjrwEntity);
        }
        return R.ok(row);
    }


}
