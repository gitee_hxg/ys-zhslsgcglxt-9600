package com.ys.zhslsgcgl.service;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import com.ys.common.core.dto.R;
import com.ys.common.core.dto.Tree;
import com.ys.common.core.util.UUIDUtil;
import com.ys.common.core.util.YsAssert;
import com.ys.middleground.start.util.UserUtils;
import com.ys.zhslsgcgl.bo.*;
import com.ys.zhslsgcgl.dto.*;
import com.ys.zhslsgcgl.entity.*;
import com.ys.zhslsgcgl.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @Description: 风险管控
 * @Author: chenxingjian
 * CreateDate: 2022/11/12 13:53
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Service
public class FxgkService {

    /**
     * 安全风险分级管控Mapper
     */
    @Autowired
    private SgcAqfxgkMapper sgcAqfxgkMapper;

    /**
     * 安全风险分级管控-管控措施Mapper
     */
    @Autowired
    private SgcAqfxgkGkcsMapper sgcAqfxgkGkcsMapper;

    /**
     * 安全风险分级管控-责任人员Mapper
     */
    @Autowired
    private SgcAqfxgkZrryMapper sgcAqfxgkZrryMapper;

    /**
     * 安全风险分级管控-组织信息Mapper
     */
    @Autowired
    private SgcAqfxgkZzxxMapper sgcAqfxgkZzxxMapper;

    /**
     * 安全风险分级管控-组织类型Mapper
     */
    @Autowired
    private SgcAqfxgkZzlxbMapper sgcAqfxgkZzlxbMapper;

    /**
     * 巡检任务Mapper
     */
    @Autowired
    private SgcXjrwMapper sgcXjrwMapper;

    /**
     * 巡检任务内容Mapper
     */
    @Autowired
    private SgcXjrwCsnrMapper sgcXjrwCsnrMapper;


    /**
     * 分页查询风险管控主表
     *
     * @param pagefxgkBo
     * @return
     */
    public PageInfo queryPageFxgk(PageFxgkBo pagefxgkBo) {
        PageHelper.startPage(pagefxgkBo.getPageNum(), pagefxgkBo.getPageSize());
        List<SgcAqfxgkEntity> list = sgcAqfxgkMapper.listFxgk(pagefxgkBo);
        return new PageInfo<>(list);
    }


    /**
     * 查询风险管控组织树
     *
     * @return
     */
    public R getFxgkTree() {
        List<Tree> tree = new ArrayList<Tree>();
        List<Tree> list = new ArrayList<>();
        //查询目录列表
        List<SgcAqfxgkZzlxbEntity> zzxxlist = sgcAqfxgkZzlxbMapper.selectList(null);
        if (CollectionUtil.isNotEmpty(zzxxlist)) {
            for (SgcAqfxgkZzlxbEntity entity : zzxxlist) {
                list.add(new Tree(entity.getZzlxid(), entity.getZzlxpid(), entity.getZzlxmc(), entity.getPxz(), 0, entity));
            }
            //生成模块树
            tree = Tree.buildSortTreeNoRoot(list);
        }
        return R.ok(tree);
    }


    /**
     * 新增风险管控危险源信息
     *
     * @param sgcAqfxgkEntity
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public R addFxgk(SgcAqfxgkEntity sgcAqfxgkEntity) {
        int row = 0;
        //风险管控主表
        SgcAqfxgkEntity entity = new SgcAqfxgkEntity();
        //操作人
        String czr = UserUtils.getUserId();
        //操作时间
        Date now = new Date();

        //验证是否存在同项目部位危险源
        if (StringUtil.isNotEmpty(sgcAqfxgkEntity.getWxxm()) && StringUtil.isNotEmpty(sgcAqfxgkEntity.getKndzsglx())) {
            int count = sgcAqfxgkMapper.selectCount(new QueryWrapper<SgcAqfxgkEntity>().eq("WXXM", sgcAqfxgkEntity.getWxxm())
                    .eq("KNDZSGLX", sgcAqfxgkEntity.getKndzsglx()));
            YsAssert.isTrue(count < 1, "该危险源部位已存在,不可以重复添加");
        }

        //新增风险管控主表
        entity.setGkid(sgcAqfxgkEntity.getGkid());
        entity.setWxylb(sgcAqfxgkEntity.getWxylb());
        entity.setWxxm(sgcAqfxgkEntity.getWxxm());
        entity.setWxyjb(sgcAqfxgkEntity.getWxyjb());
        entity.setBw(sgcAqfxgkEntity.getBw());
        entity.setKndzsglx(sgcAqfxgkEntity.getKndzsglx());
        entity.setFxdj(sgcAqfxgkEntity.getFxdj());
        entity.setXmid(sgcAqfxgkEntity.getXmid());
        entity.setBdid(sgcAqfxgkEntity.getBdid());
        //设置操作人、操作时间
        entity.setCreatedBy(czr);
        entity.setCreatedTime(now);
        row = sgcAqfxgkMapper.insert(entity);

        return R.ok(row);
    }


    /**
     * 删除风险管控危险源信息
     *
     * @param gkid
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public R deleteFxgk(String gkid) {
        List<String> gkcsids = new ArrayList<>();
        List<SgcAqfxgkGkcsEntity> gkcsEntityList = new ArrayList<>();
        //验证该项目是否绑定巡检任务
        if (StringUtil.isNotEmpty(gkid)) {
            int count = sgcXjrwMapper.selectCount(new QueryWrapper<SgcXjrwEntity>().eq("GKID", gkid));
            YsAssert.isTrue(count < 1, "该危险源已绑定巡检任务,不允许删除");
        }

        //删除危险源信息
        sgcAqfxgkMapper.delete(new QueryWrapper<SgcAqfxgkEntity>().eq("GKID", gkid));
        //删除绑定的管控单位
        sgcAqfxgkZzxxMapper.delete(new QueryWrapper<SgcAqfxgkZzxxEntity>().eq("GKID", gkid));
        //删除绑定单位下的人员信息
        sgcAqfxgkZrryMapper.delete(new QueryWrapper<SgcAqfxgkZrryEntity>().eq("GKID", gkid));

        return R.ok("操作成功");
    }


    /**
     * 编辑风险管控危险源信息
     *
     * @param sgcAqfxgkEntity
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public R updateFxgk(SgcAqfxgkEntity sgcAqfxgkEntity) {
        int row = 0;
        SgcAqfxgkEntity entity = new SgcAqfxgkEntity();
        //操作人
        String czr = UserUtils.getUserId();
        //操作时间
        Date now = new Date();

        //编辑风险管控主表
        entity.setGkid(sgcAqfxgkEntity.getGkid());
        entity.setWxylb(sgcAqfxgkEntity.getWxylb());
        entity.setWxxm(sgcAqfxgkEntity.getWxxm());
        entity.setWxyjb(sgcAqfxgkEntity.getWxyjb());
        entity.setBw(sgcAqfxgkEntity.getBw());
        entity.setKndzsglx(sgcAqfxgkEntity.getKndzsglx());
        entity.setFxdj(sgcAqfxgkEntity.getFxdj());
        entity.setXmid(sgcAqfxgkEntity.getXmid());
        entity.setBdid(sgcAqfxgkEntity.getBdid());
        //设置操作人、操作时间
        entity.setCreatedBy(czr);
        entity.setCreatedTime(now);
        row = sgcAqfxgkMapper.updateById(entity);
        return R.ok(row);
    }


    /**
     * 新增(或编辑)风险管控组织信息
     *
     * @param
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public R addFxgkzzxx(FxgkzzBo fxgkzzBo) {
        int row = 1;
        List<SgcAqfxgkZzxxEntity> list = new ArrayList<>();
        //操作人
        String czr = UserUtils.getUserId();
        //操作时间
        Date now = new Date();
        //管控ID
        String gkid = fxgkzzBo.getGkid();
        //管控人员
        List<String> rylist = new ArrayList<>();
        //管控措施
        List<String> cslist = new ArrayList<>();
        //原表组织类型IDlist
        List<String> oldzzxxlist = new ArrayList<>();
        //新增组织类型IDlist
        List<String> addzzxxlist = new ArrayList<>();
        //删除组织类型IDlist
        List<String> deletezzxxlist = new ArrayList<>();

        //获取原表组织类型id
        List<FxgkdwDto> oldgkzzlist = new ArrayList<>();
        List<String> zzlxids = new ArrayList<>();
        if (!StringUtil.isEmpty(gkid)) {
            oldgkzzlist = sgcAqfxgkZzxxMapper.listGkzzByGkid(gkid);
            for (FxgkdwDto fxgkdwDto : oldgkzzlist) {
                oldzzxxlist.add(fxgkdwDto.getZzlxid());
            }
        }

        //取出传参和原表数据的交集
        List<String> equals = oldzzxxlist.stream().filter(s -> fxgkzzBo.getZzxxlist().contains(s)).collect(Collectors.toList());

        //通过交集获得需要进行删除和新增的组织信息
        if (equals.size() != oldzzxxlist.size() || equals.size() != fxgkzzBo.getZzxxlist().size()) {
            for (String equal : equals) {
                fxgkzzBo.getZzxxlist().remove(equal);
            }
            for (String equal : equals) {
                oldzzxxlist.remove(equal);
            }
            addzzxxlist.addAll(fxgkzzBo.getZzxxlist());
            deletezzxxlist.addAll(oldzzxxlist);

            for (String zzlx : deletezzxxlist) {
                List<String> zzlxblist1 = sgcAqfxgkZzxxMapper.listFxgkByzzlx(zzlx, gkid);
                zzlxids.addAll(zzlxblist1);
            }
            int yksrws = 0;
            //验证是否有绑定的已开始的巡检任务
            for (String gkzzids : zzlxids) {
                int count1 = sgcAqfxgkZrryMapper.selectCount(new QueryWrapper<SgcAqfxgkZrryEntity>().eq("GKZZID", gkzzids));
                if (count1 > 0) {
                    List<SgcAqfxgkZrryEntity> sgcAqfxgkZrryEntities = sgcAqfxgkZrryMapper.selectList(new QueryWrapper<SgcAqfxgkZrryEntity>().eq("GKZZID", gkzzids));
                    List<String> rylist1 = sgcAqfxgkZrryEntities.stream().map(SgcAqfxgkZrryEntity::getZrrid).collect(Collectors.toList());
                    rylist.addAll(rylist1);
                }
            }
            if (CollectionUtils.isNotEmpty(rylist)) {
                for (String zrrid : rylist) {
                    int count1 = sgcXjrwMapper.selectCount(new QueryWrapper<SgcXjrwEntity>().eq("XJR", zrrid).eq("RWZT","1"));
                    int count2 = sgcXjrwMapper.selectCount(new QueryWrapper<SgcXjrwEntity>().eq("XJR", zrrid).eq("RWZT","2"));
                    yksrws = count1 + count2;
                }
            }
            YsAssert.isTrue(yksrws < 1 ,"删除的单位中已绑定开始的巡检任务,操作失败");


            //删除
            if (CollectionUtils.isNotEmpty(deletezzxxlist)) {
                //删除管控单位
                for (String zzxx : deletezzxxlist) {
                    sgcAqfxgkZzxxMapper.delete(new QueryWrapper<SgcAqfxgkZzxxEntity>().eq("GKID", gkid).eq("ZZLXID", zzxx));
                }
                //删除管控单位下的岗位
                for (String gkzzid1 : zzlxids) {
                    int count = sgcAqfxgkZzxxMapper.selectCount(new QueryWrapper<SgcAqfxgkZzxxEntity>().eq("GKZZID", gkzzid1));
                    if (count > 0) {
                        //删除岗位
                        sgcAqfxgkZzxxMapper.delete(new QueryWrapper<SgcAqfxgkZzxxEntity>().eq("GKZZID", gkzzid1));
                    }
                    int count1 = sgcAqfxgkZrryMapper.selectCount(new QueryWrapper<SgcAqfxgkZrryEntity>().eq("GKZZID", gkzzid1));
                    if (count1 > 0) {
                        //删除岗位下绑定的人员
                        sgcAqfxgkZrryMapper.delete(new QueryWrapper<SgcAqfxgkZrryEntity>().eq("GKZZID", gkzzid1));
                    }
                    int count2 = sgcAqfxgkGkcsMapper.selectCount(new QueryWrapper<SgcAqfxgkGkcsEntity>().eq("GKZZID", gkzzid1));
                    if (count2 > 0) {
                        List<SgcAqfxgkGkcsEntity> sgcAqfxgkGkcsEntities = sgcAqfxgkGkcsMapper.selectList(new QueryWrapper<SgcAqfxgkGkcsEntity>().eq("GKZZID", gkzzid1));
                        List<String> cslist1 = sgcAqfxgkGkcsEntities.stream().map(SgcAqfxgkGkcsEntity::getGkcsid).collect(Collectors.toList());
                        //删除岗位下绑定的管控措施
                        sgcAqfxgkGkcsMapper.delete(new QueryWrapper<SgcAqfxgkGkcsEntity>().eq("GKZZID", gkzzid1));
                        cslist.addAll(cslist1);
                    }
                }

                if (CollectionUtils.isNotEmpty(rylist)) {
                    //删除人员下的巡检任务
                    for (String zrrid : rylist) {
                        int count = sgcXjrwMapper.selectCount(new QueryWrapper<SgcXjrwEntity>().eq("XJR", zrrid));
                        if (count > 0) {
                            sgcXjrwMapper.delete(new QueryWrapper<SgcXjrwEntity>().eq("XJR", zrrid));
                        }
                    }
                }

                if (CollectionUtils.isNotEmpty(cslist)) {
                    //删除巡检任务内容
                    for (String gkcsid : cslist) {
                        int count = sgcXjrwCsnrMapper.selectCount(new QueryWrapper<SgcXjrwCsnrEntity>().eq("GKCSID", gkcsid));
                        if (count > 0) {
                            sgcXjrwCsnrMapper.delete(new QueryWrapper<SgcXjrwCsnrEntity>().eq("GKCSID", gkcsid));
                        }
                    }
                }
            }


            //新增
            if (CollectionUtils.isNotEmpty(addzzxxlist)) {
                List<SgcAqfxgkZzlxbEntity> zzlxblist = new ArrayList<>();
                //新增组织单位
                for (String zzxx : addzzxxlist) {
                    List<SgcAqfxgkZzlxbEntity> zzlxblist1 = sgcAqfxgkZzlxbMapper.listByZzlxid(zzxx);
                    SgcAqfxgkZzxxEntity entity = new SgcAqfxgkZzxxEntity();
                    entity.setGkid(gkid);
                    entity.setGkzzid(UUIDUtil.uuid32());
                    entity.setZzlxid(zzxx);
                    entity.setCreatedBy(czr);
                    entity.setCreatedTime(now);
                    list.add(entity);
                    zzlxblist.addAll(zzlxblist1);
                }
                //新增组织单位下的岗位信息
                for (SgcAqfxgkZzlxbEntity sgcAqfxgkZzlxb : zzlxblist) {
                    SgcAqfxgkZzxxEntity entity1 = new SgcAqfxgkZzxxEntity();
                    entity1.setGkid(gkid);
                    entity1.setGkzzid(UUIDUtil.uuid32());
                    entity1.setZzlxid(sgcAqfxgkZzlxb.getZzlxid());
                    entity1.setCreatedBy(czr);
                    entity1.setCreatedTime(now);
                    list.add(entity1);
                }
                sgcAqfxgkZzxxMapper.batchInsert(list);
            }
            return R.ok("发布成功");
        } else {
            return R.ok(row);
        }
    }


    /**
     * 根据管控id查询管控组织信息
     *
     * @param gkid
     * @return
     */
    public R listGkzzByGkid(String gkid) {
        List<FxgkdwDto> list = sgcAqfxgkZzxxMapper.listGkzzByGkid(gkid);
        return R.ok(list);
    }


    /**
     * 新增(或编辑)风险管控措施信息
     *
     * @param fxgkcsBo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public R addFxgkcs(FxgkcsBo fxgkcsBo) throws ParseException {
        int row = 1;
        //新增管控措施
        List<String> addgkid = new ArrayList<>();
        List<String> gkzzids = new ArrayList<>();
        List<SgcAqfxgkGkcsEntity> addlist = new ArrayList<>();
        //编辑管控措施list
        List<SgcAqfxgkGkcsEntity> updatelist = new ArrayList<>();
        List<String> gkcsidlist = new ArrayList<>();
        //巡检任务实体、巡检人
        List<SgcXjrwEntity> addxjrwlist = new ArrayList<>();
        List<SgcXjrwEntity> updatexjrwlist = new ArrayList<>();
        List<String> xjrid = new ArrayList<>();
        List<String> xjrids = new ArrayList<>();
        //巡检任务内容实体
        List<SgcXjrwCsnrEntity> xjrwnrlist = new ArrayList<>();
        //操作人
        String czr = UserUtils.getUserId();
        //操作时间
        Date now = new Date();
        //管控id
        String gkid = fxgkcsBo.getGkid();

        //分别取出新增措施、编辑管控措施
        for (FxgkcsmxBo fxgkcsmxBo: fxgkcsBo.getMxlist()){
            for (FxgkcsxjBo fxgkcsxjBo : fxgkcsmxBo.getCslist()){
                if (StringUtil.isNotEmpty(fxgkcsxjBo.getGkcsmc())){
                    gkzzids.add(fxgkcsmxBo.getGkgwid());
                    if (StringUtil.isEmpty(fxgkcsxjBo.getGkcsid())){
                        SgcAqfxgkGkcsEntity entity = new SgcAqfxgkGkcsEntity();
                        entity.setGkcsid(UUIDUtil.uuid32());
                        entity.setGkcsmc(fxgkcsxjBo.getGkcsmc());
                        entity.setGkzzid(fxgkcsmxBo.getGkgwid());
                        entity.setCreatedBy(czr);
                        entity.setCreatedTime(now);
                        addlist.add(entity);
                        addgkid.add(fxgkcsmxBo.getGkgwid());
                    }
                    else {
                        SgcAqfxgkGkcsEntity entity = sgcAqfxgkGkcsMapper.selectById(fxgkcsxjBo.getGkcsid());
                        //管控措施
                        entity.setGkcsid(fxgkcsxjBo.getGkcsid());
                        entity.setGkcsmc(fxgkcsxjBo.getGkcsmc());
                        entity.setGkzzid(fxgkcsmxBo.getGkgwid());
                        entity.setUpdatedBy(czr);
                        entity.setUpdatedTime(now);
                        updatelist.add(entity);
                        gkcsidlist.add(fxgkcsxjBo.getGkcsid());
                    }
                }

            }
        }


        //删除
        List<SgcAqfxgkZzxxEntity> zzlist = sgcAqfxgkZzxxMapper.selectList(new QueryWrapper<SgcAqfxgkZzxxEntity>().eq("GKID",gkid));
        List<String> oldcslist = new ArrayList<>();
        List<String> deletecslist = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(zzlist)){

            for (SgcAqfxgkZzxxEntity entity:zzlist){
                int count = sgcAqfxgkGkcsMapper.selectCount(new QueryWrapper<SgcAqfxgkGkcsEntity>().eq("GKZZID",entity.getGkzzid()));
                if (count > 0){
                    List<SgcAqfxgkGkcsEntity> cslist = sgcAqfxgkGkcsMapper.selectList(new QueryWrapper<SgcAqfxgkGkcsEntity>().eq("GKZZID",entity.getGkzzid()));
                    for (SgcAqfxgkGkcsEntity entity1 : cslist){
                        oldcslist.add(entity1.getGkcsid());
                        deletecslist.add(entity1.getGkcsid());
                    }
                }
            }
            //获取交集
            List<String> equals = oldcslist.stream().filter(s -> gkcsidlist.contains(s)).collect(Collectors.toList());
            //获取需要删除的管控措施id
            for (String gkcsid : equals){
                deletecslist.remove(gkcsid);
            }
            List<String> oldcslist1 = new ArrayList<>();
            //去除已巡检过的管控措施id
            if (CollectionUtils.isNotEmpty(deletecslist)){
                for (String gkcsid : deletecslist){
                    String sfxj = sgcXjrwCsnrMapper.selectOne(new QueryWrapper<SgcXjrwCsnrEntity>().eq("GKCSID",gkcsid)).getSfxj();
                    if (StringUtil.isNotEmpty(sfxj) && "0".equals(sfxj)){
                        oldcslist1.add(gkcsid);
                    }
                }
            }

            //对未巡检且需删除的管控措施进行删除
            if (CollectionUtils.isNotEmpty(oldcslist1)){
                List<String> ids = new ArrayList<>();
                for (String gkcsid : oldcslist1){
                    String id = sgcXjrwCsnrMapper.selectOne(new QueryWrapper<SgcXjrwCsnrEntity>().eq("GKCSID",gkcsid)).getId();
                    ids.add(id);
                }
                sgcAqfxgkGkcsMapper.deleteBatchIds(oldcslist1);
                sgcXjrwCsnrMapper.deleteBatchIds(ids);
            }
        }


        //编辑
        //更新巡检任务
        if (CollectionUtils.isNotEmpty(gkzzids)){
            List<String> gkzzids1 = gkzzids.stream().distinct().collect(Collectors.toList());
            //获取巡检人
            for (String gkzzid : gkzzids1){
                int count = sgcAqfxgkZrryMapper.selectCount(new QueryWrapper<SgcAqfxgkZrryEntity>().eq("GKZZID",gkzzid));
                if (count > 0){
                    List<SgcAqfxgkZrryEntity> entities = sgcAqfxgkZrryMapper.selectList(new QueryWrapper<SgcAqfxgkZrryEntity>().eq("GKZZID",gkzzid));
                    for (SgcAqfxgkZrryEntity entity : entities){
                        xjrids.add(entity.getZrrid());
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(xjrids)){
                List<String> xjrids1 = xjrids.stream().filter(Objects::nonNull).collect(Collectors.toList());
                for (String xjr : xjrids1) {
                    //获取管控组织id
                    String gkzzid = sgcAqfxgkZrryMapper.selectOne(new QueryWrapper<SgcAqfxgkZrryEntity>().eq("GKID", gkid).eq("ZRRID", xjr)).getGkzzid();
                    int count = sgcXjrwMapper.selectCount(new QueryWrapper<SgcXjrwEntity>().eq("GKID", gkid).eq("XJR", xjr));
                    if (count > 0){
                        //获取该巡检人所属管控id的巡检任务实体
                        SgcXjrwEntity sgcXjrwEntity = sgcXjrwMapper.selectOne(new QueryWrapper<SgcXjrwEntity>().eq("GKID", gkid).eq("XJR", xjr));
                        //更新截止时间
                        for (FxgkcsmxBo fxgkcsmxBo: fxgkcsBo.getMxlist()){
                            if (gkzzid.equals(fxgkcsmxBo.getGkgwid())){
                                sgcXjrwEntity.setRwjzsj(fxgkcsmxBo.getRwjzsj());
                            }
                        }
                        sgcXjrwEntity.setUpdatedBy(czr);
                        sgcXjrwEntity.setUpdatedTime(now);
                        updatexjrwlist.add(sgcXjrwEntity);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(updatexjrwlist)){
                sgcXjrwMapper.updateBatch(updatexjrwlist);
            }
        }
        //更新管控措施
        if(CollectionUtils.isNotEmpty(updatelist)){
            sgcAqfxgkGkcsMapper.updateBatch(updatelist);
        }


        //新增
        if(CollectionUtils.isNotEmpty(addgkid)){
            //list去重
            List<String> addgkzzids = addgkid.stream().distinct().collect(Collectors.toList());
            //如果不存在该巡检任务则新增巡检任务
            for (String gkzzid: addgkzzids){
                SgcXjrwEntity entity = new SgcXjrwEntity();
                int count1 = sgcAqfxgkZrryMapper.selectCount(new QueryWrapper<SgcAqfxgkZrryEntity>().eq("GKZZID",gkzzid));
                if (count1 > 0){
                    String xjr = sgcAqfxgkZrryMapper.selectOne(new QueryWrapper<SgcAqfxgkZrryEntity>().eq("GKZZID",gkzzid)).getZrrid();
                    int count = sgcXjrwMapper.selectCount(new QueryWrapper<SgcXjrwEntity>().eq("GKID",gkid).eq("XJR",xjr));
                    if (count == 0){
                        //巡检任务
                        String xjrwid = UUIDUtil.uuid32();
                        entity.setXjrwid(xjrwid);
                        entity.setGkid(gkid);
                        entity.setXjr(xjr);
                        entity.setRwkssj(now);
                        for (FxgkcsmxBo fxgkcsmxBo: fxgkcsBo.getMxlist()){
                            if (gkzzid.equals(fxgkcsmxBo.getGkgwid())){
                                entity.setRwjzsj(fxgkcsmxBo.getRwjzsj());
                            }
                        }
                        entity.setRwzt("0");
                        entity.setAqfkf(0.0);
                        entity.setCreatedTime(now);
                        entity.setCreatedBy(czr);
                        addxjrwlist.add(entity);

                        //将巡检任务内容插入到新增的巡检任务中
                        for (SgcAqfxgkGkcsEntity entity1 : addlist){
                            if (gkzzid.equals(entity1.getGkzzid())){
                                SgcXjrwCsnrEntity sgcXjrwCsnrEntity = new SgcXjrwCsnrEntity();
                                sgcXjrwCsnrEntity.setId(UUIDUtil.uuid32());
                                sgcXjrwCsnrEntity.setXjrwid(xjrwid);
                                sgcXjrwCsnrEntity.setSfxj("0");
                                sgcXjrwCsnrEntity.setGkcsid(entity1.getGkcsid());
                                sgcXjrwCsnrEntity.setCreatedTime(now);
                                sgcXjrwCsnrEntity.setCreatedBy(czr);
                                xjrwnrlist.add(sgcXjrwCsnrEntity);
                            }
                        }
                    }
                    else {
                        xjrid.add(xjr);
                    }
                }
            }

            //新增已存在巡检任务下的巡检内容
            if(CollectionUtils.isNotEmpty(xjrid)){
                for (String xjr : xjrid){
                    //获取管控组织id
                    String gkzzid = sgcAqfxgkZrryMapper.selectOne(new QueryWrapper<SgcAqfxgkZrryEntity>().eq("GKID",gkid).eq("ZRRID",xjr)).getGkzzid();
                    //获取该巡检人所属管控id的巡检任务实体
                    SgcXjrwEntity sgcXjrwEntity = sgcXjrwMapper.selectOne(new QueryWrapper<SgcXjrwEntity>().eq("GKID",gkid).eq("XJR",xjr));
                    String xjrwid = sgcXjrwEntity.getXjrwid();
                    for (SgcAqfxgkGkcsEntity entity : addlist){
                        if (gkzzid.equals(entity.getGkzzid())){
                            SgcXjrwCsnrEntity sgcXjrwCsnrEntity = new SgcXjrwCsnrEntity();
                            sgcXjrwCsnrEntity.setId(UUIDUtil.uuid32());
                            sgcXjrwCsnrEntity.setXjrwid(xjrwid);
                            sgcXjrwCsnrEntity.setGkcsid(entity.getGkcsid());
                            sgcXjrwCsnrEntity.setSfxj("0");
                            sgcXjrwCsnrEntity.setCreatedTime(now);
                            sgcXjrwCsnrEntity.setCreatedBy(czr);
                            xjrwnrlist.add(sgcXjrwCsnrEntity);
                        }
                    }
                }
            }

            //新增巡检任务
            if(CollectionUtils.isNotEmpty(addxjrwlist)){
                sgcXjrwMapper.batchInsert(addxjrwlist);
            }
            //新增管控措施
            if (CollectionUtils.isNotEmpty(addlist)){
                sgcAqfxgkGkcsMapper.batchInsert(addlist);
            }
            //新增巡检内容
            if (CollectionUtils.isNotEmpty(xjrwnrlist)){
                sgcXjrwCsnrMapper.batchInsert(xjrwnrlist);
            }
        }
        return R.ok(row);
    }


    /**
     * 查询风险管控措施信息
     * @return
     */
    public R listFxgkcs(String gkid){
        List<FxgkzzdwDto> result = new ArrayList<>();
        //查询管控id下的组织单位
        List<FxgkzzdwDto> list = sgcAqfxgkZzxxMapper.listFxgkzzdw(gkid);
        for (FxgkzzdwDto dto : list){
            FxgkzzdwDto dto1 = new FxgkzzdwDto();
            List<FxgkgwDto> gwlist = new ArrayList<>();
            List<FxgkgwDto> gwlist1 = new ArrayList<>();

            dto1.setDwmc(dto.getDwmc());
            dto1.setZzdwid(dto.getZzdwid());
            dto1.setGkdwid(dto.getGkdwid());
            //查询单位下的组织岗位
            gwlist = sgcAqfxgkZzxxMapper.listFxgkzzgw(dto.getZzdwid(),gkid);
            for (FxgkgwDto fxgkgwDto: gwlist){
                FxgkgwDto dto2 = new FxgkgwDto();
                List<FxgkcsDto> list1 = new ArrayList<>();
                //查询管控id下组织单位的管控措施
                list1 = sgcAqfxgkGkcsMapper.listFxgkcs(fxgkgwDto.getZzgwid(),gkid);
                dto2.setZzgwid(fxgkgwDto.getZzgwid());
                dto2.setGwmc(fxgkgwDto.getGwmc());
                dto2.setGkgwid(fxgkgwDto.getGkgwid());
                dto2.setRwjzsj(fxgkgwDto.getRwjzsj());
                dto2.setGkcslist(list1);
                gwlist1.add(dto2);
            }
            dto1.setGkgwlist(gwlist1);
            result.add(dto1);
        }
        return R.ok(result);
    }


    /**
     * 新增(或编辑)风险管控人员信息
     * @param fxgkryBo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public R addFxgkry(FxgkryBo fxgkryBo){
        int row = 1;
        //操作人
        String czr = UserUtils.getUserId();
        //操作时间
        Date now = new Date();
        //责任人员id
        List<String> ids = new ArrayList<>();
        //岗位组织类型id
        List<String> zzgwlist = new ArrayList<>();

        List<SgcAqfxgkZrryEntity> list = new ArrayList<>();
        List<SgcAqfxgkZrryEntity> list1 = new ArrayList<>();

        //需要新增人员
        List<SgcAqfxgkZrryEntity> addrylist = new ArrayList<>();
        //需要删除人员
        List<SgcAqfxgkZrryEntity> deleterylist = new ArrayList<>();
        //管控id
        String gkid = fxgkryBo.getRylist().get(0).getGkid();
        if(!CollectionUtils.isEmpty(fxgkryBo.getRylist())) {
            for (FxgkrymsBo entity : fxgkryBo.getRylist()) {
                int count = 0;
                SgcAqfxgkZrryEntity sgcAqfxgkZrryEntity = new SgcAqfxgkZrryEntity();
                sgcAqfxgkZrryEntity.setId(UUIDUtil.uuid32());
                sgcAqfxgkZrryEntity.setGkzzid(entity.getGkzzid());
                sgcAqfxgkZrryEntity.setZrrid(entity.getZrrid());
                sgcAqfxgkZrryEntity.setGkid(entity.getGkid());
                //设置操作人、操作时间
                sgcAqfxgkZrryEntity.setCreatedBy(czr);
                sgcAqfxgkZrryEntity.setCreatedTime(now);

                //检查该组织人员是否存在
                count = sgcAqfxgkZrryMapper.selectCount(new QueryWrapper<SgcAqfxgkZrryEntity>().eq("GKZZID", entity.getGkzzid())
                                                                                               .eq("GKID", entity.getGkid())
                                                                                               .eq("ZRRID", entity.getZrrid()));
                if (count == 0) {
                    list.add(sgcAqfxgkZrryEntity);
                } else {
                    SgcAqfxgkZrryEntity sgcAqfxgkZrryEntity1 = sgcAqfxgkZrryMapper.selectOne(new QueryWrapper<SgcAqfxgkZrryEntity>().eq("GKZZID", entity.getGkzzid())
                                                                                                                                    .eq("GKID", entity.getGkid())
                                                                                                                                    .eq("ZRRID", entity.getZrrid()));
                    //新增人员在原表中存在,则用原表数据替换
                    if (sgcAqfxgkZrryEntity1.getGkzzid().equals(entity.getGkzzid())) {
                        list.add(sgcAqfxgkZrryEntity1);
                    //不存在则使用新增数据
                    } else {
                        list.add(sgcAqfxgkZrryEntity);
                    }
                    //获取原表数据
                    List<SgcAqfxgkZrryEntity> list2 = sgcAqfxgkZrryMapper.selectList(new QueryWrapper<SgcAqfxgkZrryEntity>().eq("GKID", entity.getGkid()));
                    list1.addAll(list2);
                }
            }

            //数据去重
            List<SgcAqfxgkZrryEntity> oldlist = list1.stream().distinct().collect(Collectors.toList());
            //取出传参和原表数据的交集
            List<SgcAqfxgkZrryEntity> equals = oldlist.stream().filter(s -> list.contains(s)).collect(Collectors.toList());

            //通过交集获得需要进行删除和新增的组织信息
            if (equals.size() != oldlist.size() || equals.size() != list.size()) {
                for (SgcAqfxgkZrryEntity entity : equals) {
                    oldlist.remove(entity);
                    list.remove(entity);
                }
                addrylist.addAll(list);
                deleterylist.addAll(oldlist);
                //获取删除人员的组织类型id
                for (SgcAqfxgkZrryEntity sgcAqfxgkZrryEntity : deleterylist) {
                    zzgwlist.add(sgcAqfxgkZrryEntity.getGkzzid());
                }


                //删除
                if (CollectionUtils.isNotEmpty(deleterylist)) {
                    List<String> xjrwlist = new ArrayList<>();
                    //获取可以删除的巡检任务id以及人员id
                    for (SgcAqfxgkZrryEntity entity : deleterylist) {
                        int count = sgcAqfxgkZrryMapper.selectCount(new QueryWrapper<SgcAqfxgkZrryEntity>().eq("ZRRID", entity.getZrrid()));
                        if (count > 0) {
                            List<SgcAqfxgkZrryEntity> sgcAqfxgkZrryEntity = sgcAqfxgkZrryMapper.selectList(new QueryWrapper<SgcAqfxgkZrryEntity>().eq("ZRRID", entity.getZrrid())
                                                                                                                                                  .eq("GKID", gkid)
                                                                                                                                                  .eq("GKZZID", entity.getGkzzid()));
                            for (SgcAqfxgkZrryEntity sgcAqfxgkZrryEntity1 : sgcAqfxgkZrryEntity) {
                                List<SgcXjrwEntity> sgcXjrwEntities = sgcXjrwMapper.selectList(new QueryWrapper<SgcXjrwEntity>().eq("GKID",sgcAqfxgkZrryEntity1.getGkid())

                                                                                                                                .eq("XJR",sgcAqfxgkZrryEntity1.getZrrid()));
                                //巡检任务未开始才能被删除
                                for (SgcXjrwEntity sgcXjrwEntity : sgcXjrwEntities){
                                    if (sgcXjrwEntity.getRwzt().equals("0")){
                                        xjrwlist.add(sgcXjrwEntity.getXjrwid());
                                        ids.add(entity.getId());
                                    }
                                }

                            }
                        }
                    }

                    //删除管控人员对应的未开始的巡检任务
                    if(CollectionUtils.isNotEmpty(xjrwlist)){
                        for (String xjrw : xjrwlist){
                            sgcXjrwCsnrMapper.delete(new QueryWrapper<SgcXjrwCsnrEntity>().eq("XJRWID",xjrw));
                        }
                        sgcXjrwMapper.deleteBatchIds(xjrwlist);
                    }
                    //删除管控人员
                    if (CollectionUtils.isNotEmpty(ids)){
                        sgcAqfxgkZrryMapper.deleteBatchIds(ids);
                    }
                }


                //新增
                if (CollectionUtils.isNotEmpty(addrylist)) {
                    sgcAqfxgkZrryMapper.batchInsert(addrylist);
                }
            }
            return R.ok("发布成功");
        }
        else {
            return R.ok(row);
        }
    }


    /**
     * 查询风险管控人员信息
     * @param gkid
     * @return
     */
    public R listFxgkry(String gkid){
        List<FxgkzzdwDto> result = new ArrayList<>();
        //查询管控id下的组织单位
        List<FxgkzzdwDto> list = sgcAqfxgkZzxxMapper.listFxgkzzdw(gkid);
        for (FxgkzzdwDto dto : list){
            FxgkzzdwDto dto1 = new FxgkzzdwDto();
            List<FxgkgwDto> gwlist = new ArrayList<>();
            List<FxgkgwDto> gwlist1 = new ArrayList<>();
            dto1.setDwmc(dto.getDwmc());
            dto1.setZzdwid(dto.getZzdwid());
            dto1.setGkdwid(dto.getGkdwid());
            //查询单位下的组织岗位
            gwlist = sgcAqfxgkZzxxMapper.listFxgkzzgw(dto.getZzdwid(),gkid);
            for (FxgkgwDto fxgkgwDto: gwlist){
                FxgkgwDto dto2 = new FxgkgwDto();
                List<FxgkryDto> list1 = new ArrayList<>();
                //查询管控id下组织单位的管控措施
                list1 = sgcAqfxgkZrryMapper.listFxgkry(fxgkgwDto.getZzgwid(),gkid);
                dto2.setZzgwid(fxgkgwDto.getZzgwid());
                dto2.setGwmc(fxgkgwDto.getGwmc());
                dto2.setGkgwid(fxgkgwDto.getGkgwid());
                dto2.setGkrylist(list1);
                gwlist1.add(dto2);
            }
            dto1.setGkgwlist(gwlist1);
            result.add(dto1);
        }
        return R.ok(result);
    }


    /**
     * 查询履职情况
     * @param fxgklzqkBo
     * @return
     */
    public R listLzqkgl( FxgklzqkBo fxgklzqkBo){
        //当为传入组织类型,则查询所有岗位的履职信息
        if (StringUtil.isEmpty(fxgklzqkBo.getZzlxid())){
            List<FxgklzqkDto> list = new ArrayList<>();
            //获取岗位信息、监督措施
            List<FxgklzqkDto> list2 = sgcAqfxgkGkcsMapper.listLzqkgw(fxgklzqkBo);
            List<FxgklzqkmxDto> list1 = sgcAqfxgkGkcsMapper.listjdcs(fxgklzqkBo);
            for (FxgklzqkDto fxgklzqkDto : list2){
                FxgklzqkDto dto = new FxgklzqkDto();
                List<String> jdcs = new ArrayList<>();
                String gkzzid = fxgklzqkDto.getGkzzid();
                dto.setDh(fxgklzqkDto.getDh());
                dto.setGkid(fxgklzqkDto.getGkid());
                dto.setGkzzid(gkzzid);
                dto.setRwkssj(fxgklzqkDto.getRwkssj());
                dto.setRwjzsj(fxgklzqkDto.getRwjzsj());
                dto.setXjrwid(fxgklzqkDto.getXjrwid());
                dto.setXm(fxgklzqkDto.getXm());
                dto.setRwzt(fxgklzqkDto.getRwzt());
                dto.setZrdw(fxgklzqkDto.getZrdw());
                dto.setZw(fxgklzqkDto.getZw());
                dto.setZzlxid(fxgklzqkDto.getZzlxid());
                for (FxgklzqkmxDto fxgklzqkmxDto : list1){
                    if (StringUtil.isNotEmpty(fxgklzqkmxDto.getJdcs()) && fxgklzqkmxDto.getGkgwid().equals(gkzzid)){
                        jdcs.add(fxgklzqkmxDto.getJdcs());
                    }
                }
                List<String> jdcss = jdcs.stream().distinct().collect(Collectors.toList());
                dto.setJdcslist(jdcss);
                list.add(dto);
            }
            return R.ok(list);
        }else {
            List<FxgklzqkDto> list = new ArrayList<>();
            //查询传入的组织类型级别
            SgcAqfxgkZzlxbEntity sgcAqfxgkZzlxb = sgcAqfxgkZzlxbMapper.selectOne(new QueryWrapper<SgcAqfxgkZzlxbEntity>().eq("ZZLXID",fxgklzqkBo.getZzlxid()));
            String jb = sgcAqfxgkZzlxb.getJb();
            //获得该组织类型下的所有岗位信息
            List<FxgkgwDto> gwlist =sgcAqfxgkZzxxMapper.listFxgkzzgw(fxgklzqkBo.getZzlxid(), fxgklzqkBo.getGkid());
            //传入的组织类型为单位
            if (jb.equals("1")){
                for (FxgkgwDto fxgkgwDto : gwlist){
                    String zzgwid = fxgkgwDto.getZzgwid();
                    fxgklzqkBo.setZzlxid(zzgwid);
                    //各岗位的履职情况
                    List<FxgklzqkDto> list2 = sgcAqfxgkGkcsMapper.listLzqkgw(fxgklzqkBo);
                    //监督措施
                    List<FxgklzqkmxDto> list1 = sgcAqfxgkGkcsMapper.listjdcs(fxgklzqkBo);
                    for (FxgklzqkDto fxgklzqkDto : list2){
                        FxgklzqkDto dto = new FxgklzqkDto();
                        List<String> jdcs = new ArrayList<>();
                        String gkzzid = fxgklzqkDto.getGkzzid();
                        dto.setDh(fxgklzqkDto.getDh());
                        dto.setGkid(fxgklzqkDto.getGkid());
                        dto.setGkzzid(gkzzid);
                        dto.setRwkssj(fxgklzqkDto.getRwkssj());
                        dto.setRwjzsj(fxgklzqkDto.getRwjzsj());
                        dto.setXjrwid(fxgklzqkDto.getXjrwid());
                        dto.setXm(fxgklzqkDto.getXm());
                        dto.setRwzt(fxgklzqkDto.getRwzt());
                        dto.setZrdw(fxgklzqkDto.getZrdw());
                        dto.setZw(fxgklzqkDto.getZw());
                        dto.setZzlxid(fxgklzqkDto.getZzlxid());
                        for (FxgklzqkmxDto fxgklzqkmxDto : list1){
                            if (StringUtil.isNotEmpty(fxgklzqkmxDto.getJdcs()) && fxgklzqkmxDto.getGkgwid().equals(gkzzid)){
                                jdcs.add(fxgklzqkmxDto.getJdcs());
                            }
                        }
                        List<String> jdcss = jdcs.stream().distinct().collect(Collectors.toList());
                        dto.setJdcslist(jdcss);
                        list.add(dto);
                    }
                }
                return R.ok(list);
            }
            //传入的组织类型为岗位
            else if(jb.equals("2")){
                List<FxgklzqkDto> list2 = sgcAqfxgkGkcsMapper.listLzqkgw(fxgklzqkBo);
                //获取监督措施
                List<FxgklzqkmxDto> list1 = sgcAqfxgkGkcsMapper.listjdcs(fxgklzqkBo);
                for (FxgklzqkDto fxgklzqkDto : list2){
                    FxgklzqkDto dto = new FxgklzqkDto();
                    List<String> jdcs = new ArrayList<>();
                    String gkzzid = fxgklzqkDto.getGkzzid();
                    dto.setDh(fxgklzqkDto.getDh());
                    dto.setGkid(fxgklzqkDto.getGkid());
                    dto.setGkzzid(gkzzid);
                    dto.setRwkssj(fxgklzqkDto.getRwkssj());
                    dto.setRwjzsj(fxgklzqkDto.getRwjzsj());
                    dto.setXjrwid(fxgklzqkDto.getXjrwid());
                    dto.setXm(fxgklzqkDto.getXm());
                    dto.setRwzt(fxgklzqkDto.getRwzt());
                    dto.setZrdw(fxgklzqkDto.getZrdw());
                    dto.setZw(fxgklzqkDto.getZw());
                    dto.setZzlxid(fxgklzqkDto.getZzlxid());
                    for (FxgklzqkmxDto fxgklzqkmxDto : list1){
                        if (StringUtil.isNotEmpty(fxgklzqkmxDto.getJdcs()) && fxgklzqkmxDto.getGkgwid().equals(gkzzid)){
                            jdcs.add(fxgklzqkmxDto.getJdcs());
                        }
                    }
                    List<String> jdcss = jdcs.stream().distinct().collect(Collectors.toList());
                    dto.setJdcslist(jdcss);
                    list.add(dto);
                }
            }return R.ok(list);
        }
    }


    /**
     * 履职审核
     * @param xjrwid
     * @param gkzzid
     * @return
     */
    public R listLzqk(String xjrwid,String gkzzid){
        List<FxgkLzpgDto> jdcs = sgcAqfxgkGkcsMapper.listLzqkxq(xjrwid, gkzzid);
        List<FxgkLzpgDto> jdcs1 = jdcs.stream().filter(fxgkLzpgDto -> fxgkLzpgDto.getGkcsmc()!=null).collect(Collectors.toList());
        return R.ok(jdcs1);
    }

}