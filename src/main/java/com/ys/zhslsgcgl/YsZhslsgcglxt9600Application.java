package com.ys.zhslsgcgl;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.FilterType;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

@SpringBootApplication
@MapperScan("com.ys.zhslsgcgl.mapper.**")
@ComponentScans(@ComponentScan(basePackages = { "com.netsdk.demo.module","com.netsdk.utils"},includeFilters = {
    // 仅仅使用了 @Component 注解声明的类
    @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = { Component.class })
}, useDefaultFilters = false))
@EnableScheduling
public class YsZhslsgcglxt9600Application {

    public static void main(String[] args) {
        try{
        SpringApplication.run(YsZhslsgcglxt9600Application.class, args);
    }catch(Exception e) {
        e.printStackTrace();
    }
    }

}
