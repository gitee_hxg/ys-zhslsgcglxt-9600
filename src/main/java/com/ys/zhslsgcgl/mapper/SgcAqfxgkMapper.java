package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.bo.PageFxgkBo;
import com.ys.zhslsgcgl.dto.AppFxgkDto;
import com.ys.zhslsgcgl.entity.SgcAqfxgkEntity;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SgcAqfxgkMapper extends BaseMapper<SgcAqfxgkEntity> {
    int batchInsert(@Param("list") List<SgcAqfxgkEntity> list);

    /**
     * 查询风险管控主表
     * @param fxgkBo
     * @return
     */
    List<SgcAqfxgkEntity> listFxgk(PageFxgkBo fxgkBo);
}