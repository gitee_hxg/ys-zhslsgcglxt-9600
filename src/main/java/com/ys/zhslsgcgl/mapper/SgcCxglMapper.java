package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.bo.DwxxBo;
import com.ys.zhslsgcgl.dto.CxglDto;
import com.ys.zhslsgcgl.dto.SxbgDto;
import com.ys.zhslsgcgl.entity.SgcDwglEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SgcCxglMapper extends BaseMapper<SgcDwglEntity> {
    /**
     * 分页查询单位信息
     *
     * @param dwxxBo
     * @return
     */
    List<CxglDto> queryPageCxgl(DwxxBo dwxxBo);

    /**
     * 分页查询单位信息黑名单
     *
     * @param dwxxBo
     * @return
     */
    List<CxglDto> queryPageCxglHmd(DwxxBo dwxxBo);

    /**
     * 分页查询单位信息重点关注
     *
     * @param dwxxBo
     * @return
     */
    List<CxglDto> queryPageCxglZdgz(DwxxBo dwxxBo);

    /**
     * 分页查询单位信息失信曝光
     *
     * @param dwxxBo
     * @return
     */
    List<SxbgDto> queryPageCxglSxbg(DwxxBo dwxxBo);




}
