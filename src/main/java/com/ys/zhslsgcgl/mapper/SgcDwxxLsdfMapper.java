package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.bo.PageDwLsdfBo;import com.ys.zhslsgcgl.dto.DwlsdfjlDto;
import com.ys.zhslsgcgl.entity.SgcDwxxLsdfEntity;
import java.util.List;
import com.ys.zhslsgcgl.entity.SgcDwxxLsdfEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SgcDwxxLsdfMapper extends BaseMapper<SgcDwxxLsdfEntity> {
    int batchInsert(@Param("list") List<SgcDwxxLsdfEntity> list);

    /**
     * 分页查询单位信息历史得分
     *
     * @param dwLsdfBo
     * @return
     */
    List<SgcDwxxLsdfEntity> queryPageLsdf(PageDwLsdfBo dwLsdfBo);

    /**
     * 查询计算单位历史得分记录
     *
     * @return
     */
    List<DwlsdfjlDto> listDwmtdfqk();
}