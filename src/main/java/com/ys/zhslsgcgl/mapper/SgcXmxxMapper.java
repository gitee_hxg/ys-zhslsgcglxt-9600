package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.dto.TzqkDto;
import com.ys.zhslsgcgl.dto.XmfbDto;
import com.ys.zhslsgcgl.dto.XmzlDto;
import com.ys.zhslsgcgl.dto.ZjxmDto;
import com.ys.zhslsgcgl.entity.SgcXmxxEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SgcXmxxMapper extends BaseMapper<SgcXmxxEntity> {

    /**
     * 查询项目总览
     * @return
     */
    XmzlDto getXmzl();

    /**
     * 查询项目分布
     */
    XmfbDto getXmfb();

    /**
     * 查询投资情况
     * @return
     */
    TzqkDto getTzqk();

    /**
     * 查询在建项目情况
     * @return
     */
    ZjxmDto getZjxm();
    /**
     * 查询项目类型字典值
     * @return
     */
    String selectXmlxzdz(@Param("xmlx") String xmlx);

    /**
     * 查询行政区划类型字典值
     *
     * @param xzqh
     * @return
     */
    String selectXzqhxzdz(@Param("xzqh")String xzqh);
}