package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.entity.SgcSbbdGlbEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SgcSbbdGlbMapper extends BaseMapper<SgcSbbdGlbEntity> {
    int batchInsert(@Param("list") List<SgcSbbdGlbEntity> list);

    /**
     * 根据设备ID删除绑定的标段
     * @param sbid
     * @return
     */
    int deleteBySbid(@Param("sbid") String sbid);
}