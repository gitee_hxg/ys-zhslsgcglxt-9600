package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.entity.SgcAqfxgkZzlxbEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SgcAqfxgkZzlxbMapper extends BaseMapper<SgcAqfxgkZzlxbEntity> {
    /**
     * 根据组织类型id查询组织类型详情
     * @param zzlxid
     * @return
     */
    List<SgcAqfxgkZzlxbEntity> listByZzlxid(@Param("zzlxid") String zzlxid);
}