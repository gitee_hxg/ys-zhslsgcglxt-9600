package com.ys.zhslsgcgl.mapper;

import com.ys.common.mybaitsplus.mapper.YsMapper;
import com.ys.zhslsgcgl.entity.SgcRykqDayEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgcRykqDayMapper extends YsMapper<SgcRykqDayEntity> {

    /**
     * 生成当前时间前一天未打卡的考勤数据
     * @return
     */
    int insertSgcRykqDay();

    /**
     * 生成每月考勤汇总记录
     * @return
     */
    int insertSgcRykqMonth();

}