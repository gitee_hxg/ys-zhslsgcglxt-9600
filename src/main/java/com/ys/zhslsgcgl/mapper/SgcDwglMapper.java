package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.bo.DwxxBo;
import com.ys.zhslsgcgl.dto.DwxxDto;
import com.ys.zhslsgcgl.entity.SgcDwglEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SgcDwglMapper extends BaseMapper<SgcDwglEntity> {

    /**
     * 分页查询单位信息
     * @param dwxxBo
     * @return
     */
    List<DwxxDto> queryPageDwxx(DwxxBo dwxxBo);

    /**
     * 查询单位下拉信息
     * @return
     */
    List<Map<String,String>> listDwxx();

    /*通过标段信息查找中标单位列表
     *@param bdid
     */
    List<SgcDwglEntity> listDwxxByBb(@Param("bdid") String bdid);
}