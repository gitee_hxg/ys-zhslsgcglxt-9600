package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.bo.XjrwQueryPageBo;
import com.ys.zhslsgcgl.dto.XjrwDto;
import com.ys.zhslsgcgl.dto.XjrwgcnrDto;
import com.ys.zhslsgcgl.dto.XjrwxqDto;
import com.ys.zhslsgcgl.entity.SgcAqfxgkGkcsEntity;
import com.ys.zhslsgcgl.entity.SgcAqfxgkZrryEntity;
import com.ys.zhslsgcgl.entity.SgcXjrwEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SgcXjrwMapper extends BaseMapper<SgcXjrwEntity> {
    int batchInsert(@Param("list") List<SgcXjrwEntity> list);

    int updateBatch(List<SgcXjrwEntity> list);

    /**
     * 分页查询巡检任务
     * @param xjrwQueryPageBo
     * @return
     */
    List<XjrwDto> queryPageXjrw(XjrwQueryPageBo xjrwQueryPageBo);

    /**
     * 根据巡检任务ID查询巡检详情
     * @param xjrwid
     * @return
     */
    XjrwxqDto getXjrwXq(@Param("xjrwid") String xjrwid);

    /**
     *根据巡检任务ID查询巡检内容措施列表
     * @param xjrwid
     * @return
     */
    List<XjrwgcnrDto> listAqfxgkGkcsByXjrwid(@Param("xjrwid") String xjrwid);
}