package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.dto.FxgkryDto;
import com.ys.zhslsgcgl.entity.SgcAqfxgkZrryEntity;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SgcAqfxgkZrryMapper extends BaseMapper<SgcAqfxgkZrryEntity> {
    int batchInsert(@Param("list") List<SgcAqfxgkZrryEntity> list);

    /**
     * 根据管控组织id查询管控人员
     * @param zzgwid,gkid
     * @return
     */
    List<FxgkryDto> listFxgkry(@Param("zzgwid") String zzgwid,@Param("gkid") String gkid);
}