package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.bo.PageRylbBo;
import com.ys.zhslsgcgl.bo.RykqayhzBo;
import com.ys.zhslsgcgl.bo.XmxxBo;
import com.ys.zhslsgcgl.dto.*;
import com.ys.zhslsgcgl.entity.SgcRyglEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SgcRyglMapper extends BaseMapper<SgcRyglEntity> {

    /**
     * 分页查询人员
     * @param pageRylbBo 人员信息分页查询参数
     * @return List<RyglPageQueryDto>
     */
    List<PageRylbDto> queryPageRygl(PageRylbBo pageRylbBo);

    /**
     * 根据项目id和标段id查询人员信息
     * @param xmid
     * @param bdid
     * @param ygxm
     * @return
     */
    List<PageRylbDto> listRyxxByXmidOrBdid(@Param("xmid") String xmid, @Param("bdid") String bdid, @Param("ygxm") String ygxm);

    /**
     * 根据单位分页查询人员信息
     * @param pageRylbBo
     * @return
     */
    List<PageRylbDto> queryPageRyByDwid(PageRylbBo pageRylbBo);

    /**
     * 搜索面板查询项目列表
     * @param xmxxBo
     * @return
     */
    List<PageXmxxDto> pageXmlb(XmxxBo xmxxBo);

    /**
     * 查询项目信息下拉数据
     * @return
     */
    List<Map<String,String>> listXmxx();

    /**
     * 查询标段信息下拉数据
     * @param xmid
     * @return
     */
    List<Map<String,String>> listBdxx(@Param("xmid") String xmid);

    /**
     * 查询单位信息下拉数据
     * @return
     */
    List<Map<String,String>> listDwxx(@Param("bdid") String bdid);

    /**
     * 按月查询人员实际到岗天数
     * @param rq
     * @return
     */
    List<RyKqxxDto> getRyKqtsByNy(@Param("rq")String rq);

    /**
     * 按月查询人员考勤天数
     * @param rq
     * @return
     */
    Integer getKqtsByRq(@Param("rq")String rq);

    /**
     * 查询当天打卡人员记录图片
     * @return
     */
    List<RyJrdkjlDto> getRyJrdkjl();

    /**
     * 根据人员id和月份查询考勤记录
     * @param ygid
     * @param rq
     * @param sfdk 是否打卡  0：否，1：是
     * @return
     */
    List<RykqDayDto> listRyKqjlByMonth(@Param("ygid")String ygid, @Param("rq")String rq, @Param("sfdk")Integer sfdk);

    /**
     * 查询员工人脸信息图片url
     * @return
     */
    String getUrl(@Param("ygid") String ygid);

    /**
     * 根据项目ID查询人员信息
     *
     * @param xmid
     * @return
     */
    List<QueryPageBdryByXmidDto> listBdryByXmid(@Param("xmid")String xmid);

    /**
     * 计算月份内单位下的所有人员考勤扣分
     * @param dwid
     * @param rq
     * @return
     */
    List<DwLsdfDto> listDwKqkfByMonth(@Param("dwid") String dwid, @Param("rq") String rq);

    /**
     * 分页查询人员考勤按月汇总
     * @param rykqayhzBo
     * @return
     */
    List<RykqayhzDto> queryPageRykqayhz(RykqayhzBo rykqayhzBo);
}