package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.dto.PfkfmxDto;
import com.ys.zhslsgcgl.entity.SgcPfkfMxbEntity;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
* @Description: 评分扣分明细表Mapper
* @Author: chenxingjian
* CreateDate: 2022/11/11 11:01
* @Version: 1.0
* Copyright (c)2022,武汉中地云申科技有限公司
* All rights reserved.
**/

@Mapper
public interface SgcPfkfMxbMapper extends BaseMapper<SgcPfkfMxbEntity> {
    /**
     * 批量新增
     * @param list
     * @return
     */
    int batchInsert(@Param("list") List<SgcPfkfMxbEntity> list);

    /**
     * 查询评分扣分明细
     * @param pfkfid
     * @return
     */
    List<PfkfmxDto> listPfkfmx(@Param("pfkfid") String pfkfid);


}