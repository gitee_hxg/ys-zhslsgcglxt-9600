package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.bo.PfbzBo;
import com.ys.zhslsgcgl.dto.PagePfbzDto;
import com.ys.zhslsgcgl.entity.SgcPfbzEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SgcPfbzMapper extends BaseMapper<SgcPfbzEntity> {

    /**
     * 分页查询评分标准
     * @param pfbzBo
     * @return
     */
    List<PagePfbzDto> queryPagePfbz(PfbzBo pfbzBo);
}