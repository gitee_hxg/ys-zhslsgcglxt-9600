package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.bo.PfjfBo;
import com.ys.zhslsgcgl.bo.PfjfShxxBo;
import com.ys.zhslsgcgl.dto.PfjfCsxxDto;
import com.ys.zhslsgcgl.dto.PfjfDto;
import com.ys.zhslsgcgl.dto.PfjfFsxxDto;
import com.ys.zhslsgcgl.dto.PfjfShDto;
import com.ys.zhslsgcgl.entity.SgcPfjfEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SgcPfjfMapper extends BaseMapper<SgcPfjfEntity> {

    /**
     * 分页查询加分申请列表
     *
     * @param pfjfBo
     * @return
     */
    List<PfjfDto> queryPagePfjf(PfjfBo pfjfBo);

    /**
     * 根据pfjfid查询申请信息和审核信息
     *
     * @param pfjfid
     * @return
     */
    PfjfShDto queryPfjfById(String pfjfid);

    /**
     * 查询初审信息
     *
     * @param pfjfShxxBo
     * @return
     */
    PfjfCsxxDto queryPfjfCsxx(PfjfShxxBo pfjfShxxBo);

    /**
     * 查询复审信息
     *
     * @param pfjfShxxBo
     * @return
     */
    PfjfFsxxDto queryPfjfFsxx(PfjfShxxBo pfjfShxxBo);


}