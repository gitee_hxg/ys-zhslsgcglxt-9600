package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.entity.SgcZdgzEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SgcZdgzMapper extends BaseMapper<SgcZdgzEntity> {

}