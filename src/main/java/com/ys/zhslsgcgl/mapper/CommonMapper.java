package com.ys.zhslsgcgl.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Description:公共Mapper
 * @Author: wugangzhi
 * @CreateDate: 2022/11/6 15:43
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Mapper
public interface CommonMapper {
    /**
     * 获取字典值
     *
     * @param tableParas
     * @return
     */
    List<Map<String, Object>> listZdByParam(@Param("tableParas") String tableParas);

    /**
     * 根据角色id查询用户
     *
     * @param jsid
     * @return
     */
    List<Map<String,String>> selectYhByJsid(@Param("jsid")String jsid);
}
