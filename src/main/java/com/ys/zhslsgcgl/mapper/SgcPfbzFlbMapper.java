package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.entity.SgcPfbzFlbEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface SgcPfbzFlbMapper extends BaseMapper<SgcPfbzFlbEntity> {
    /**
     * 大分类-详细分类下拉查询
     * @param flid
     * @return
     */
    List<SgcPfbzFlbEntity> listDflXxfl(@Param("flid") String flid);
}