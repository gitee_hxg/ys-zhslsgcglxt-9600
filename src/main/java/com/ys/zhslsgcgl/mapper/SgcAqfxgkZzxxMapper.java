package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.dto.FxgkdwDto;
import com.ys.zhslsgcgl.dto.FxgkgwDto;
import com.ys.zhslsgcgl.dto.FxgkzzdwDto;
import com.ys.zhslsgcgl.entity.SgcAqfxgkZzxxEntity;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SgcAqfxgkZzxxMapper extends BaseMapper<SgcAqfxgkZzxxEntity> {
    int batchInsert(@Param("list") List<SgcAqfxgkZzxxEntity> list);

    /**
     * 根据管控id查询管控组织单位
     * @param gkid
     * @return
     */
    List<FxgkzzdwDto> listFxgkzzdw(@Param("gkid") String gkid);

    /**
     * 查询风险管控组织岗位
     * @param zzlxpid
     * @param gkid
     * @return
     */
    List<FxgkgwDto> listFxgkzzgw(@Param("zzlxpid") String zzlxpid,@Param("gkid") String gkid);

    /**
     * 根据管控id查询管控组织信息
     * @param gkid
     * @return
     */
    List<FxgkdwDto> listGkzzByGkid(@Param("gkid") String gkid);

    /**
     * 根据组织类型查询风险管控组织id
     * @param zzlxid
     * @param gkid
     * @return
     */
    List<String> listFxgkByzzlx(@Param("zzlxid") String zzlxid,@Param("gkid") String gkid);
}