package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.entity.SgcRykqjlEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;
import java.util.List;

@Mapper
public interface SgcRykqjlMapper extends BaseMapper<SgcRykqjlEntity> {

    /**
     * 根据日期和员工信息查询考勤记录
     * @param ygid
     * @param sbid
     * @param yesterday
     */
    List<SgcRykqjlEntity> listKqjlByRqAndYgxx(@Param("ygid") String ygid, @Param("sbid") String sbid, @Param("yesterday") LocalDate yesterday);
}