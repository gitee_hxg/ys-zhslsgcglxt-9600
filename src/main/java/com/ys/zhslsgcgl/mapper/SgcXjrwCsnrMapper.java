package com.ys.zhslsgcgl.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.entity.SgcXjrwCsnrEntity;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SgcXjrwCsnrMapper extends BaseMapper<SgcXjrwCsnrEntity> {
    int updateBatch(List<SgcXjrwCsnrEntity> list);

    int batchInsert(@Param("list") List<SgcXjrwCsnrEntity> list);

    /**
     * 根据巡检任务ID删除巡检措施内容
     * @param xjrwid
     * @return
     */
    int deleteByXjrwid(@Param("xjrwid") String xjrwid);
}