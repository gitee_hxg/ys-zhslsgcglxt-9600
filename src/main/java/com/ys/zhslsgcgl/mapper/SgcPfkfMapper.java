package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.bo.PagekfglBo;
import com.ys.zhslsgcgl.dto.PfkfDto;
import com.ys.zhslsgcgl.dto.PfkfmxDto;
import com.ys.zhslsgcgl.entity.SgcPfkfEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SgcPfkfMapper extends BaseMapper<SgcPfkfEntity> {
    /**
     * 分页查询扣分管理信息
     * @param pagekfglBo
     * @return
     */
    List<PfkfDto> queryPagePfkf(PagekfglBo pagekfglBo);

}