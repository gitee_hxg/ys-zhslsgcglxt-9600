package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.bo.FxgkcsmxBo;
import com.ys.zhslsgcgl.bo.FxgklzqkBo;
import com.ys.zhslsgcgl.dto.FxgkLzpgDto;
import com.ys.zhslsgcgl.dto.FxgkcsDto;
import com.ys.zhslsgcgl.dto.FxgklzqkDto;
import com.ys.zhslsgcgl.dto.FxgklzqkmxDto;
import com.ys.zhslsgcgl.entity.SgcAqfxgkGkcsEntity;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SgcAqfxgkGkcsMapper extends BaseMapper<SgcAqfxgkGkcsEntity> {
    int batchInsert(@Param("list") List<SgcAqfxgkGkcsEntity> list);

    int updateBatch(List<SgcAqfxgkGkcsEntity> list);
    /**
     * 根据组织类型id查询管控措施
     * @param zzlxid
     * @return
     */
    List<FxgkcsDto> listFxgkcs(@Param("zzlxid") String zzlxid,@Param("gkid")String gkid);

    /**
     * 根据姓名查找用户id
     * @param cellzb90
     * @return
     */
    Map<String,String> selectYhidByName(@Param("name")String cellzb90);

    /**
     * 查询履职情况
     * @param fxgklzqkBo
     * @return
     */
    List<FxgklzqkDto> listLzqkgw(FxgklzqkBo fxgklzqkBo);

    /**
     * 查询履职审核
     * @param xjrwid
     * @param gkzzid
     * @return
     */
    List<FxgkLzpgDto> listLzqkxq(@Param("xjrwid")String xjrwid,@Param("gkzzid")String gkzzid);

    /**
     * 查询监督措施
     * @param fxgklzqkBo
     * @return
     */
    List<FxgklzqkmxDto> listjdcs(FxgklzqkBo fxgklzqkBo);
}