package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.bo.SbryxxBo;
import com.ys.zhslsgcgl.bo.SbxxBo;
import com.ys.zhslsgcgl.dto.BdsbglDto;
import com.ys.zhslsgcgl.dto.SbryxxDto;
import com.ys.zhslsgcgl.dto.SbxxDto;
import com.ys.zhslsgcgl.entity.SgcSbxxEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SgcSbxxMapper extends BaseMapper<SgcSbxxEntity> {
    /**
     * 分页查询设备信息
     * @param sbxxBo
     * @return
     */
    List<SbxxDto> queryPageSbxx(SbxxBo sbxxBo);

    /**
     * 根据设备Id查询绑定的设备信息
     * @param sbid
     * @return
     */
    List<BdsbglDto> listBdxxBySbid(@Param("sbid") String sbid);

    /**
     * 分页查询设备人员
     * @param sbryxxBo
     * @return
     */
    List<SbryxxDto> queryPageSbry(SbryxxBo sbryxxBo);
}