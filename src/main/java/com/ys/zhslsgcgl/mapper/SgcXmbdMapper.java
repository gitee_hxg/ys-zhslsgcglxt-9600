package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.entity.SgcXmbdEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgcXmbdMapper extends BaseMapper<SgcXmbdEntity> {
}