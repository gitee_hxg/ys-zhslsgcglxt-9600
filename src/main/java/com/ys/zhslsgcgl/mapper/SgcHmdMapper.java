package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.entity.SgcHmdEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgcHmdMapper extends BaseMapper<SgcHmdEntity> {
}