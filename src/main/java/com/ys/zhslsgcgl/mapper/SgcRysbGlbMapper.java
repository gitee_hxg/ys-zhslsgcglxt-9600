package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.entity.SgcRysbGlbEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgcRysbGlbMapper extends BaseMapper<SgcRysbGlbEntity> {
}