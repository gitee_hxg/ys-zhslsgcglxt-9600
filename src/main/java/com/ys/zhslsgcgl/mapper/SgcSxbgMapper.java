package com.ys.zhslsgcgl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ys.zhslsgcgl.entity.SgcSxbgEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgcSxbgMapper extends BaseMapper<SgcSxbgEntity> {
}