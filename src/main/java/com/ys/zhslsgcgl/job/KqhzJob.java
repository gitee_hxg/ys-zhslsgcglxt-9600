package com.ys.zhslsgcgl.job;

import cn.hutool.core.collection.CollectionUtil;
import com.netsdk.demo.module.DahuaApp;
import com.netsdk.demo.module.DeviceCache;
import com.ys.zhslsgcgl.dto.DwlsdfjlDto;
import com.ys.zhslsgcgl.entity.SgcDwxxLsdfEntity;
import com.ys.zhslsgcgl.mapper.SgcDwxxLsdfMapper;
import com.ys.zhslsgcgl.mapper.SgcRykqDayMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Description: 考勤汇总定时任务
 * @Author: wugangzhi
 * @CreateDate: 2022/11/6 15:43
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Component
@Slf4j
public class KqhzJob {

    /**
     * 人员每天考勤汇总Mapper
     */
    @Autowired
    private SgcRykqDayMapper rykqDayMapper;

    /**
     * 计算每天单位得分汇总Mapper
     */
    @Autowired
    private SgcDwxxLsdfMapper dwxxLsdfMapper;


    @Autowired
    private DahuaApp dahuaApp;

    /**
     * 生成当前时间前一天未打卡的考勤数据
     */
/*
    @Scheduled(cron = "0 0 1 * * ?")
    @Transactional(rollbackFor = Exception.class)
    public void attendanceSummaryJob() {
        try {
            rykqDayMapper.insertSgcRykqDay();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
*/

    /**
     * 生成每月考勤汇总记录(每月凌晨1点执行)
     */
    @Scheduled(cron = "0 0 1 1 * ?")
    @Transactional(rollbackFor = Exception.class)
    public void insertSgcRykqMonth() {
        try {
            rykqDayMapper.insertSgcRykqMonth();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }

    /**
     * 生成计算每天单位得分汇总
     */
    @Scheduled(cron = "0 0 1 * * ?")
    @Transactional(rollbackFor = Exception.class)
    public void insertSgcDwmtdfqk() {
        try {
            SgcDwxxLsdfEntity entity=null;
            List<SgcDwxxLsdfEntity> lsdfLit=new ArrayList<>();
            //计算每天单位的得分情况
            List<DwlsdfjlDto> list=dwxxLsdfMapper.listDwmtdfqk();
          if(CollectionUtil.isNotEmpty(list)){
               for(DwlsdfjlDto dto:list){
                   entity=new SgcDwxxLsdfEntity();
                   //评分加分
                   if(dto.getJf()>dto.getScjf()){
                       entity.setJf(dto.getJf()+dto.getScjf());
                   }

                   //检查记分
                   if(dto.getJcjf()>dto.getScjcjf()){
                       entity.setJcjf(dto.getJcjf()+dto.getScjcjf());
                   }

                   //考勤记分
                   if(dto.getKqjf()>dto.getSckqjf()){
                       entity.setKqjf(dto.getKqjf()+dto.getSckqjf());
                   }
                   //单位ID
                   entity.setDwid(dto.getDwid());

                   //日期
                   entity.setRq(dto.getRq());
                   //计算总得分
                   entity.setZzdf(dto.getSczzdf()+dto.getJf()-dto.getJcjf()-dto.getKqjf());
                   entity.setCreatedBy("admin");
                   entity.setCreatedTime(new Date());
                   lsdfLit.add(entity);

               }
          }
          //批量插入每天单位历史得分
          if(CollectionUtil.isNotEmpty(lsdfLit)){
              dwxxLsdfMapper.batchInsert(lsdfLit);
          }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }

    /**
     * 考勤补卡
     */
     @Scheduled(cron = "0 30 23 * * ?")
     @Transactional(rollbackFor = Exception.class)
    public void getAllkqjlJob() {
        System.out.println("------------考勤补卡执行开始");
         /*for (String key : DeviceCache.deviceInfoMap.keySet()) {
            dahuaApp.QueryRecordByTime(key,DeviceCache.deviceInfoMap.get(key).m_hLoginHandle);
        }*/
         for (String key : DeviceCache.deviceInfoMap.keySet()) {
             dahuaApp.findAccessRecordByTime(DeviceCache.deviceInfoMap.get(key).m_hLoginHandle,key);
         }
        //findAccessRecordByTime
        System.out.println("------------考勤补卡执行结束");

     }

}
