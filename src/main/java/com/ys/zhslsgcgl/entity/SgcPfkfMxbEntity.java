package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.Data;

/**
* @Description: TODO
* @Author: chenxingjian
* CreateDate: 2022/11/11 11:01
* @Version: 1.0
* Copyright (c)2022,武汉中地云申科技有限公司
* All rights reserved.
**/

/**
    * 评分扣分表
    */
@ApiModel(value="评分扣分表")
@Data
@TableName(value = "sgc_pfkf_mxb")
public class SgcPfkfMxbEntity implements Serializable {
    /**
     * 评分扣分明细ID
     */
    @TableId(value = "PFKFMXID", type = IdType.INPUT)
    @ApiModelProperty(value="评分扣分明细ID")
    private String pfkfmxid;

    /**
     * 评分扣分ID
     */
    @TableField(value = "PFKFID")
    @ApiModelProperty(value="评分扣分ID")
    private String pfkfid;

    /**
     * 评分标准ID
     */
    @TableField(value = "PFBZID")
    @ApiModelProperty(value="评分标准ID")
    private String pfbzid;

    /**
     * 次数
     */
    @TableField(value = "CS")
    @ApiModelProperty(value="次数")
    private Integer cs;

    /**
     * 创建人
     */
    @TableField(value = "CREATED_BY")
    @ApiModelProperty(value="创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME")
    @ApiModelProperty(value="创建时间")
    private Date createdTime;

    private static final long serialVersionUID = 1L;
}