package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "sgc_sbbd_glb")
public class SgcSbbdGlbEntity implements Serializable {
    /**
     * 主键ID
     */
    @TableField(value = "ID")
    @ApiModelProperty(value = "主键ID")
    private String id;

    /**
     * 设备ID
     */
    @TableField(value = "SBID")
    @ApiModelProperty(value = "设备ID")
    private String sbid;

    /**
     * 标段ID
     */
    @TableField(value = "BDID")
    @ApiModelProperty(value = "标段ID")
    private String bdid;

    /**
     * 创建人
     */
    @TableField(value = "CREATED_BY")
    @ApiModelProperty(value = "创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME")
    @ApiModelProperty(value = "创建时间")
    private Date createdTime;

    /**
     * 更新人
     */
    @TableField(value = "UPDATED_BY")
    @ApiModelProperty(value = "更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(value = "UPDATED_TIME")
    @ApiModelProperty(value = "更新时间")
    private Date updatedTime;

    private static final long serialVersionUID = 1L;
}