package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName(value = "sgc_pfjf_shzt")
public class SgcPfjfShztEntity implements Serializable {
    /**
     * 审核状态ID
     */
    @TableId(value = "SHZTID", type = IdType.INPUT)
    @ApiModelProperty(value = "审核状态ID")
    private String shztid;

    /**
     * 评分加分ID
     */
    @TableField(value = "PFJFID")
    @ApiModelProperty(value = "评分加分ID",required = true)
    private String pfjfid;

    /**
     * 质量飞检情况 (0-飞检不合格 1-飞检合格)
     */
    @TableField(value = "ZLFJQK")
    @ApiModelProperty(value = "质量飞检情况 (0-飞检不合格 1-飞检合格)",required = true)
    private String zlfjqk;

    /**
     * 事故管理 (对应字典值)
     */
    @TableField(value = "SGGL")
    @ApiModelProperty(value = "事故管理 (对应字典值)",required = true)
    private String sggl;

    /**
     * 流程类型 (A-县级初审 B-市级复审)
     */
    @TableField(value = "LCLX")
    @ApiModelProperty(value = "流程类型 (A-县级初审 B-市级复审)")
    private String lclx;

    /**
     * 审核状态 (0-待审核  1-审核中(县级审批等待市级审批)  2-已审核 )
     */
    @TableField(value = "SHZT")
    @ApiModelProperty(value = "审核状态 (0-待审核  1-审核中(县级审批等待市级审批)  2-已审核 )")
    private String shzt;

    /**
     * 审核意见 (A-初审同意 B初审不同意 C-复审同意 D-复审不同意)
     */
    @TableField(value = "SHYJ")
    @ApiModelProperty(value = "审核意见 (A-初审同意 B初审不同意 C-复审同意 D-复审不同意)",required = true)
    private String shyj;

    /**
     * 审核备注
     */
    @TableField(value = "SHBZ")
    @ApiModelProperty(value = "审核备注")
    private String shbz;

    /**
     * 审核单位
     */
    @TableField(value = "SHDW")
    @ApiModelProperty(value = "审核单位",required = true)
    private String shdw;

    /**
     * 审核人
     */
    @TableField(value = "SHRY")
    @ApiModelProperty(value = "审核人员",required = true)
    private String shry;

    /**
     * 审批内容
     */
    @TableField(value = "SPNR")
    @ApiModelProperty(value = "审批内容")
    private String spnr;

    /**
     * 审核时间
     */
    @TableField(value = "SHSJ")
    @ApiModelProperty(value = "审核时间",required = true)
    private Date shsj;

    private static final long serialVersionUID = 1L;
}