package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "sgc_xmbd")
public class SgcXmbdEntity implements Serializable {
    /**
     * 标段ID
     */
    @TableId(value = "BDID", type = IdType.INPUT)
    @ApiModelProperty(value = "标段ID")
    private String bdid;

    /**
     * 标段名称
     */
    @TableField(value = "BDMC")
    @ApiModelProperty(value = "标段名称")
    private String bdmc;

    /**
     * 标段描述
     */
    @TableField(value = "BDMS")
    @ApiModelProperty(value = "标段描述")
    private String bdms;

    /**
     * 建设状态:(0-在建工程 1-完工待验收 2-完工已验收)
     */
    @TableField(value = "JSZT")
    @ApiModelProperty(value = "建设状态:(0-在建工程 1-完工待验收 2-完工已验收)")
    private String jszt;

    /**
     * 标段金额(万元)
     */
    @TableField(value = "BDJE")
    @ApiModelProperty(value = "标段金额(万元)")
    private BigDecimal bdje;

    /**
     * 项目ID
     */
    @TableField(value = "XMID")
    @ApiModelProperty(value = "项目ID")
    private String xmid;

    /**
     * 创建人
     */
    @TableField(value = "CREATED_BY")
    @ApiModelProperty(value = "创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME")
    @ApiModelProperty(value = "创建时间")
    private Date createdTime;

    /**
     * 更新人
     */
    @TableField(value = "UPDATED_BY")
    @ApiModelProperty(value = "更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(value = "UPDATED_TIME")
    @ApiModelProperty(value = "更新时间")
    private Date updatedTime;

    private static final long serialVersionUID = 1L;
}