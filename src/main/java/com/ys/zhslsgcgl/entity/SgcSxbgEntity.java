package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "sgc_sxbg")
public class SgcSxbgEntity implements Serializable {
    /**
     * 曝光ID
     */
    @TableId(value = "PBID", type = IdType.INPUT)
    @ApiModelProperty(value="曝光ID")
    private String pbid;

    /**
     * 单位ID
     */
    @TableField(value = "DWID")
    @ApiModelProperty(value="单位ID")
    private String dwid;

    /**
     * 标题
     */
    @TableField(value = "BT")
    @ApiModelProperty(value="标题")
    private String bt;

    /**
     * 外部连接
     */
    @TableField(value = "WBLL")
    @ApiModelProperty(value="外部连接")
    private String wbll;

    /**
     * 简介
     */
    @TableField(value = "JJ")
    @ApiModelProperty(value="简介")
    private String jj;

    /**
     * 内容
     */
    @TableField(value = "NR")
    @ApiModelProperty(value="内容")
    private String nr;

    /**
     * 创建人
     */
    @TableField(value = "CREATED_BY")
    @ApiModelProperty(value="创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME")
    @ApiModelProperty(value="创建时间")
    private Date createdTime;

    /**
     * 更新人
     */
    @TableField(value = "UPDATED_BY")
    @ApiModelProperty(value="更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(value = "UPDATED_TIME")
    @ApiModelProperty(value="更新时间")
    private Date updatedTime;

    private static final long serialVersionUID = 1L;
}