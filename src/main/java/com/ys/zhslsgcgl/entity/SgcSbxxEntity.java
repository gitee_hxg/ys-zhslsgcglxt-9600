package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "sgc_sbxx")
public class SgcSbxxEntity implements Serializable {
    /**
     * 设备序列号ID
     */
    @TableField(value = "ID")
    @ApiModelProperty(value = "设备序列号ID")
    private String id;

    /**
     * 注册设备的IP
     */
    @TableField(value = "IPADDRESS")
    @ApiModelProperty(value = "注册设备的IP")
    private String ipaddress;

    /**
     * 注册设备的端口
     */
    @TableField(value = "PORT")
    @ApiModelProperty(value = "注册设备的端口")
    private Integer port;

    /**
     * 注册设备的用户名
     */
    @TableField(value = "USERNAME")
    @ApiModelProperty(value = "注册设备的用户名")
    private String username;

    /**
     * 注册设备的密码
     */
    @TableField(value = "PASSWORD")
    @ApiModelProperty(value = "注册设备的密码")
    private String password;

    /**
     * 创建人
     */
    @TableField(value = "CREATED_BY")
    @ApiModelProperty(value = "创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME")
    @ApiModelProperty(value = "创建时间")
    private Date createdTime;

    private static final long serialVersionUID = 1L;
}