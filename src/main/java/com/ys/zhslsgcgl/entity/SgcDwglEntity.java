package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName(value = "sgc_dwgl")
@Validated
public class SgcDwglEntity implements Serializable {
    /**
     * 单位ID
     */
    @TableId(value = "DWID", type = IdType.INPUT)
    @ApiModelProperty(value="单位ID")
    private String dwid;

    /**
     * 单位名称
     */
    @TableField(value = "DWMC")
    @ApiModelProperty(value="单位名称",required = true)
    @NotBlank(message = "单位名称不能为空")
    private String dwmc;

    /**
     * 单位类别 (对应字典值:1-业主单位 2-设计单位 3-监理单位 4-施工单位 5-质检单位)
     */
    @TableField(value = "DWLB")
    @ApiModelProperty(value="单位类别 (对应字典值:1-业主单位 2-设计单位 3-监理单位 4-施工单位 5-质检单位)")
    @NotBlank(message = "单位类别不能为空")
    private String dwlb;


    /**
     * 营业执照
     */
    @TableField(value = "YYZZ")
    @ApiModelProperty(value="营业执照")
    private String yyzz;

    /**
     * 联系人
     */
    @TableField(value = "LXR")
    @ApiModelProperty(value="联系人")
    private String lxr;

    /**
     * 联系电话
     */
    @TableField(value = "LXDH")
    @ApiModelProperty(value="联系电话")
    private String lxdh;

    /**
     * 所属行政区划编码
     */
    @TableField(value = "XZQHDM")
    @ApiModelProperty(value="所属行政区划编码")
    private String xzqhdm;

    /**
     * 单位地址
     */
    @TableField(value = "DWDZ")
    @ApiModelProperty(value="单位地址")
    private String dwdz;

    /**
     * 法人代表
     */
    @TableField(value = "FRDB")
    @ApiModelProperty(value="法人代表")
    private String frdb;

    /**
     * 公司介绍
     */
    @TableField(value = "GSJS")
    @ApiModelProperty(value="公司介绍")
    private String gsjs;

    /**
     * 公司状态 (0-黑名单 1-正常 2-重点关注)
     */
    @TableField(value = "GSZT")
    @ApiModelProperty(value="公司状态 (0-黑名单 1-正常 2-重点关注)")
    private String gszt;

    /**
     * 得分
     */
    @TableField(value = "DF")
    @ApiModelProperty(value="得分")
    private BigDecimal df;

    /**
     * 创建人
     */
    @TableField(value = "CREATED_BY")
    @ApiModelProperty(value="创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME")
    @ApiModelProperty(value="创建时间")
    private Date createdTime;

    /**
     * 更新人
     */
    @TableField(value = "UPDATED_BY")
    @ApiModelProperty(value="更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(value = "UPDATED_TIME")
    @ApiModelProperty(value="更新时间")
    private Date updatedTime;

    private static final long serialVersionUID = 1L;


}