package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "sgc_pfkf")
public class SgcPfkfEntity implements Serializable {
    /**
     * 评分扣分ID
     */
    @TableId(value = "PFKFID", type = IdType.INPUT)
    @ApiModelProperty(value="评分扣分ID")
    private String pfkfid;

    /**
     * 项目ID
     */
    @TableField(value = "XMID")
    @ApiModelProperty(value="项目ID")
    private String xmid;

    /**
     * 标段ID
     */
    @TableField(value = "BDID")
    @ApiModelProperty(value="标段ID")
    private String bdid;

    /**
     * 单位ID
     */
    @TableField(value = "DWID")
    @ApiModelProperty(value="单位ID")
    private String dwid;

    /**
     * 评分时间
     */
    @TableField(value = "PFSJ")
    @ApiModelProperty(value="评分时间")
    private Date pfsj;

    /**
     * 评分失效时间
     */
    @TableField(value = "PFSXSJ")
    @ApiModelProperty(value="评分失效时间")
    private Date pfsxsj;

    /**
     * 检查人
     */
    @TableField(value = "JRX")
    @ApiModelProperty(value="检查人")
    private String jrx;

    /**
     * 总扣分值
     */
    @TableField(value = "ZKFZ")
    @ApiModelProperty(value="总扣分值")
    private BigDecimal zkfz;

    /**
     * 一般问题次数
     */
    @TableField(value = "YBWTCS")
    @ApiModelProperty(value="一般问题次数")
    private Integer ybwtcs;

    /**
     * 较重问题次数
     */
    @TableField(value = "JZWTCS")
    @ApiModelProperty(value="较重问题次数")
    private Integer jzwtcs;

    /**
     * 严重问题次数
     */
    @TableField(value = "YZWTCS")
    @ApiModelProperty(value="严重问题次数")
    private Integer yzwtcs;

    /**
     * 备注
     */
    @TableField(value = "BZ")
    @ApiModelProperty(value="备注")
    private String bz;

    /**
     * 创建人
     */
    @TableField(value = "CREATED_BY")
    @ApiModelProperty(value="创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME")
    @ApiModelProperty(value="创建时间")
    private Date createdTime;

    /**
     * 更新人
     */
    @TableField(value = "UPDATED_BY")
    @ApiModelProperty(value="更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(value = "UPDATED_TIME")
    @ApiModelProperty(value="更新时间")
    private Date updatedTime;

    private static final long serialVersionUID = 1L;
}