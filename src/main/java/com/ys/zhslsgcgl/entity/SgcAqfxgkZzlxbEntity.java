package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.Data;

@ApiModel(value="风险管控组织类型表")
@Data
@TableName(value = "sgc_aqfxgk_zzlxb")
public class SgcAqfxgkZzlxbEntity implements Serializable {
    /**
     * 组织类型ID
     */
    @TableId(value = "ZZLXID", type = IdType.INPUT)
    @ApiModelProperty(value="组织类型ID")
    private String zzlxid;

    /**
     * 组织类型名称
     */
    @TableField(value = "ZZLXMC")
    @ApiModelProperty(value="组织类型名称")
    private String zzlxmc;

    /**
     * 组织类型父ID
     */
    @TableField(value = "ZZLXPID")
    @ApiModelProperty(value="组织类型父ID")
    private String zzlxpid;

    /**
     * 级别 (1-责任单位 2-岗位)
     */
    @TableField(value = "JB")
    @ApiModelProperty(value="级别 (1-责任单位 2-岗位)")
    private String jb;

    /**
     * 排序值
     */
    @TableField(value = "PXZ")
    @ApiModelProperty(value="排序值")
    private Integer pxz;

    private static final long serialVersionUID = 1L;
}