package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "sgc_kqjjr")
public class SgcKqjjrEntity implements Serializable {
    /**
     * 日期
     */
    @TableId(value = "RQ", type = IdType.INPUT)
    @ApiModelProperty(value="日期")
    private Date rq;

    /**
     * 状态 (0-休息日 1-上班日)
     */
    @TableField(value = "ZT")
    @ApiModelProperty(value="状态 (0-休息日 1-上班日)")
    private String zt;

    /**
     * 备注
     */
    @TableField(value = "BZ")
    @ApiModelProperty(value="备注")
    private String bz;

    /**
     * 创建人
     */
    @TableField(value = "CREATED_BY")
    @ApiModelProperty(value="创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME")
    @ApiModelProperty(value="创建时间")
    private Date createdTime;

    private static final long serialVersionUID = 1L;
}