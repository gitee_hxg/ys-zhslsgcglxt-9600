package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.Data;

@ApiModel(value="安全分级风险管控组织信息")
@Data
@TableName(value = "sgc_aqfxgk_zzxx")
public class SgcAqfxgkZzxxEntity implements Serializable {
    /**
     * 管控组织ID
     */
    @TableId(value = "GKZZID", type = IdType.INPUT)
    @ApiModelProperty(value="管控组织ID")
    private String gkzzid;

    /**
     * 管控组织名称
     */
    @TableField(value = "ZZLXID")
    @ApiModelProperty(value="组织类型ID")
    private String zzlxid;


    /**
     * 管控ID
     */
    @TableField(value = "GKID")
    @ApiModelProperty(value="管控ID")
    private String gkid;


    /**
     * 创建人
     */
    @TableField(value = "CREATED_BY")
    @ApiModelProperty(value="创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME")
    @ApiModelProperty(value="创建时间")
    private Date createdTime;

    /**
     * 更新人
     */
    @TableField(value = "UPDATED_BY")
    @ApiModelProperty(value="更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(value = "UPDATED_TIME")
    @ApiModelProperty(value="更新时间")
    private Date updatedTime;

    private static final long serialVersionUID = 1L;
}