package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName(value = "sgc_rygl")
public class SgcRyglEntity implements Serializable {
    /**
     * 员工ID
     */
    @TableId(value = "YGID", type = IdType.INPUT)
    @ApiModelProperty(value="员工ID")
    private String ygid;

    /**
     * 员工姓名
     */
    @TableField(value = "YGXM")
    @ApiModelProperty(value="员工姓名")
    private String ygxm;

    /**
     * 工号
     */
    @TableField(value = "GH")
    @ApiModelProperty(value="工号")
    private String gh;

    /**
     * 性别 (0-女 1-男)
     */
    @TableField(value = "XB")
    @ApiModelProperty(value="性别 (0-女 1-男)")
    private String xb;

    /**
     * 身份证号码
     */
    @TableField(value = "SFZHM")
    @ApiModelProperty(value="身份证号码")
    private String sfzhm;

    /**
     * 手机号
     */
    @TableField(value = "SJH")
    @ApiModelProperty(value="手机号")
    private String sjh;

    /**
     * 职位 (对应字典表值：A-项目经理 B-技术负责人 C-安全员 D-质检员 E-施工员 F-总监理工程师 G-专业监理工程师 H-监理员)
     */
    @TableField(value = "ZW")
    @ApiModelProperty(value="职位 (对应字典表值：A-项目经理 B-技术负责人 C-安全员 D-质检员 E-施工员 F-总监理工程师 G-专业监理工程师 H-监理员)")
    private String zw;

    /**
     * 员工类型 (对应字典表值：A-关键岗位人员 B-特殊工种人员 C-危大作业人员 D-劳务人员)
     */
    @TableField(value = "YGLX")
    @ApiModelProperty(value="员工类型 (对应字典表值：A-关键岗位人员 B-特殊工种人员 C-危大作业人员 D-劳务人员)")
    private String yglx;

    /**
     * 所属单位ID
     */
    @TableField(value = "DWID")
    @ApiModelProperty(value="所属单位ID")
    private String dwid;

    /**
     * 所属部门名称
     */
    @TableField(value = "SSBM")
    @ApiModelProperty(value="所属部门名称")
    private String ssbm;

    /**
     * 标段ID
     */
    @TableField(value = "BDID")
    @ApiModelProperty(value="标段ID")
    private String bdid;

    /**
     * 设备ID
     */
   /* @TableField(value = "SBID")
    @ApiModelProperty(value="设备ID")
    private String sbid;*/

    /**
     * 创建人
     */
    @TableField(value = "CREATED_BY")
    @ApiModelProperty(value="创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME")
    @ApiModelProperty(value="创建时间")
    private Date createdTime;

    /**
     * 更新人
     */
    @TableField(value = "UPDATED_BY")
    @ApiModelProperty(value="更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(value = "UPDATED_TIME")
    @ApiModelProperty(value="更新时间")
    private Date updatedTime;

    private static final long serialVersionUID = 1L;
}