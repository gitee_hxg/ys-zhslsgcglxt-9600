package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.Data;

@ApiModel(value="安全分析管控措施表")
@Data
@TableName(value = "sgc_aqfxgk_gkcs")
public class SgcAqfxgkGkcsEntity implements Serializable {
    /**
     * 管控措施ID
     */
    @TableId(value = "GKCSID", type = IdType.INPUT)
    @ApiModelProperty(value="管控措施ID")
    private String gkcsid;

    /**
     * 管控措施名称
     */
    @TableField(value = "GKCSMC")
    @ApiModelProperty(value="管控措施名称")
    private String gkcsmc;

    /**
     * 管控措施组织ID(对应岗位)
     */
    @TableField(value = "GKZZID")
    @ApiModelProperty(value="管控措施组织ID(对应岗位)")
    private String gkzzid;

    /**
     * 排序值
     */
    @TableField(value = "PXZ")
    @ApiModelProperty(value="排序值")
    private Integer pxz;

    /**
     * 创建人
     */
    @TableField(value = "CREATED_BY")
    @ApiModelProperty(value="创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME")
    @ApiModelProperty(value="创建时间")
    private Date createdTime;

    /**
     * 更新人
     */
    @TableField(value = "UPDATED_BY")
    @ApiModelProperty(value="更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(value = "UPDATED_TIME")
    @ApiModelProperty(value="更新时间")
    private Date updatedTime;

    private static final long serialVersionUID = 1L;
}