package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
@TableName(value = "sgc_pfjf_shjl")
public class SgcPfjfShjlEntity implements Serializable {
    /**
     * 审核记录ID
     */
    @TableId(value = "SHJLID", type = IdType.INPUT)
    @ApiModelProperty(value = "审核记录ID")
    private String shjlid;

    /**
     * 评分加分ID
     */
    @TableField(value = "PFJFID")
    @ApiModelProperty(value = "评分加分ID")
    private String pfjfid;

    /**
     * 流程类型 (A-县级初审 B-市级复审)
     */
    @TableField(value = "LCLX")
    @ApiModelProperty(value = "流程类型 (A-县级初审 B-市级复审)")
    private String lclx;

    /**
     * 审核状态 (0-待审核  1-审核中(县级审批等待市级审批)  2-已审核 )
     */
    @TableField(value = "SHZT")
    @ApiModelProperty(value = "审核状态 (0-待审核  1-审核中(县级审批等待市级审批)  2-已审核 )")
    private String shzt;

    /**
     * 审核意见 (A-初审同意 B初审不同意 C-复审同意 D-复审不同意)
     */
    @TableField(value = "SHYJ")
    @ApiModelProperty(value = "审核意见 (A-初审同意 B初审不同意 C-复审同意 D-复审不同意)")
    private String shyj = "";

    /**
     * 审核备注
     */
    @TableField(value = "SHBZ")
    @ApiModelProperty(value = "审核备注")
    private String shbz = "";

    /**
     * 审核单位
     */
    @TableField(value = "SHDW")
    @ApiModelProperty(value = "审核单位")
    private String shdw = "";

    /**
     * 审核人员
     */
    @TableField(value = "SHRY")
    @ApiModelProperty(value = "审核人员")
    private String shry = "";

    /**
     * 审核内容
     */
    @TableField(value = "SPNR")
    @ApiModelProperty(value = "审核内容")
    private String spnr = "";

    /**
     * 审核时间
     */
    @TableField(value = "SHSJ")
    @ApiModelProperty(value = "审核时间")
    private Date shsj;

    private static final long serialVersionUID = 1L;
}