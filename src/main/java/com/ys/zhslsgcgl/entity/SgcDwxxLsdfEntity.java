package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.Data;

/**
    * 单位历史得分记录表
    */
@Data
@TableName(value = "sgc_dwxx_lsdf")
public class SgcDwxxLsdfEntity {
    /**
     * 单位id
     */
    @TableId(value = "DWID", type = IdType.INPUT)
    @ApiModelProperty(value="单位id")
    private String dwid;

    /**
     * 日期
     */
    @TableField(value = "RQ")
    @ApiModelProperty(value="日期")
    private LocalDate rq;

    /**
     * 加分
     */
    @TableField(value = "JF")
    @ApiModelProperty(value="加分")
    private double jf;

    /**
     * 检查记分
     */
    @TableField(value = "JCJF")
    @ApiModelProperty(value="检查记分")
    private double jcjf;

    /**
     * 考勤记分
     */
    @TableField(value = "KQJF")
    @ApiModelProperty(value="考勤记分")
    private double kqjf;

    /**
     * 最终得分
     */
    @TableField(value = "ZZDF")
    @ApiModelProperty(value="最终得分")
    private double zzdf;

    /**
     * 创建人
     */
    @TableField(value = "CREATED_BY")
    @ApiModelProperty(value="创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME")
    @ApiModelProperty(value = "创建时间(发布时间)")
    private Date createdTime;

}