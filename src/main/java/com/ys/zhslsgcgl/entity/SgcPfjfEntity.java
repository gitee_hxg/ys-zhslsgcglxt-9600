package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName(value = "sgc_pfjf")
public class SgcPfjfEntity implements Serializable {
    /**
     * 评分加分ID
     */
    @TableId(value = "PFJFID", type = IdType.INPUT)
    @ApiModelProperty(value="评分加分ID",required = true)
    private String pfjfid;

    /**
     * 标题
     */
    @TableField(value = "BT")
    @ApiModelProperty(value="标题",required = true)
    private String bt;

    /**
     * 内容
     */
    @TableField(value = "NR")
    @ApiModelProperty(value="内容",required = true)
    private String nr;

    /**
     * 单位ID
     */
    @TableField(value = "DWID")
    @ApiModelProperty(value="单位ID",required = true)
    private String dwid;

    /**
     * 项目ID
     */
    @TableField(value = "XMID")
    @ApiModelProperty(value="项目ID",required = true)
    private String xmid;

    /**
     * 标段ID
     */
    @TableField(value = "BDID")
    @ApiModelProperty(value="标段ID",required = true)
    private String bdid;

    /**
     * 联系人
     */
    @TableField(value = "LXR")
    @ApiModelProperty(value="联系人",required = true)
    private String lxr;
    /**
     * 联系电话
     */
    @TableField(value = "LXDH")
    @ApiModelProperty(value="联系电话",required = true)
    private String lxdh;

    /**
     * 加分分数
     */
    @TableField(value = "JFFS")
    @ApiModelProperty(value="加分分数",required = true)
    private double jffs;


    /**
     * 创建人
     */
    @TableField(value = "CREATED_BY")
    @ApiModelProperty(value="创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME")
    @ApiModelProperty(value="创建时间")
    private Date createdTime;

    /**
     * 更新人
     */
    @TableField(value = "UPDATED_BY")
    @ApiModelProperty(value="更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(value = "UPDATED_TIME")
    @ApiModelProperty(value="更新时间")
    private Date updatedTime;

    private static final long serialVersionUID = 1L;
}