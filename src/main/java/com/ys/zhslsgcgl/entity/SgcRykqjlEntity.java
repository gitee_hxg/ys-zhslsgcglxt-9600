package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@ApiModel(value="com.ys.zhslsgcgl.entity.SgcRykqjlEntity")
@Data
@TableName(value = "sgc_rykqjl")
public class SgcRykqjlEntity implements Serializable {
    /**
     * 主键ID
     */
    @TableId(value = "JLID", type = IdType.INPUT)
    @ApiModelProperty(value="主键ID")
    private String jlid;

    /**
     * 员工ID
     */
    @TableField(value = "YGID")
    @ApiModelProperty(value="员工ID")
    private String ygid;

    /**
     * 打卡时间
     */
    @TableField(value = "DKSJ")
    @ApiModelProperty(value="打卡时间")
    private Date dksj;

    /**
     * 设备ID
     */
    @TableField(value = "SBID")
    @ApiModelProperty(value="设备ID")
    private String sbid;

    /**
     * 事件类型(1-进门 2-出门)
     */
    @TableField(value = "SJLX")
    @ApiModelProperty(value="事件类型(1-进门 2-出门)")
    private String sjlx;

    private static final long serialVersionUID = 1L;
}