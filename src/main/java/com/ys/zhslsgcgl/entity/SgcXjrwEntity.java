package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "sgc_xjrw")
public class SgcXjrwEntity implements Serializable {
    /**
     * 巡检任务ID
     */
    @TableId(value = "XJRWID", type = IdType.INPUT)
    @ApiModelProperty(value = "巡检任务ID")
    private String xjrwid;

    /**
     * 管控ID
     */
    @TableField(value = "GKID")
    @ApiModelProperty(value = "管控ID")
    private String gkid;

    /**
     * 巡检人ID
     */
    @TableField(value = "XJR")
    @ApiModelProperty(value = "巡检人ID")
    private String xjr;

    /**
     * 巡检天气
     */
    @TableField(value = "XJTQ")
    @ApiModelProperty(value = "巡检天气")
    private String xjtq;

    /**
     * 履职情况
     */
    @TableField(value = "LZQK")
    @ApiModelProperty(value = "履职情况")
    private String lzqk;

    /**
     * 任务计划开始时间
     */
    @TableField(value = "RWKSSJ")
    @ApiModelProperty(value = "任务计划开始时间(yyyy-MM-dd)")
    private Date rwkssj;

    /**
     * 任务截止时间
     */
    @TableField(value = "RWJZSJ")
    @ApiModelProperty(value = "任务截止时间(yyyy-MM-dd)")
    private Date rwjzsj;

    /**
     * 任务实际开始时间 (yyyy-MM-dd HH:mm:ss)
     */
    @TableField(value = "SJKSSJ")
    @ApiModelProperty(value = "任务实际开始时间 (yyyy-MM-dd HH:mm:ss)")
    private Date sjkssj;

    /**
     * 任务实际完成时间 (yyyy-MM-dd HH:mm:ss)
     */
    @TableField(value = "SJWCSJ")
    @ApiModelProperty(value = "任务实际完成时间 (yyyy-MM-dd HH:mm:ss)")
    private Date sjwcsj;

    /**
     * 任务状态 (0-未开始 1-巡检中 2-已完成)
     */
    @TableField(value = "RWZT")
    @ApiModelProperty(value = "任务状态 (0-未开始 1-巡检中 2-已完成)")
    private String rwzt;


    /**
     * 安全分扣分
     */
    @TableField(value = "AQFKF")
    @ApiModelProperty(value = "安全分扣分")
    private double aqfkf;

    /**
     * 督查人员
     */
    @TableField(value = "DCRY")
    @ApiModelProperty(value = "督查人员")
    private String dcry;

    /**
     * 督查备注
     */
    @TableField(value = "BZ")
    @ApiModelProperty(value = "备注")
    private String bz;

    @TableField(value = "POINTS")
    @ApiModelProperty(value = "巡检轨迹坐标点(多组用分号分隔)")
    private String points;

    /**
     * 创建人
     */
    @TableField(value = "CREATED_BY")
    @ApiModelProperty(value = "创建人")
    private String createdBy;

    /**
     * 创建时间(发布时间)
     */
    @TableField(value = "CREATED_TIME")
    @ApiModelProperty(value = "创建时间(发布时间)")
    private Date createdTime;

    /**
     * 更新人
     */
    @TableField(value = "UPDATED_BY")
    @ApiModelProperty(value = "更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(value = "UPDATED_TIME")
    @ApiModelProperty(value = "更新时间")
    private Date updatedTime;

    private static final long serialVersionUID = 1L;
}