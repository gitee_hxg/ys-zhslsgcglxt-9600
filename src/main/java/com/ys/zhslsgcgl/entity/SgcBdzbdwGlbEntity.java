package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "sgc_bdzbdw_glb")
public class SgcBdzbdwGlbEntity implements Serializable {
    /**
     * 主键ID
     */
    @TableId(value = "ID", type = IdType.INPUT)
    @ApiModelProperty(value="主键ID")
    private String id;

    /**
     * 标段ID
     */
    @TableField(value = "BDID")
    @ApiModelProperty(value="标段ID")
    private String bdid;

    /**
     * 中标单位ID
     */
    @TableField(value = "DWID")
    @ApiModelProperty(value="中标单位ID")
    private String dwid;

    /**
     * 法人代表
     */
    @TableField(value = "FRDB")
    @ApiModelProperty(value="法人代表")
    private String frdb;

    /**
     * 联系电话
     */
    @TableField(value = "LXDH")
    @ApiModelProperty(value="联系电话")
    private String lxdh;


    /**
     * 总得分
     */
    @TableField(value = "ZDF")
    @ApiModelProperty(value="总得分")
    private BigDecimal zdf;

    /**
     * 创建人
     */
    @TableField(value = "CREATED_BY")
    @ApiModelProperty(value="创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME")
    @ApiModelProperty(value="创建时间")
    private Date createdTime;

    /**
     * 更新人
     */
    @TableField(value = "UPDATED_BY")
    @ApiModelProperty(value="更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(value = "UPDATED_TIME")
    @ApiModelProperty(value="更新时间")
    private Date updatedTime;

    private static final long serialVersionUID = 1L;
}