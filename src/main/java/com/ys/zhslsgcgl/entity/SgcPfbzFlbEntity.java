package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.Data;

@ApiModel(value="评分标准分类信息表")
@Data
@TableName(value = "sgc_pfbz_flb")
public class SgcPfbzFlbEntity implements Serializable {
    /**
     * 分类ID
     */
    @TableField(value = "FLID")
    @ApiModelProperty(value="分类ID")
    private String flid;

    /**
     * 分类名称
     */
    @TableField(value = "FLMC")
    @ApiModelProperty(value="分类名称")
    private String flmc;

    /**
     * 父分类ID
     */
    @TableField(value = "PFLID")
    @ApiModelProperty(value="父分类ID")
    private String pflid;

    /**
     * 排序值
     */
    @TableField(value = "PXZ")
    @ApiModelProperty(value="排序值")
    private Integer pxz;

    /**
     * 级别 (1-大分类 2-小分类)
     */
    @TableField(value = "JB")
    @ApiModelProperty(value="级别 (1-大分类 2-小分类)")
    private Integer jb;

    private static final long serialVersionUID = 1L;
}