package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.regex.Pattern;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

@Data
@TableName(value = "sgc_xmxx")
public class SgcXmxxEntity implements Serializable {
    /**
     * 项目ID
     */
    @TableId(value = "XMID", type = IdType.INPUT)
    @ApiModelProperty(value = "项目ID")
    private String xmid;

    /**
     * 项目名称
     */
    @TableField(value = "XMMC")
    @ApiModelProperty(value = "项目名称")
    private String xmmc;

    /**
     * 批复投资(万元)
     */
    @TableField(value = "PFTZ")
    @ApiModelProperty(value = "批复投资(万元)")
    private BigDecimal pftz;

    /**
     * 项目法人代表
     */
    @TableField(value = "XMFRDB")
    @ApiModelProperty(value = "项目法人代表")
    private String xmfrdb;

    /**
     * 项目法人
     */
    @TableField(value = "XMFR")
    @ApiModelProperty(value = "项目法人")
    private String xmfr;

    /**
     * 联系电话
     */
    @TableField(value = "LXDH")
    @ApiModelProperty(value = "联系电话")
    private String lxdh;

    /**
     * 开工日期
     */
    @TableField(value = "KGRQ")
    @ApiModelProperty(value = "开工日期")
    //@DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd")
    private Date kgrq;

    /**
     * 合同工期
     */
    @TableField(value = "HTGQ")
    @ApiModelProperty(value = "合同工期")
    private String htgq;

    /**
     * 项目类型(0-三峡后续 1-移民后扶 2-河道堤防 3-湖库工程 4-农村水利 5-泵站工程 6-饮水安全 7-连通工程 8-水美乡村 9-水土保持 10-山洪灾害 11-节水项目 12-维修养护 13-其他)
     */
    @TableField(value = "XMLX")
    @ApiModelProperty(value = "项目类型(对应字典：0-三峡后续 1-移民后扶 2-河道堤防 3-湖库工程 4-农村水利 5-泵站工程 6-饮水安全 7-连通工程 8-水美乡村 9-水土保持 10-山洪灾害 11-节水项目 12-维修养护 13-其他)")
    private String xmlx;

    /**
     * 项目规模(0-小型 1-中型 2-大型)
     */
    @TableField(value = "XMGM")
    @ApiModelProperty(value = "项目规模(0-小型 1-中型 2-大型)")
    private String xmgm;

    /**
     * 项目状态(0-在建工程 1-完工待验收 2-完工已验收)
     */
    @TableField(value = "XMZT")
    @ApiModelProperty(value = "项目状态(0-在建工程 1-完工待验收 2-完工已验收)")
    private String xmzt;

    /**
     * 所属区域ID
     */
    @TableField(value = "XZQHDM")
    @ApiModelProperty(value = "所属区域ID")
    private String xzqhdm;

    /**
     * 详细地址
     */
    @TableField(value = "XXDZ")
    @ApiModelProperty(value = "详细地址")
    private String xxdz;

    /**
     * 质量监督单位
     */
    @TableField(value = "ZLJDDW")
    @ApiModelProperty(value = "质量监督单位")
    private String zljddw;

    /**
     * 安全监督单位
     */
    @TableField(value = "AQJDDW")
    @ApiModelProperty(value = "安全监督单位")
    private String aqjddw;

    /**
     * 经度
     */
    @TableField(value = "LON")
    @ApiModelProperty(value = "经度")
    private BigDecimal lon;

    /**
     * 纬度
     */
    @TableField(value = "LAT")
    @ApiModelProperty(value = "纬度")
    private BigDecimal lat;

    /**
     * 创建人ID
     */
    @TableField(value = "CREATED_BY")
    @ApiModelProperty(value = "创建人ID")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME")
    @ApiModelProperty(value = "创建时间")
    private Date createdTime;

    /**
     * 修改人ID
     */
    @TableField(value = "UPDATED_BY")
    @ApiModelProperty(value = "修改人ID")
    private String updatedBy;

    /**
     * 修改时间
     */
    @TableField(value = "UPDATED_TIME")
    @ApiModelProperty(value = "修改时间")
    private Date updatedTime;

    /**
     * 备注
     */
    @TableField(value = "BZ")
    @ApiModelProperty(value = "项目简介")
    private String bz;

    private static final long serialVersionUID = 1L;
}