package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import lombok.Data;

/**
 * 人员每天考勤汇总
 */
@ApiModel(value = "人员每天考勤汇总")
@Data
@TableName(value = "sgc_rykq_day")
public class SgcRykqDayEntity implements Serializable {
    /**
     * 主键ID
     */
    @TableId(value = "ID", type = IdType.INPUT)
    @ApiModelProperty(value = "主键ID")
    private String id;

    /**
     * 日期
     */
    @TableField(value = "RQ")
    @ApiModelProperty(value = "日期")
    private LocalDate rq;

    /**
     * 员工ID
     */
    @TableField(value = "YGID")
    @ApiModelProperty(value = "员工ID")
    private String ygid;

    /**
     * 首次刷卡时间
     */
    @TableField(value = "SCSKSJ")
    @ApiModelProperty(value = "首次刷卡时间")
    private LocalDateTime scsksj;

    /**
     * 最后一次刷卡时间
     */
    @TableField(value = "ZHYCSKSJ")
    @ApiModelProperty(value = "最后一次刷卡时间")
    private LocalDateTime zhycsksj;

    /**
     * 扣分值
     */
    @TableField(value = "KFZ")
    @ApiModelProperty(value = "扣分值")
    private double kfz;

    /**
     * 设备ID
     */
    @TableField(value = "SBID")
    @ApiModelProperty(value = "设备ID")
    private String sbid;

    /**
     * 创建人
     */
    @TableField(value = "CREATED_BY")
    @ApiModelProperty(value = "创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME")
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createdTime;

    private static final long serialVersionUID = 1L;
}