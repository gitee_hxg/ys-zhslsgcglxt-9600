package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "sgc_xjrw_csnr")
public class SgcXjrwCsnrEntity implements Serializable {
    /**
     * 主键ID
     */
    @TableId(value = "ID", type = IdType.INPUT)
    @ApiModelProperty(value = "主键ID")
    private String id;

    /**
     * 巡检任务ID
     */
    @TableField(value = "XJRWID")
    @ApiModelProperty(value = "巡检任务ID")
    private String xjrwid;

    /**
     * 管控措施ID
     */
    @TableField(value = "GKCSID")
    @ApiModelProperty(value = "管控措施ID")
    private String gkcsid;

    /**
     * 是否巡检(0-未巡检 1-已巡检)
     */
    @TableField(value = "SFXJ")
    @ApiModelProperty(value = "是否巡检(0-未巡检 1-已巡检)")
    private String sfxj;

    /**
     * 创建人
     */
    @TableField(value = "CREATED_BY")
    @ApiModelProperty(value = "创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME")
    @ApiModelProperty(value = "创建时间")
    private Date createdTime;

    private static final long serialVersionUID = 1L;
}