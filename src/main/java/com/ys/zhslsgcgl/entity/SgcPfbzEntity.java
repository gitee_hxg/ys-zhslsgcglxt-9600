package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "sgc_pfbz")
public class SgcPfbzEntity implements Serializable {
    /**
     * 评分标准ID
     */
    @TableId(value = "PFBZID", type = IdType.INPUT)
    @ApiModelProperty(value="评分标准ID")
    private String pfbzid;

    /**
     * 标题
     */
    @TableField(value = "BT")
    @ApiModelProperty(value="标题")
    private String bt;

    /**
     * 类型 (0-扣分项 1-加分项)
     */
    @TableField(value = "LX")
    @ApiModelProperty(value="类型 (0-扣分项 1-加分项)")
    private String lx;

    /**
     * 大分类(对应字典值)
     */
    @TableField(value = "DFL")
    @ApiModelProperty(value="大分类(对应字典值)")
    private String dfl;

    /**
     * 详细分类(对应字典值)
     */
    @TableField(value = "XXFL")
    @ApiModelProperty(value="详细分类(对应字典值)")
    private String xxfl;

    /**
     * 分值
     */
    @TableField(value = "FZ")
    @ApiModelProperty(value="分值")
    private BigDecimal fz;

    /**
     *
     */
    @TableField(value = "JFDW")
    @ApiModelProperty(value="记分单位")
    private BigDecimal jfdw;

    /**
     * 性质分类 (字典值：0-一般 1-较重 2-严重)
     */
    @TableField(value = "XZFL")
    @ApiModelProperty(value="性质分类 (字典值：0-一般 1-较重 2-严重)")
    private String xzfl;

    /**
     * 内容
     */
    @TableField(value = "NR")
    @ApiModelProperty(value="内容")
    private String nr;

    /**
     * 创建人
     */
    @TableField(value = "CREATED_BY")
    @ApiModelProperty(value="创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME")
    @ApiModelProperty(value="创建时间")
    private Date createdTime;

    /**
     * 更新人
     */
    @TableField(value = "UPDATED_BY")
    @ApiModelProperty(value="更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(value = "UPDATED_TIME")
    @ApiModelProperty(value="更新时间")
    private Date updatedTime;

    private static final long serialVersionUID = 1L;
}