package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "sgc_zdgz")
public class SgcZdgzEntity implements Serializable {
    /**
     * 单位ID
     */
    @TableId(value = "DWID", type = IdType.INPUT)
    @ApiModelProperty(value="单位ID")
    private String dwid;

    /**
     * 备注
     */
    @TableField(value = "BZ")
    @ApiModelProperty(value="备注")
    private String bz;

    /**
     * 创建人
     */
    @TableField(value = "CREATED_BY")
    @ApiModelProperty(value="创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME")
    @ApiModelProperty(value="创建时间")
    private Date createdTime;

    private static final long serialVersionUID = 1L;
}