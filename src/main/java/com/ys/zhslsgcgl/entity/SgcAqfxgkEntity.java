package com.ys.zhslsgcgl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.Data;

@ApiModel(value="安全分级风险管控表")
@Data
@TableName(value = "sgc_aqfxgk")
public class SgcAqfxgkEntity implements Serializable {
    /**
     * 管控ID
     */
    @TableId(value = "GKID", type = IdType.INPUT)
    @ApiModelProperty(value="管控ID")
    private String gkid;

    /**
     * 危险源类别
     */
    @TableField(value = "WXYLB")
    @ApiModelProperty(value="危险源类别")
    private String wxylb;

    /**
     * 危险项目
     */
    @TableField(value = "WXXM")
    @ApiModelProperty(value="危险项目")
    private String wxxm;

    /**
     * 危险源级别 (一般，重大)
     */
    @TableField(value = "WXYJB")
    @ApiModelProperty(value="危险源级别 (一般，重大)")
    private String wxyjb;

    /**
     * 部位
     */
    @TableField(value = "BW")
    @ApiModelProperty(value="部位")
    private String bw;

    /**
     * 可能导致的事故类型
     */
    @TableField(value = "KNDZSGLX")
    @ApiModelProperty(value="可能导致的事故类型")
    private String kndzsglx;

    /**
     * 风险等级（范围） (低风险，一般风险，重大风险)
     */
    @TableField(value = "FXDJ")
    @ApiModelProperty(value="风险等级（范围） (低风险，一般风险，重大风险)")
    private String fxdj;

    /**
     * 项目ID
     */
    @TableField(value = "XMID")
    @ApiModelProperty(value="项目ID")
    private String xmid;

    /**
     * 标段ID
     */
    @TableField(value = "BDID")
    @ApiModelProperty(value="标段ID")
    private String bdid;

    /**
     * 创建人
     */
    @TableField(value = "CREATED_BY")
    @ApiModelProperty(value="创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME")
    @ApiModelProperty(value="创建时间")
    private Date createdTime;

    /**
     * 更新人
     */
    @TableField(value = "UPDATED_BY")
    @ApiModelProperty(value="更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(value = "UPDATED_TIME")
    @ApiModelProperty(value="更新时间")
    private Date updatedTime;

    private static final long serialVersionUID = 1L;
}