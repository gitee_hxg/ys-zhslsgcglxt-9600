package com.ys.zhslsgcgl.controller;

import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.ys.common.core.annotation.log.YsLog;
import com.ys.common.core.dto.R;
import com.ys.common.core.enums.log.LogType;
import com.ys.common.core.enums.log.OprType;
import com.ys.zhslsgcgl.bo.PfjfBo;
import com.ys.zhslsgcgl.bo.PfjffsBo;
import com.ys.zhslsgcgl.dto.PfjfDto;
import com.ys.zhslsgcgl.dto.PfjfJffs;
import com.ys.zhslsgcgl.dto.PfjfShDto;
import com.ys.zhslsgcgl.entity.SgcPfjfEntity;
import com.ys.zhslsgcgl.entity.SgcPfjfShztEntity;
import com.ys.zhslsgcgl.service.PfjfService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @Description:检查计分-加分管理
 * @Author: dzg
 * @CreateDate: 2022/11/10 14:26
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Api(tags = "检查计分-加分管理")
@RestController
@Validated
@RequestMapping("/pfjf")
public class PfjfController {

    @Autowired
    private PfjfService pfjfService;

    @ApiOperation(value = "分页查询加分申请列表", notes = "")
    @ApiOperationSupport(author = "dzg")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "分页查询加分申请列表", module = "加分管理")
    @PostMapping("/queryPagePfjf")
    public R<PageInfo<List<PfjfDto>>> queryPagePfjf(@RequestBody PfjfBo pfjfBo) {
        return R.ok(pfjfService.queryPagePfjf(pfjfBo));
    }

    @ApiOperation(value = "根据申请id查询申请信息和审核信息", notes = "")
    @ApiOperationSupport(author = "dzg")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "根据申请id查询申请信息和审核信息", module = "加分管理")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pfjfid", value = "评分加分ID", required = true, dataType = "String")
    })
    @GetMapping("/queryPfjfById")
    public R<PfjfShDto> queryPfjfById(@NotBlank(message = "评分加分ID不能为空") @RequestParam("pfjfid") String pfjfid) {
        return R.ok(pfjfService.queryPfjfById(pfjfid));
    }

    @ApiOperation(value = "根据标段合同金额得到分数", notes = "")
    @ApiOperationSupport(author = "dzg")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "根据标段合同金额得到分数", module = "加分管理")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "bdid", value = "标段ID", required = true, dataType = "String")
    })
    @GetMapping("/queryJffs")
    public R<PfjfJffs> getJffs(@NotBlank(message = "标段ID不能为空") @RequestParam("bdid") String bdid) {
        return R.ok(pfjfService.queryJffs(bdid));
    }


    @ApiOperation(value = "新增加分申请", notes = "")
    @ApiOperationSupport(author = "dzg", ignoreParameters = {"createdBy", "createdTime", "updatedBy", "updatedTime"})
    @YsLog(oprType = OprType.INSERT, logType = LogType.ACCESS, description = "新增加分申请", module = "加分管理")
    @PostMapping("/addPfjf")
    public R addPfjf(@RequestBody @Validated SgcPfjfEntity sgcPfjfEntity) {
        return R.ok(pfjfService.addPfjf(sgcPfjfEntity));
    }

    @ApiOperation(value = "提交初审信息", notes = "")
    @ApiOperationSupport(author = "dzg",ignoreParameters = {"shzt", "lclx","shztid"})
    @YsLog(oprType = OprType.INSERT, logType = LogType.ACCESS, description = "提交初审信息", module = "加分管理")
    @PostMapping("/submitCsxx")
    public R submitCsxx(@RequestBody @Validated SgcPfjfShztEntity sgcPfjfShztEntity) {
        return R.ok(pfjfService.submitCsxx(sgcPfjfShztEntity));
    }

    @ApiOperation(value = "提交复审信息", notes = "")
    @ApiOperationSupport(author = "dzg")
    @YsLog(oprType = OprType.INSERT, logType = LogType.ACCESS, description = "提交复审信息", module = "加分管理")
    @PostMapping("/submitFsxx")
    public R submitFsxx(@RequestBody @Validated PfjffsBo pfjffsBo) {
        return R.ok(pfjfService.submitFsxx(pfjffsBo));
    }

    @ApiOperation(value = "修改加分申请", notes = "")
    @ApiOperationSupport(author = "dzg", ignoreParameters = {"createdBy", "createdTime","updatedBy","updatedTime"})
    @YsLog(oprType = OprType.INSERT, logType = LogType.ACCESS, description = "修改加分申请", module = "加分管理")
    @PostMapping("/updatePfjf")
    public R updatePfjf(@RequestBody @Validated SgcPfjfEntity sgcPfjfEntity) {
        return R.ok(pfjfService.updatePfjf(sgcPfjfEntity));
    }

    @ApiOperation(value = "根据id删除加分申请")
    @ApiOperationSupport(author = "dzg")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.DELETE, description = "根据id删除加分申请", module = "加分管理")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pfjfid", value = "评分加分id", required = true, dataType = "String")
    })
    @GetMapping("/deletePfjfById")
    public R deletePfjfById(@NotBlank(message = "ID不能为空") @RequestParam("pfjfid") String pfjfid) {
        int result = pfjfService.deletePfjfById(pfjfid);
        if (result > 0) {
            return R.ok(pfjfService.deletePfjfById(pfjfid), "删除成功");
        } else {
            return R.ok(pfjfService.deletePfjfById(pfjfid), "审核中和已审核的数据不能删除");
        }
    }


}
