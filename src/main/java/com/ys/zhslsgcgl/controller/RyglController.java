package com.ys.zhslsgcgl.controller;

import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.ys.common.core.annotation.log.YsLog;
import com.ys.common.core.dto.R;
import com.ys.common.core.enums.log.LogType;
import com.ys.common.core.enums.log.OprType;
import com.ys.zhslsgcgl.bo.AddRyBo;
import com.ys.zhslsgcgl.bo.PageRylbBo;
import com.ys.zhslsgcgl.bo.RykqayhzBo;
import com.ys.zhslsgcgl.bo.XmxxBo;
import com.ys.zhslsgcgl.dto.*;
import com.ys.zhslsgcgl.service.RyglService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;

/**
 * @Description: 市场监督-人员管理
 * @Author: fsx
 * @CreateDate: 2022/10/31 11:33
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Api(tags = "市场监督-人员管理")
@RestController
@Validated
@RequestMapping("rygl")
public class RyglController {
    @Resource
    private RyglService ryglService;

    @ApiOperation("分页查询人员信息")
    @ApiOperationSupport(author = "fsx")
    @PostMapping("/queryPageRygl")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "分页查询人员信息")
    public R<PageInfo<PageRylbDto>> queryPageRygl(@Validated @RequestBody PageRylbBo pageRylbBo) {
        return R.ok(ryglService.queryPageRygl(pageRylbBo));
    }

    @ApiOperation("根据单位分页查询人员信息")
    @ApiOperationSupport(author = "fsx",ignoreParameters = {"rylx","xmlx"})
    @PostMapping("/queryPageRyByDwid")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "根据单位分页查询人员信息")
    public R<PageInfo<PageRylbDto>> queryPageRyByDwid(@Validated @RequestBody PageRylbBo pageRylbBo) {
        return R.ok(ryglService.queryPageRyByDwid(pageRylbBo));
    }

    @ApiOperation("根据项目id和标段id查询人员信息")
    @ApiOperationSupport(author = "fsx")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "xmid", value = "项目ID", required = true, dataType = "String"),
            @ApiImplicitParam(name = "bdid", value = "标段ID", required = false, dataType = "String"),
            @ApiImplicitParam(name = "rq", value = "日期", required = false, dataType = "String"),
            @ApiImplicitParam(name = "ygxm", value = "员工姓名", required = false, dataType = "String")
    })
    @GetMapping("/listRyxxByXmidOrBdid")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "根据单位分页查询人员信息")
    public R<List<RyxxByDwfzDto>> listRyxxByXmidOrBdid(
            @NotBlank(message = "项目id必传") @RequestParam("xmid") String xmid,
            String bdid,
            @NotBlank(message = "日期：年-月，必传，默认当前") @RequestParam("rq") String rq,String ygxm) {
        return R.ok(ryglService.listRyxxByXmidOrBdid(xmid,bdid,rq,ygxm));
    }

    @ApiOperation("根据人员id和月份查询考勤记录")
    @ApiOperationSupport(author = "fsx")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ygid", value = "用户id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "rq", value = "日期：年-月", required = true, dataType = "String"),
            @ApiImplicitParam(name = "sfdk", value = "是否打卡  0：否，1：是", required = false, dataType = "Integer")
    })
    @GetMapping("/listRyKqjlByMonth")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "根据人员id和月份查询考勤记录")
    public R<List<RykqDayDto>> listRyKqjlByMonth(
            @NotBlank(message = "用户id必传") @RequestParam("ygid") String ygid,
            @NotBlank(message = "日期：年-月，必传，默认当前") @RequestParam("rq") String rq,
            Integer sfdk) {
        return R.ok(ryglService.listRyKqjlByMonth(ygid,rq,sfdk));
    }

    @ApiOperation("搜索面板分页查询项目列表")
    @ApiOperationSupport(author = "fsx")
    @PostMapping("/pageXmlb")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "搜索面板分页查询项目列表")
    public R<PageInfo<PageXmxxDto>> pageXmlb(@Validated @RequestBody XmxxBo xmxxBo) {
        return R.ok(ryglService.pageXmlb(xmxxBo));
    }

    @ApiOperation("项目信息下拉")
    @ApiOperationSupport(author = "fsx")
    @GetMapping("/listXmxx")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "查询项目信息下拉数据")
    public R<List<Map<String,String>>> listXmxx() {
        return R.ok(ryglService.listXmxx());
    }

    @ApiOperation("标段信息下拉")
    @ApiOperationSupport(author = "fsx")
    @ApiImplicitParam(name = "xmid", value = "项目id", required = true, dataType = "String")
    @GetMapping("/listBdxx")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "查询标段信息下拉数据")
    public R<List<Map<String,String>>> listBdxx(@NotBlank(message = "项目id必传") @RequestParam("xmid") String xmid) {
        return R.ok(ryglService.listBdxx(xmid));
    }

    @ApiOperation("单位信息下拉")
    @ApiOperationSupport(author = "fsx")
    @ApiImplicitParam(name = "bdid", value = "标段id", required = true, dataType = "String")
    @GetMapping("/listDwxx")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "查询单位信息下拉数据")
    public R<List<Map<String,String>>> listDwxx(@NotBlank(message = "标段id必传") @RequestParam("bdid") String bdid) {
        return R.ok(ryglService.listDwxx(bdid));
    }

    @ApiOperation("计算月份内单位下的历史得分")
    @ApiOperationSupport(author = "fsx")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dwid", value = "单位id", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "rq", value = "日期：年-月", required = true, dataType = "String")
    })
    @GetMapping("/listDwKqkfByMonth")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "计算月份内单位下的历史得分")
    public R<List<DwLsdfDto>> listDwKqkfByMonth(@NotBlank(message = "单位id必传") @RequestParam("dwid") String dwid,
                                                         @NotBlank(message = "日期年-月必传") @RequestParam("rq") String rq) {
        return R.ok(ryglService.listDwKqkfByMonth(dwid,rq));
    }

    @ApiOperation("新增人员信息")
    @ApiOperationSupport(author = "fsx", ignoreParameters = {"createdBy","createdTime","updatedBy","updatedTime"})
    @PostMapping("/insertRygl")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.INSERT, description = "新增人员信息")
    public R<Integer> insertRygl(@Validated @RequestBody AddRyBo ryBo) {
        return R.ok(ryglService.insertRygl(ryBo));
    }

    @ApiOperation("修改人员信息")
    @ApiOperationSupport(author = "fsx", ignoreParameters = {"createdBy","createdTime","updatedBy","updatedTime"})
    @PostMapping("/updateRygl")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.UPDATE, description = "修改人员信息")
    public R<Integer> updateRygl(@Validated @RequestBody AddRyBo ryBo) {
        return R.ok(ryglService.updateRygl(ryBo));
    }

    @ApiOperation("删除人员信息")
    @ApiOperationSupport(author = "fsx")
    @GetMapping("/deleteRygl")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.DELETE, description = "删除人员信息")
    public R<Integer> deleteRygl(@NotBlank(message = "员工id不能为空")  @RequestParam("ygid") String ygid) {
        return R.ok(ryglService.deleteRygl(ygid));
    }

    @ApiOperation(value = "分页查询人员考勤按月汇总", notes = "")
    @ApiOperationSupport(author = "wugangzhi", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "分页查询人员考勤按月汇总", module = "人员管理")
    @PostMapping("/queryPageRykqayhz")
    public R<PageInfo<List<RykqayhzDto>>> queryPageRykqayhz(@RequestBody RykqayhzBo bo) {
        return R.ok(ryglService.queryPageRykqayhz(bo));
    }

}

