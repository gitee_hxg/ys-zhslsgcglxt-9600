package com.ys.zhslsgcgl.controller;

import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.ys.common.core.annotation.log.YsLog;
import com.ys.common.core.dto.R;
import com.ys.common.core.enums.log.LogType;
import com.ys.common.core.enums.log.OprType;
import com.ys.zhslsgcgl.bo.DwxxBo;
import com.ys.zhslsgcgl.dto.DwxxDto;
import com.ys.zhslsgcgl.entity.SgcDwglEntity;
import com.ys.zhslsgcgl.service.DwglService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;

/**
 * @Description: 市场监督-单位管理
 * @Author: wugangzhi
 * @CreateDate: 2022/11/05 20:33
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Api(tags = "市场监督-单位管理")
@RestController
@Validated
@RequestMapping("dwgl")
public class DwglController {

    @Autowired
    private DwglService dwglService;


    @ApiOperation(value = "分页查询单位信息", notes = "")
    @ApiOperationSupport(author = "wugangzhi", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS,oprType = OprType.QUERY,description = "分页查询单位信息", module = "单位管理")
    @PostMapping("/queryPageDwxx")
    public R<PageInfo<List<DwxxDto>>> queryPageSzjcxx(@RequestBody DwxxBo dwxxBo){
        return R.ok(dwglService.queryPageDwxx(dwxxBo));
    }

    @ApiOperation(value = "新增单位信息", notes = "")
    @ApiOperationSupport(author = "wugangzhi", ignoreParameters = {"yyzz","createdBy","createdTime","updatedBy","updatedTime"})
    @YsLog(oprType=OprType.INSERT,logType = LogType.ACCESS,description = "新增单位信息", module = "单位管理")
    @PostMapping("/addDwxx")
    public R addDwxx(@RequestBody @Validated SgcDwglEntity sgcDwglEntity){
        return R.ok(dwglService.addDwxx(sgcDwglEntity));
    }

    @ApiOperation(value = "修改单位信息", notes = "")
    @ApiOperationSupport(author = "wugangzhi", ignoreParameters = {"yyzz","createdBy","createdTime","updatedBy","updatedTime"})
    @YsLog(oprType=OprType.UPDATE,logType = LogType.ACCESS,description = "修改单位信息", module = "单位管理")
    @PostMapping("/updateDwxx")
    public R updateDwxx(@RequestBody @Validated SgcDwglEntity sgcDwglEntity){
        return R.ok(dwglService.updateDwxx(sgcDwglEntity));
    }

    @ApiOperation(value = "根据单位ID删除单位信息")
    @ApiOperationSupport(author = "wugangzhi")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.DELETE, description = "根据单位ID删除单位信息", module = "单位管理")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dwid", value = "单位ID", required = true, dataType = "String")
    })
    @GetMapping("/deleteDwxxByDwid")
    public R deleteDwxxByDwid(@NotBlank(message = "单位ID不能为空") @RequestParam("dwid")String dwid) {
        return R.ok(dwglService.deleteDwxxByDwid(dwid));
    }

    @ApiOperation("查询单位信息下拉列表")
    @ApiOperationSupport(author = "fsx")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "查询单位信息下拉列表")
    @ApiImplicitParams({
    })
    @GetMapping("/queryXmxx")
    public R<List<Map<String,String>>> listDwxx() {
        return R.ok(dwglService.listDwxx());
    }

}
