package com.ys.zhslsgcgl.controller;

import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.ys.common.core.annotation.log.YsLog;
import com.ys.common.core.dto.R;
import com.ys.common.core.enums.log.LogType;
import com.ys.common.core.enums.log.OprType;
import com.ys.zhslsgcgl.bo.SbbdBo;
import com.ys.zhslsgcgl.bo.SbryxxBo;
import com.ys.zhslsgcgl.bo.SbxxBo;
import com.ys.zhslsgcgl.dto.SbryxxDto;
import com.ys.zhslsgcgl.dto.SbxxDto;
import com.ys.zhslsgcgl.service.SbxxService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description:设备管理Controller
 * @Author: wugangzhi
 * @CreateData: 2022/11/17 1:10
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Api(tags = "设备管理")
@RestController
@Validated
@RequestMapping("sbgl")
public class SbxxController {
    @Autowired
    private SbxxService sbxxService;

    @ApiOperation(value = "分页查询设备信息", notes = "")
    @ApiOperationSupport(author = "wugangzhi", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS,oprType = OprType.QUERY,description = "分页查询设备信息", module = "设备管理")
    @PostMapping("/queryPageSbxx")
    public R<PageInfo<List<SbxxDto>>> queryPageSzjcxx(@RequestBody SbxxBo sbxxBo){
        return R.ok(sbxxService.queryPageSbxx(sbxxBo));
    }

    @ApiOperation(value = "修改设备标段绑定", notes = "")
    @ApiOperationSupport(author = "wugangzhi", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS,oprType = OprType.UPDATE,description = "修改设备标段绑定", module = "设备管理")
    @PostMapping("/updateSbdbgl")
    public R updateSbdbgl(@RequestBody SbbdBo sbbdBo){
        return R.ok(sbxxService.updateSbdbgl(sbbdBo));
    }

    @ApiOperation(value = "分页查询设备人员信息", notes = "")
    @ApiOperationSupport(author = "wugangzhi", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS,oprType = OprType.QUERY,description = "分页查询设备人员信息", module = "设备管理")
    @PostMapping("/queryPageSbry")
    public R<PageInfo<List<SbryxxDto>>> queryPageSbry(@RequestBody SbryxxBo sbryxxBo){
        return R.ok(sbxxService.queryPageSbry(sbryxxBo));
    }


}
