package com.ys.zhslsgcgl.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.ys.common.core.annotation.log.YsLog;
import com.ys.common.core.dto.R;
import com.ys.common.core.enums.log.LogType;
import com.ys.common.core.enums.log.OprType;
import com.ys.zhslsgcgl.service.CommonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;

/**
 * @Description: 公共Controller
 * @Author: wugangzhi
 * @CreateDate: 2022/11/6 15:43
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Api(tags = "公共模块")
@Validated
@RestController
@RequestMapping("common")
public class CommonController {
    @Autowired
    private CommonService commonService;


    @ApiOperation(value = "查询字典值", notes = "")
    @ApiOperationSupport(author = "wugangzhi", ignoreParameters = {})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tableParas", value = "业务记录字段名(多个逗号隔开)", required =true, dataType = "String")
    })
    @YsLog(logType = LogType.ACCESS,oprType = OprType.QUERY,description = "查询字典值")
    @GetMapping("/listByDicCode")
    public R listByDicCode(@NotBlank(message = "业务记录字段名不能为空") @RequestParam("tableParas") String tableParas){
        return R.ok(commonService.listByDicCode(tableParas));
    }

    @ApiOperation(value = "根据角色id查询用户")
    @ApiOperationSupport(author = "tjy")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "jsid", value = "角色id", required =true, dataType = "String")
    })
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "根据角色id查询用户", module = "查询字典值")
    @GetMapping("/listYhByJsid")
    public R<List<Map<String,String>>> listYhByJsid(@NotBlank(message = "角色id不能为空") @RequestParam("jsid") String jsid) {
        return R.ok(commonService.listYhByJsid(jsid));
    }

}
