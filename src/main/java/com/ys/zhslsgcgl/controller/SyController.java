package com.ys.zhslsgcgl.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.ys.common.core.annotation.log.YsLog;
import com.ys.common.core.dto.R;
import com.ys.common.core.enums.log.LogType;
import com.ys.common.core.enums.log.OprType;
import com.ys.zhslsgcgl.dto.TzqkDto;
import com.ys.zhslsgcgl.dto.XmfbDto;
import com.ys.zhslsgcgl.dto.XmzlDto;
import com.ys.zhslsgcgl.dto.ZjxmDto;
import com.ys.zhslsgcgl.service.SyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * @Description: 首页Controller
 * @Author: chenxingjian
 * CreateDate: 2022/11/11 16:23
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/

@Api(tags = "首页")
@RestController
@Validated
@RequestMapping("sy")
public class SyController {

    @Autowired
    private SyService syService;

    @ApiOperation(value = "查询项目总览", notes = "")
    @ApiOperationSupport(author = "tjy", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "查询项目总览")
    @GetMapping("/getXmzl")
    public R<XmzlDto> getXmzl() {
        return syService.getXmzl();
    }


    @ApiOperation(value = "查询项目分布", notes = "")
    @ApiOperationSupport(author = "tjy", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "查询项目分布")
    @GetMapping("/getXmfb")
    public R<XmfbDto> getXmfb() {
        return syService.getXmfb();
    }


    @ApiOperation(value = "查询投资情况", notes = "")
    @ApiOperationSupport(author = "tjy", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "查询投资情况")
    @GetMapping("/getTzqk")
    public R<TzqkDto> getTzqk() {
        return syService.getTzqk();
    }


    @ApiOperation(value = "查询在建项目情况", notes = "")
    @ApiOperationSupport(author = "tjy", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "查询在建项目情况")
    @GetMapping("/getZjxm")
    public R<ZjxmDto> getZjxm() {
        return syService.getZjxm();
    }
}
