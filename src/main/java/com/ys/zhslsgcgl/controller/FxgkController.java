package com.ys.zhslsgcgl.controller;

import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.ys.common.core.annotation.log.YsLog;
import com.ys.common.core.dto.R;
import com.ys.common.core.enums.log.LogType;
import com.ys.common.core.enums.log.OprType;
import com.ys.zhslsgcgl.bo.*;
import com.ys.zhslsgcgl.dto.FxgkLzpgDto;
import com.ys.zhslsgcgl.dto.FxgkdwDto;
import com.ys.zhslsgcgl.dto.FxgklzqkDto;
import com.ys.zhslsgcgl.dto.FxgkzzdwDto;
import com.ys.zhslsgcgl.entity.SgcAqfxgkEntity;
import com.ys.zhslsgcgl.entity.SgcAqfxgkZzlxbEntity;
import com.ys.zhslsgcgl.service.FxgkService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

/**
 * @Description: 风险管控
 * @Author: chenxingjian
 * CreateDate: 2022/11/12 21:43
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Api(tags = "风险分级管控")
@RestController
@Validated
@RequestMapping("fxgk")
public class FxgkController {

    @Autowired
    private FxgkService fxgkService;

    @ApiOperation(value = "分页查询风险管控主表", notes = "")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS,oprType = OprType.QUERY,description = "分页查询风险管控主表", module = "风险管控")
    @PostMapping("/queryPageFxgk")
    public R<PageInfo<List<SgcAqfxgkEntity>>> queryPageFxgk(@Validated @RequestBody PageFxgkBo fxgkBo){
        return R.ok(fxgkService.queryPageFxgk(fxgkBo));
    }

    @ApiOperation("获取风险管控树")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {})
    @GetMapping("/getFxgkTree")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "获取风险管控树", module = "风险管控")
    public R<SgcAqfxgkZzlxbEntity> getFxgkTree() {
        return fxgkService.getFxgkTree();
    }

    @ApiOperation("新增风险管控危险源信息")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {"createdBy","createdTime","updatedBy","updatedTime"})
    @PostMapping("/addFxgk")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.INSERT, description = "新增风险管控危险源信息", module = "风险管控")
    public R addFxgk(@Validated @RequestBody SgcAqfxgkEntity sgcAqfxgkEntity) {
        return fxgkService.addFxgk(sgcAqfxgkEntity);
    }

    @ApiOperation("删除风险管控危险源信息")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "gkid", value = "管控ID", required = true, dataType = "String")
    })
    @GetMapping("/deleteFxgk")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.DELETE, description = "删除风险管控危险源信息", module = "风险管控")
    public R deleteFxgk(@Validated @RequestParam String gkid) {
        return fxgkService.deleteFxgk(gkid);
    }

    @ApiOperation("编辑风险管控危险源信息")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {"createdBy","createdTime","updatedBy","updatedTime"})
    @PostMapping("/updateFxgk")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.UPDATE, description = "编辑风险管控危险源信息", module = "风险管控")
    public R updateFxgk(@Validated @RequestBody SgcAqfxgkEntity sgcAqfxgkEntity) {
        return fxgkService.updateFxgk(sgcAqfxgkEntity);
    }


    @ApiOperation("新增(或编辑)风险管控组织信息")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {})
    @PostMapping("/addFxgkzzxx")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.INSERT, description = "新增(或编辑)风险管控组织信息", module = "风险管控")
    public R addFxgkzzxx(@Validated @RequestBody FxgkzzBo fxgkzzBo) {
        return fxgkService.addFxgkzzxx(fxgkzzBo);
    }

    @ApiOperation("根据管控ID查询单位信息")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "gkid", value = "管控ID", required = true, dataType = "String")
    })
    @GetMapping("/listGkzzByXmidandBdid")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "根据管控ID查询单位信息", module = "风险管控")
    public R<List<FxgkdwDto>> listGkzzByGkid(@Validated @RequestParam String gkid) {
        return fxgkService.listGkzzByGkid(gkid);
    }

    @ApiOperation("新增(或编辑)风险管控措施信息")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {"pxz"})
    @PostMapping("/addFxgkcs")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.INSERT, description = "新增(或编辑)风险管控措施信息", module = "风险管控")
    public R addFxgkcs(@Validated @RequestBody FxgkcsBo fxgkcsBo) throws ParseException {
        return fxgkService.addFxgkcs(fxgkcsBo);
    }


    @ApiOperation("查询风险管控措施信息")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "gkid", value = "管控ID", required = true, dataType = "String")
    })
    @GetMapping("/listFxgkcs")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "根据管控组织ID查询风险管控措施信息", module = "风险管控")
    public R<List<FxgkzzdwDto>> listFxgkcs(@Validated @RequestParam String gkid) {
        return fxgkService.listFxgkcs(gkid);
    }


    @ApiOperation("新增(或编辑)风险管控人员信息")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {"id","createdBy","createdTime","updatedBy","updatedTime"})
    @PostMapping("/addFxgkry")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.INSERT, description = "新增(或编辑)风险管控人员信息", module = "风险管控")
    public R addFxgkry(@Validated @RequestBody FxgkryBo fxgkryBo) {
        return fxgkService.addFxgkry(fxgkryBo);
    }


    @ApiOperation("查询风险管控人员信息")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "gkid", value = "管控ID", required = true, dataType = "String")
    })
    @GetMapping("/listFxgkry")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "根据管控组织ID查询风险管控人员信息", module = "风险管控")
    public R<List<FxgkzzdwDto>> listFxgkry(@Validated @RequestParam String gkid) {
        return fxgkService.listFxgkry(gkid);
    }


    @ApiOperation("查询履职情况")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {})
    @PostMapping("/listLzqkgl")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "查询履职情况", module = "风险管控")
    public R<List<FxgklzqkDto>> listLzqkgl(@Validated @RequestBody FxgklzqkBo fxgklzqkBo) {
        return fxgkService.listLzqkgl(fxgklzqkBo);
    }

    @ApiOperation("查询监督措施")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "xjrwid", value = "巡检任务ID", required = true, dataType = "String"),
            @ApiImplicitParam(name = "gkzzid", value = "管控组织ID", required = true, dataType = "String")
    })
    @GetMapping("/listLzqk")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "查询监督措施", module = "风险管控")
    public R<List<FxgkLzpgDto>> listLzqk(@Validated @RequestParam String xjrwid,@Validated @RequestParam String gkzzid) {
        return fxgkService.listLzqk(xjrwid, gkzzid);
    }

}
