package com.ys.zhslsgcgl.controller;

import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.ys.common.core.annotation.log.YsLog;
import com.ys.common.core.dto.R;
import com.ys.common.core.enums.log.LogType;
import com.ys.common.core.enums.log.OprType;
import com.ys.zhslsgcgl.bo.*;
import com.ys.zhslsgcgl.dto.*;
import com.ys.zhslsgcgl.entity.SgcDwglEntity;
import com.ys.zhslsgcgl.entity.SgcXmxxEntity;
import com.ys.zhslsgcgl.service.XmglService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.io.IOException;
import java.util.List;

/**
 * @Description: 项目管理
 * @Author: tjy
 * @CreateDate: 2022/11/09 9:33
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Api(tags = "项目管理")
@RestController
@Validated
@RequestMapping("xmgl")
public class XmglController {
    @Autowired
    private XmglService xmglService;

    @ApiOperation(value = "分页查询项目信息", notes = "")
    @ApiOperationSupport(author = "tjy", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "分页查询项目信息", module = "项目管理")
    @PostMapping("/queryPageXmxx")
    public R<PageInfo<List<QueryPageXmxxDto>>> queryPageXmxx(@RequestBody XmxxglBo xmxxglBo) {
        return R.ok(xmglService.queryPageXmxx(xmxxglBo));
    }

    @ApiOperation(value = "新增项目信息", notes = "")
    @ApiOperationSupport(author = "tjy", ignoreParameters = {"xmid", "createdBy", "createdTime", "updatedBy", "updatedTime"})
    @YsLog(oprType = OprType.INSERT, logType = LogType.ACCESS, description = "新增项目信息", module = "项目管理")
    @PostMapping("/addXmxx")
    public R addXmxx(@RequestBody @Validated SgcXmxxEntity sgcXmxxEntity) {
        return R.ok(xmglService.addXmxx(sgcXmxxEntity));
    }

    @ApiOperation(value = "编辑项目经纬度", notes = "")
    @ApiOperationSupport(author = "tjy", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS, oprType = OprType.UPDATE, description = "编辑项目经纬度", module = "项目管理")
    @PostMapping("/updateXmjwd")
    public R updateXmjwd(@RequestBody XmjwdBo xmjwdBo) {
        return R.ok(xmglService.updateXmjwd(xmjwdBo));
    }

    @ApiOperation(value = "编辑项目信息", notes = "")
    @ApiOperationSupport(author = "tjy", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS, oprType = OprType.UPDATE, description = "编辑项目信息", module = "项目管理")
    @PostMapping("/updateXmxx")
    public R updateXmjwd(@RequestBody SgcXmxxEntity sgcXmxxEntity) {
        return R.ok(xmglService.updateXmxx(sgcXmxxEntity));
    }

    @ApiOperation(value = "根据项目ID删除项目信息")
    @ApiOperationSupport(author = "tjy")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.DELETE, description = "根据项目ID删除项目信息", module = "项目管理")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "xmid", value = "项目ID", required = true, dataType = "String")
    })
    @GetMapping("/deleteXmxxByXmid")
    public R deleteDwxxByDwid(@NotBlank(message = "项目ID不能为空") @RequestParam("xmid") String xmid) {
        return xmglService.deleteXmxxByXmid(xmid);
    }

    @ApiOperation(value = "根据项目ID查询项目信息")
    @ApiOperationSupport(author = "tjy")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "根据项目ID查询项目信息", module = "项目管理")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "xmid", value = "项目ID", required = true, dataType = "String")
    })
    @GetMapping("/listXmxxByXmid")
    public R<SgcXmxxEntity> listXmxxByXmid(@NotBlank(message = "项目ID不能为空") @RequestParam("xmid") String xmid) {
        return R.ok(xmglService.listXmxxByXmid(xmid));
    }

    @ApiOperation(value = "根据项目ID查询标段和单位信息")
    @ApiOperationSupport(author = "tjy")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "根据项目ID查询标段和单位信息", module = "项目管理")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "xmid", value = "项目ID", required = true, dataType = "String")
    })
    @GetMapping("/listBdxxByXmid")
    public R<List<ListBdxxByXmidDto>> listBdxxByXmid(@NotBlank(message = "项目ID不能为空") @RequestParam("xmid") String xmid) {
        return R.ok(xmglService.listBdxxByXmid(xmid));
    }

    @ApiOperation(value = "根据项目ID查询标段和单位,单位名称一行")
    @ApiOperationSupport(author = "tjy")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "根据项目ID查询标段和单位信息", module = "项目管理")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "xmid", value = "项目ID", required = true, dataType = "String")
    })
    @GetMapping("/listBdxxByXmidmcyh")
    public R<List<ListBdxxByXmidmcyhDto>> listBdxxByXmidmcyh(@NotBlank(message = "项目ID不能为空") @RequestParam("xmid") String xmid) {
        return R.ok(xmglService.listBdxxByXmidmcyh(xmid));
    }


    @ApiOperation(value = "根据项目ID删除标段信息")
    @ApiOperationSupport(author = "tjy")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.DELETE, description = "根据项目ID删除标段信息", module = "项目管理")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "xmid", value = "项目ID", required = true, dataType = "String")
    })
    @GetMapping("/deleteBdxxByXmid")
    public R deleteBdxxByXmid(@NotBlank(message = "项目ID不能为空") @RequestParam("xmid") String xmid) {
        return R.ok(xmglService.deleteBdxxByXmid(xmid));
    }

    @ApiOperation(value = "新增项目标段信息", notes = "")
    @ApiOperationSupport(author = "tjy", ignoreParameters = {"createdBy", "createdTime", "updatedBy", "updatedTime"})
    @YsLog(oprType = OprType.INSERT, logType = LogType.ACCESS, description = "新增标段信息", module = "项目管理")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dwid", value = "中标单位ID", required = true, dataType = "String")
    })
    @PostMapping("/addXmBdxx")
    public R addXmxx(@RequestBody @Validated AddXmBdxxBo addXmBdxxBo) {
        return R.ok(xmglService.addXmBdxx(addXmBdxxBo));
    }

    @ApiOperation(value = "根据标段ID查询单位信息")
    @ApiOperationSupport(author = "tjy")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "根据标段ID查询单位信息", module = "项目管理")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "bdid", value = "标段ID", required = true, dataType = "String")
    })
    @GetMapping("/listDwxxByBdid")
    public R<List<SgcDwglEntity>> listDwxxByBdid(@NotBlank(message = "标段ID不能为空") @RequestParam("bdid") String bdid) {
        return R.ok(xmglService.listDwxxByBdid(bdid));
    }


    @ApiOperation(value = "编辑单位信息", notes = "")
    @ApiOperationSupport(author = "tjy", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS, oprType = OprType.UPDATE, description = "编辑单位信息", module = "项目管理")
    @PostMapping("/updateDwxx")
    public R updateXmjwd(@RequestBody SgcDwglEntity sgcDwglEntity) {
        return R.ok(xmglService.updateDwxx(sgcDwglEntity));
    }

    @ApiOperation(value = "编辑标段信息", notes = "")
    @ApiOperationSupport(author = "tjy", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS, oprType = OprType.UPDATE, description = "编辑单位信息", module = "项目管理")
    @PostMapping("/updateBdxx")
    public R updateXmjwd(@RequestBody @Validated AddXmBdxxBo addXmBdxxBo) {
        return xmglService.updateBdxx(addXmBdxxBo);
    }

    @ApiOperation(value = "根据标段ID删除标段信息")
    @ApiOperationSupport(author = "tjy")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.DELETE, description = "根据标段ID删除标段信息", module = "项目管理")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "bdid", value = "标段ID", required = true, dataType = "String")
    })
    @GetMapping("/deleteBdxxByBdid")
    public R deleteBdxxByBdid(@NotBlank(message = "标段ID不能为空") @RequestParam("bdid") String bdid) {
        return xmglService.deleteBdxxByBdid(bdid);
    }

    @ApiOperation(value = "根据单位ID删除单位信息")
    @ApiOperationSupport(author = "tjy")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.DELETE, description = "根据单位ID删除单位信息", module = "项目管理")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dwid", value = "单位ID", required = true, dataType = "String")
    })
    @GetMapping("/deleteDwxxByBdid")
    public R deleteDwxxByBdid(@NotBlank(message = "单位ID不能为空") @RequestParam("dwid") String dwid) {
        return xmglService.deleteDwxxByBdid(dwid);
    }

    @ApiOperation(value = "根据名称和状态模糊查询项目信息")
    @ApiOperationSupport(author = "tjy")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "根据名称和状态模糊查询项目信息", module = "项目管理")
    @PostMapping("/listXmxxByXmmcZt")
    public R<ListXmxxByXmmcZtDto> listXmxxByXmmcZt(@Validated @RequestBody ListXmxxByXmmcZtBo listXmxxByXmmcZtBo) {
        return R.ok(xmglService.listXmxxByXmmcZt(listXmxxByXmmcZtBo.getXmmc(), listXmxxByXmmcZtBo.getXmzt()));
    }

    @ApiOperation(value = "根据项目ID查询项目信息和投资信息")
    @ApiOperationSupport(author = "tjy")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "根据项目ID查询项目信息和投资信息", module = "项目管理")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "xmid", value = "项目ID", required = true, dataType = "String")
    })
    @GetMapping("/listXmxxAndTzByXmid")
    public R<ListXmxxAndTzByXmidDto> listXmxxAndTzByXmid(@NotBlank(message = "项目ID不能为空") @RequestParam("xmid") String xmid) {
        return R.ok(xmglService.listXmxxAndTzByXmid(xmid));
    }

    //@ApiOperation(value = "根据标段ID和单位id删除标段中标单位中间表")
    //@ApiOperationSupport(author = "tjy")
    //@YsLog(logType = LogType.ACCESS, oprType = OprType.DELETE, description = "根据标段ID删除标段信息", module = "项目管理")
    //@ApiImplicitParams({
    //        @ApiImplicitParam(name = "bdid", value = "标段ID", required = true, dataType = "String")
    //})
    //@GetMapping("/deleteBdxxByBdid")
    //public R deleteBdxxByBdid(@NotBlank(message = "标段ID不能为空") @RequestParam("bdid") String bdid,
    //                          @NotBlank(message = "单位ID不能为空") @RequestParam("dwid") String dwid) {
    //    return xmglService.deleteBdxxByBdid(bdid);
    //}
    @ApiOperation(value = "导入安全4表")
    @ApiOperationSupport(author = "tjy")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "导入安全4表", module = "项目管理")
    @GetMapping("/downAqb")
    public void downAqb() throws IOException, InvalidFormatException {
        xmglService.downAqb();
    }


}
