package com.ys.zhslsgcgl.controller;

import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.ys.common.core.annotation.log.YsLog;
import com.ys.common.core.dto.R;
import com.ys.common.core.enums.log.LogType;
import com.ys.common.core.enums.log.OprType;
import com.ys.zhslsgcgl.bo.PfbzBo;
import com.ys.zhslsgcgl.dto.PagePfbzDto;
import com.ys.zhslsgcgl.entity.SgcPfbzEntity;
import com.ys.zhslsgcgl.entity.SgcPfbzFlbEntity;
import com.ys.zhslsgcgl.service.PfbzService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @Description: 检查计分-评分标准管理
 * @Author: chenxingjian
 * CreateDate: 2022/11/8 11:07
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Api(tags = "检查计分-评分标准管理")
@RestController
@Validated
@RequestMapping("/pfbz")
public class PfbzController {

    @Autowired
    private PfbzService pfbzService;

    @ApiOperation(value = "分页查询评分标准", notes = "")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS,oprType = OprType.QUERY,description = "分页查询评分标准", module = "评分标准管理")
    @PostMapping("/queryPagePfbz")
    public R<PageInfo<List<PagePfbzDto>>> queryPagePfbz(@Validated @RequestBody PfbzBo pfbzBo){
        return R.ok(pfbzService.queryPagePfbz(pfbzBo));
    }

    @ApiOperation(value = "新增评分标准", notes = "")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {"createdBy","createdTime","updatedBy","updatedTime"})
    @YsLog(logType = LogType.ACCESS,oprType = OprType.INSERT,description = "新增评分标准", module = "评分标准管理")
    @PostMapping("/addPfbz")
    public R addPfbz(@Validated @RequestBody SgcPfbzEntity sgcPfbzEntity){
        return R.ok(pfbzService.addPfbz(sgcPfbzEntity));
    }

    @ApiOperation(value = "修改评分标准", notes = "")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {"createdBy","createdTime","updatedBy","updatedTime"})
    @YsLog(logType = LogType.ACCESS,oprType = OprType.INSERT,description = "修改评分标准", module = "评分标准管理")
    @PostMapping("/updatePfbz")
    public R updatePfbz(@Validated @RequestBody SgcPfbzEntity sgcPfbzEntity){
        return R.ok(pfbzService.updatePfbz(sgcPfbzEntity));
    }

    @ApiOperation(value = "删除评分标准", notes = "")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pfbzid", value = "评分标准ID", required = true, dataType = "String")
    })
    @YsLog(logType = LogType.ACCESS,oprType = OprType.INSERT,description = "删除评分标准", module = "评分标准管理")
    @GetMapping("/removePfbz")
    public R removePfbz(@NotBlank(message = "评分标准id不能为空") @RequestParam String pfbzid){
        return R.ok(pfbzService.removePfbz(pfbzid));
    }

    @ApiOperation(value = "大分类-详细分类查询", notes = "")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "flid", value = "分类ID", required = false, dataType = "String")
    })
    @YsLog(logType = LogType.ACCESS,oprType = OprType.INSERT,description = "大分类-详细分类查询", module = "评分标准管理")
    @GetMapping("/listDflorXfl")
    public R<List<SgcPfbzFlbEntity>> listDflorXfl(@RequestParam String flid){
        return R.ok(pfbzService.listDflorXfl(flid));
    }
}
