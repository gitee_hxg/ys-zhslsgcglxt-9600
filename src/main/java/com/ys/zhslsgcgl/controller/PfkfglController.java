package com.ys.zhslsgcgl.controller;

import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.ys.common.core.annotation.log.YsLog;
import com.ys.common.core.dto.R;
import com.ys.common.core.enums.log.LogType;
import com.ys.common.core.enums.log.OprType;
import com.ys.zhslsgcgl.bo.KfglBo;
import com.ys.zhslsgcgl.bo.PagekfglBo;
import com.ys.zhslsgcgl.dto.PfkfDto;
import com.ys.zhslsgcgl.service.PfkfglService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

/**
 * @Description: 检查计分-扣分管理
 * @Author: chenxingjian
 * CreateDate: 2022/11/9 14:44
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Api(tags = "检查计分-扣分管理")
@RestController
@Validated
@RequestMapping("/kfgl")
public class PfkfglController {

    @Autowired
    private PfkfglService pfkfglService;


    @ApiOperation(value = "新增项目评分", notes = "")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS,oprType = OprType.INSERT,description = "新增项目评分", module = "检查计分")
    @PostMapping("/addPfkf")
    public R addPfkf(@Validated @RequestBody KfglBo kfglBo){
        return pfkfglService.addPfkf(kfglBo);
    }


    @ApiOperation(value = "分页查询扣分管理信息", notes = "")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS,oprType = OprType.QUERY,description = "分页查询扣分管理信息", module = "检查计分")
    @PostMapping("/queryPagePfkf")
    public R<PageInfo<List<PfkfDto>>> queryPagePfkf(@Validated @RequestBody PagekfglBo pagekfglBo){
        return R.ok(pfkfglService.queryPagePfkf(pagekfglBo));
    }


    @ApiOperation(value = "验证20天内是否新增", notes = "")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dwid", value = "单位ID", required = true, dataType = "String"),
            @ApiImplicitParam(name = "bdid", value = "标段ID", required = true, dataType = "String")
    })
    @YsLog(logType = LogType.ACCESS,oprType = OprType.QUERY,description = "验证20天内是否新增", module = "检查计分")
    @GetMapping("/checkTjsj")
    public R checkTjsj(@RequestParam String dwid, @RequestParam String bdid) throws ParseException {
        return pfkfglService.checkTjsj(dwid,bdid);
    }


    @ApiOperation(value = "编辑项目评分", notes = "")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS,oprType = OprType.UPDATE,description = "编辑项目评分", module = "检查计分")
    @PostMapping("/updatePfkf")
    public R updatePfkf(@Validated @RequestBody KfglBo kfglBo){
        return R.ok(pfkfglService.updatePfkf(kfglBo));
    }

    @ApiOperation(value = "删除项目评分", notes = "")
    @ApiOperationSupport(author = "chenxingjian", ignoreParameters = {})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pfkfid", value = "评分扣分ID", required = true, dataType = "String")
    })
    @YsLog(logType = LogType.ACCESS,oprType = OprType.DELETE,description = "删除项目评分", module = "检查计分")
    @GetMapping("/deletePfkfByid")
    public R deletePfkfByid( @RequestParam String pfkfid){
        return pfkfglService.deletePfkfByid(pfkfid);
    }
}
