package com.ys.zhslsgcgl.controller;

import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.ys.common.core.annotation.log.YsLog;
import com.ys.common.core.dto.R;
import com.ys.common.core.enums.log.LogType;
import com.ys.common.core.enums.log.OprType;
import com.ys.zhslsgcgl.bo.XjlzqkUpdateBo;
import com.ys.zhslsgcgl.bo.XjrwQueryPageBo;
import com.ys.zhslsgcgl.bo.XjrwUpdateBo;
import com.ys.zhslsgcgl.dto.XjrwDto;
import com.ys.zhslsgcgl.dto.XjrwxqDto;
import com.ys.zhslsgcgl.service.XjrwService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @Description:巡检任务Controller
 * @Author: wugangzhi
 * @CreateData: 2022/11/17 1:10
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Api(tags = "巡检任务")
@Validated
@RestController
@RequestMapping("xjrw")
public class XjrwController {
    @Autowired
    private XjrwService xjrwService;

    @ApiOperation(value = "分页查询巡检任务", notes = "")
    @ApiOperationSupport(author = "wugangzhi", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "分页查询巡检任务", module = "巡检任务")
    @PostMapping("/queryPageXjrw")
    public R<PageInfo<List<XjrwDto>>> queryPageXjrw(@RequestBody XjrwQueryPageBo bo) {
        return R.ok(xjrwService.queryPageXjrw(bo));
    }

    @ApiOperation(value = "修改巡检任务状态", notes = "")
    @ApiOperationSupport(author = "wugangzhi", ignoreParameters = {})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "xjrwid", value = "巡检任务ID", required =true, dataType = "String"),
            @ApiImplicitParam(name = "rwzt", value = "任务状态 (0-未开始 1-巡检中 2-已完成)", required =true, dataType = "String")
    })
    @YsLog(logType = LogType.ACCESS,oprType = OprType.UPDATE,description = "修改巡检任务状态", module = "巡检任务")
    @GetMapping("/updateXjRwzt")
    public R updateXjRwzt(@NotBlank(message = "巡检任务ID不能为空") @RequestParam("xjrwid") String xjrwid,@NotBlank(message = "任务状态不能为空") @RequestParam("rwzt") String rwzt){
        return R.ok(xjrwService.updateXjRwzt(xjrwid,rwzt));
    }

    @ApiOperation(value = "根据巡检任务ID查询巡检详情", notes = "")
    @ApiOperationSupport(author = "wugangzhi", ignoreParameters = {})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "xjrwid", value = "巡检任务ID", required =true, dataType = "String")
    })
    @YsLog(logType = LogType.ACCESS,oprType = OprType.QUERY,description = "根据巡检任务ID查询巡检详情")
    @GetMapping("/getXjrwXq")
    public R<XjrwxqDto> getXjrwXq(@NotBlank(message = "巡检任务ID不能为空") @RequestParam("xjrwid") String xjrwid){
        return R.ok(xjrwService.getXjrwXq(xjrwid));
    }

    @ApiOperation(value = "提交巡检情况", notes = "")
    @ApiOperationSupport(author = "wugangzhi", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS, oprType = OprType.UPDATE, description = "提交巡检情况", module = "巡检任务")
    @PostMapping("/updateXjqk")
    public R updateXjqk(@RequestBody XjrwUpdateBo bo) {
        return R.ok(xjrwService.updateXjqk(bo));
    }


    @ApiOperation(value = "履职情况填写", notes = "")
    @ApiOperationSupport(author = "wugangzhi", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS, oprType = OprType.UPDATE, description = "履职情况填写", module = "巡检任务")
    @PostMapping("/updateXjlzqk")
    public R updateXjlzqk(@RequestBody XjlzqkUpdateBo bo) {
        return R.ok(xjrwService.updateXjlzqk(bo));
    }

}
