package com.ys.zhslsgcgl.controller;

import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.ys.common.core.annotation.log.YsLog;
import com.ys.common.core.dto.R;
import com.ys.common.core.enums.log.LogType;
import com.ys.common.core.enums.log.OprType;
import com.ys.zhslsgcgl.bo.DwxxBo;
import com.ys.zhslsgcgl.bo.PageDwLsdfBo;
import com.ys.zhslsgcgl.dto.CxglDto;
import com.ys.zhslsgcgl.dto.SxbgDto;
import com.ys.zhslsgcgl.entity.SgcDwxxLsdfEntity;
import com.ys.zhslsgcgl.entity.SgcHmdEntity;
import com.ys.zhslsgcgl.entity.SgcSxbgEntity;
import com.ys.zhslsgcgl.entity.SgcZdgzEntity;
import com.ys.zhslsgcgl.service.CxglService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @Description:市场监督-诚信管理
 * @Author: dzg
 * @CreateDate: 2022/11/8 11:26
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Api(tags = "市场监督-诚信管理")
@RestController
@Validated
@RequestMapping("cxgl")
public class CxglController {

    @Autowired
    private CxglService cxglService;

    @ApiOperation(value = "分页查询单位历史得分")
    @ApiOperationSupport(author = "fsx")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "分页查询单位历史得分", module = "诚信管理")
    @PostMapping("/queryPageLsdf")
    public R<PageInfo<List<SgcDwxxLsdfEntity>>> queryPageLsdf(@RequestBody PageDwLsdfBo dwLsdfBo) {
        return R.ok(cxglService.queryPageLsdf(dwLsdfBo));
    }

    @ApiOperation(value = "分页查询诚信管理单位信息", notes = "")
    @ApiOperationSupport(author = "dzg", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "分页查询诚信管理单位信息", module = "诚信管理")
    @PostMapping("/queryPageCxgl")
    public R<PageInfo<List<CxglDto>>> queryPageCxgl(@RequestBody DwxxBo dwxxBo) {
        return R.ok(cxglService.queryPageCxgl(dwxxBo));
    }

    @ApiOperation(value = "分页查询诚信管理单位信息黑名单", notes = "")
    @ApiOperationSupport(author = "dzg", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "分页查询诚信管理单位信息黑名单", module = "诚信管理")
    @PostMapping("/queryPageCxglHmd")
    public R<PageInfo<List<CxglDto>>> queryPageCxglHmd(@RequestBody DwxxBo dwxxBo) {
        return R.ok(cxglService.queryPageCxglHmd(dwxxBo));
    }

    @ApiOperation(value = "分页查询诚信管理单位信息重点关注", notes = "")
    @ApiOperationSupport(author = "dzg", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "分页查询诚信管理单位信息重点关注", module = "诚信管理")
    @PostMapping("/queryPageCxglZdgz")
    public R<PageInfo<List<CxglDto>>> queryPageCxglZdgz(@RequestBody DwxxBo dwxxBo) {
        return R.ok(cxglService.queryPageCxglZdgz(dwxxBo));
    }

    @ApiOperation(value = "分页查询诚信管理单位信息失信曝光", notes = "")
    @ApiOperationSupport(author = "dzg", ignoreParameters = {})
    @YsLog(logType = LogType.ACCESS, oprType = OprType.QUERY, description = "分页查询诚信管理单位信息失信曝光", module = "诚信管理")
    @PostMapping("/queryPageCxglSxbg")
    public R<PageInfo<List<SxbgDto>>> queryPageCxglSxbg(@RequestBody DwxxBo dwxxBo) {
        return R.ok(cxglService.queryPageCxglSxbg(dwxxBo));
    }


    @ApiOperation(value = "新增黑名单", notes = "")
    @ApiOperationSupport(author = "dzg", ignoreParameters = {"createdBy", "createdTime", "updatedBy", "updatedTime"})
    @YsLog(oprType = OprType.INSERT, logType = LogType.ACCESS, description = "新增单位加入黑名单", module = "诚信管理")
    @PostMapping("/addHmd")
    public R addDwxx(@RequestBody @Validated SgcHmdEntity sgcHmdEntity) {
        return R.ok(cxglService.addHmd(sgcHmdEntity));
    }

    @ApiOperation(value = "新增失信曝光", notes = "")
    @ApiOperationSupport(author = "dzg", ignoreParameters = {"createdBy", "createdTime", "updatedBy", "updatedTime"})
    @YsLog(oprType = OprType.INSERT, logType = LogType.ACCESS, description = "新增失信曝光", module = "诚信管理")
    @PostMapping("/addSxbg")
    public R addSxbg(@RequestBody @Validated SgcSxbgEntity sgcSxbgEntity) {
        return R.ok(cxglService.addSxbg(sgcSxbgEntity));
    }

    @ApiOperation(value = "新增重点关注", notes = "")
    @ApiOperationSupport(author = "dzg", ignoreParameters = {"createdBy", "createdTime", "updatedBy", "updatedTime"})
    @YsLog(oprType = OprType.INSERT, logType = LogType.ACCESS, description = "新增重点关注", module = "诚信管理")
    @PostMapping("/addZdgz")
    public R addZdgz(@RequestBody @Validated SgcZdgzEntity sgcZdgzEntity) {
        return R.ok(cxglService.addZdgz(sgcZdgzEntity));
    }

    @ApiOperation(value = "解除黑名单")
    @ApiOperationSupport(author = "dzg")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.DELETE, description = "解除黑名单", module = "诚信管理")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dwid", value = "单位ID", required = true, dataType = "String")
    })
    @GetMapping("/deleteHmdByDwid")
    public R deleteHmdByDwid(@NotBlank(message = "单位ID不能为空") @RequestParam("dwid") String dwid) {
        return R.ok(cxglService.deleteHmdByDwid(dwid));
    }

    @ApiOperation(value = "解除关注")
    @ApiOperationSupport(author = "dzg")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.DELETE, description = "解除关注", module = "诚信管理")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dwid", value = "单位ID", required = true, dataType = "String")
    })
    @GetMapping("/deleteZdgzByDwid")
    public R deleteZdgzByDwid(@NotBlank(message = "单位ID不能为空") @RequestParam("dwid") String dwid) {
        return R.ok(cxglService.deleteZdgzByDwid(dwid));
    }

    @ApiOperation(value = "删除失信曝光")
    @ApiOperationSupport(author = "dzg")
    @YsLog(logType = LogType.ACCESS, oprType = OprType.DELETE, description = "删除失信曝光", module = "诚信管理")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pbid", value = "曝光ID", required = true, dataType = "String")
    })
    @GetMapping("/deleteSxbgByPbid")
    public R deleteSxbgByPbid(@NotBlank(message = "曝光ID不能为空") @RequestParam("pbid") String pbid) {
        return R.ok(cxglService.deleteSxbgByPbid(pbid));
    }

    @ApiOperation(value = "修改失信曝光信息", notes = "")
    @ApiOperationSupport(author = "dzg", ignoreParameters = {"createdBy", "createdTime", "updatedBy", "updatedTime"})
    @YsLog(oprType = OprType.UPDATE, logType = LogType.ACCESS, description = "修改失信曝光信息", module = "单位管理")
    @PostMapping("/updateSxbg")
    public R updateSxbg(@RequestBody @Validated SgcSxbgEntity sgcSxbgEntity) {
        return R.ok(cxglService.updateSxbg(sgcSxbgEntity));
    }


}
