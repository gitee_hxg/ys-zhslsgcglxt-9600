package com.ys.zhslsgcgl.util;

import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

/**
 * @Description: list工具
 * @Author: 黄祥光 1306175106@qq.com
 * @CreateDate: 2022/9/15 20:30
 * @Version: 1.0
 * Copyright (c) 2020,武汉中地云申科技有限公司
 * All rights reserved.
 **/
public class ListUtil {

    public  static  <T> void handleSerialNumber(List<T> dataList, BiConsumer<T, Integer> m) {
        if(!CollectionUtils.isEmpty(dataList)){
            for (int i = 0; i < dataList.size(); i++) {
                m.accept(dataList.get(i),i+1);
            }
        }

    }


    /**
     * 交集
     */
    public static final String LIST1_AND_LIST2 = "0";

    /**
     * 差集（list1 - list2）
     */
    public static final String LIST1_REMOVE_LIST2 = "1";

    /**
     * 差集（list2 - list1）
     */
    public static final String LIST2_REMOVE_LIST1 = "2";


    public static <T> Map<String, List<T>> getMap(List<T> list1, List<T> list2) {

        //交集
        List<T> intersection = list1.stream().filter(item -> list2.contains(item)).collect(Collectors.toList());

        //差集（list1 - list2）
        List<T> reduce1 = list1.stream().filter(item -> !list2.contains(item)).collect(Collectors.toList());

        //差集（list2 - list1）
        List<T> reduce2 = list2.stream().filter(item -> !list1.contains(item)).collect(Collectors.toList());

        Map<String, List<T>> map = new HashMap<>();
        map.put(LIST1_AND_LIST2, intersection);
        map.put(LIST1_REMOVE_LIST2, reduce1);
        map.put(LIST2_REMOVE_LIST1, reduce2);

        return map;
    }
}
