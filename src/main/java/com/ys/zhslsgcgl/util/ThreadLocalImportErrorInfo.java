package com.ys.zhslsgcgl.util;


import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 导入错误描述
 * @Author: 黄祥光 1306175106@qq.com
 * @CreateDate: 2021/3/23 22:59
 * @Version: 1.0
 * Copyright (c) 2020,武汉中地云申科技有限公司
 * All rights reserved.
 **/
public class ThreadLocalImportErrorInfo {
    private static final  ThreadLocal<List<String>> threadLocal=ThreadLocal.withInitial(ArrayList::new);

    public static List<String> get(){
       return threadLocal.get();
    }

    public static void set(String  errorInfo){
        threadLocal.get().add(errorInfo);
    }

    public static void remove(){
        threadLocal.remove();
    }


}
