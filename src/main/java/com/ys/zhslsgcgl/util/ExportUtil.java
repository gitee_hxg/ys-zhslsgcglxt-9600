package com.ys.zhslsgcgl.util;

import org.jxls.common.Context;
import org.jxls.transform.Transformer;
import org.jxls.transform.poi.PoiTransformer;
import org.jxls.util.JxlsHelper;
import org.springframework.core.io.ClassPathResource;

import java.io.*;
import java.util.Calendar;
import java.util.Map;
import java.util.Objects;

/**
 * @Description: 导出相关简单操作
 * @Author: 黄祥光 1306175106@qq.com
 * @CreateDate: 2021/1/4 17:08
 * @UpdateUser:
 * @UpdateDate: 2021/1/4 17:08
 * @UpdateRemark:
 * @Version: 1.0
 * Copyright (c) 2020,武汉中地云申科技有限公司
 * All rights reserved.
 **/
public class ExportUtil {


    /**
     * @Author huangxiangguang
     * @Description 根据模板导出
     * @Date 10:04 2021/1/11
     * @Param [templatePath, targetPath, model]
     * @return void
     **/
    public static void exportWithTemplate( String  templatePath,  String  targetPath,  Map<String, Object> model,Integer lastCommentedColumn) {
        ClassPathResource cpr = new ClassPathResource(templatePath);
        try {
            File f=new File(targetPath);
            if(!f.getParentFile().exists()){
                f.getParentFile().mkdirs();
            }
            f.deleteOnExit();
            FileOutputStream os = new FileOutputStream(f);
            exportWithTemplate(cpr.getInputStream(),os,model,lastCommentedColumn);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * @Author huangxiangguang
     * @Description 根据模板导出
     * @Date 10:04 2021/1/11
     * @Param [is, os, model]
     * @return void
     **/
    public static void exportWithTemplate( InputStream is,  OutputStream  os,  Map<String, Object> model,Integer lastCommentedColumn) throws IOException {
        Context context = PoiTransformer.createInitialContext();
        if (model != null) {
            for (String key : model.keySet()) {
                context.putVar(key, model.get(key));
            }
        }
        JxlsHelper jxlsHelper = JxlsHelper.getInstance();
        try {
        Transformer transformer  = jxlsHelper.createTransformer(is, os);
        if(Objects.nonNull(lastCommentedColumn)){
            ((PoiTransformer)transformer).setLastCommentedColumn(lastCommentedColumn);
        }
        //必须要这个，否者表格函数统计会错乱
        jxlsHelper.setUseFastFormulaProcessor(false).processTemplate(context, transformer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally{
            is.close();
            os.close();
        }

    }


    }
