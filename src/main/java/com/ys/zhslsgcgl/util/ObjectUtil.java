package com.ys.zhslsgcgl.util;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @Description: map工具类
 * @Author: 黄祥光 1306175106@qq.com
 * @CreateDate: 2022/7/14 18:31
 * @Version: 1.0
 * Copyright (c) 2020,武汉中地云申科技有限公司
 * All rights reserved.
 **/
public class ObjectUtil {

    public static  <T>T safeConvert(Object value,Class<T> clazz) {
        if(Objects.isNull(value)) {
            return null;
        }
        String vStr = String.valueOf(value).trim();
        if(!StringUtils.hasText(vStr)){
            return null;
        }

            if (clazz.equals(String.class)) {
                return (T) vStr.trim();
            } else if (clazz.equals(Integer.class)) {
                return (T) Integer.valueOf(vStr);
            } else if (clazz.equals(Double.class)) {
                return (T) Double.valueOf(vStr);
            } else if (clazz.equals(Float.class)) {
                return (T) Float.valueOf(vStr);
            } else if (clazz.equals(LocalDate.class)) {
                if(vStr.length()<4){
                    return null;
                }
                LocalDateTime dateTime;
                try {
                    dateTime = DateUtil.parseLocalDateTime(vStr);
                }catch (DateTimeParseException e){
                    try {
                        dateTime = DateUtil.parseLocalDateTime(vStr, "yyyy-MM-dd");
                    }catch (DateTimeParseException e2){
                        String normalize = normalize(vStr);
                        String[] split = normalize.split("-");
                        int year = Integer.parseInt(split[0]);
                        int month = split.length >= 2 ? Integer.parseInt(split[1]) : 1;
                        int dayOfMonth = split.length >= 3 ? Integer.parseInt(split[2]) : 1;
                        dateTime = LocalDate.of(year, month, dayOfMonth).atStartOfDay();
                    }

                }
                return (T) dateTime.toLocalDate();

            } else if (clazz.equals(LocalDateTime.class)) {
                if(vStr.length()<4){
                    return null;
                }
                return (T) DateUtil.parseLocalDateTime(vStr);

            }

        return null;
    }

    /**
     * 标准化日期，默认处理以空格区分的日期时间格式，空格前为日期，空格后为时间：<br>
     * 将以下字符替换为"-"
     *
     * <pre>
     * "."
     * "/"
     * "年"
     * "月"
     * </pre>
     * <p>
     * 将以下字符去除
     *
     * <pre>
     * "日"
     * </pre>
     * <p>
     * 将以下字符替换为":"
     *
     * <pre>
     * "时"
     * "分"
     * "秒"
     * </pre>
     * <p>
     * 当末位是":"时去除之（不存在毫秒时）
     *
     * @param dateStr 日期时间字符串
     * @return 格式化后的日期字符串
     */
    private static String normalize(CharSequence dateStr) {
        if (StrUtil.isBlank(dateStr)) {
            return StrUtil.str(dateStr);
        }

        // 日期时间分开处理
        final List<String> dateAndTime = StrUtil.splitTrim(dateStr, ' ');
        final int size = dateAndTime.size();
        if (size < 1 || size > 2) {
            // 非可被标准处理的格式
            return StrUtil.str(dateStr);
        }

        final StringBuilder builder = StrUtil.builder();

        // 日期部分（"\"、"/"、"."、"年"、"月"都替换为"-"）
        String datePart = dateAndTime.get(0).replaceAll("[/.年月]", "-");
        datePart = StrUtil.removeSuffix(datePart, "日");
        builder.append(datePart);

        // 时间部分
        if (size == 2) {
            builder.append(' ');
            String timePart = dateAndTime.get(1).replaceAll("[时分秒]", ":");
            timePart = StrUtil.removeSuffix(timePart, ":");
            builder.append(timePart);
        }

        return builder.toString();
    }



}
