package com.ys.zhslsgcgl.util;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.*;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.geotools.geojson.geom.GeometryJSON;
import org.geotools.geometry.jts.JTSFactoryFinder;

import java.io.*;

/**
 * @Description: 地理工具
 * @Author: 黄祥光 1306175106@qq.com
 * @CreateDate: 2022/1/7 20:10
 * @Version: 1.0
 * Copyright (c) 2020,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Slf4j
public class GeometryUtil {



    /**
     * geo转bytes
     * @param geometry
     * @return
     */
    public static  byte[] convertToBytes(Geometry geometry) {
        WKBWriter writer = new WKBWriter();
        byte[] geometryBytes = writer.write(geometry);
        byte[] wkb = new byte[geometryBytes.length + 4];
        //设置SRID为4326
        ByteOrderValues.putInt(4326, wkb, ByteOrderValues.LITTLE_ENDIAN);
        System.arraycopy(geometryBytes, 0, wkb, 4, geometryBytes.length);
        return wkb;
    }

    /**
     * 流 转 geometry
     * */
    public static Geometry getGeometryFromInputStream(InputStream inputStream) throws Exception {

        Geometry dbGeometry = null;

        if (inputStream != null) {
            // 二进制流转成字节数组
            byte[] buffer = new byte[255];

            int bytesRead;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                baos.write(buffer, 0, bytesRead);
            }
            // 得到字节数组
            byte[] geometryAsBytes = baos.toByteArray();
            dbGeometry = getGeometryByBytes(geometryAsBytes);
        }
        return dbGeometry;
    }
    /**
     * 直接把数据库中的byte[]转Geometry对象
     * @param geometryAsBytes
     * @return
     * @throws Exception
     */
    public static Geometry getGeometryByBytes(byte[]  geometryAsBytes){
        Geometry dbGeometry = null;
        // 字节数组小于5，说明geometry有问题
        if (geometryAsBytes.length < 5) {
            return null;
        }

        //这里是取字节数组的前4个来解析srid
        byte[] sridBytes = new byte[4];
        System.arraycopy(geometryAsBytes, 0, sridBytes, 0, 4);
        boolean bigEndian = (geometryAsBytes[4] == 0x00);
        // 解析srid
        int srid = 0;
        if (bigEndian) {
            for (int i = 0; i < sridBytes.length; i++) {
                srid = (srid << 8) + (sridBytes[i] & 0xff);
            }
        } else {
            for (int i = 0; i < sridBytes.length; i++) {
                srid += (sridBytes[i] & 0xff) << (8 * i);
            }
        }
        //use the JTS WKBReader for WKB parsing
        WKBReader wkbReader = new WKBReader();
        // 使用geotool的WKBReader 把字节数组转成geometry对象。
        byte[] wkb = new byte[geometryAsBytes.length - 4];
        System.arraycopy(geometryAsBytes, 4, wkb, 0, wkb.length);
        try {
            dbGeometry = wkbReader.read(wkb);
        } catch (ParseException e) {
            log.error("空间类型转换失败，{}",e);
        }
        dbGeometry.setSRID(srid);
        return dbGeometry;
    }

    /**
     * 由wkt格式字符串获取geometry
     * @param wkt
     * @return
     */
    public static Geometry getGeometryByString(String wkt){
        WKTReader reader = new WKTReader();
        Geometry geometry = null;
        try {
            geometry = reader.read(wkt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return geometry;
    }


    /**
     * 由wkt格式的geometry生成geojson
     * @param geometryAsBytes
     * @return
     */
    public static String wktToJson(byte[]  geometryAsBytes) {
        Geometry geometry = getGeometryByBytes(geometryAsBytes);
        return wktToJson(geometry);
    }


    /**
     * 由wkt格式的geometry生成geojson
     * @param wkt
     * @return
     */
    public static String wktToJson(String wkt) {
        Geometry geometry = getGeometryByString(wkt);
        return wktToJson(geometry);
    }

    /**
     * 由wkt格式的geometry生成geojson
     * @param geometry
     * @return
     */
    public static String wktToJson(Geometry  geometry){
        String json = null;
        try {
            StringWriter writer = new StringWriter();
            GeometryJSON g = new GeometryJSON(20);
            g.write(geometry, writer);
            json = writer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }


    /**
     * 由geojson格式生成geometry
     * @param geojson
     * @return
     */
    public static Geometry jsonToGeometry(String geojson){
        GeometryJSON gjson = new GeometryJSON(20);
        Geometry geometry = null;
        try {
            geometry = gjson.read(geojson);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return geometry;
    }

    /**
     * 点在多边形
     * @param x
     * @param y
     * @param polygonBytes
     * @return
     */
    @SneakyThrows
    public static  boolean pointInPolygon(Double x, Double y, byte[] polygonBytes){
        String wktPointTemplate="POINT (%f %f)";
        String wktPoint=String.format(wktPointTemplate,x,y);
        WKTReader reader = new WKTReader(JTSFactoryFinder.getGeometryFactory());
        Geometry point = reader.read(wktPoint);
        Geometry polygon = getGeometryByBytes(polygonBytes);
        return polygon.contains(point);
    }

    public static Double coordinateTrans(String degree,String minute,String second){
        if(StringUtils.isBlank(degree)&&StringUtils.isBlank(minute)&&StringUtils.isBlank(second)){
            return null;
        }
        Double d =0.0;
        Double m =0.0;
        Double s =0.0;
        if( StringUtils.isNotBlank(degree)){
            d =Double.parseDouble(degree);
        }if( StringUtils.isNotBlank(minute)){
            m =Double.parseDouble(minute)/60;
        }if( StringUtils.isNotBlank(second)){
            s =Double.parseDouble(second)/3600;
        }

        return d+m+s;

    }

    //地球平均半径
    private static final double EARTH_RADIUS = 6378137;
    //把经纬度转为度（°）
    private static double rad(double d){
        return d * Math.PI / 180.0;
    }

    /**
     * 根据两点间经纬度坐标（double值），计算两点间距离，单位为米
     * @param lng1
     * @param lat1
     * @param lng2
     * @param lat2
     * @return
     */
    public static double getDistance(double lng1, double lat1, double lng2, double lat2){
        double radLat1 = rad(lat1);
        double radLat2 = rad(lat2);
        double a = radLat1 - radLat2;
        double b = rad(lng1) - rad(lng2);
        double s = 2 * Math.asin(
                Math.sqrt(
                        Math.pow(Math.sin(a/2),2)
                                + Math.cos(radLat1)*Math.cos(radLat2)*Math.pow(Math.sin(b/2),2)
                )
        );
        s = s * EARTH_RADIUS;
        s = Math.round(s * 10000) / 10000;
        return s;
    }

    /**
     * 将经纬度转换为度分秒格式
     * @param du 度数 116.418847
     * @return 116°25'7.85"
     */
    public static Integer[] changeToDFMArray(double du) {
        //[0] 度 ，[1] 分 ,[2] 秒
        Integer[] dfmArray=new Integer[3];
        try {
            String str = "" + du;
            int p = str.indexOf(".");
            int dt = Integer.parseInt(str.substring(0, p));
            du = du - dt;
            double M = du * 60;
            int mt = (int) M;
            M = (M - mt) * 60;
            //秒精确到小数点后三位，小于0.001向前进位，秒为0，分加1，分为60，度加1
            if (Math.abs(M - 60) < 0.001) {
                M = 0;
                mt = mt + 1;
            }
            if (mt == 60) {
                dt = dt + 1;
                mt = 0;
            }
            dfmArray[0]=dt;
            dfmArray[1]=mt ;
            // 四舍五入 小数点前整数进位
            dfmArray[2]=(int) Math.round(M);
        } catch(Exception e)  {
            log.error("小数格式度转度分秒异常："+e.getMessage(),e);
        }
        return dfmArray;
    }


    public static void main(String[] args) {
        System.out.println(jsonToGeometry("{\n" +
                "  \"type\": \"MultiPolygon\",\n" +
                "  \"coordinates\":\n" +
                "    [ \n" +
                "        [\n" +
                "            [\n" +
                "                [109.2041015625,30.088107753367257],\n" +
                "                [115.02685546875,30.088107753367257],\n" +
                "                [115.02685546875,32.7872745269555],\n" +
                "                [109.2041015625,32.7872745269555],\n" +
                "                [109.2041015625,30.088107753367257]\n" +
                "          \n" +
                "          \n" +
                "            ]\n" +
                "        ],\n" +
                "        [\n" +
                "            [\n" +
                "                [112.9833984375,26.82407078047018],\n" +
                "                [116.69677734375,26.82407078047018],\n" +
                "                [116.69677734375,29.036960648558267],\n" +
                "                [112.9833984375,29.036960648558267],\n" +
                "                [112.9833984375,26.82407078047018]\n" +
                "            ]\n" +
                "        ]\n" +
                "    ]\n" +
                "             }"));
    }
}
