package com.ys.zhslsgcgl.util;

import cn.hutool.core.util.ReflectUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Objects;

/**
 * @Description: 项目模块工具类
 * @Author: 黄祥光 1306175106@qq.com
 * @CreateDate: 2022/7/8 16:01
 * @Version: 1.0
 * Copyright (c) 2020,武汉中地云申科技有限公司
 * All rights reserved.
 **/
public class ProjectModelUtil {

    /***
     * 判断模块的数据是不是空的
     * @return
     */
    public static  boolean isEmptyModel(Object model){
        if(Objects.nonNull(model)){

            Field[] fields = ReflectUtil.getFields(model.getClass());
            for (Field field : fields) {
                if("xmid".equalsIgnoreCase(field.getName())||"id".equalsIgnoreCase(field.getName())||"tbr".equalsIgnoreCase(field.getName())||"tbrmc".equalsIgnoreCase(field.getName())||"shzt".equalsIgnoreCase(field.getName())||"tbsj".equalsIgnoreCase(field.getName())){
                    continue;
                }
                field.setAccessible(true);
                try {
                    Object obj = field.get(model);
                    if(Objects.nonNull(obj)){
                        if(String.class.equals(field.getType())){
                            if(StringUtils.isNotBlank((String) obj)){
                                return false;
                            }
                        }else{
                            return false;
                        }
                    }
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return true;
    };


    /***
     * 判断模块的数据是否有变化
     * @return
     */
    public static  boolean isDataChange(Object oldModel,Object newModel){
        if(Objects.isNull(oldModel)&&Objects.isNull(newModel)){
            return false;
        }
        if(Objects.isNull(oldModel)||Objects.isNull(newModel)){
            return true;
        }


            Field[] fields = ReflectUtil.getFields(oldModel.getClass());
            for (Field field : fields) {
                if("id".equalsIgnoreCase(field.getName())||"tbr".equalsIgnoreCase(field.getName())||"tbrmc".equalsIgnoreCase(field.getName())||"tbsj".equalsIgnoreCase(field.getName())||"shzt".equalsIgnoreCase(field.getName())){
                    continue;
                }

                try {
                    field.setAccessible(true);
                    Object oldValue = field.get(oldModel);
                    Object newValue = field.get(newModel);

                    if(oldValue instanceof Collection&&newValue instanceof Collection){
                        if(((Collection<?>) oldValue).size()!=((Collection<?>) newValue).size()){
                            boolean isEquals = CollectionUtils.isEqualCollection((Collection<?>) oldValue, (Collection<?>) newValue);
                            if(!isEquals){
                                return false;
                            }
                        }
                    }
                    boolean isEquals = Objects.equals(oldValue, newValue);
                    if(!isEquals){
                        return false;
                    }
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }


            }

        return true;
    };

}
