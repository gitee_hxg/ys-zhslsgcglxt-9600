package com.netsdk.utils;


import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Description: ReadExcel工具类
 * @Author: 黄祥光 1306175106@qq.com
 * @CreateDate: 2021/7/7 14:24
 * @Version: 1.0
 * Copyright (c) 2020,武汉中地云申科技有限公司
 * All rights reserved.
 **/
public class ReadExcel {

    /**
     * getWorkbook
     * @param file
     * @return
     * @throws IOException
     */
    public static Workbook getWorkbook(MultipartFile file) throws IOException {
        Workbook workbook = null;
        //判断文件是否存在
        String filename=file.getOriginalFilename();
            //判断文件名后缀
            if (!filename.endsWith("xls") && !filename.endsWith("xlsx")) {
                throw new IOException("文件格式不正确");
            } else {
                //产生文件流
                InputStream is = file.getInputStream();
                //如果是xls
                if (filename.endsWith("xls")) {
                    //2003版本
                    workbook = new HSSFWorkbook(is);
                } else {
                    //2007及以上版本
                    workbook = new XSSFWorkbook(is);
                }
            }
        return workbook;
    }

    /**
     * getCellValue
     * @param cell
     * @return
     */
    public static String getCellValue(Cell cell) {
        String cellValue = null;
        if (cell != null) {
            switch (cell.getCellType()) {
                //数字又分日期和非日期
                case NUMERIC:
                    if (HSSFDateUtil.isCellDateFormatted(cell)) {
                        //如果单元格设置成了日期格式，就进入这个判断
                        SimpleDateFormat sdf = null;
                        if (cell.getCellStyle().getDataFormat() == HSSFDataFormat
                                .getBuiltinFormat("h:mm")) {
                            sdf = new SimpleDateFormat("HH:mm");
                        } else {// 日期
                            sdf = new SimpleDateFormat("yyyy-MM-dd");
                        }
                        Date date = cell.getDateCellValue();
                        cellValue = sdf.format(date);
                    } else if (cell.getCellStyle().getDataFormat() == 58) {
                        //如果单元格没有设置日期格式，而是自己输入的就是日期格式，就进入这个判断
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
                        double value = cell.getNumericCellValue();
                        Date date = org.apache.poi.ss.usermodel.DateUtil
                                .getJavaDate(value);
                        cellValue = sdf.format(date);
                    } else {
                        BigDecimal bd = new BigDecimal(cell.getNumericCellValue());
                        cellValue = bd.toString();
                    }
                    break;
                case STRING:
                    cellValue = String.valueOf(cell.getStringCellValue());
                    break;
                case BOOLEAN:
                    cellValue = String.valueOf(cell.getBooleanCellValue());
                    break;
                case FORMULA:
                    cellValue = String.valueOf(cell.getCellFormula());
                    break;
                case BLANK:
                    cellValue = "";
                    break;
                case ERROR:
                    cellValue = "未知类型";
                    break;
            }
        }
        return cellValue;
    }

}
