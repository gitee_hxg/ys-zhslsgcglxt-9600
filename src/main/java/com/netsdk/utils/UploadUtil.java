package com.netsdk.utils;

import cn.hutool.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 文件上传
 * @Author: hm
 * @CreateDate: 2022/11/22 14:30
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Component
public class UploadUtil {

    //当前工具类
    private static com.netsdk.utils.UploadUtil uploadUtil;

    @PostConstruct
    public void init() {
        uploadUtil = this;
    }

    @Autowired
    RestTemplate restTemplate;

    private static String hostIp;
    @Value("${custom.hostIp}")
    public void setHostIp(String hostIp) {    //注意这里的set方法不能是静态的
        uploadUtil.hostIp = hostIp;
    }
    private static int hostPort;
    @Value("${custom.hostPort}")
    public void setHostPort(int hostPort) {    //注意这里的set方法不能是静态的
        uploadUtil.hostPort = hostPort;
    }
    private static String filePrefix;
    @Value("${custom.filePrefix}")
    public void setFilePrefix(String filePrefix) {    //注意这里的set方法不能是静态的
        uploadUtil.filePrefix = filePrefix;
    }

    private static String dkljpic;
    @Value("${custom.dkljpic}")
    public void setDkljpic(String dkljpic) {    //注意这里的set方法不能是静态的
        uploadUtil.dkljpic = dkljpic;
    }


    /**
     * 文件上传
     * @param files
     * @return
     */
    public String upload(MultipartFile[] files, String id) {
        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        List<Object> fileList = new ArrayList<>();
        for(MultipartFile file : files) {
            ByteArrayResource byteArrayResource = null;
            try {
                byteArrayResource = new ByteArrayResource(file.getBytes()){
                    @Override
                    public String getFilename() throws IllegalStateException {
                        return file.getOriginalFilename();
                    }
                };
            } catch (IOException e) {
                e.printStackTrace();
            }
            fileList.add(byteArrayResource);
        }
        //使用files上传文件
        map.put("files", fileList);
        map.add("json"," [{\"uploadBy\": \"admin\",  \"ywjlbm\": \"SGC_RYKQJL\", \"ywjlzdm\": \"RYKQJL\",\"ywjlzj\": \""+ id +"\"}]" );
        map.add( "url",dkljpic);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType( MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<>(map, httpHeaders);
        String uploadUrl =  "http://" + hostIp + ":" + hostPort + "/api/common-file/dmtgl/commonUpload";
        ResponseEntity<JSONObject> result = uploadUtil.restTemplate.exchange(uploadUrl, HttpMethod.POST, request, JSONObject.class);
        return result.getBody().toString();
    }

}
