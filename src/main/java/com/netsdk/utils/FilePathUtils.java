package com.netsdk.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ClassUtils;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.net.URL;

/***
 * @Description: 文件工具类
 * @Author: 徐铭阳
 * @CreateDate: 2022/11/8 21:28
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/

@Slf4j
public class FilePathUtils {

    public static String encodeUrlPath(String path) {
        int index = path.lastIndexOf("/");
        String fileName = path.substring(index+1);
        try {
            fileName = URLEncoder.encode(fileName, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        path = path.substring(0, index) + "/slt_" + fileName;
        return path;
    }
}
