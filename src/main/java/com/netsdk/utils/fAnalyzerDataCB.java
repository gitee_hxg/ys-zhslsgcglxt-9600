package com.netsdk.utils;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.netsdk.lib.NetSDKLib;
import com.netsdk.lib.ToolKits;
import com.sun.jna.Pointer;
import com.ys.common.core.util.UUIDUtil;
import com.ys.zhslsgcgl.entity.SgcRykqDayEntity;
import com.ys.zhslsgcgl.entity.SgcRykqjlEntity;
import com.ys.zhslsgcgl.entity.SgcRysbGlbEntity;
import com.ys.zhslsgcgl.mapper.SgcRykqDayMapper;
import com.ys.zhslsgcgl.mapper.SgcRykqjlMapper;
import com.ys.zhslsgcgl.mapper.SgcRysbGlbMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @Description: 智能报警事件回调
 * @Author: hm
 * @CreateDate: 2022/11/8 15:33
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Component
public  class fAnalyzerDataCB implements
        NetSDKLib.fAnalyzerDataCallBack{
    @Autowired
    SgcRykqjlMapper sgcRykqjlMapper;
    /*@Autowired
    SgcRyglMapper sgcRyglMapper;*/
    @Autowired
    SgcRykqDayMapper sgcRykqDayMapper;
    //人员设备关联信息表
    @Autowired
    SgcRysbGlbMapper sgcRysbGlbMapper;
    @Autowired
    UploadUtil uploadUtil;

    private static String filePrefix;
    @Value("${custom.filePrefix}")
    public void setFilePrefix(String filePrefix) {    //注意这里的set方法不能是静态的
        fAnalyzerDataCB.filePrefix = filePrefix;
    }

    private static String dkljpic;
    @Value("${custom.dkljpic}")
    public void setDkljpic(String dkljpic) {    //注意这里的set方法不能是静态的
        fAnalyzerDataCB.dkljpic = dkljpic;
    }


    //当前工具类
    private static com.netsdk.utils.fAnalyzerDataCB fAnalyzerDataCB;

    @PostConstruct
    public void init() {
        fAnalyzerDataCB = this;
        //fAnalyzerDataCB.sgcRykqjlMapper = this.sgcRykqjlMapper;
    }

    private  static int count=0;
    private fAnalyzerDataCB() {
    }

    private static class fAnalyzerDataCBHolder {
        private static final com.netsdk.utils.fAnalyzerDataCB instance = new fAnalyzerDataCB();
    }

    public static com.netsdk.utils.fAnalyzerDataCB getInstance() {
        return fAnalyzerDataCBHolder.instance;
    }

    @Override
    public int invoke(NetSDKLib.LLong lAnalyzerHandle, int dwAlarmType,
                      Pointer pAlarmInfo, Pointer pBuffer, int dwBufSize,
                      Pointer dwUser, int nSequence, Pointer reserved) {
        System.out.println("dwAlarmType:" + dwAlarmType);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");//定义新的日期格式
        //format():将给定的 Date 格式化为日期/时间字符串。即：date--->String
        String dateStr = formatter.format(new Date());
        File path = new File(filePrefix+ dkljpic );
        if (!path.exists()) {
            String paths[] = (filePrefix+dkljpic).split("\\\\");
            String dir = paths[0];
            for (int i = 0; i < paths.length - 1; i++) {
                try {
                    dir = dir + File.separator + paths[i + 1];
                    File dirFile = new File(dir);
                    if (!dirFile.exists()) {
                        dirFile.mkdir();
                        //System.out.println("创建目录为：" + dir);
                    }
                } catch (Exception err) {
                    System.err.println("文件夹创建发生异常");
                }
            }
        }
        switch (dwAlarmType) {
            case NetSDKLib.EVENT_IVS_ACCESS_CTL: // /< 门禁事件
            {
                NetSDKLib.DEV_EVENT_ACCESS_CTL_INFO msg = new NetSDKLib.DEV_EVENT_ACCESS_CTL_INFO();
                ToolKits.GetPointerData( pAlarmInfo, msg );

                System.out.println( "事件名称 :" + new String( msg.szName ).trim() );
                String id = UUIDUtil.uuid32();
                if (msg.bStatus == 1) {
                    SgcRykqjlEntity sgcRykqjlEntity = new SgcRykqjlEntity();
                    sgcRykqjlEntity.setJlid( id );
                    sgcRykqjlEntity.setYgid( new String( msg.szUserID ).trim() );
                    sgcRykqjlEntity.setDksj( new Date() );
                    //根据员工id查询设备id
                    List<SgcRysbGlbEntity> sbList = fAnalyzerDataCB.sgcRysbGlbMapper.selectList
                            (new QueryWrapper<SgcRysbGlbEntity>().eq("YGID",sgcRykqjlEntity.getYgid()));
                    //SgcRyglEntity sgcRyglEntity = fAnalyzerDataCB.sgcRyglMapper.selectById( sgcRykqjlEntity.getYgid() );
                    if (CollectionUtil.isNotEmpty( sbList )) {
                        String sb = "";
                        for(int i=0;i<sbList.size();i++){
                            if(i==sbList.size()-1){
                                sb += sbList.get(i).getSbid();
                            }else{
                                sb += sbList.get(i).getSbid() + ",";
                            }
                            sgcRykqjlEntity.setSbid(sb);
                        }
                        if (msg.emEventType == 1) {
                            sgcRykqjlEntity.setSjlx( "1" );
                        } else if (msg.emEventType == 2) {
                            sgcRykqjlEntity.setSjlx( "2" );
                        }
                        fAnalyzerDataCB.sgcRykqjlMapper.insert( sgcRykqjlEntity );
                        //更新考勤汇总
                        insertOrUpdateKqhz(sgcRykqjlEntity.getSbid(),sgcRykqjlEntity.getYgid());
                        System.out.println( "刷卡结果 : 成功！" );
                    }
                } else if (msg.bStatus == 0) {
                    System.out.println( "刷卡结果 : 失败！" );
                }

                String facePicPath = "";
                MultipartFile[] files = new MultipartFile[msg.nImageInfoCount];
                if (msg.nImageInfoCount > 0) {
                for (int i = 0; i < msg.nImageInfoCount; i++) {

                    facePicPath = path + "\\" + System.currentTimeMillis()
                            + "人脸图.jpg"; // 保存图片地址

                    byte[] faceBuf = pBuffer.getByteArray(
                            msg.stuImageInfo[i].nOffSet,
                            msg.stuImageInfo[i].nLength );

                    ByteArrayInputStream byteArrInputFace = new ByteArrayInputStream(
                            faceBuf );
                    try {
                        BufferedImage bufferedImage = ImageIO
                                .read( byteArrInputFace );
                        if (bufferedImage != null) {
                            ByteArrayOutputStream out = new ByteArrayOutputStream();
                            ImageIO.write( bufferedImage, "jpg", out );
                            byte[] imageByte = out.toByteArray();
                            //将 byte[] 转为 MultipartFile
                            MultipartFile multipartFile = new ConvertToMultipartFile( imageByte, System.currentTimeMillis()
                                    + "人脸图.jpg",
                                    System.currentTimeMillis()
                                            + "人脸图.jpg", "jpg", imageByte.length );
                            files[i] = multipartFile;
                            /*ImageIO.write(bufferedImage, "jpg", new File(
                                    facePicPath));
                            System.out.println("人脸图保存路径：" + facePicPath);*/
                        }
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                }
                //上传图片
                    fAnalyzerDataCB.uploadUtil.upload( files,id );
            }
                break;
            }
            case NetSDKLib.EVENT_IVS_FACEDETECT: {		// 人脸检测事件
                NetSDKLib.DEV_EVENT_FACEDETECT_INFO msg = new NetSDKLib.DEV_EVENT_FACEDETECT_INFO();
                ToolKits.GetPointerData(pAlarmInfo, msg);
                Calendar cal = Calendar.getInstance();
                count++;
                System.out.println("count:"+count);
                Date date1 = cal.getTime();
                System.out.println("start:"+new SimpleDateFormat("yyyy/MM/dd-HH:mm:ss:SSS").format(date1));
                //System.out.println("人的运动速度 :" + msg.stuObject.Speed);
            }
            default:
                System.out.println("other--");
                break;
        }
        return 0;
    }
    /**
     * 新增或修改考勤汇总信息
     * @param sbid
     * @param ygid
     */
    public void insertOrUpdateKqhz(String sbid,String ygid){
        SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd");
        SgcRykqDayEntity sgcRykqDayEntity = fAnalyzerDataCB.sgcRykqDayMapper.selectOne( new QueryWrapper<SgcRykqDayEntity>()
                .eq("RQ" ,dateFormat.format(new Date()))
                .eq("YGID" ,ygid) );
        if(ObjectUtil.isNotEmpty( sgcRykqDayEntity )){
            sgcRykqDayEntity.setZhycsksj(LocalDateTime.now());
            fAnalyzerDataCB.sgcRykqDayMapper.updateById(sgcRykqDayEntity);
        }else{
            sgcRykqDayEntity = new SgcRykqDayEntity();
            sgcRykqDayEntity.setId(UUIDUtil.uuid32());
            sgcRykqDayEntity.setRq( LocalDate.now());
            sgcRykqDayEntity.setYgid(ygid);
            sgcRykqDayEntity.setScsksj( LocalDateTime.now());
            sgcRykqDayEntity.setKfz(0.0);
            sgcRykqDayEntity.setSbid(sbid);
            sgcRykqDayEntity.setCreatedTime(LocalDateTime.now());
            sgcRykqDayEntity.setCreatedBy("admin");
            fAnalyzerDataCB.sgcRykqDayMapper.insert(sgcRykqDayEntity);
        }
    }
}
