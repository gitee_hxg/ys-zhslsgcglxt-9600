package com.netsdk.demo.module;

import com.netsdk.lib.NetSDKLib;
import com.netsdk.lib.enumeration.ENUMERROR;

/***
 * @Description: 主动注册监听
 * @Author: 徐铭阳
 * @CreateDate: 2022/11/8 20:18
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/

public class AutoRegisterModule {

    static NetSDKLib NetSdk = NetSDKLib.NETSDK_INSTANCE;

    // 开启监听
    public static NetSDKLib.LLong ServerStartListen(String serverIPAddr, int listenPort, NetSDKLib.fServiceCallBack ServerListenCB) {
        // 这里的 nTimeout 其实是无效的
        NetSDKLib.LLong m_hListenHandle = NetSdk.CLIENT_ListenServer(serverIPAddr, listenPort, 1000, ServerListenCB, null);
        if (m_hListenHandle == null || m_hListenHandle.longValue() == 0) {
            System.err.println("开启监听失败: " + ENUMERROR.getErrorCode());
            return m_hListenHandle;
        }
        System.out.println("开启监听成功   CLIENT_ListenServer Sucess");
        return m_hListenHandle;
    }

    // 结束监听
    public static void ServerStopListen(NetSDKLib.LLong m_hListenHandle) {
        if (m_hListenHandle.longValue() == 0) return;
        boolean ret = NetSdk.CLIENT_StopListenServer(m_hListenHandle);
        if (!ret) {
            System.err.println("结束监听失败: " + ENUMERROR.getErrorCode());
            return;
        }
        System.out.println("结束监听成功 CLIENT_StopListenServer Sucess");
    }
}
