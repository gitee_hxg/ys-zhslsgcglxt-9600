package com.netsdk.demo.module;

import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @Description: TODO
 * @Author 徐铭阳
 * @CreateDate 2022/11/6 20:55
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Slf4j
@Component
@Order(0)
public class DaHuaSdkApplication implements ApplicationListener<ApplicationReadyEvent>, Ordered {

	//	@Resource
//	SgcSbxxMapper sgcSbxxMapper;
	@Resource
	DahuaApp dahuaApp;

	@Override
	public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
		log.info("[DaHuaSdkApplication]-[onApplicationEvent]-[INFO]-:[启动自动注册监听]");
		LoginModule.init(null, null);
		dahuaApp.serverStartListen();
	//	dahuaApp.openDysj();
	}

	@Override
	public int getOrder() {
		return Ordered.LOWEST_PRECEDENCE;
	}
}
