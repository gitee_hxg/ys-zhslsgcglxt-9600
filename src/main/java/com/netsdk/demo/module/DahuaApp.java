package com.netsdk.demo.module;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.util.StringUtil;
import com.netsdk.bean.DeviceInfo;
import com.netsdk.bean.ListenInfo;
import com.netsdk.demo.module.callback.RegisterServiceCallBack;
import com.netsdk.lib.NativeString;
import com.netsdk.lib.NetSDKLib;
import com.netsdk.lib.NetSDKLib.FAIL_CODE;
import com.netsdk.lib.NetSDKLib.LLong;
import com.netsdk.lib.NetSDKLib.NET_ACCESS_FACE_INFO;
import com.netsdk.lib.NetSDKLib.NET_ACCESS_USER_INFO;
import com.netsdk.lib.NetSDKLib.NET_EM_ACCESS_CTL_FACE_SERVICE;
import com.netsdk.lib.NetSDKLib.NET_EM_ACCESS_CTL_USER_SERVICE;
import com.netsdk.lib.NetSDKLib.NET_ENUM_USER_TYPE;
import com.netsdk.lib.NetSDKLib.NET_IN_ACCESS_FACE_SERVICE_INSERT;
import com.netsdk.lib.NetSDKLib.NET_IN_ACCESS_FACE_SERVICE_REMOVE;
import com.netsdk.lib.NetSDKLib.NET_IN_ACCESS_FACE_SERVICE_UPDATE;
import com.netsdk.lib.NetSDKLib.NET_IN_ACCESS_USER_SERVICE_INSERT;
import com.netsdk.lib.NetSDKLib.NET_IN_ACCESS_USER_SERVICE_REMOVE;
import com.netsdk.lib.NetSDKLib.NET_OUT_ACCESS_FACE_SERVICE_INSERT;
import com.netsdk.lib.NetSDKLib.NET_OUT_ACCESS_FACE_SERVICE_REMOVE;
import com.netsdk.lib.NetSDKLib.NET_OUT_ACCESS_FACE_SERVICE_UPDATE;
import com.netsdk.lib.NetSDKLib.NET_OUT_ACCESS_USER_SERVICE_INSERT;
import com.netsdk.lib.NetSDKLib.NET_OUT_ACCESS_USER_SERVICE_REMOVE;
import com.netsdk.lib.ToolKits;
import com.netsdk.utils.AccessUtil;
import com.netsdk.utils.UploadUtil;
import com.sun.jna.Memory;
import com.ys.common.core.util.UUIDUtil;
import com.ys.zhslsgcgl.entity.*;
import com.ys.zhslsgcgl.mapper.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.OutputStream;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * @Description: 大华设备Api
 * @Author 徐铭阳
 * @CreateDate 2022/11/6 20:01
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Slf4j
@Component
@Data
public class DahuaApp {

	static NetSDKLib netsdkApi 	= NetSDKLib.NETSDK_INSTANCE;

	@Resource
	SgcSbxxMapper sgcSbxxMapper;

	@Resource
	SgcRyglMapper sgcRyglMapper;

	//人员设备关联信息表
	@Autowired
	SgcRysbGlbMapper sgcRysbGlbMapper;
	@Autowired
	SgcRykqjlMapper sgcRykqjlMapper;
	@Autowired
	SgcRykqDayMapper sgcRykqDayMapper;
	@Autowired
	UploadUtil uploadUtil;


	@PostConstruct
	public void init() {
		dahuaApp = this;
	}

	//	后期需要维护到配置文件
	private static String serverIpAddr;
	private static int serverPort;   // 注意不要和其他程序发生冲突

	@Value("${custom.serverIpAddr}")
	public void setServerIpAddr(String serverIpAddr) {    //注意这里的set方法不能是静态的
		DahuaApp.serverIpAddr = serverIpAddr;
	}

	@Value("${custom.serverPort}")
	public void setServerPort(int serverPort) {    //注意这里的set方法不能是静态的
		DahuaApp.serverPort = serverPort;
	}

	private static DahuaApp dahuaApp;

	// 监听句柄
	private LLong m_hListenHandle = new LLong(0);

  // 循环监听标识
	private volatile Boolean taskIsOpen = false;

	// 接口调用超时时间
	private static final int TIME_OUT = 6 * 1000;

	/**
	 * 开启监听
	 */
	public void serverStartListen() {
		m_hListenHandle = AutoRegisterModule
				.ServerStartListen(serverIpAddr, serverPort, RegisterServiceCallBack.getInstance());
		if (m_hListenHandle.longValue() == 0) return;
		taskIsOpen = true;
		new Thread(this::eventListTask).start();
	}
	// 获取监听回调数据并放入缓存
	public void eventListTask() {
		while (taskIsOpen) {
			try {
				// 稍微延迟一下，避免循环的太快
				Thread.sleep(100);
				// 阻塞获取
				ListenInfo listenInfo = RegisterServiceCallBack.ServerInfoQueue.poll(50, TimeUnit.MILLISECONDS);
				if (listenInfo == null) continue;
				// 结果放入缓存
				if (!DeviceCache.deviceInfoMap.containsKey(listenInfo.devSerial)) {
					log.info(
							"...有新设备上报注册信息... Serial:" + listenInfo.devSerial + ",设备ip:" + listenInfo.devIpAddress
									+ ",设备端口:" + listenInfo.devPort);
					SgcSbxxEntity sgcSbxxEntity = new SgcSbxxEntity();
					sgcSbxxEntity.setId(listenInfo.devSerial);
					sgcSbxxEntity.setIpaddress(listenInfo.devIpAddress);
					sgcSbxxEntity.setPort(listenInfo.devPort);
					sgcSbxxEntity.setUsername("admin");
					sgcSbxxEntity.setPassword("admin123");
					List<SgcSbxxEntity> sgcSbxxEntityList = dahuaApp.sgcSbxxMapper.selectList(
							Wrappers.<SgcSbxxEntity>lambdaQuery().eq(SgcSbxxEntity::getId, listenInfo.devSerial));
					List<String> ipList = sgcSbxxEntityList.stream().map(SgcSbxxEntity::getIpaddress)
							.collect(Collectors.toList());
					List<String> idList = sgcSbxxEntityList.stream().map(SgcSbxxEntity::getId)
							.collect(Collectors.toList());
					if (!ipList.contains(listenInfo.devIpAddress) && !idList.contains(listenInfo.devSerial)) {
						sgcSbxxEntity.setCreatedBy("admin");
						sgcSbxxEntity.setCreatedTime(new Date());
						dahuaApp.sgcSbxxMapper.insert(sgcSbxxEntity);
					} else {
						dahuaApp.sgcSbxxMapper.updateById(sgcSbxxEntity);
					}
					LLong lLong = LoginModule.loginWithPort(sgcSbxxEntity.getIpaddress(), sgcSbxxEntity.getPort(),
							sgcSbxxEntity.getUsername(), sgcSbxxEntity.getPassword(),
							sgcSbxxEntity.getId());
					DeviceCache.deviceInfoMap.put(listenInfo.devSerial,
							new DeviceInfo(listenInfo.devIpAddress, listenInfo.devPort, lLong));
					//验证登录是否成功，成功则开启智能订阅
					if(lLong.longValue() != 0){
						AccessUtil.realLoadPicture();
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 开启门禁的订阅事件，收集打卡信息
	 */
	/*public void openDysj(){
		List<SgcSbxxEntity> list = dahuaApp.sgcSbxxMapper.selectList(Wrappers.lambdaQuery());
		if(CollectionUtil.isNotEmpty(list)){
			list.forEach(it->{
				boolean flag = LoginModule.login(it.getIpaddress(), it.getPort(), it.getUsername(), it.getPassword());
				//验证登录是否成功，成功则开启智能订阅
				if(flag){
					AccessUtil.realLoadPicture();
				}
			});
		}
	}*/

	/**
	 * 批量添加/修改用户
	 */
	public void addUser(SgcRyglEntity sgcRyglEntity, LLong m_hLoginHandle, String sbid) {

		// 用户操作类型
		// 添加用户
		int emtype = NET_EM_ACCESS_CTL_USER_SERVICE.NET_EM_ACCESS_CTL_USER_SERVICE_INSERT;

		// 添加的用户个数
//		int nMaxNum = userInfos.length;
		int nMaxNum = 1;

		/**
		 * 用户信息数组
		 */
		// 先初始化用户信息数组
		NET_ACCESS_USER_INFO[] users = new NET_ACCESS_USER_INFO[nMaxNum];
		// 初始化返回的失败信息数组
		FAIL_CODE[] failCodes = new FAIL_CODE[nMaxNum];

		for (int i = 0; i < nMaxNum; i++) {
			users[i] = new NET_ACCESS_USER_INFO();
			failCodes[i] = new FAIL_CODE();
		}

		/**
		 * 用户信息赋值
		 */
		String ygid = sgcRyglEntity.getYgid();
		for (int i = 0; i < nMaxNum; i++) {
			// 用户ID, 用于后面的添加卡、人脸、指纹
			System.arraycopy(ygid.getBytes(), 0,
					users[i].szUserID, 0, ygid.getBytes().length);

			// 用户名称
			try {
				System.arraycopy(sgcRyglEntity.getYgxm().getBytes("GBK"),
						0,
						users[i].szName, 0,
						sgcRyglEntity.getYgxm().getBytes("GBK").length);

				// 用户部门
				if (StringUtil.isNotEmpty(sgcRyglEntity.getSsbm())) {
					System.arraycopy(sgcRyglEntity.getSsbm().getBytes("GBK"), 0,users[i].szPhoneNumber,0,
							sgcRyglEntity.getSsbm().getBytes("GBK").length);
				}
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			// 用户手机号
			System.arraycopy(sgcRyglEntity.getSjh().getBytes(), 0,users[i].szPhoneNumber,0,
					sgcRyglEntity.getSjh().getBytes().length);

			// 用户身份证号码
			System.arraycopy(sgcRyglEntity.getSfzhm().getBytes(), 0,users[i].szCitizenIDNo,0,
					sgcRyglEntity.getSfzhm().getBytes().length);
			// 用户类型
			users[i].emUserType = NET_ENUM_USER_TYPE.NET_ENUM_USER_TYPE_NORMAL;
			// 用户性别
			users[i].emSex = Integer.parseInt(sgcRyglEntity.getXb());
			// 密码, UserID+密码开门时密码
			System.arraycopy(
					(ygid + "123456").getBytes(),
//					(sgcRyglEntity.getYgid() + userInfos[i].passwd).getBytes(),
					0,
					users[i].szPsw,
					0,
					(ygid + "123456").getBytes().length);

			// 来宾卡的通行次数
			users[i].nUserTime = 100;

			// 有效门数, 门个数 表示双门控制器
			users[i].nDoorNum = 1;

			// 有权限的门序号, 表示第一个门有权限
			users[i].nDoors[0] = 0;

			// 与门数对应
			users[i].nTimeSectionNum = 1;

			// 表示第一个门全天有效
			users[i].nTimeSectionNo[0] = 255;

			// 开始有效期
			users[i].stuValidBeginTime.setTime(LocalDateTime.now().getYear(), LocalDateTime.now().getMonthValue(), LocalDateTime.now().getDayOfMonth(), LocalDateTime.now().getHour(), LocalDateTime.now().getMinute(), LocalDateTime.now().getSecond());

			// 结束有效期
			users[i].stuValidEndTime.setTime(2099, 12, 1, 14, 1, 1);
		}

		// /////////////////////////// 以下固定写法
		// /////////////////////////////////////
		/**
		 * 入参
		 */
		NET_IN_ACCESS_USER_SERVICE_INSERT stIn = new NET_IN_ACCESS_USER_SERVICE_INSERT();
		stIn.nInfoNum = nMaxNum;
		stIn.pUserInfo = new Memory(users[0].size() * nMaxNum); // 申请内存
		stIn.pUserInfo.clear(users[0].size() * nMaxNum);

		// 将用户信息传给指针
		ToolKits.SetStructArrToPointerData(users, stIn.pUserInfo);

		/**
		 * 出参
		 */
		NET_OUT_ACCESS_USER_SERVICE_INSERT stOut = new NET_OUT_ACCESS_USER_SERVICE_INSERT();
		stOut.nMaxRetNum = nMaxNum;
		stOut.pFailCode = new Memory(failCodes[0].size() * nMaxNum); // 申请内存
		stOut.pFailCode.clear(failCodes[0].size() * nMaxNum);

		ToolKits.SetStructArrToPointerData(failCodes, stOut.pFailCode);

		stIn.write();
		stOut.write();
		if (LoginModule.netsdk.CLIENT_OperateAccessUserService(m_hLoginHandle, emtype,
				stIn.getPointer(), stOut.getPointer(), TIME_OUT)) {
			// 将指针转为具体的信息
			ToolKits.GetPointerDataToStructArr(stOut.pFailCode, failCodes);

			/**
			 * 具体的打印信息
			 */
			for (int i = 0; i < nMaxNum; i++) {
				System.out.println("[" + i + "]添加用户结果："
						+ failCodes[i].nFailCode);
			}
			//	添加用户成功才保存设备id
			/*LambdaUpdateWrapper<SgcRyglEntity> wrapper = new LambdaUpdateWrapper<>();
			wrapper.eq(SgcRyglEntity::getYgid, ygid);
			wrapper.set(SgcRyglEntity::getSbid, sbid);
			sgcRyglEntity = new SgcRyglEntity();
			sgcRyglEntity.setYgid(ygid);
			sgcRyglMapper.update(sgcRyglEntity, wrapper);*/
			//添加用户设备关联信息
			QueryWrapper<SgcRysbGlbEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("YGID",ygid);
			queryWrapper.eq("SBID",sbid);
			SgcRysbGlbEntity sgcRysbGlbEntity = dahuaApp.sgcRysbGlbMapper.selectOne(queryWrapper);
			if(ObjectUtil.isEmpty(sgcRysbGlbEntity)) {
				sgcRysbGlbEntity = new SgcRysbGlbEntity();
				sgcRysbGlbEntity.setId( UUIDUtil.uuid32() );
				sgcRysbGlbEntity.setYgid( ygid );
				sgcRysbGlbEntity.setSbid( sbid );
				sgcRysbGlbEntity.setCreatedBy( "admin" );
				sgcRysbGlbEntity.setCreatedTime( new Date() );
				dahuaApp.sgcRysbGlbMapper.insert( sgcRysbGlbEntity );
			}
		} else {
			System.err.println("添加用户失败");
		}

		stIn.read();
		stOut.read();
	}

	public boolean addFace(String ygid, String url, LLong m_hLoginHandle) {
		// ////////////////////////// 获取图片数据 ///////////////////////

		byte[] buf1 = image2byte(url);
//		byte[] buf1 = GetFacePhotoData(url);

		FACE_INFO[] faceInfos = new FACE_INFO[1];

		for (int i = 0; i < faceInfos.length; i++) {
			faceInfos[i] = new FACE_INFO();
		}
		faceInfos[0].setFace(ygid, buf1);
		// //////////////////// 将图片数据传入数组FACE_INFO， 用于存储图片数据
		// ////////////////////
		// ///////////////////////////////////////////////////////////////////////////////////////////
		// 以上是获取人脸图片信息
		// 以下可以固定写法
		// ///////////////////////////////////////////////////////////////////////////////////////////

		// 添加人脸的用户最大个数
		int nMaxCount = 1;

		// ////////////////////// 每个用户的人脸信息初始化 ////////////////////////
		NET_ACCESS_FACE_INFO[] faces = new NET_ACCESS_FACE_INFO[nMaxCount];
		for (int i = 0; i < faces.length; i++) {
			faces[i] = new NET_ACCESS_FACE_INFO();

			faces[i].nInFacePhotoLen[0] = 200 * 1024;
			faces[i].pFacePhotos[0].pFacePhoto = new Memory(200 * 1024); // 人脸照片数据,大小不超过200K
			faces[i].pFacePhotos[0].pFacePhoto.clear(200 * 1024);
		}

		// ////////////////////////////// 人脸信息赋值 ///////////////////////////////
		for (int i = 0; i < faces.length; i++) {
			// 用户ID
			System.arraycopy(ygid.getBytes(), 0,
					faces[i].szUserID, 0, ygid.getBytes().length);

			// 人脸照片个数
			faces[i].nFacePhoto = 1;

			// 每张照片实际大小
            faces[i].nOutFacePhotoLen[0] = faceInfos[i].szFacePhotoData.length;

			// 图片数据
			faces[i].pFacePhotos[0].pFacePhoto.write(0,
					faceInfos[i].szFacePhotoData, 0,
					faceInfos[i].szFacePhotoData.length);
		}
		// ///////////////////////////////////////////////////////////////////////

		// 初始化
		FAIL_CODE[] failCodes = new FAIL_CODE[nMaxCount];
		for (int i = 0; i < failCodes.length; i++) {
			failCodes[i] = new FAIL_CODE();
		}

		// 人脸操作类型
		// 添加人脸信息
		int emtype = NET_EM_ACCESS_CTL_FACE_SERVICE.NET_EM_ACCESS_CTL_FACE_SERVICE_INSERT;

		/**
		 * 入参
		 */
		NET_IN_ACCESS_FACE_SERVICE_INSERT stIn = new NET_IN_ACCESS_FACE_SERVICE_INSERT();
		stIn.nFaceInfoNum = nMaxCount;
		stIn.pFaceInfo = new Memory(faces[0].size() * nMaxCount);
		stIn.pFaceInfo.clear(faces[0].size() * nMaxCount);

		ToolKits.SetStructArrToPointerData(faces, stIn.pFaceInfo);

		/**
		 * 出参
		 */
		NET_OUT_ACCESS_FACE_SERVICE_INSERT stOut = new NET_OUT_ACCESS_FACE_SERVICE_INSERT();
		stOut.nMaxRetNum = nMaxCount;
		stOut.pFailCode = new Memory(failCodes[0].size() * nMaxCount);
		stOut.pFailCode.clear(failCodes[0].size() * nMaxCount);

		ToolKits.SetStructArrToPointerData(failCodes, stOut.pFailCode);

		stIn.write();
		stOut.write();
		if (LoginModule.netsdk.CLIENT_OperateAccessFaceService(m_hLoginHandle, emtype,
				stIn.getPointer(), stOut.getPointer(), TIME_OUT)) {
			// 将获取到的结果信息转成具体的结构体
			ToolKits.GetPointerDataToStructArr(stOut.pFailCode, failCodes);

			// 打印具体信息
			for (int i = 0; i < nMaxCount; i++) {
				log.info("[" + i + "]添加人脸结果 : "
						+ failCodes[i].nFailCode);
			}
			stIn.read();
			stOut.read();
			return true;
		} else {
			log.info("添加人脸失败");
			stIn.read();
			stOut.read();
			return false;
		}

	}

	/**
	 * 修改人脸
	 */
	public void modifyFace(String ygid, String url, LLong m_hLoginHandle) {
		// //////////////////////////获取图片数据 ///////////////////////
//		String[] szPaths = { "d:/123.jpg", "d:/girl.jpg" };
		byte[] buf1 = image2byte(url);

//		byte[] buf1 = GetFacePhotoData(szPaths[0]);
//		byte[] buf2 = GetFacePhotoData(szPaths[1]);

		// //////////////////// 将图片数据传入数组FACE_INFO， 用于存储图片数据
		// ////////////////////
		// new 两个用户
		FACE_INFO[] faceInfos = new FACE_INFO[1];
		for (int i = 0; i < faceInfos.length; i++) {
			faceInfos[i] = new FACE_INFO();
		}

		faceInfos[0].setFace(ygid, buf1);
//		faceInfos[1].setFace("2022", buf2);

		// ///////////////////////////////////////////////////////////////////////////////////////////
		// 以上是获取人脸图片信息
		// 以下可以固定写法
		// ///////////////////////////////////////////////////////////////////////////////////////////

		// 修改人脸的用户最大个数
		int nMaxCount = faceInfos.length;

		// ////////////////////// 每个用户的人脸信息初始化 ////////////////////////
		NET_ACCESS_FACE_INFO[] faces = new NET_ACCESS_FACE_INFO[nMaxCount];
		for (int i = 0; i < faces.length; i++) {
			faces[i] = new NET_ACCESS_FACE_INFO();

			// 根据每个用户的人脸图片的实际个数申请内存
			faces[i].nInFacePhotoLen[0] = 200 * 1024;
			faces[i].pFacePhotos[0].pFacePhoto = new Memory(200 * 1024); // 人脸照片数据,大小不超过200K
			faces[i].pFacePhotos[0].pFacePhoto.clear(200 * 1024);
		}

		// ////////////////////////////// 人脸信息赋值 ///////////////////////////////
		for (int i = 0; i < faces.length; i++) {
			// 用户ID
			System.arraycopy(faceInfos[i].userId.getBytes(), 0,
					faces[i].szUserID, 0, faceInfos[i].userId.getBytes().length);

			// 人脸照片个数
			faces[i].nFacePhoto = 1;

			// 每张照片实际大小
			faces[i].nOutFacePhotoLen[0] = faceInfos[i].szFacePhotoData.length;
			// 图片数据
			faces[i].pFacePhotos[0].pFacePhoto.write(0,
					faceInfos[i].szFacePhotoData, 0,
					faceInfos[i].szFacePhotoData.length);
		}
		// ///////////////////////////////////////////////////////////////////////

		// 初始化
		FAIL_CODE[] failCodes = new FAIL_CODE[nMaxCount];
		for (int i = 0; i < failCodes.length; i++) {
			failCodes[i] = new FAIL_CODE();
		}

		// 人脸操作类型
		// 修改人脸信息
		int emtype = NET_EM_ACCESS_CTL_FACE_SERVICE.NET_EM_ACCESS_CTL_FACE_SERVICE_UPDATE;

		/**
		 * 入参
		 */
		NET_IN_ACCESS_FACE_SERVICE_UPDATE stIn = new NET_IN_ACCESS_FACE_SERVICE_UPDATE();
		stIn.nFaceInfoNum = nMaxCount;
		stIn.pFaceInfo = new Memory(faces[0].size() * nMaxCount);
		stIn.pFaceInfo.clear(faces[0].size() * nMaxCount);

		ToolKits.SetStructArrToPointerData(faces, stIn.pFaceInfo);

		/**
		 * 出参
		 */
		NET_OUT_ACCESS_FACE_SERVICE_UPDATE stOut = new NET_OUT_ACCESS_FACE_SERVICE_UPDATE();
		stOut.nMaxRetNum = nMaxCount;
		stOut.pFailCode = new Memory(failCodes[0].size() * nMaxCount);
		stOut.pFailCode.clear(failCodes[0].size() * nMaxCount);

		ToolKits.SetStructArrToPointerData(failCodes, stOut.pFailCode);

		stIn.write();
		stOut.write();
		if (LoginModule.netsdk.CLIENT_OperateAccessFaceService(m_hLoginHandle, emtype,
				stIn.getPointer(), stOut.getPointer(), TIME_OUT)) {
			// 将获取到的结果信息转成具体的结构体
			ToolKits.GetPointerDataToStructArr(stOut.pFailCode, failCodes);

			// 打印具体信息
			for (int i = 0; i < nMaxCount; i++) {
				System.out.println("[" + i + "]修改人脸结果 : "
						+ failCodes[i].nFailCode);
			}
		} else {
			System.err.println("修改人脸失败");
		}

		stIn.read();
		stOut.read();
	}

	/**
	 * 根据用户ID删除人脸
	 */
	public void deleteFace(String ygid, LLong m_hLoginHandle) {
		String[] userIDs = { ygid };

		// 删除人脸的用户最大个数
		int nMaxCount = userIDs.length;

		// 初始化
		FAIL_CODE[] failCodes = new FAIL_CODE[nMaxCount];
		for (int i = 0; i < failCodes.length; i++) {
			failCodes[i] = new FAIL_CODE();
		}

		// 人脸操作类型
		// 删除人脸信息
		int emtype = NET_EM_ACCESS_CTL_FACE_SERVICE.NET_EM_ACCESS_CTL_FACE_SERVICE_REMOVE;

		/**
		 * 入参
		 */
		NET_IN_ACCESS_FACE_SERVICE_REMOVE stIn = new NET_IN_ACCESS_FACE_SERVICE_REMOVE();
		stIn.nUserNum = nMaxCount;
		for (int i = 0; i < nMaxCount; i++) {
			System.arraycopy(userIDs[i].getBytes(), 0,
					stIn.szUserIDs[i].szUserID, 0, userIDs[i].getBytes().length);
		}

		/**
		 * 出参
		 */
		NET_OUT_ACCESS_FACE_SERVICE_REMOVE stOut = new NET_OUT_ACCESS_FACE_SERVICE_REMOVE();
		stOut.nMaxRetNum = nMaxCount;

		stOut.pFailCode = new Memory(failCodes[0].size() * nMaxCount);
		stOut.pFailCode.clear(failCodes[0].size() * nMaxCount);

		ToolKits.SetStructArrToPointerData(failCodes, stOut.pFailCode);

		stIn.write();
		stOut.write();
		if (LoginModule.netsdk.CLIENT_OperateAccessFaceService(m_hLoginHandle, emtype,
				stIn.getPointer(), stOut.getPointer(), TIME_OUT)) {
			// 将获取到的结果信息转成具体的结构体
			ToolKits.GetPointerDataToStructArr(stOut.pFailCode, failCodes);

			// 打印具体信息
			for (int i = 0; i < nMaxCount; i++) {
				System.out.println("[" + i + "]删除人脸结果 : "
						+ failCodes[i].nFailCode);
			}
		} else {
			System.err.println("删除人脸失败");
		}

		stIn.read();
		stOut.read();
	}


	/************************************************************************************************
	 * 人脸操作：添加/修改/删除/获取/清空 可以批量添加多个用户的人脸，但是每个用户只能添加一张图片，大小最大200K
	 ************************************************************************************************/
	// 获取图片大小
	public static int GetFileSize(String filePath) {
		File f = new File(filePath);
		if (f.exists() && f.isFile()) {
			return (int) f.length();
		} else {
			return 0;
		}
	}

	public static byte[] GetFacePhotoData(String file) {
		int fileLen = GetFileSize(file);
		if (fileLen <= 0) {
			return null;
		}

		try {
			File infile = new File(file);
			if (infile.canRead()) {
				FileInputStream in = new FileInputStream(infile);
				byte[] buffer = new byte[fileLen];
				long currFileLen = 0;
				int readLen = 0;
				while (currFileLen < fileLen) {
					readLen = in.read(buffer);
					currFileLen += readLen;
				}

				in.close();
				return buffer;
			} else {
				System.err.println("Failed to open file %s for read!!!\n");
				return null;
			}
		} catch (Exception e) {
			System.err.println("Failed to open file %s for read!!!\n");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 每个用户的人脸图片信息
	 */
	public class FACE_INFO {
		public String userId; // 用户ID
		public byte[] szFacePhotoData; // 图片数据，目前一个用户ID只支持添加一张

		public void setFace(String userId, byte[] szFacePhotoData) {
			this.userId = userId;
			this.szFacePhotoData = szFacePhotoData;
		}
	}

	/**
	 * 图片转为byte数组
	 *
	 * @param path 网络图片url
	 * @return byte[]
	 */
	public static byte[] image2byte(String path) {
		byte[] data;
		URL url;
		InputStream input;
		try{
			url = new URL(path);
			HttpURLConnection httpUrl = (HttpURLConnection) url.openConnection();
			httpUrl.connect();
			httpUrl.getInputStream();
			input = httpUrl.getInputStream();
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
		int numBytesRead = 0;
		while (true) {
			try {
				if ((numBytesRead = input.read(buf)) == -1)
					break;
			} catch (IOException e) {
				e.printStackTrace();
			}
			output.write(buf, 0, numBytesRead);
		}
		data = output.toByteArray();
		try {
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return data;
	}

	/**
	 * 删除指定用户ID的用户
	 */
	public void deleteUser(String ygid, LLong m_hLoginHandle) {
//		String[] userIDs = { "1011", "2022" };

		// 删除的用户个数
//		int nMaxNum = userIDs.length;
		int nMaxNum = 1;

		// /////////////////////////// 以下固定写法
		// /////////////////////////////////////
		// 用户操作类型
		// 删除用户
		int emtype = NET_EM_ACCESS_CTL_USER_SERVICE.NET_EM_ACCESS_CTL_USER_SERVICE_REMOVE;

		// 初始化返回的失败信息数组
		FAIL_CODE[] failCodes = new FAIL_CODE[nMaxNum];
		for (int i = 0; i < nMaxNum; i++) {
			failCodes[i] = new FAIL_CODE();
		}

		/**
		 * 入参
		 */
		NET_IN_ACCESS_USER_SERVICE_REMOVE stIn = new NET_IN_ACCESS_USER_SERVICE_REMOVE();
		// 用户ID个数
		stIn.nUserNum = 1;
//		stIn.nUserNum = userIDs.length;

		// 用户ID
//		for (int i = 0; i < userIDs.length; i++) {
//			System.arraycopy(userIDs[i].getBytes(), 0,
//					stIn.szUserIDs[i].szUserID, 0, userIDs[i].getBytes().length);
//		}
		System.arraycopy(ygid.getBytes(), 0,
				stIn.szUserIDs[0].szUserID, 0, ygid.getBytes().length);

		/**
		 * 出参
		 */
		NET_OUT_ACCESS_USER_SERVICE_REMOVE stOut = new NET_OUT_ACCESS_USER_SERVICE_REMOVE();
		stOut.nMaxRetNum = nMaxNum;

		stOut.pFailCode = new Memory(failCodes[0].size() * nMaxNum); // 申请内存
		stOut.pFailCode.clear(failCodes[0].size() * nMaxNum);

		ToolKits.SetStructArrToPointerData(failCodes, stOut.pFailCode);

		stIn.write();
		stOut.write();
		if (LoginModule.netsdk.CLIENT_OperateAccessUserService(m_hLoginHandle, emtype,
				stIn.getPointer(), stOut.getPointer(), TIME_OUT)) {
			// 将指针转为具体的信息
			ToolKits.GetPointerDataToStructArr(stOut.pFailCode, failCodes);

			/**
			 * 打印具体的信息
			 */
			for (int i = 0; i < nMaxNum; i++) {
				System.out.println("[" + i + "]删除用户结果："
						+ failCodes[i].nFailCode);
			}
		} else {
			System.err.println("删除用户失败");
		}

		stIn.read();
		stOut.read();
	}

    ///按时间查找门禁刷卡记录
    public void findAccessRecordByTime( LLong m_hLoginHandle,String sbid) {
        ///查询条件
        NetSDKLib.FIND_RECORD_ACCESSCTLCARDREC_CONDITION_EX recordCondition = new NetSDKLib.FIND_RECORD_ACCESSCTLCARDREC_CONDITION_EX();
        recordCondition.bTimeEnable = 1;  // 启用时间段查询
		Calendar calendar = Calendar.getInstance();
		System.out.println("**********************抓取时间：" + calendar.get(Calendar.YEAR) + "-"+ (calendar.get(Calendar.MONTH)+1) + "-" +  calendar.get(Calendar.DATE));
		//开始时间
        recordCondition.stStartTime.dwYear = calendar.get(Calendar.YEAR);
        recordCondition.stStartTime.dwMonth = calendar.get(Calendar.MONTH) + 1;
        recordCondition.stStartTime.dwDay = calendar.get(Calendar.DATE);
        recordCondition.stStartTime.dwHour = 0;
        recordCondition.stStartTime.dwMinute = 0;
        recordCondition.stStartTime.dwSecond = 0;
        //结束时间
        recordCondition.stEndTime.dwYear = calendar.get(Calendar.YEAR);
        recordCondition.stEndTime.dwMonth = calendar.get(Calendar.MONTH) + 1;
        recordCondition.stEndTime.dwDay = calendar.get(Calendar.DATE);
        recordCondition.stEndTime.dwHour = 23;
        recordCondition.stEndTime.dwMinute = 30;
        recordCondition.stEndTime.dwSecond = 0;

        ///CLIENT_FindRecord入参
        NetSDKLib.NET_IN_FIND_RECORD_PARAM stuFindInParam = new NetSDKLib.NET_IN_FIND_RECORD_PARAM();
        stuFindInParam.emType = NetSDKLib.EM_NET_RECORD_TYPE.NET_RECORD_ACCESSCTLCARDREC_EX;
        stuFindInParam.pQueryCondition = recordCondition.getPointer();

        ///CLIENT_FindRecord出参
        NetSDKLib.NET_OUT_FIND_RECORD_PARAM stuFindOutParam = new NetSDKLib.NET_OUT_FIND_RECORD_PARAM();

        recordCondition.write();
        if (netsdkApi.CLIENT_FindRecord(m_hLoginHandle, stuFindInParam, stuFindOutParam, 3000)) {
            recordCondition.read();
            System.out.println("FindRecord Succeed" + "\n" + "FindHandle :" + stuFindOutParam.lFindeHandle);
            int count = 0;  //循环的次数
            int nFindCount = 0;
            int nRecordCount = 10;  // 每次查询的个数
            ///门禁刷卡记录记录集信息
            NetSDKLib.NET_RECORDSET_ACCESS_CTL_CARDREC[] pstRecord = new NetSDKLib.NET_RECORDSET_ACCESS_CTL_CARDREC[nRecordCount];
            for (int i = 0; i < nRecordCount; i++) {
                pstRecord[i] = new NetSDKLib.NET_RECORDSET_ACCESS_CTL_CARDREC();
            }
            ///CLIENT_FindNextRecord入参
            NetSDKLib.NET_IN_FIND_NEXT_RECORD_PARAM stuFindNextInParam = new NetSDKLib.NET_IN_FIND_NEXT_RECORD_PARAM();
            stuFindNextInParam.lFindeHandle = stuFindOutParam.lFindeHandle;
            stuFindNextInParam.nFileCount = nRecordCount;  //想查询的记录条数

            ///CLIENT_FindNextRecord出参
            NetSDKLib.NET_OUT_FIND_NEXT_RECORD_PARAM stuFindNextOutParam = new NetSDKLib.NET_OUT_FIND_NEXT_RECORD_PARAM();
            stuFindNextOutParam.nMaxRecordNum = nRecordCount;
            stuFindNextOutParam.pRecordList = new Memory(pstRecord[0].dwSize * nRecordCount);
            stuFindNextOutParam.pRecordList.clear(pstRecord[0].dwSize * nRecordCount);

            ToolKits.SetStructArrToPointerData(pstRecord, stuFindNextOutParam.pRecordList);    //将数组内存拷贝给Pointer指针

            while (true) {  //循环查询
                if (netsdkApi.CLIENT_FindNextRecord(stuFindNextInParam, stuFindNextOutParam, 3000)) {
                    ToolKits.GetPointerDataToStructArr(stuFindNextOutParam.pRecordList, pstRecord);

                    for (int i = 0; i < stuFindNextOutParam.nRetRecordNum; i++) {
                        nFindCount = i + count * nRecordCount;

                        if (new String(pstRecord[i].szCardNo).trim() != null) {
                            System.out.println("[" + nFindCount + "]刷卡时间:" + pstRecord[i].stuTime.toStringTimeEx());
                            System.out.println("[" + nFindCount + "]用户ID:" + new String(pstRecord[i].szUserID).trim());
                            System.out.println("[" + nFindCount + "]卡号:" + new String(pstRecord[i].szCardNo).trim());
                            System.out.println("[" + nFindCount + "]门号:" + pstRecord[i].nDoor);
                            if (pstRecord[i].emDirection == 1) {
                                System.out.println("[" + nFindCount + "]开门方向: 进门");
                            } else if (pstRecord[i].emDirection == 2) {
                                System.out.println("[" + nFindCount + "]开门方向: 出门");
                            }
                        }
						//补卡开始
						String sksj = pstRecord[i].stuTime.toStringTimeEx().substring(0,10);
						if(pstRecord[i].bStatus == 1){
							//查询当日时间范围的考勤记录
							QueryWrapper<SgcRykqjlEntity> kqQueryWrapper = new QueryWrapper<>();
							kqQueryWrapper.eq( "YGID", new String( pstRecord[i].szUserID ).trim() );
							/*kqQueryWrapper.ge( "DKSJ", dateFormat.format( new Date() ) + " 00:00:00" );
							kqQueryWrapper.le( "DKSJ", dateFormat.format( new Date() ) + " 23:59:59" );*/
							kqQueryWrapper.ge( "DKSJ", sksj + " 00:00:00" );
							kqQueryWrapper.le( "DKSJ", sksj + " 23:59:59" );
							List<SgcRykqjlEntity> kqList = dahuaApp.sgcRykqjlMapper.selectList( kqQueryWrapper );
							//SgcRykqjlEntity sgcRykqjlEntity =dahuaApp.sgcRykqjlMapper.selectOne(kqQueryWrapper);
							//无记录则插入考勤记录
							if (CollectionUtil.isEmpty( kqList )) {
								//判断当前用户是否在系统中
								QueryWrapper<SgcRyglEntity> ryQueryWrapper = new QueryWrapper<>();
								ryQueryWrapper.eq( "YGID", new String( pstRecord[i].szUserID ).trim() );
								SgcRyglEntity sgcRyglEntity = dahuaApp.sgcRyglMapper.selectOne( ryQueryWrapper );
								if (ObjectUtil.isNotEmpty( sgcRyglEntity )) {
									//插入考勤记录表
									String id = UUIDUtil.uuid32();
									SgcRykqjlEntity sgcRykqjlEntity = new SgcRykqjlEntity();
									sgcRykqjlEntity.setJlid( id );
									sgcRykqjlEntity.setSbid( sbid );
									sgcRykqjlEntity.setYgid( sgcRyglEntity.getYgid() );
									try {
										sgcRykqjlEntity.setDksj( new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" ).parse( pstRecord[i].stuTime.toStringTimeEx() ) );
									} catch (ParseException e) {
										e.printStackTrace();
									}
									sgcRykqjlEntity.setSjlx( String.valueOf( pstRecord[i].emDirection ) );
									dahuaApp.sgcRykqjlMapper.insert( sgcRykqjlEntity );
									//插入考勤记录汇总表
									SgcRykqDayEntity sgcRykqDayEntity = new SgcRykqDayEntity();
									sgcRykqDayEntity.setId( UUIDUtil.uuid32() );
									sgcRykqDayEntity.setRq( LocalDate.parse( sksj, DateTimeFormatter.ofPattern( "yyyy-MM-dd" ) ));
									sgcRykqDayEntity.setYgid( sgcRyglEntity.getYgid() );
									sgcRykqDayEntity.setScsksj( LocalDateTime.parse( pstRecord[i].stuTime.toStringTimeEx(), DateTimeFormatter.ofPattern( "yyyy-MM-dd HH:mm:ss" ) ) );
									sgcRykqDayEntity.setKfz( 0.0 );
									sgcRykqDayEntity.setSbid( sbid );
									sgcRykqDayEntity.setCreatedBy( "admin" );
									sgcRykqDayEntity.setCreatedTime( LocalDateTime.now() );
									dahuaApp.sgcRykqDayMapper.insert( sgcRykqDayEntity );
									//下载考勤图片
									downloadRemoteFile( new String( pstRecord[i].szSnapFtpUrl ).trim(), m_hLoginHandle, id );
								}
							}
						}
                    }
                    if (stuFindNextOutParam.nRetRecordNum < nRecordCount) {
                        break;
                    } else {
                        count++;
                    }
                } else {
                    System.err.println("FindNextRecord Failed" + netsdkApi.CLIENT_GetLastError());
                    break;
                }
            }
            netsdkApi.CLIENT_FindRecordClose(stuFindOutParam.lFindeHandle);
        } else {
            System.err.println("Can Not Find This Record" + String.format("0x%x", netsdkApi.CLIENT_GetLastError()));
        }
    }



	public void QueryRecordByTime(String sbid , LLong m_hLoginHandle) {
		NetSDKLib.FIND_RECORD_ACCESSCTLCARDREC_CONDITION_EX queryCondition = new NetSDKLib.FIND_RECORD_ACCESSCTLCARDREC_CONDITION_EX();
		queryCondition.bTimeEnable = 1;
		Calendar calendar = Calendar.getInstance();
		System.out.println("**********************抓取时间：" + calendar.get(Calendar.YEAR) + "-"+ (calendar.get(Calendar.MONTH)+1) + "-" +  calendar.get(Calendar.DATE));
		queryCondition.stStartTime.setTime(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1,  calendar.get(Calendar.DATE), 6, 0, 0);
		queryCondition.stEndTime.setTime(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DATE), 23, 0, 0);

		queryAccessRecords(sbid,queryCondition,m_hLoginHandle);

		// 获取到的图片，只是一个图片地址，想要获取到图片，需要调用   downloadRemoteFile() 下载图片
		//downloadRemoteFile("/mnt/appdata1/snapshot/SnapShot/2018-06-12/175720[C][0].jpg");
	}
	/**
	 * 查询门禁刷卡记录
	 * @param queryCondition 查询条件
	 */
	public void queryAccessRecords(String sbid,NetSDKLib.FIND_RECORD_ACCESSCTLCARDREC_CONDITION_EX queryCondition,LLong m_hLoginHandle) {
		/**
		 * 查询条件
		 */
		if(queryCondition == null) {
			queryCondition = new NetSDKLib.FIND_RECORD_ACCESSCTLCARDREC_CONDITION_EX();
		}
		/**
		 * CLIENT_FindRecord 入参
		 */
		NetSDKLib.NET_IN_FIND_RECORD_PARAM findRecordIn = new NetSDKLib.NET_IN_FIND_RECORD_PARAM();
		findRecordIn.emType = NetSDKLib.EM_NET_RECORD_TYPE.NET_RECORD_ACCESSCTLCARDREC_EX;
		findRecordIn.pQueryCondition = queryCondition.getPointer();
		/**
		 * CLIENT_FindRecord 出参
		 */
		NetSDKLib.NET_OUT_FIND_RECORD_PARAM findRecordOut = new NetSDKLib.NET_OUT_FIND_RECORD_PARAM();

		queryCondition.write();
		findRecordIn.write();
		findRecordOut.write();
		boolean success = netsdkApi.CLIENT_FindRecord(m_hLoginHandle, findRecordIn, findRecordOut, 3000);
		findRecordOut.read();
		findRecordIn.read();
		queryCondition.read();

		if(!success) {
			System.err.println("Can Not Find This Record: " + String.format("0x%x", netsdkApi.CLIENT_GetLastError()));
			return;
		}
		final int nRecordCount = 10;  // 每次查询的最大个数
		/**
		 * 门禁刷卡记录记录集信息
		 */
		NetSDKLib.NET_RECORDSET_ACCESS_CTL_CARDREC[] records = new NetSDKLib.NET_RECORDSET_ACCESS_CTL_CARDREC[nRecordCount];
		for(int i = 0; i < nRecordCount; i++) {
			records[i] = new NetSDKLib.NET_RECORDSET_ACCESS_CTL_CARDREC();
		}
		/**
		 * CLIENT_FindNextRecord 入参
		 */
		NetSDKLib.NET_IN_FIND_NEXT_RECORD_PARAM findNextRecordIn = new NetSDKLib.NET_IN_FIND_NEXT_RECORD_PARAM();
		findNextRecordIn.lFindeHandle = findRecordOut.lFindeHandle;
		findNextRecordIn.nFileCount = nRecordCount;  //想查询的记录条数

		/**
		 * CLIENT_FindNextRecord 出参
		 */
		NetSDKLib.NET_OUT_FIND_NEXT_RECORD_PARAM findNextRecordOut = new NetSDKLib.NET_OUT_FIND_NEXT_RECORD_PARAM();
		findNextRecordOut.nMaxRecordNum = nRecordCount;
		findNextRecordOut.pRecordList = new Memory(records[0].dwSize * nRecordCount); // 申请内存
		findNextRecordOut.pRecordList.clear(records[0].dwSize * nRecordCount);

		// 将  native 数据初始化
		ToolKits.SetStructArrToPointerData(records, findNextRecordOut.pRecordList);
		SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd");
		while(true) {  //循环查询
			if(!netsdkApi.CLIENT_FindNextRecord(findNextRecordIn, findNextRecordOut, 2000) )  {
				System.err.println("FindNextRecord Failed" + ToolKits.getErrorCode());
				break;
			}
			/// 将 native 数据转为 java 数据
			ToolKits.GetPointerDataToStructArr(findNextRecordOut.pRecordList, records);
			for(int i = 0; i < findNextRecordOut.nRetRecordNum; i++) {
				System.out.println( "刷卡时间:" + records[i].stuTime.toStringTimeEx()
						+ "\n" + "记录集编号:" + records[i].nRecNo
						+ "\n" + "卡号:" + new String( records[i].szCardNo ).trim()
						+ "\n" + "卡类型:" + records[i].emCardType
						+ "\n" + "门号:" + records[i].nDoor
						+ "\n" + "开门方式:" + records[i].emMethod
						+ "\n" + "开门失败错误码:" + records[i].nErrorCode
						+ "\n" + "开锁抓拍上传的FTP地址:" + new String( records[i].szSnapFtpUrl ).trim()
						+ "\n" + "开门结果：" + (records[i].bStatus == 1 ? "成功" : "失败")
						+ "\n" + "用户id：" + (new String( records[i].szUserID ).trim())
				);
			String sksj = records[i].stuTime.toStringTimeEx().substring(0,10);
			if(records[i].bStatus == 1){
				//查询当日时间范围的考勤记录
				QueryWrapper<SgcRykqjlEntity> kqQueryWrapper = new QueryWrapper<>();
				kqQueryWrapper.eq( "YGID", new String( records[i].szUserID ).trim() );
				/*kqQueryWrapper.ge( "DKSJ", dateFormat.format( new Date() ) + " 00:00:00" );
				kqQueryWrapper.le( "DKSJ", dateFormat.format( new Date() ) + " 23:59:59" );*/
				kqQueryWrapper.ge( "DKSJ", sksj + " 00:00:00" );
				kqQueryWrapper.le( "DKSJ", sksj + " 23:59:59" );
				List<SgcRykqjlEntity> kqList = dahuaApp.sgcRykqjlMapper.selectList( kqQueryWrapper );
				//SgcRykqjlEntity sgcRykqjlEntity =dahuaApp.sgcRykqjlMapper.selectOne(kqQueryWrapper);
				//无记录则插入考勤记录
				if (CollectionUtil.isEmpty( kqList )) {
					//判断当前用户是否在系统中
					QueryWrapper<SgcRyglEntity> ryQueryWrapper = new QueryWrapper<>();
					ryQueryWrapper.eq( "YGID", new String( records[i].szUserID ).trim() );
					SgcRyglEntity sgcRyglEntity = dahuaApp.sgcRyglMapper.selectOne( ryQueryWrapper );
					if (ObjectUtil.isNotEmpty( sgcRyglEntity )) {
						//插入考勤记录表
						String id = UUIDUtil.uuid32();
						SgcRykqjlEntity sgcRykqjlEntity = new SgcRykqjlEntity();
						sgcRykqjlEntity.setJlid( id );
						sgcRykqjlEntity.setSbid( sbid );
						sgcRykqjlEntity.setYgid( sgcRyglEntity.getYgid() );
						try {
							sgcRykqjlEntity.setDksj( new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" ).parse( records[i].stuTime.toStringTimeEx() ) );
						} catch (ParseException e) {
							e.printStackTrace();
						}
						sgcRykqjlEntity.setSjlx( String.valueOf( records[i].emDirection ) );
						dahuaApp.sgcRykqjlMapper.insert( sgcRykqjlEntity );
						//插入考勤记录汇总表
						SgcRykqDayEntity sgcRykqDayEntity = new SgcRykqDayEntity();
						sgcRykqDayEntity.setId( UUIDUtil.uuid32() );
						sgcRykqDayEntity.setRq( LocalDate.parse( records[i].stuTime.toStringTimeEx(), DateTimeFormatter.ofPattern( "yyyy-MM-dd" ) ));
						sgcRykqDayEntity.setYgid( sgcRyglEntity.getYgid() );
						sgcRykqDayEntity.setScsksj( LocalDateTime.parse( records[i].stuTime.toStringTimeEx(), DateTimeFormatter.ofPattern( "yyyy-MM-dd HH:mm:ss" ) ) );
						sgcRykqDayEntity.setKfz( 0.0 );
						sgcRykqDayEntity.setSbid( sbid );
						sgcRykqDayEntity.setCreatedBy( "admin" );
						sgcRykqDayEntity.setCreatedTime( LocalDateTime.now() );
						dahuaApp.sgcRykqDayMapper.insert( sgcRykqDayEntity );
						//下载考勤图片
						downloadRemoteFile( new String( records[i].szSnapFtpUrl ).trim(), m_hLoginHandle, id );
						}
					}
				}
			}
		}
		System.out.println("************************本次循环结束1******************************");
		success = netsdkApi.CLIENT_FindRecordClose(findRecordOut.lFindeHandle);
		if (!success) {
			System.err.println("Failed to Close: " + ToolKits.getErrorCode());
		}
		System.out.println("************************本次循环结束2******************************");
	}

	/**
	 * 下载图片
	 * @param szFileName 需要下载的文件名
	 */
	public boolean downloadRemoteFile(String szFileName , LLong m_hLoginHandle,String id) {
		File path = new File("./AccessPicture/");
		if (!path.exists()) {
			path.mkdir();
		}
		// 入参
		NetSDKLib.NET_IN_DOWNLOAD_REMOTE_FILE stIn = new NetSDKLib.NET_IN_DOWNLOAD_REMOTE_FILE();
		stIn.pszFileName = new NativeString(szFileName).getPointer();
		stIn.pszFileDst = new NativeString("./AccessPicture/" + id + ".jpg").getPointer(); // 存放路径

		// 出参
		NetSDKLib.NET_OUT_DOWNLOAD_REMOTE_FILE stOut = new NetSDKLib.NET_OUT_DOWNLOAD_REMOTE_FILE();

		if(netsdkApi.CLIENT_DownloadRemoteFile(m_hLoginHandle, stIn, stOut, 5000)) {
			System.out.println("下载图片成功!");
			File file = new File("./AccessPicture/" + id + ".jpg");
			FileItem fileItem = createFileItem(file);
			MultipartFile multipartFile = new CommonsMultipartFile(fileItem);
			MultipartFile[] files = new MultipartFile[1];
			files[0] = multipartFile;
			System.out.println("****************上传图片开始*****************************");
			dahuaApp.uploadUtil.upload(files,id);
			System.out.println("****************上传图片结束*****************************");
			//file.delete();
		} else {
			System.err.println("下载图片失败!" + ToolKits.getErrorCode());
			return false;
		}
		return true;
	}
	//文件转换
	private static FileItem createFileItem(File file) {
		FileItemFactory factory = new DiskFileItemFactory(16, null);
		FileItem item = factory.createItem("textField", "text/plain", true, file.getName());
		int bytesRead = 0;
		byte[] buffer = new byte[8192];
		try {
			FileInputStream fis = new FileInputStream(file);
			OutputStream os = item.getOutputStream();
			while ((bytesRead = fis.read(buffer, 0, 8192)) != -1) {
				os.write(buffer, 0, bytesRead);
			}
			os.close();
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return item;
	}


}
