package com.netsdk.demo.module;

import com.netsdk.bean.DeviceInfo;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.stereotype.Component;

/**
 * @Description: 设备信息缓存Map
 * @Author 徐铭阳
 * @CreateDate 2022/11/6 23:33
 * @Version: 1.0
 * Copyright (c)2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/
@Component
public class DeviceCache {
	public static Map<String, DeviceInfo> deviceInfoMap = new ConcurrentHashMap<>();
}
