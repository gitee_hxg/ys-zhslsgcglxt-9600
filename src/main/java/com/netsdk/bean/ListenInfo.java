package com.netsdk.bean;

/***
 * @Description: 监听回调数据
 * @Author: 徐铭阳
 * @CreateDate: 2022/11/8 21:30
 * @Version: 1.0
 * Copyright (c) 2022,武汉中地云申科技有限公司
 * All rights reserved.
 **/

public class ListenInfo {
    /**
     * 设备序列号
     */
    public String devSerial;
    /**
     * 设备 IP
     */
    public String devIpAddress;
    /**
     * 设备 Port
     */
    public Integer devPort;

    public ListenInfo(String devSerial, String devIpAddress, Integer devPort) {
        this.devSerial = devSerial;
        this.devIpAddress = devIpAddress;
        this.devPort = devPort;
    }
}
