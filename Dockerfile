#FROM java:8
FROM openjdk:8-jre
VOLUME /tmp
ADD src/main/resources/linux64 /tmp
ADD target/ys-zhslsgcglxt-9600-0.0.1-SNAPSHOT.jar ys-zhslsgcglxt-9600.jar
ENV JAVA_OPTS="-server -Xms512m -Xmx1024m -Dfile.encoding=UTF8"
ENTRYPOINT exec java ${JAVA_OPTS} -Djava.security.egd=file:/dev/./urandom -jar /ys-zhslsgcglxt-9600.jar
EXPOSE 9600
